/** Fast search */
var fast_search = function() {
    var fast_search_inp = document.getElementById("search-input");

    var query = fast_search_inp.value;
    // Prepare an URL
    var full_url = fast_search_url + "?query=" + query;
    // Go to url
    window.location.assign(full_url);
};

// Execute a function when the user releases an Enter key (code 13) on the keyboard
document.getElementById("search-input").addEventListener("keyup", function(event) {
    if (event.key === 'Enter') {
        fast_search();
  }
});
