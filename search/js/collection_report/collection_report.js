/** Chart defaults ================================================================================================== */
Chart.defaults.global.defaultFontSize = 14;

// Pallets
const pal4_1 = ["#35618f", "#95fb68", "#a152d6", "#338821"];
const pal8_1 = ["#208eb7", "#cd4281", "#bce333", "#8438ba", "#76f014", "#da73f8", "#5e9222", "#de19f7"]
const pal10_1 = ["#0cc0aa", "#145a6a", "#9ecbf4", "#6457d9", "#f597fa",
                 "#653857", "#ed2bb1", "#8a0458", "#967b95", "#1f84ec"];
const pal10_2 = ["#208eb7", "#64f8f4", "#194f46", "#a2d665", "#788c3b",
                 "#62f065", "#1d8a20", "#87cefe", "#3451d3", "#bd9bf4"]
const pal20_1 = ["#68affc", "#4f28af", "#c667f3", "#2a538a", "#3fadaf", "#64f8f4", "#22695e",
                 "#63ef85", "#2aa63a","#cde8e2", "#658114", "#b4d170", "#7b2c31", "#e78085",
                 "#dc3c07", "#fda547", "#6d5b4e", "#a79b9e", "#b70d61", "#a2fa12"]
const pal20_2 = ["#4f8c9d", "#37fcea", "#0e503e", "#b8f394", "#3a427d", "#dab9ff", "#2b19d9",
                 "#f67fec", "#761e7e", "#bce9fe", "#48a421", "#5eff92", "#744822", "#feb786",
                 "#d6061a", "#758c45", "#fbe423", "#7d76e4", "#b9f617", "#e313ee"]

const convertHexToRGBA = (hex, opacity) => {
  const tempHex = hex.replace('#', '');
  const r = parseInt(tempHex.substring(0, 2), 16);
  const g = parseInt(tempHex.substring(2, 4), 16);
  const b = parseInt(tempHex.substring(4, 6), 16);

  return `rgba(${r},${g},${b},${opacity / 100})`;
};

function get_options(max, horiz=true) {
    return {
        legend: {display: false},
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                gridLines: {
                    offsetGridLines: false,
                },
                offset: horiz,
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: max
                }
            }],
            xAxes: [{
                gridLines: {
                    offsetGridLines: false,
                },
                offset: !horiz,
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: max
                }
            }]
        },
    };
}

function add_offset(data) {
    let maximum = Math.max.apply(Math, data)
    let max_off = maximum + Math.ceil(maximum * 0.1)
    return max_off
}

// Horizontal charts constructor
function horiz_barchart_const(data_obj, dom_obj, data_label, pal, horiz=true) {
    // Prepare data
    let data = [];
    let axis_labels = [];

    data_obj.forEach((entry) => {
        // in entry, element `rn` is row number, therefore, we can use it as data position in an array
        // Capitalize target name
        // Prevent elements without target name
        if (entry.target_name) {
            axis_labels.push(entry.target_name.charAt(0).toUpperCase() + entry.target_name.slice(1));
            data.push(entry.counts);
        }
    });
    let type = 'horizontalBar';
    if (!horiz) {
        type = 'bar'
    }
    return new Chart(dom_obj, {
        type: type,
        data: {
            labels: axis_labels,
            datasets: [
                {
                    label: data_label,
                    hoverBackgroundColor: pal,
                    borderColor: pal,
                    borderWidth: 2,
                    backgroundColor: pal.map((elem) => {return convertHexToRGBA(elem, 60)}),
                    data: data
                }
            ]
        },
        options: get_options(add_offset(data), horiz)
    });
}

// Handsontable constructor charts constructor
function hat_constructor(data_obj, dom_obj, cat_name) {
    return new Handsontable(dom_obj, {
        data: data_obj,
        minSpareRows: 0,
        colHeaders: [cat_name, 'Терминальных таксонов'],
        columns: [
            {data: 'target_name', editor: false},
            {data: 'counts', editor: false}
        ],
        contextMenu: false,
        rowHeaders: true,
        dropdownMenu: false,
        filters: false,
        // preventOverflow: 'horizontal',
        fillHandle: false,
        columnSorting: true,
        autoColumnSize: {useHeaders: true},
        autoRowSize: true,
        stretchH: 'all',
        language: 'ru-Ru',
        licenseKey: 'non-commercial-and-evaluation'
    });
}

/** Format label function (wrap long labels) ======================================================================== */
// Taken from https://stackoverflow.com/a/38806741/5762368
function formatLabel(str, maxwidth){
    let sections = [];
    let words = str.split(" ");
    let temp = "";

    words.forEach(function(item, index){
        if(temp.length > 0)
        {
            let concat = temp + ' ' + item;

            if(concat.length > maxwidth){
                sections.push(temp);
                temp = "";
            }
            else{
                if(index == (words.length-1))
                {
                    sections.push(concat);
                    return;
                }
                else{
                    temp = concat;
                    return;
                }
            }
        }

        if(index == (words.length-1))
        {
            sections.push(item);
            return;
        }

        if(item.length < maxwidth) {
            temp = item;
        }
        else {
            sections.push(item);
        }

    });

    return sections;
}


/** Format label function (wrap long labels) ======================================================================== */
// Taken from https://stackoverflow.com/a/52613528/5762368

function getMinMax(arr) {
    let min = arr[0];
    let max = arr[0];
    let i = arr.length;

    while (i--) {
        min = arr[i] < min ? arr[i] : min;
        max = arr[i] > max ? arr[i] : max;
    }
    return { min, max };
}

/** Class counts ==================================================================================================== */
// Construct barchart
class_canvas = document.getElementById('class-chart');

let class_chart = horiz_barchart_const(
    class_count_json, class_canvas, 'Терминальных таксонов', pal10_1
);

// Construct table
let class_tab = hat_constructor(class_count_tab, document.getElementById('hs-table-class'), 'Класс')


/** Order counts ==================================================================================================== */
// Construct barchart
order_canvas = document.getElementById('order-chart');
let order_chart = horiz_barchart_const(
    order_count_json, order_canvas, 'Терминальных таксонов', pal10_2
);

// Construct table
let order_tab = hat_constructor(order_count_tab, document.getElementById('hs-table-order'), 'Порядок')

/** Family counts =================================================================================================== */
// Construct barchart
family_canvas = document.getElementById('family-chart');
let family_chart = horiz_barchart_const(
    family_count_json, family_canvas, 'Терминальных таксонов', pal20_1
);

// Construct table
let family_tab = hat_constructor(family_count_tab, document.getElementById('hs-table-family'), 'Семейство')

/** Genera counts =================================================================================================== */
// Construct barchart
genera_canvas = document.getElementById('genera-chart');
let genera_chart = horiz_barchart_const(
    genera_count_json, genera_canvas, 'Терминальных таксонов', pal20_2
);

// Construct table
let genera_tab = hat_constructor(genera_count_tab, document.getElementById('hs-table-genera'), 'Род')

/** IUCN categories counts ========================================================================================== */
// Construct barchart
category_canvas = document.getElementById('category-chart');
let category_chart = horiz_barchart_const(
    iucn_cat_json, category_canvas, 'Терминальных таксонов', pal10_1, horiz=false
);

// Construct table
let iucn_tab = hat_constructor(iucn_cat_tab, document.getElementById('hs-table-iucn'), 'Категория МСОП')

/** TU income outcome timing counts ================================================================================= */
// Construct barchart
let alive_count = []
let dead_count = []
let dates = []

tu_timing_json.forEach((entry) => {
    dates.push(new Date(entry.year));
    alive_count.push({y: entry.alive, x: new Date(entry.year)});
    dead_count.push({y: entry.dead, x: new Date(entry.year)});
})

tu_timing_canvas = document.getElementById('tu-timing-chart');

new Chart(tu_timing_canvas, {
  type: 'line',
  data: {
    labels: dates,
    datasets: [
        {
            data: alive_count,
            label: "Живые",
            borderColor: "#3ecd94",
            backgroundColor: "rgba(62,205,148,0.8)",
            fill: 'origin',
            pointRadius: 1, // small points
            pointHitRadius: 5, // area around point which trigger point hover effect (tooltip)
            cubicInterpolationMode: 'monotone',
            steppedLine: true
        },
        {
            data: dead_count,
            label: "Выпавшие",
            borderColor: "#f39393",
            backgroundColor: "rgba(243,147,147,0.85)",
            fill: 0,
            pointRadius: 1, // small points
            pointHitRadius: 5, // area around point which trigger point hover effect (tooltip)
            cubicInterpolationMode: 'monotone',
            steppedLine: true
        }
    ]
  },
  options: {
      title: {display: false,},
      legend: { display: true },
      maintainAspectRatio: false,
      scales: {
          xAxes: [{
              type: 'time',
              time: {
                  displayFormats: {year: 'YYYY'}
              }
          }],
          yAxes: [{
              stacked: true
          }]
      },
      plugins: {
          filler: {propagate: true}
      },
      tooltips: {mode: 'x'}
  }
});

/** TU cultivating type ============================================================================================= */
// Prepare data
const cult_trans_dict = {
    cont_og: 'контейнер, откр. грунт',
    cont_pg: 'контейнер, закр. грунт',
    grd_og: 'открытый грунт',
    grd_pg: 'закрытый грунт'
}

let cult_labels = [];
let cult_data = [];
let cult_type_tab = [];
Object.entries(cult_type_json).forEach((entry) => {
    cult_labels.push(cult_trans_dict[entry[0]]);
    cult_data.push(entry[1])
    cult_type_tab.push({target_name: cult_trans_dict[entry[0]], counts: entry[1]})
})

const cult_type_chart = new Chart(document.getElementById('ind-cult-type'), {
    type: 'doughnut',
    data: {
        datasets: [{
            data: cult_data,
            backgroundColor: pal4_1,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: cult_labels
    },
    options: {
        legend: { display: true, position: 'right' },
        maintainAspectRatio: false,
    }
});

let cult_tab = hat_constructor(cult_type_tab, document.getElementById('hs-table-cult'), 'Тип культивирования')


/** TU counts by department ========================================================================================= */
// Prepare data
let dep_count_labels = Object.entries(tu_counts_by_dep.cult_name).map((entry) => {
        return formatLabel(entry[0], 25)
    })
let dep_count_data = {
    cult_name: {label: 'Культивары', data: [], backgroundColor: "#f67fec"},
    infra_name: {label: 'Субвидовые таксоны', data: [], backgroundColor: "#208eb7"},
    species_name: {label: 'Виды', data: [], backgroundColor: "#609060"},
}

Object.entries(tu_counts_by_dep).forEach((cult_type) => {
     Object.entries(cult_type[1]).forEach((entry) => {
        dep_count_data[cult_type[0]].data.push(entry[1])
    })
})

const tu_counts_by_dep_opt = {
        legend: { display: true, position: 'top' },
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                gridLines: {
                    offsetGridLines: false,
                },
                offset: false,
                ticks: {
                    suggestedMin: 0,
                    suggestedMax: Math.max.apply(Math, dep_count_data.species_name.data) + 1
                }
            }],
            xAxes: [{
                gridLines: {
                    offsetGridLines: true,
                },
                offset: true,
                ticks: {
                    suggestedMin: 0
                }
            }]
        },
    };

new Chart(document.getElementById('tu-counts-by-dep'), {
    type: 'bar',
    data: {
        labels: dep_count_labels,
        datasets: [
            dep_count_data.cult_name, dep_count_data.infra_name, dep_count_data.species_name
        ]
    },
    options: tu_counts_by_dep_opt
});


/** Charts download button ========================================================================================== */
//Download Chart Image
let foo;
Array.from(document.getElementsByClassName("chart-download")).forEach((obj) => {
    // Add event listener to each object
    obj.addEventListener('click', (e) => {
        // Get canvas element
        let chart_elem = e.target.parentElement.parentElement.querySelector('canvas');
        // Convert canvas to base64 and add it as href component to link
        e.target.closest('a').href = chart_elem.toDataURL("image/png");
    })
});

/** Individuals positions heatmap =================================================================================== */
// Create leaflet map
let heatmap_obj = document.getElementById("ind-position-heatmap");
let ind_heatmap = L.map(heatmap_obj, {
    dragging: !L.Browser.mobile, tap: !L.Browser.mobile
}).setView([48.010, 37.88], 17);
let heat_tiles = L.tileLayer('/territory-map/{z}/{x}/{y}.png', {
            maxZoom: 22,
            attribution: '&copy; ГУ "ДБС"'
        });
heat_tiles.addTo(ind_heatmap);

// Prepare heatmap data
let heat_data = {max: 1, data: []};
ind_spat_pos_geojoson.features.forEach((feature) => {
    let point_data = {};
    point_data['lon'] = feature.geometry.coordinates[0];
    point_data['lat'] = feature.geometry.coordinates[1];
    point_data['count'] = 1;
    heat_data.data.push(point_data);
})

// Compute the heatmap point radius. 50 for 10 point or lower, 2 for more then 1000
let radius = 50;
let data_len = heat_data.data.length
if (data_len > 10 && data_len < 1000) {
    let divider = (1000 - data_len) / 1000;
    radius = 2 + 48 * divider;
}
if (data_len >= 1000) {
    radius = 2;
}

// Construct heat map layer
let ind_heat_layer = new HeatmapOverlay({
    radius: radius,
    maxOpacity: .7,
    scaleRadius: false, // we have points with count 1 only
    latField: 'lat',
    lngField: 'lon',
    valueField: 'count'
});

ind_heat_layer.setData(heat_data);
ind_heat_layer.addTo(ind_heatmap);


/** Individuals origin cities great circle ========================================================================== */
// Prepare line color and width scale functions (basing on d3)
// Get min max counts from ind_orig_city_json
let counts = ind_orig_city_json.features.map((feature) => {
    return feature.properties.count;
});

let count_range = getMinMax(counts);

// Linear gradient for color
const color_scale = d3.scaleLinear()
    .domain([Math.sqrt(count_range.min), Math.sqrt(count_range.max)])
    .range(["blue", "red"]);

// Linear gradient for lines width
const weight_scale = d3.scaleLinear()
    .domain([Math.sqrt(count_range.min), Math.sqrt(count_range.max)])
    .range([2, 5]);

let ind_cities_div = document.getElementById('ind-origin-city')
let ind_cities = L.map(ind_cities_div, {
    dragging: !L.Browser.mobile, tap: !L.Browser.mobile
}).setView([20, 37.88], 2);
let osm_tiles = L.tileLayer('https://tile.thunderforest.com/mobile-atlas/{z}/{x}/{y}.png?apikey=76b86a05865149049ca5f34d080514ef', {
            attribution: '&copy; <a href="http://www.thunderforest.com/">Thunderforest</a>, &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 10}
            );
osm_tiles.addTo(ind_cities);

let city_paths = L.geoJSON(ind_orig_city_json, {
        style: (feature) => {
            let count = Math.sqrt(feature.properties.count)
            return {color: color_scale(count),
                    weight: weight_scale(count)};
        },
        onEachFeature: (feature, layer) => {
            layer.bindPopup(`<h5>${feature.properties.name} ⇨ Донецк</h5>
                             <h6>Количество поступлений: ${feature.properties.count}</h6>`)
        }
    }
    );

// Zoom to layer
ind_cities.fitBounds(city_paths.getBounds())
city_paths.addTo(ind_cities)

/** Individuals origin nature heatmap =============================================================================== */
// Create leaflet map
let ind_origin_nature_div = document.getElementById("ind-origin-nature");
let nature_heatmap = L.map(ind_origin_nature_div, {
    dragging: !L.Browser.mobile, tap: !L.Browser.mobile
}).setView([48.010, 37.88], 8);
let nature_heat_tiles = L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
            maxZoom: 22,
            attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
        });
nature_heat_tiles.addTo(nature_heatmap);

// Compute the heatmap point radius. 50 for 10 point or lower, 2 for more then 1000
let nature_radius = 50;
let nature_data_len = ind_orig_nature.features.length
if (nature_data_len > 10 && nature_data_len < 1000) {
    let divider = (1000 - nature_data_len) / 1000;
    nature_radius = 2 + 48 * divider;
}
if (data_len >= 1000) {
    nature_radius = 2;
}

// Prepare data
let nature_heat_data = {max: 1, data: []};
ind_orig_nature.features.forEach((feature) => {
    let point_data = {};
    point_data['lon'] = feature.geometry.coordinates[0];
    point_data['lat'] = feature.geometry.coordinates[1];
    point_data['count'] = 1;
    nature_heat_data.data.push(point_data);
})
// Construct heat map layer
let nature_heat_layer = new HeatmapOverlay({
    radius: nature_radius,
    maxOpacity: .7,
    scaleRadius: false, // we have points with count 1 only
    latField: 'lat',
    lngField: 'lon'
});

nature_heat_layer.setData(nature_heat_data);
nature_heat_layer.addTo(nature_heatmap);

// Zoom layer to data (HeatmapOverlay doesnot provide `getBounds`), therefore, create point layer from geojson
// and use its bounds
nature_heatmap.fitBounds(L.geoJSON(ind_orig_nature).getBounds())
