/** Leaflet preparation and functions =============================================================================== */

/** Variables */
var saved_pks = {"Уровень 1": [], "Уровень 2": [], "Уровень 3": []};
var cur_layer = "Уровень 1";
var selected_pk = null;
var map;

style_selected = {
    fillColor : 'blue',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.5
};

style_base = {
    fillColor : 'transparent',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.2
};

style_saved = {
    fillColor : '#c733ff',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.9
};

/** Misc functions for leaflet */
function on_baselayer_change(e) {
    /** Fires when map base layer change
     * Such we can control which layer is currently on the map */
    cur_layer = e.name;
}

function move_layer(source_lyr, dest_lyr, pk, source_name) {
    source_lyr.eachLayer(function(lyr) {
        if (lyr.feature.properties.pk === pk) {
            var lyr_geojson = lyr.toGeoJSON();
            // add layer name to geojson
            lyr_geojson.properties.lyr_name = source_name;
            dest_lyr.addData(lyr.toGeoJSON());
        }
    })
}

function save_layer() {
    /** The function for AddButton
     * on click, saves layer `pk` to `saved_pks` variable */
    // Get layer according to `cur_layer` and `selected_pk`
    move_layer(base_layers[cur_layer], saved_lyr, selected_pk, cur_layer);
    // add pk to saved layers
    saved_pks[cur_layer].push(selected_pk);
    // Update saved layer style
    saved_lyr.setStyle(style_saved);
}

function remove_layer() {
    /** The function for RemoveButton
     * on click, removes layer `pk` to `saved_pks` variable*/
    saved_lyr.eachLayer(function (lyr) {
        // Check if saved layer is of current layers (layer name property was created in `save_layer` function)
        if (lyr.feature.properties.lyr_name === cur_layer) {
            // Find layer with selected pk
            if (lyr.feature.properties.pk === selected_pk) {
                // temporary save pk
                var pk = lyr.feature.properties.pk;
                // Remove layer
                saved_lyr.removeLayer(lyr._leaflet_id);
                // Remove pk from saved_pks list
                saved_pks[cur_layer].splice(saved_pks[cur_layer].indexOf(pk), 1);
            }
        }
    })
}


/** Leaflet initialization ========================================================================================== */
// Create tiles layer
var tile_layer = L.tileLayer('/territory-map/{z}/{x}/{y}.png', {
    maxZoom: 22,
    attribution: '&copy; ГУ "ДБС"'
});

// Create geojson layers
var level1_json = L.geoJSON(poly_1).setStyle(style_base);
var level2_json = L.geoJSON(poly_2).setStyle(style_base);
var level3_json = L.geoJSON(poly_3).setStyle(style_base);
var saved_lyr = L.geoJSON(null, {'interactive':false});

var base_layers = {
        "Уровень 1": level1_json,
        "Уровень 2": level2_json,
        "Уровень 3": level3_json
    };

// Initiate map
document.addEventListener('DOMContentLoaded', function() {
    map = L.map('map', {
        center: [48.010, 37.88],
        zoom: 17,
        layers: [tile_layer, saved_lyr, level1_json],
        zoomControl: false
    });
    // Add layers control
    var overlay_layers = {"Сохраненые": saved_lyr};
    L.control.layers(base_layers, overlay_layers).addTo(map);

    // Add function for layers change detection
    map.on('baselayerchange', on_baselayer_change);
});


/** Control buttons ================================================================================================= */
/** Add layer Button */
L.Control.AddButton = L.Control.extend({
    onAdd: function(map) {
        var container = L.DomUtil.create('a');
        container.role="button";
        container.innerHTML = "Сохранить";

        container.onclick = save_layer;
        return container;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.add_button = function(opts) {
    return new L.Control.AddButton(opts);
};

// L.control.add_button({position: 'bottomleft'}).addTo(map);
var add_button = L.control.add_button({position: 'topleft'});

/** Remove layer Button */
L.Control.RemoveButton = L.Control.extend({
    onAdd: function(map) {
        var container = L.DomUtil.create('a');
        container.role="button";
        container.innerHTML = "Удалить";

        container.onclick = remove_layer;
        return container;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.remove_button = function(opts) {
    return new L.Control.RemoveButton(opts);
};

// L.control.add_button({position: 'bottomleft'}).addTo(map);
var remove_button = L.control.remove_button({position: 'topleft'});




/** On click functions ============================================================================================== */
/** Attach one click functions **/
level1_json.on('click', oneClick);
level2_json.on('click', oneClick);
level3_json.on('click', oneClick);

function oneClick(e){
    /** Highlight clicked polygon
     *   If current polygon of current layer is not added add button show
     *  **/
    this.setStyle(style_base);
    // second, change style of selected polygon
    e.layer.setStyle(style_selected);
    selected_pk = e.layer.feature.properties.pk;

    // if selected pk in saved_pks, show Remove button
    if (saved_pks[cur_layer].includes(selected_pk)) {
        // remove ADD button from map and add REMOVE button
        remove_button.addTo(map);
        add_button.remove()
    // Else, show Add button
    } else {
        // remove REMOVE button from map and add ADD button
        remove_button.remove();
        add_button.addTo(map);
    }
    saved_lyr.setStyle(style_saved);
}


/** Presubmit ======================================================================================================= */
// get form
var form = document.getElementById("spatial-search-submit");
// get hidden data field
var data_field = document.getElementById("id_data");

function populate_data() {
    // Replace cyrillic 'Уровень 1'... with `sp_level_1`...
    var saved_pks_lat = {
        sp_level_1: saved_pks["Уровень 1"],
        sp_level_2: saved_pks["Уровень 2"],
        sp_level_3: saved_pks["Уровень 3"]
    };
    // Populate hidden data field with saved_pks
    data_field.value = JSON.stringify(saved_pks_lat);
    form.submit();
}
