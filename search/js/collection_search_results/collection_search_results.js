// Initialize material objects
document.addEventListener('DOMContentLoaded', function() {
    var modal = document.querySelectorAll('.modal');
    var modal_inst = M.Modal.init(modal, null);
});

/* The table preparation ============================================================================================ */
var container = document.getElementById('hs-table');
var hs_table;

var emoji_check = '✅';
var emoji_false = '❌';

document.addEventListener('DOMContentLoaded', function() {
    // Process data
    // add links url
    var link_base = tu_link_template.slice(0, -2);
    tab_data.forEach(function (element) {
        // Prepare tu and individual links
        var pk = element.pk;
        var ind_id = element.ind_id;
        element.tu_link = '<a href=' + link_base + pk + '/><i class="material-icons"> link </i></a>';
        element.ind_link = '<a href=' + link_base + pk + '/' + ind_id + '/><i class="material-icons"> link </i></a>';

        // Replace true/false with emoji
        // Catalogue of life
        if (element.col_check) {
            element.col_check = emoji_check
        } else {
            element.col_check = emoji_false
        }

        // IUCN Red List
        if (element.in_red_list) {
            element.in_red_list = emoji_check
        } else {
            element.in_red_list = emoji_false
        }

        // Hybrid
        if (element.hybrid_mark) {
            element.hybrid_mark = emoji_check
        } else {
            element.hybrid_mark = ''
        }

        // Is alive
        if (target === 'ind') {
            if (element.is_alive) {
                element.is_alive = emoji_check
            } else {
                element.is_alive = emoji_false
            }
        }
    });

    /** We need different initializations for Individuals and Taxonomic units
     * Therefore we will create lists with common rows, which could be altered in separate initializations */
    var common_headers = ['Семейство', 'Род', 'Видовой эпитет', 'Внутривид. метка', 'Внутривид. эпитет',
        'Культивар', 'Автор', 'Тип культивара', 'Порядок', 'Класс', 'Отдел','Гибрид', 'Сверено с COL',
        'В красном списке', 'Категория МСОП', 'Дата внесения таксона', 'Подразд. ДБС', 'ЖФ по Раункиеру', 'Эко. влажность',
        'Эко. свет', 'Эко. плодородие', 'Эко. засоление', 'Эко. кислотность', 'Феноритмотип', 'Ценотип'];

    var common_cols = [
        {data: 'col_family', editor: false},
        {data: 'genera_name', editor: false},
        {data: 'species_name', editor: false},
        {data: 'infra_mark', editor: false},
        {data: 'infra_name', editor: false},
        {data: 'cult_name', editor: false},
        {data: 'author', editor: false},
        {data: 'cult_mark', editor: false},
        {data: 'col_order', editor: false},
        {data: 'col_class', editor: false},
        {data: 'col_phylum', editor: false},
        {data: 'hybrid_mark', editor: false, className: "htCenter"},
        {data: 'col_check', editor: false, className: "htCenter"},
        {data: 'in_red_list', editor: false, className: "htCenter"},
        {data: 'category', editor: false, className: "htCenter"},
        {data: 'input_date', editor: false},
        {data: 'name', editor: false},
        {data: 'raunc_form', editor: false},
        {data: 'moisture', editor: false},
        {data: 'light', editor: false},
        {data: 'fertility', editor: false},
        {data: 'salinity', editor: false},
        {data: 'acidity', editor: false},
        {data: 'phenotype', editor: false},
        {data: 'cenotype', editor: false}
    ];

    if (target === 'tu') {
        hs_table = new Handsontable(container, {
            data: tab_data,
            minSpareRows: 0,
            colHeaders: ['Профайл таксона', 'ID таксона'].concat(common_headers),
            columns: [
                {data: 'tu_link', editor: false, renderer: "html", className: "htCenter"},
                {data: 'pk', editor: false}
            ].concat(common_cols),
            contextMenu: false,
            rowHeaders: true,
            dropdownMenu: ['filter_by_condition2', 'filter_by_value', 'filter_action_bar'],
            // dropdownMenu: true,
            filters: true,
            // preventOverflow: 'horizontal',
            fillHandle: false,
            columnSorting: true,
            autoColumnSize: {useHeaders: true},
            autoRowSize: true,
            language: 'ru-Ru',
            licenseKey: 'non-commercial-and-evaluation'
        });
    } else {
        hs_table = new Handsontable(container, {
            data: tab_data,
            minSpareRows: 0,
            colHeaders: [
                'Профайл особи', 'ID особи', 'Живая', 'Дата поступления',
                'Дата выпада', 'Город (источник)'
            ].concat(common_headers),

            columns: [
                {data: 'ind_link', editor: false, renderer: "html", className: "htCenter"},
                {data: 'ind_id', editor: false},
                {data: 'is_alive', editor: false, className: "htCenter"},
                {data: 'income_date', editor: false},
                {data: 'out_date', editor: false},
                {data: 'name_ru', editor: false}
            ].concat(common_cols),
            contextMenu: false,
            rowHeaders: true,
            dropdownMenu: ['filter_by_condition2', 'filter_by_value', 'filter_action_bar'],
            // dropdownMenu: true,
            filters: true,
            // preventOverflow: 'horizontal',
            fillHandle: false,
            columnSorting: true,
            autoColumnSize: {useHeaders: true},
            autoRowSize: true,
            language: 'ru-Ru',
            licenseKey: 'non-commercial-and-evaluation'
        });
    }


    /** The logic for data download (CSV currently) */
    var button = document.getElementById("export-table-btn");
    var export_plugin = hs_table.getPlugin('exportFile');

    button.addEventListener('click', function() {
        export_plugin.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: true,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Результаты_поиска_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: false
        });
    });
});

/* Save buttons ===================================================================================================== */
/* TODO This part is working, still it could be replaced with native html form send. So it is possible to
*   replace this procedure with <form onsubmit='function()'> call with redirect to this same page*/
function save_search_data(type) {
    var form;
    var close_btn;
    // Get form object
    if (type === 'filters') {
        form = document.getElementById('search-filters-save');
        close_btn = document.getElementById('search-filters-close-btn');
    } else if (type === 'ids') {
        form = document.getElementById('search-ids-save');
        close_btn = document.getElementById('search-ids-close-btn') ;
    } else {
        return
    }
    // Set type field
    var type_field = form.querySelector('[name="type"]');
    type_field.value = type;

    // Check if form is valid:
    if (!form.checkValidity()) {
        var name_field = form.querySelector('[name="name"]');
        name_field.setAttribute('class', 'invalid');
        return
    }

    // Form is valid ==> prepare FormData
    var f_data = new FormData(form);

    // Send data with Ajax
    var request = new XMLHttpRequest();

    // Set error action
    request.addEventListener( 'Error', function(data) {
        console.log( data );
    } );

    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            var resp = JSON.parse(request.responseText);
            var form_parent;
            // If saved replace form with conformation:
            if (resp.status === 'saved') {
                form_parent = form.parentElement;
                // Attach new liternet to `Убрать` button click (it is on current modal) for this page reload
                close_btn.classList.remove("modal-close");
                close_btn.addEventListener('click', function() {
                    window.location.reload();
                });
                // Replace form with conformation message
                form_parent.innerHTML = "<div class='center'><h4 class='text-col-true'>Данные сохранены</h4></div>";
                // Remove save button
                if (type === 'ids') {
                    document.getElementById('search-ids-save-btn').remove();
                    document.getElementById('save-ids-modal-btn').remove();
                    document.getElementById('save-filters-modal-btn').remove();
                } else {
                    document.getElementById('search-filters-save-btn').remove();
                    document.getElementById('save-ids-modal-btn').remove();
                    document.getElementById('save-filters-modal-btn').remove();
                }
            } else {
                // Form contains errors
                // If __all__ error return error message
                if (resp['__all__']) {
                    form_parent = form.parentElement;
                    form_parent.innerHTML = "<div class='center'><h4 class='text-col-false'>" + resp['__all__'] + "</h4></div>";
                    // Remove save button
                    if (type === 'ids') {
                        document.getElementById('search-ids-save-btn').remove();
                    } else {
                        document.getElementById('search-filters-save-btn').remove();
                    }
                } else if (resp['name']) {
                    var name_field = form.querySelector('[name="name"]');
                    name_field.setAttribute('class', 'invalid');
                }
            }
        }
    };

    request.open("POST", save_url);
    request.send(f_data);
}