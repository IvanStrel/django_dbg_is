let dep_select_elem;
let from_year_elem;
let to_year_elem;

// Process in_out_json => add link to profile
const link_base = link_template.slice(0, -2);
in_out_json.forEach((tu) => {
    tu.tu_link = '<a href=' + link_base + tu.id + '/><i class="material-icons"> link </i></a>';
})

document.addEventListener('DOMContentLoaded', function() {
    dep_select_elem = document.querySelector('#id_dep_id');
    let dep_select = M.FormSelect.init(dep_select_elem, null);

    from_year_elem = document.querySelector('#id_from_year');
    let from_year = M.FormSelect.init(from_year_elem, null);

    to_year_elem = document.querySelector('#id_to_year');
    let to_year = M.FormSelect.init(to_year_elem, null);

    /** Years filtering ( in #id_to_year all years smaller then from year should be disabled) ======================= */
    from_year_elem.addEventListener('change', (event) => {
        let from_year_val = parseInt(event.target.value);
        let to_year_val = parseInt(to_year_elem.value)
        // Get `to_year` options
        let to_year_opt = to_year_elem.options;
        for (let i = 0; i < to_year_opt.length; i++) {
            let cur_val = parseInt(to_year_opt[i].value)
            to_year_opt[i].disabled = cur_val < from_year_val;
            // If to_year_val is less then current from_year, make it equal to `from_year_val`
            if (to_year_val < from_year_val) {
                to_year_opt[i].selected = cur_val === from_year_val;
            }
        }
        to_year = M.FormSelect.init(to_year_elem, null);
    })
});

/** Handson table =============================================================================================== */
// Prepare custom renderer
function in_out_renderer(instance, td, row, col, prop, value, cellProperties) {
  Handsontable.renderers.TextRenderer.apply(this, arguments);
  // if row contains negative number
  if (value === '↗') {
    td.className = 'income htCenter';
  }

  if (value === '↘️') {
    td.className = 'outcome htCenter';
  }
}

// Register renderer
Handsontable.renderers.registerRenderer('in_out_render', in_out_renderer);

// Initiate table only if data is present
if (in_out_json.length) {
    hs_table = new Handsontable(document.getElementById('hs-table'), {
        data: in_out_json,
        minSpareRows: 0,
        colHeaders: ['Год', 'Поступление / выпад', 'ID', 'Профайл', 'Род', 'Вид', 'Внутривид. метка',
                     'Внутривид. эпитет', 'Культивар', 'Автор'],
        columns: [
            {data: 'year', editor: false},
            {data: 'type', editor: false, renderer: 'in_out_render'},
            {data: 'id', editor: false},
            {data: 'tu_link', editor: false, renderer: 'html', className: "htCenter"},
            {data: 'genera_name', editor: false},
            {data: 'species_name', editor: false},
            {data: 'infra_mark', editor: false},
            {data: 'infra_name', editor: false},
            {data: 'cult_name', editor: false},
            {data: 'author', editor: false}
        ],
        contextMenu: false,
        rowHeaders: true,
        dropdownMenu: ['filter_by_condition2', 'filter_by_value', 'filter_action_bar'],
        // dropdownMenu: true,
        filters: true,
        // preventOverflow: 'horizontal',
        fillHandle: false,
        columnSorting: true,
        autoColumnSize: {useHeaders: true},
        autoRowSize: true,
        stretchH: 'all',
        language: 'ru-Ru',
        licenseKey: 'non-commercial-and-evaluation'
    });
}

