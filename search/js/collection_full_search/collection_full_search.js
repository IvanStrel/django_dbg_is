// prepare function for datepicker initiation
function datepicker_init(elems) {
    M.Datepicker.init(elems, {
        firstDay: true,
        format: 'yyyy-mm-dd',
        minDate: new Date(1964, 1, 1),
        maxDate: new Date(Date.now()),
        showClearBtn: true,
        yearRange: [1964, new Date().getFullYear()],
        i18n: {
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                     "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            weekdays: ["Воскресенье","Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            weekdaysShort: ["Вск","Пон", "Втр", "Срд", "Чет", "Пят", "Суб"],
            weekdaysAbbrev: ["В","По", "В", "Ср", "Ч", "Пя", "Су"],
            cancel: "Отменить",
            clear: "Очистить",
            done: "Готово"
        }
    });
}

var tabs_inst;
var dep_select_inst;
var rl_cat_select_inst;
var phylum_select_inst;
var class_select_inst;
var order_select_inst;
var family_select_inst;
var genera_select_inst;
var raunc_select_inst;
var moisture_select_inst;
var light_select_inst;
var fertility_select_inst;
var salinity_select_inst;
var acidity_select_inst;
var phenotype_select_inst;
var cenotype_select_inst;


document.addEventListener('DOMContentLoaded', function() {
    var tabs = document.querySelectorAll('.tabs');
    tabs_inst = M.Tabs.init(tabs, null);

    // Initiate modals
    var spat_modal_inst = M.Modal.init(document.querySelector('#spat-instruction'), null);

    // Initiate select
    // Departments
    var dep_select = document.querySelector('#id_departments_ids');
    dep_select_inst= M.FormSelect.init(dep_select, null);
    // Red List categories
    var rl_cat_select = document.querySelector('#id_red_list_cat');
    rl_cat_select_inst = M.FormSelect.init(rl_cat_select, null);

    // Initiate date pickers
    var date_elems = document.querySelectorAll(".datepicker");
    datepicker_init(date_elems);

    // Initiate Dual Boxes
    var phylum_select = document.querySelector('#id_col_phylum');
    phylum_select_inst = M.FormSelect.init(phylum_select, null);
    var class_select = document.querySelector('#id_col_class');
    class_select_inst = M.FormSelect.init(class_select, null);
    var order_select = document.querySelector('#id_col_order');
    order_select_inst = M.FormSelect.init(order_select, null);
    var family_select = document.querySelector('#id_col_family');
    family_select_inst = M.FormSelect.init(family_select, null);
    var genera_select = document.querySelector('#id_genera_name');
    genera_select_inst = M.FormSelect.init(genera_select, null);

    // Eco filters
    var raunc_select = document.querySelector('#id_raunc_form');
    raunc_select_inst = M.FormSelect.init(raunc_select, null);
    var moisture_select = document.querySelector('#id_moisture');
    moisture_select_inst = M.FormSelect.init(moisture_select, null);
    var light_select = document.querySelector('#id_light');
    light_select_inst = M.FormSelect.init(light_select, null);
    var fertility_select = document.querySelector('#id_fertility');
    fertility_select_inst = M.FormSelect.init(fertility_select, null);
    var salinity_select = document.querySelector('#id_salinity');
    salinity_select_inst = M.FormSelect.init(salinity_select, null);
    var acidity_select = document.querySelector('#id_acidity');
    acidity_select_inst = M.FormSelect.init(acidity_select, null);
    var phenotype_select = document.querySelector('#id_phenotype');
    phenotype_select_inst = M.FormSelect.init(phenotype_select, null);
    var cenotype_select = document.querySelector('#id_cenotype');
    cenotype_select_inst = M.FormSelect.init(cenotype_select, null);
});

/** Leaflet preparation and functions =============================================================================== */

/** Variables */
var saved_pks = {"Уровень 1": [], "Уровень 2": [], "Уровень 3": []};
var cur_layer = "Уровень 1";
var selected_pk = null;
var map;
var inputs_for_lay = {
    "Уровень 1": document.getElementById('id_sp_level_1'),
    "Уровень 2": document.getElementById('id_sp_level_2'),
    "Уровень 3": document.getElementById('id_sp_level_3')
};

style_selected = {
    fillColor : 'blue',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.5
};

style_base = {
    fillColor : 'transparent',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.2
};

style_saved = {
    fillColor : '#c733ff',
    color : 'blue',
    weight: 2,
    dashArray: null,
    fillOpacity: 0.9
};

/** Misc functions for leaflet */
function on_baselayer_change(e) {
    /** Fires when map base layer change
     * Such we can control which layer is currently on the map */
    cur_layer = e.name;
}

function move_layer(source_lyr, dest_lyr, pk, source_name) {
    source_lyr.eachLayer(function(lyr) {
        if (lyr.feature.properties.pk === pk) {
            var lyr_geojson = lyr.toGeoJSON();
            // add layer name to geojson
            lyr_geojson.properties.lyr_name = source_name;
            dest_lyr.addData(lyr.toGeoJSON());
        }
    })
}


function assign_multiple_input(obj, values) {
    // Remove old options
    while (obj.options.length) {
        obj.remove(0);
    }

    // Create new option
    for (i = 0; i < values.length; i++) {
      var new_option = new Option(values[i], values[i]);
      new_option.selected = true;
      obj.options.add(new_option);
    }
}


function populate_input(inputs_for_lay, cur_layer) {
    assign_multiple_input(inputs_for_lay[cur_layer], saved_pks[cur_layer]);
    // Fire 'change' event for filters counts update
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("change", false, true);
    inputs_for_lay[cur_layer].dispatchEvent(evt);
}


function save_layer() {
    /** The function for AddButton
     * on click, saves layer `pk` to `saved_pks` variable */
    // Get layer according to `cur_layer` and `selected_pk`
    move_layer(base_layers[cur_layer], saved_lyr, selected_pk, cur_layer);
    // add pk to saved layers
    saved_pks[cur_layer].push(selected_pk);
    // Update saved layer style
    saved_lyr.setStyle(style_saved);
    // Populate input
    populate_input(inputs_for_lay, cur_layer);
    // Remove AddButton
    add_button.remove();
    // Clear selection
    base_layers[cur_layer].setStyle(style_base);
}

function remove_layer() {
    /** The function for RemoveButton
     * on click, removes layer `pk` to `saved_pks` variable*/
    saved_lyr.eachLayer(function (lyr) {
        // Check if saved layer is of current layers (layer name property was created in `save_layer` function)
        if (lyr.feature.properties.lyr_name === cur_layer) { // cur_layer came from global
            // Find layer with selected pk
            if (lyr.feature.properties.pk === selected_pk) {
                // temporary save pk
                var pk = lyr.feature.properties.pk;
                // Remove layer
                saved_lyr.removeLayer(lyr._leaflet_id);
                // Remove pk from saved_pks list
                saved_pks[cur_layer].splice(saved_pks[cur_layer].indexOf(pk), 1);
            }
        }
    });
    // Populate input
    populate_input(inputs_for_lay, cur_layer);
}


/** Leaflet initialization ========================================================================================== */
// Create tiles layer
var tile_layer = L.tileLayer('/territory-map/{z}/{x}/{y}.png', {
    maxZoom: 22,
    attribution: '&copy; ГУ "ДБС"'
});

// Create geojson layers
var level1_json = L.geoJSON(poly_1).setStyle(style_base);
var level2_json = L.geoJSON(poly_2).setStyle(style_base);
var level3_json = L.geoJSON(poly_3).setStyle(style_base);
var saved_lyr = L.geoJSON(null, {'interactive':false});

var base_layers = {
        "Уровень 1": level1_json,
        "Уровень 2": level2_json,
        "Уровень 3": level3_json
    };


/** Listen to map visibility ======================================================================================== */
// The point is, our map is positioned in hidden div = it cant calculate own dimensions,
// therefore we need to initiate map, when it is on the screen
respondToVisibility = function(element, callback) {
  var options = {
    root: document.documentElement
  };

  var observer = new IntersectionObserver(function (entries, observer) {
    entries.forEach(function (entry) {
      callback(entry.intersectionRatio > 0);
    });
  }, options);

  observer.observe(element);
};

var is_already_visible = false;
document.addEventListener('DOMContentLoaded', function() {
    respondToVisibility(document.getElementById("tab4"), function (visible) {
        if (visible && (!is_already_visible)) {
            map = L.map('map', {
                center: [48.010, 37.88],
                zoom: 17,
                layers: [tile_layer, saved_lyr, level1_json],
                zoomControl: false,
                dragging: !L.Browser.mobile,
                tap: !L.Browser.mobile
            });
            // Add layers control
            var overlay_layers = {"Сохраненые": saved_lyr};
            L.control.layers(base_layers, overlay_layers).addTo(map);

            // Add function for layers change detection
            map.on('baselayerchange', on_baselayer_change);

            // Set visible to True (prevent reinitialization of map)
            is_already_visible = true;

        } else if (visible) {
            map.invalidateSize();
        }
    });
});

/** Control buttons ================================================================================================= */
/** Add layer Button */
L.Control.AddButton = L.Control.extend({
    onAdd: function(map) {
        var container = L.DomUtil.create('a');
        container.role="button";
        container.innerHTML = "Сохранить";

        container.onclick = save_layer;
        return container;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.add_button = function(opts) {
    return new L.Control.AddButton(opts);
};

// L.control.add_button({position: 'bottomleft'}).addTo(map);
var add_button = L.control.add_button({position: 'topleft'});

/** Remove layer Button */
L.Control.RemoveButton = L.Control.extend({
    onAdd: function(map) {
        var container = L.DomUtil.create('a');
        container.role="button";
        container.innerHTML = "Удалить";

        container.onclick = remove_layer;
        return container;
    },

    onRemove: function(map) {
        // Nothing to do here
    }
});

L.control.remove_button = function(opts) {
    return new L.Control.RemoveButton(opts);
};

// L.control.add_button({position: 'bottomleft'}).addTo(map);
var remove_button = L.control.remove_button({position: 'topleft'});




/** On click functions ============================================================================================== */
/** Attach one click functions **/
level1_json.on('click', oneClick);
level2_json.on('click', oneClick);
level3_json.on('click', oneClick);

function oneClick(e){
    /** Highlight clicked polygon
     *   If current polygon of current layer is not added add button show
     *  **/
    this.setStyle(style_base);
    // second, change style of selected polygon
    e.layer.setStyle(style_selected);
    selected_pk = e.layer.feature.properties.pk;

    // if selected pk in saved_pks, show Remove button
    if (saved_pks[cur_layer].includes(selected_pk)) {
        // remove ADD button from map and add REMOVE button
        remove_button.addTo(map);
        add_button.remove()
    // Else, show Add button
    } else {
        // remove REMOVE button from map and add ADD button
        remove_button.remove();
        add_button.addTo(map);
    }
    saved_lyr.setStyle(style_saved);
}


/** =================================================================================================================
 * Inputs watch and update
 * ================================================================================================================== */
// Prepare an object with all form input elements
var inputs_to_watch = {
    // Common filters
    common: {
        alive_only: {obj: document.getElementById('id_alive_only'), type: 'check', state: false},
        after_time: {obj: document.getElementById('id_after_time'), type: 'text', state: ""},
        before_time: {obj: document.getElementById('id_before_time'), type: 'text', state: ""},
        in_col: {obj: document.getElementById('id_in_col'), type: 'check', state: true},
        not_in_col: {obj: document.getElementById('id_not_in_col'), type: 'check', state: true},
        hybrid_only: {obj: document.getElementById('id_hybrid_only'), type: 'check', state: false},
        with_images: {obj: document.getElementById('id_with_images'), type: 'check', state: false},
        departments_ids: {obj: document.getElementById('id_departments_ids'), type: 'multiple', state: []},
        in_red_list: {obj: document.getElementById('id_in_red_list'), type: 'check', state: false},
        red_list_cat: {obj: document.getElementById('id_red_list_cat'), type: 'multiple', state: []}
    },

    // Taxonomy
    taxonomy: {
        col_phylum: {obj: document.getElementById('id_col_phylum'), type: 'multiple', state: ['Tracheophyta']},
        col_class: {obj: document.getElementById('id_col_class'), type: 'multiple', state: []},
        col_order: {obj: document.getElementById('id_col_order'), type: 'multiple', state: []},
        col_family: {obj: document.getElementById('id_col_family'), type: 'multiple', state: []}
    },
    ecology: {
        // Ecology
        genera_name: {obj: document.getElementById('id_genera_name'), type: 'multiple', state: []},
        raunc_form: {obj: document.getElementById('id_raunc_form'), type: 'multiple', state: []},
        moisture: {obj: document.getElementById('id_moisture'), type: 'multiple', state: []},
        light: {obj: document.getElementById('id_light'), type: 'multiple', state: []},
        fertility: {obj: document.getElementById('id_fertility'), type: 'multiple', state: []},
        salinity: {obj: document.getElementById('id_salinity'), type: 'multiple', state: []},
        acidity: {obj: document.getElementById('id_acidity'), type: 'multiple', state: []},
        phenotype: {obj: document.getElementById('id_phenotype'), type: 'multiple', state: []},
        cenotype: {obj: document.getElementById('id_cenotype'), type: 'multiple', state: []}
    },
    // Spatial
    spatial: {
        sp_level_1: {obj: document.getElementById('id_sp_level_1'), type: 'text_mult', state: ['']},
        sp_level_2: {obj: document.getElementById('id_sp_level_2'), type: 'text_mult', state: ['']},
        sp_level_3: {obj: document.getElementById('id_sp_level_3'), type: 'text_mult', state: ['']}
    }
};

// Prepare object with tabs objects
var tab_obj_numbers = {
    common: document.getElementById('common-filter-tab'),
    taxonomy: document.getElementById('tax-filter-tab'),
    ecology: document.getElementById('eco-filter-tab'),
    spatial: document.getElementById('spatial-filter-tab')
};

// Initiate number of selected filters
var filters_num = {common: 0, taxonomy: 0, ecology: 0, spatial: 0};

// Function for filters number update
function filters_num_update(obj_list, filter_num) {
    // Reinitiate filters numbers first
    Object.entries(filter_num).forEach(function(e) {
        filter_num[e[0]] = 0;
    });
    Object.entries(obj_list).forEach(function (entry) {
        var cur_category = entry[0];
        // Loop over each input in category and compare current value with `state`
        Object.entries(entry[1]).forEach(function(inp) {
            var inp_name = inp[0];
            var inp_obj = inp[1].obj;
            var inp_type = inp[1].type;
            var inp_state = inp[1].state;

            var cur_state;
            // Get current state
            if (inp_type === 'check') {
                cur_state = inp_obj.checked
            } else if (inp_type === 'multiple') {
                if (inp_obj.selectedOptions.length) {
                    cur_state = Array.from(inp_obj.selectedOptions).map(function (o) {
                        return (o.value)
                    });
                } else {
                    cur_state = []
                }
            } else if (inp_type === 'text_mult') {
                // The case for Spatial inputs. Values are of type = '1,2,3', therefore cast to array
                cur_state = inp_obj.value.split(',');
            } else {
                cur_state = inp_obj.value;
            }
            // Update filters_num object if states differ
            if (String(cur_state) !== String(inp_state)) {
                if (Array.isArray(cur_state)) {
                    // Add number of new filters in multiple select fields
                    filter_num[cur_category] += cur_state.length - inp_state.length;
                    // As in spatial filters empty value is [''], ie of length 1 as is as ['1'] we need to increment
                    // by 1 for any spatial filter != init_state
                    if (inp_type === 'text_mult') {
                        filter_num[cur_category] += 1;
                    }
                } else {
                    filter_num[cur_category] += 1;
                }
            }
        });
    })
}

// Function for tab numbers update
function tab_numbers_update(tab_obj_numbers, filters_num) {
    Object.entries(tab_obj_numbers).forEach(function(tab) {
        tab_obj_numbers[tab[0]].dataset.count =  filters_num[tab[0]]
    })
}

/** Initiate counts on tabs*/
document.addEventListener('DOMContentLoaded', function() {
    // Update values form previously filled form
    filters_num_update(inputs_to_watch, filters_num);
    // Update numbers on tabs
    tab_numbers_update(tab_obj_numbers, filters_num);

    // Assign `filters_num_update` function on `change` event for each input
    Object.entries(inputs_to_watch).forEach(function (cat) {
        Object.entries(cat[1]).forEach(function(cat_obj) {
            cat_obj[1].obj.addEventListener('change', function () {
                // Update filters_num
                filters_num_update(inputs_to_watch, filters_num);
                // Update numbers on tabs
                tab_numbers_update(tab_obj_numbers, filters_num)
            })
        })
    });
});


/** =================================================================================================================
 * Submit button
 * somehow it does not work properly, therefore I will add click event listener
 * ================================================================================================================== */
document.getElementById('send-button').addEventListener('click', function (ev) {
    ev.preventDefault();
    document.getElementById('main-form').submit();
});