from django import forms
import json
from datetime import datetime

from mptt.forms import TreeNodeMultipleChoiceField
from mptt.forms import TreeNodeChoiceField

# Import models
from collection.models import ColTaxonomy
from collection.models import TaxonomicUnit
from collection.models import ObligatoryInfo
from users_and_organization.models import Departments
from spatial.models import PolygonsLevel1
from spatial.models import PolygonsLevel2
from spatial.models import PolygonsLevel3

# Import utils
from collection.utils import RedListCatConverter
from collection.utils import CollectionSearchRequestData
from collection.utils import remove_empty_choices


class CollectionSpatialSearch(forms.Form):
    type = forms.ChoiceField(label='Цель поиска', choices=(('ind', 'Ососби'), ('tu', 'Таксоны')),
                             widget=forms.RadioSelect, initial='ind')
    data = forms.CharField(widget=forms.HiddenInput())
    data_parsed = forms.CharField(widget=forms.HiddenInput(), required=False)

    def clean(self):
        cleaned_data = super().clean()
        data_raw = cleaned_data.get("data")

        # Check if data field contains at least one pk
        if data_raw:
            data_parsed = json.loads(data_raw)
            pks = [len(v) for v in data_parsed.values()]
            if not any(pks):
                raise forms.ValidationError("Для поиска, сначала нужно выбрать хотя бы один участок")
        return cleaned_data

    def clean_data_parsed(self):
        cleaned_data = self.cleaned_data
        data_parsed = cleaned_data.get("data")

        if data_parsed:
            try:
                # Convert to dict
                data_parsed = json.loads(data_parsed)
                # Convert strings to integers
                data_parsed = {k: [int(x) for x in v] for k, v in data_parsed.items()}
            except:
                raise forms.ValidationError("Нефиг баловаться с запросами !!!")
            else:
                return data_parsed
        return data_parsed


class SaveSearchData(forms.Form):
    type = forms.CharField(widget=forms.HiddenInput())
    name = forms.CharField(max_length=150, required=True)

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('type') not in ('ids', 'filters'):
            raise forms.ValidationError("Уй ти мой маленький хакер, нашел скрытое поле ввода")
        if not cleaned_data.get('name'):
            raise forms.ValidationError("Имя списка обязательно")

        return cleaned_data


class CollectionFullSearchForm(forms.Form):
    type = forms.ChoiceField(label='Цель поиска', choices=(('ind', 'Особи'), ('tu', 'Таксоны')),
                             widget=forms.RadioSelect, initial='ind')
    alive_only = forms.BooleanField(label="Отобрать только живые", required=False)
    after_time = forms.DateField(label="Дата поступления после", required=False)
    before_time = forms.DateField(label="Дата поступления до", required=False)
    in_col = forms.BooleanField(label="Сверенные с Catalogue of Life", required=False, initial=True)
    not_in_col = forms.BooleanField(label="Не сверенные с Catalogue of Life", required=False, initial=True)
    hybrid_only = forms.BooleanField(label="Только гибриды", required=False, initial=False)
    with_images = forms.BooleanField(label="Только с изображениями", required=False, initial=False)

    # Departments filter
    departments_ids = TreeNodeMultipleChoiceField(queryset=Departments.objects.all(), label='Подразделение ДБС',
                                                  required=False)

    # IUCN Red List
    in_red_list = forms.BooleanField(label="Только входящие в красный список МСОП?", initial=False, required=False)

    # Prepare choices for IUCN Red List categories
    red_list_cat_choices = RedListCatConverter().cat_choices()
    red_list_cat = forms.MultipleChoiceField(label="Категории угроз МСОП", widget=forms.SelectMultiple,
                                             choices=red_list_cat_choices, required=False)

    # Taxonomy fields
    col_phylum = forms.MultipleChoiceField(label="Отделы", widget=forms.SelectMultiple,
                                           choices=[('', '')], initial='Tracheophyta')
    col_class = forms.MultipleChoiceField(label="Классы", widget=forms.SelectMultiple, choices=[('', '')],
                                          required=False)
    col_order = forms.MultipleChoiceField(label="Порядки", widget=forms.SelectMultiple, choices=[('', '')],
                                          required=False)
    col_family = forms.MultipleChoiceField(label="Семейства", widget=forms.SelectMultiple, choices=[('', '')],
                                           required=False)
    genera_name = forms.MultipleChoiceField(label="Роды", widget=forms.SelectMultiple, choices=[('', '')],
                                            required=False)

    # Ecology filters
    raunc_form = forms.MultipleChoiceField(label="Жизненная форма по Раункиеру", widget=forms.SelectMultiple,
                                           choices=remove_empty_choices(ObligatoryInfo.raunc_form_choices),
                                           required=False)
    moisture = forms.MultipleChoiceField(label="Эко. группа по отношению к увлажнению", widget=forms.SelectMultiple,
                                         choices=remove_empty_choices(ObligatoryInfo.moisture_choices), required=False)
    light = forms.MultipleChoiceField(label="Эко. группа по отношению к освещению", widget=forms.SelectMultiple,
                                      choices=remove_empty_choices(ObligatoryInfo.light_choices), required=False)
    fertility = forms.MultipleChoiceField(label="Эко. группа по отношению к плодородию", widget=forms.SelectMultiple,
                                          choices=remove_empty_choices(ObligatoryInfo.fertil_choices), required=False)
    salinity = forms.MultipleChoiceField(label="Эко. группа по отношению к засолению", widget=forms.SelectMultiple,
                                         choices=remove_empty_choices(ObligatoryInfo.salin_choices), required=False)
    acidity = forms.MultipleChoiceField(label="Эко. группа по отношению к кислотности", widget=forms.SelectMultiple,
                                        choices=remove_empty_choices(ObligatoryInfo.acidity_choices), required=False)
    phenotype = forms.MultipleChoiceField(label="Феноритмотип", widget=forms.SelectMultiple,
                                          choices=remove_empty_choices(ObligatoryInfo.pheno_choices), required=False)
    cenotype = forms.MultipleChoiceField(label="Ценотип", widget=forms.SelectMultiple,
                                         choices=remove_empty_choices(ObligatoryInfo.cenotype_choices), required=False)

    # Spatial filters
    # Get level 1 ids
    sp_level_1 = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'is_hidden': True}), required=False,
                                           choices=[('', '')])
    sp_level_2 = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'is_hidden': True}), required=False,
                                           choices=[('', '')])
    sp_level_3 = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={'is_hidden': True}), required=False,
                                           choices=[('', '')])

    def __init__(self, *args, **kwargs):
        super(CollectionFullSearchForm, self).__init__(*args, **kwargs)

        # Prepare initial choices for phylum, class, order, family and genera
        tax_objects = TaxonomicUnit.objects.filter(family_col__col_phylum='Tracheophyta').select_related()
        phylum_choices = tax_objects.values_list("family_col__col_phylum", "family_col__col_phylum").distinct().order_by("family_col__col_phylum")
        class_choices = tax_objects.values_list("family_col__col_class", "family_col__col_class").distinct().order_by("family_col__col_class")
        order_choices = tax_objects.values_list("family_col__col_order", "family_col__col_order").distinct().order_by("family_col__col_order")
        family_choices = tax_objects.values_list("family_col__col_family", "family_col__col_family").distinct().order_by("family_col__col_family")
        genera_choices = tax_objects.values_list("genera_name", "genera_name").distinct().distinct().order_by("genera_name")
        # Add choices to fields
        self.fields["col_phylum"].choices = phylum_choices
        self.fields["col_class"].choices = class_choices
        self.fields["col_order"].choices = order_choices
        self.fields["col_family"].choices = family_choices
        self.fields["genera_name"].choices = genera_choices

        # Prepare choices for Spatial filters
        self.fields["sp_level_1"].choices = PolygonsLevel1.objects.values_list('id', 'id')
        self.fields["sp_level_2"].choices = PolygonsLevel2.objects.values_list('id', 'id')
        self.fields["sp_level_2"].choices = PolygonsLevel3.objects.values_list('id', 'id')

    def clean_departments_ids(self):
        cleaned_data = self.cleaned_data
        dep_queryset = cleaned_data.get("departments_ids")

        if dep_queryset:
            return [x[0] for x in dep_queryset.values_list('id')]

        return None

    def clean_after_time(self):
        cleaned_data = self.cleaned_data
        after_time = cleaned_data.get("after_time")
        if after_time:
            return str(after_time)
        else:
            return after_time

    def clean_before_time(self):
        cleaned_data = self.cleaned_data
        before_time = cleaned_data.get("before_time")
        if before_time:
            return str(before_time)
        else:
            return before_time

    def clean(self):
        cleaned_data = super().clean()
        # remove empty values
        return {k: v for k, v in cleaned_data.items() if v != []}


class CollectionInOutReportForm(forms.Form):
    CUR_YEAR = datetime.today().year
    YEAR_CHOICES = [[x, x] for x in range(1964, CUR_YEAR + 1)]
    YEAR_CHOICES.reverse()
    from_year = forms.ChoiceField(label="Начиная с года:", required=True,
                                  choices=YEAR_CHOICES, initial=CUR_YEAR)
    to_year = forms.ChoiceField(label="Заканчивая годом:", required=True,
                                choices=YEAR_CHOICES, initial=CUR_YEAR)
    dep_id = TreeNodeChoiceField(queryset=Departments.objects.all(), label='Подразделение ДБС',
                                 required=True, initial=1)

    def clean_to_year(self):
        """
        To Year should be after or be equal to `from_year`
        """
        cleaned_data = self.cleaned_data
        to_year = cleaned_data.get("to_year")
        from_year = cleaned_data.get("from_year")
        if to_year < from_year:
            raise forms.ValidationError("Должен быть меньше, чем в поле 'Начиная с года:'")
        else:
            return to_year
