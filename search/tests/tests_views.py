import re
import codecs
from django.test import TestCase
from django.test import Client


class SimpleTaxFilterTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    def test_not_post_request(self):
        """
        Not POST request should return 404
        :return: none
        """
        c = Client()
        response = c.get("/search/simple-tax-filter/")
        self.assertEqual(response.status_code, 404)

    def test_nonexisting_taxon_level(self):
        """
        Should return JSON with error if request
        contains non existing taxon level names
        :return: none
        """
        c = Client()

        # The case with nonexisting filter, but existing target
        response = c.post("/search/simple-tax-filter/",
                          {'filter': '{"wrong_taxon":"some_name"}',
                            'target': 'itis_kingdom'})
        resp_text = codecs.decode(response.content, 'unicode_escape')
        self.assertEqual(bool(re.search('wrong_filter', resp_text)), True)

        # The case with existing filter, but nonexisting target
        response = c.post("/search/simple-tax-filter/", {'filter': '{"itis_class":"some_name"}',
                                                         'target': 'wrong_taxon'})
        resp_text = codecs.decode(response.content, 'unicode_escape')
        self.assertEqual(bool(re.search('wrong_target', resp_text)), True)

    def test_normal_response(self):
        """
        Normal post request should return
        a list with taxon names
        :return: none
        """
        c = Client()
        response = c.post("/search/simple-tax-filter/",
                          {'filter': '{"itis_class":"Magnoliopsida", "itis_order":"Rosales"}',
                           'target': 'itis_family'})
        response_obj = eval(codecs.decode(response.content, 'unicode_escape'))
        self.assertEqual(type(response_obj), type([]))
