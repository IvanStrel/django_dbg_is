from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    # Taxonomy filter
    path('collection-report/<str:target>', views.collection_report, name='collection_report'),
    path('collection-inout-report/', views.collection_income_outcome_report, name='collection_income_outcome_report'),
    path('simple-tax-filter/', views.simple_tax_filter, name='simple_tax_filter'),
    path('simple_city_filter/', views.simple_city_filter, name='simple_city_filter'),
    path('collection-fast-search', views.CollectionFastSearch.as_view(), name='collection_fast_search'),
    path('collection-spatial-search', views.collection_spatial_search, name='collection_spatial_search'),
    path('collection-search-results', views.collection_search_results, name='collection_search_results'),
    path('collection-save-search', views.collection_save_search, name='collection_save_search'),
    path('collection-full-search', views.collection_full_search, name='collection_full_search'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
