import json
import pandas as pd
from datetime import datetime
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.http import Http404
from django.views import View
from django.db.models.query import prefetch_related_objects

from django.contrib.auth.decorators import login_required

from django.core.serializers import serialize

from django.db.models import Count
from django.db.models import Case
from django.db.models import When
from django.db.models import F

# Import models
from collection.models import ColTaxonomy
from collection.models import TaxonomicUnit
from collection.models import Individual
from collection.models import RedListIUCN

from spatial.models import PolygonsLevel1
from spatial.models import PolygonsLevel2
from spatial.models import PolygonsLevel3

from spatial.models import SovCountries
from spatial.models import AdminRegions
from spatial.models import PopulatedPlaces

from users_and_organization.models import SavedSearchLists
from users_and_organization.models import Departments

# Import forms
from .forms import CollectionSpatialSearch
from .forms import SaveSearchData
from .forms import CollectionFullSearchForm
from .forms import CollectionInOutReportForm

# Import dataclasses
from collection.utils import CollectionSearchRequestData
from collection.utils import cumulative_tu_income_outcome

# Import serializers
from collection.serializers import ColTuSearchResultSerializer
from collection.serializers import ColIndSearchResultSerializer
from collection.serializers import ColTopNCountsSerializer
from collection.serializers import ColTuIncomeOutcomeSerializer
from collection.serializers import ColTuIncomeOutcomeByYearSerializer
from spatial.serializers import CitiesGreatCircleSerializer


def simple_tax_filter(request):
    """
    Taxonomy filter. The view accept post request.
    Request has to contain two elements:
        filter -- json like string with pairs of taxon level and taxon name
            For example: "{'class': 'rosales', 'family': 'moraceae'}"
        target -- string with desired taxon level
            For example: "genera"
    :param request: http request of type POST
    :return: json with
    """

    # Return 404 if request type is not POST
    if request.method != 'POST':
        raise Http404

    # Get arguments for filtering
    filter_args = request.POST.get("filter", None)
    kwargs = eval(filter_args)

    # Capitalize taxon names in kwargs
    for k, v in kwargs.items():
        kwargs[k] = v[:1].upper() + v[1:]

    # Get target taxon name
    target = request.POST.get("target", None)

    # Test if filtering arguments keys are valid column names
    taxonomy_fields = [field.name for field in ColTaxonomy._meta.get_fields()]

    if not all([x in taxonomy_fields for x in kwargs.keys()]):
        return HttpResponse("{'error': 'wrong_filter'}", content_type='application/json')

    # Test if a target string is a valid column name
    if target not in taxonomy_fields:
        return HttpResponse("{'error': 'wrong_target'}", content_type='application/json')

    # Perform filtering
    # filtered = Taxonomy.objects.filter(**kwargs).only(target).distinct(target).order_by(target)
    # data = serializers.serialize('json', filtered, fields=target)

    filtered = ColTaxonomy.objects.filter(**kwargs).distinct(target).order_by(target).values_list(target, flat=True)
    data = json.dumps(list(filtered))

    return HttpResponse(data, content_type='application/json')


def simple_city_filter(request):
    """
    The filter for dynamic city name choosing with drop downs. The view accept post request.
    Request must to contain a json with `country` and `admin` elements.
    The view should return all admin names for country
    AND
        1. All cities names for country if admin is null
        2. All cities names for admin if admin is not null
    :param request:
    :return: :str JSON with `admin` and `city` names, ids and lon lat for cities.
    """
    reques_data = json.loads(request.body)
    country_id = reques_data.get('country')
    admin_id = reques_data.get('admin')
    # Prepare response dictionary:
    resp_dict = {"error": False}
    admin_list = ()
    city_list = ()

    # Prepare Country and admin choices
    country_obj = SovCountries.objects.filter(pk=country_id)
    admin_obj = AdminRegions.objects.filter(pk=admin_id)

    # Prepare admin choices, first check if such country exists
    # Retain only admin regions where cities exists
    if country_obj.exists():
        admin_list = AdminRegions.objects.filter(sov_id_id=country_obj.get(), populatedplaces__isnull=False)\
            .values_list('id', 'name_ru').order_by('name_ru').distinct()
        admin_list = tuple(admin_list)

        # Prepare cities for country
        city_query = PopulatedPlaces.objects.filter(sov_id_id=country_obj.get())
        # If admin exists and is not none filter by admin
        if admin_obj.exists():
            city_query = city_query.filter(admin_id_id=admin_obj.get())
            # Add error if admin_id is not None, but do not exists
            if admin_id:
                resp_dict.update({'error': "wrong admin"})

        # Get cities list (replace ru_name with name of ru_name = null)
        city_list = city_query.\
            annotate(name_res=(Case(When(name_ru__isnull=False, then=F('name_ru')), default=F('name'))))\
            .values_list('id', 'name_res', 'point').order_by('name_res')
        city_list = [(x, {'name_ru': y, 'lon': z.x, 'lat': z.y}) for x, y, z in city_list]
    else:
        resp_dict.update({'error': "wrong country"})

    resp_dict.update({'admin': admin_list, 'city': city_list})
    resp = json.dumps(resp_dict)
    return HttpResponse(resp, content_type='application/json')


class CollectionFastSearch(View):
    def get(self, request):
        # We need to get TaxonomicUnit objects annotated with field "table"
        # and "sim" which contains table name and Trigram similarity of request with TaxonomicUnit.full_name,
        # Synonyms.full_name and Vernaculars.vernacular simultaneously

        # Get search string
        target = request.GET.get("query")

        if target is None or target == '':
            return render(request, 'fast_search/fast_search.html', {"target": None})

        tu_res = TaxonomicUnit.objects.raw('''SELECT "id",
                                                       "full_name" <-> %s AS "sim",
                                                       'tu' AS "table",
                                                       "full_html_name" AS "html",
                                                       "full_html_name"
                                                FROM collection_taxonomicunit AS tu
                                                WHERE tu."full_name" <-> %s < 0.8
                                                UNION (
                                                    SELECT tu."id",
                                                           syn."col_full_name" <-> %s AS "sim",
                                                           'syn' AS "table",
                                                           syn."col_full_name_html" AS "html",
                                                           tu."full_html_name"
                                                    FROM collection_taxonomicunit AS tu
                                                    LEFT JOIN collection_colspecies AS acc ON acc."col_id" = tu."col_id"
                                                    LEFT JOIN collection_colspecies AS syn
                                                        ON syn."col_accepted_id" = acc."col_id"
                                                    WHERE syn."col_full_name" <-> %s < 0.8 AND tu.cult_name IS NULL
                                                )
                                                UNION (
                                                    SELECT tu."id",
                                                           vern."vernacular" <-> %s AS "sim",
                                                           'vern' AS "table",
                                                           vern."vernacular" AS "html",
                                                           tu."full_html_name"
                                                    FROM collection_taxonomicunit AS tu
                                                    LEFT JOIN collection_vernacular AS vern ON vern."tu_id_id" = tu."id"
                                                    WHERE vern."vernacular" <-> %s < 0.8
                                                )
                                                ORDER BY sim LIMIT 25;
                                                ''', (target, target, target, target, target, target))

        data = {
            "target": target,
            "res": tu_res,
        }
        return render(request, 'fast_search/fast_search.html', data)


def collection_spatial_search(request):
    """
    The view for individuals filtering and retrieval with spatial lookups.
    On GET request ==> interactive map to choose polygons of interest
    On POST request (receive polygons layers and ids) ==> get individual points ==> save individuals ids to session ==>
        redirect to search results page
    :param request: request object
    :return: html page on GET request or redirect on POST requests
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Get request handling
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Get polygons (all 3 layers) as geojson
    poly_1 = serialize("geojson", PolygonsLevel1.objects.all(), geometry_field='poly')
    poly_2 = serialize("geojson", PolygonsLevel2.objects.all(), geometry_field='poly')
    poly_3 = serialize("geojson", PolygonsLevel3.objects.all(), geometry_field='poly')

    if request.method != 'POST':

        # Prepare form
        form = CollectionSpatialSearch()
        context = {"poly_1": poly_1, "poly_2": poly_2, "poly_3": poly_3, "form": form}

        # Render output html
        return render(request, "collection_spatial_search/collection_spatial_search.html", context)

    else:
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # Get request handling
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        form = CollectionSpatialSearch(request.POST)

        if form.is_valid():
            ''' Construct query dataclass instance, save it to session and redirect to results '''
            qd = CollectionSearchRequestData(
                type=form.cleaned_data.get("type"),
                sp_level_1=form.cleaned_data.get("data_parsed").get("sp_level_1"),
                sp_level_2=form.cleaned_data.get("data_parsed").get("sp_level_2"),
                sp_level_3=form.cleaned_data.get("data_parsed").get("sp_level_3"),
            )

            # Create dict for session. Keys:
            #  saved -- if the search query data was saved to user profile (should be not)
            #  data -- actual search query data = qd.as_dict()
            search_data = {
                "saved": False,
                "data": qd.as_dict(),
                "data_readable": qd.readable_filters()
            }
            request.session['search_data'] = search_data

            # redirect to search results
            return redirect('collection_search_results')
        else:
            # Return form with error messages
            context = {"poly_1": poly_1, "poly_2": poly_2, "poly_3": poly_3, "form": form}
            return render(request, "collection_spatial_search/collection_spatial_search.html", context)


def collection_search_results(request):
    """
    The view for search results presenting as a table.
    Should work only if current session contains `search_data`, else, should show error message.
    :param request: django request object (any type)
    :return: html response
    """
    # Prepare context
    context = {"data_missed": True}

    # Prepare form for search data saving to User profile
    save_form = SaveSearchData()
    context.update({"save_form": save_form})

    # Check if session contains `search_data`
    search_data = request.session.get("search_data")
    if not search_data:
        context.update({"data_missed": True})
        return render(request, "collection_search_results/collection_search_results.html", context)

    # Else, proceed
    # Create CollectionSearchRequestData instance with data saved in session
    qd = CollectionSearchRequestData(**request.session.get("search_data").get('data'))
    search_queryset = TaxonomicUnit.objects.full_search(qd)

    # Prefetch related obligatoryinfo = prevent duplicated calls
    prefetch_related_objects(search_queryset, 'obligatoryinfo')
    # Serialize depending on type
    if search_data.get("data").get("type") == "tu":
        table_json = json.dumps(ColTuSearchResultSerializer(search_queryset, many=True).data)
    else:
        table_json = json.dumps(ColIndSearchResultSerializer(search_queryset, many=True).data)

    context.update({"data": search_data.get("data"),
                    "saved": search_data.get("saved"),
                    "table_json": table_json,
                    "data_readable": search_data.get("data_readable")
                    })

    return render(request, "collection_search_results/collection_search_results.html", context)

@login_required
def collection_save_search(request):
    """
    The view for current search results saving as list of filters (for reuse)
    or hardcoded list of IDs (tu or individuals)
    :param request: POST request
    :return:
    """
    # TODO currently it is working with AJAX call from JS. Still it is possible to replace this with normal html
    #  form send with redirect tot this same page.
    if request.method != 'POST':
        return

    # Get search data
    search_data = request.session.get("search_data")
    # If search data is absent
    if not search_data:
        return HttpResponse('Завязывай заниматься фигней', content_type='application/json')

    # Check POST data
    form = SaveSearchData(request.POST)
    if form.is_valid():
        c_data = form.cleaned_data
        # If type == 'filters' save `search_data` from session
        # If type == 'ids' perform TaxonomicUnit.object.full_search() call with `search_data` from session
        #   get tu or individuals ids, populate new CollectionSearchRequestData with them and save it as dict to DB
        if c_data.get('type') == "ids":
            qd = CollectionSearchRequestData(**search_data.get('data'))
            search_queryset = TaxonomicUnit.objects.full_search(qd)
            if search_data.get('data').get("type") == "tu":
                search_dict = ColTuSearchResultSerializer(search_queryset, many=True).data
                ids_list = [x.get('pk') for x in search_dict]
                new_qd = CollectionSearchRequestData(type='tu', tu_ids=ids_list)
            else:
                search_dict = ColIndSearchResultSerializer(search_queryset, many=True).data
                ids_list = [x.get('ind_id') for x in search_dict]
                new_qd = CollectionSearchRequestData(type='ind', ind_ids=ids_list)
            # Replace search_data with new CollectionSearchRequestData object
            search_data['data'] = new_qd.as_dict()
            search_data['data_readable'] = new_qd.readable_filters()

        # Update saved tag on search_data
        search_data.update({"saved": True})
        # Save it to DB
        db_obj = SavedSearchLists(user=request.user,
                                  type=c_data.get('type'),
                                  name=c_data.get('name'),
                                  search_query=search_data)
        db_obj.save()

        # Update session search data
        request.session['search_data'] = search_data
        # Return success
        return HttpResponse(json.dumps({'status': "saved"}), content_type='application/json')
    else:
        errors = form.errors
        errors.update({'status': "error"})
        # Send errors
        return HttpResponse(json.dumps(form.errors), content_type='application/json')


def collection_full_search(request):
    # Prepare spatial data
    poly_1 = serialize("geojson", PolygonsLevel1.objects.all(), geometry_field='poly')
    poly_2 = serialize("geojson", PolygonsLevel2.objects.all(), geometry_field='poly')
    poly_3 = serialize("geojson", PolygonsLevel3.objects.all(), geometry_field='poly')

    context = {"poly_1": poly_1, "poly_2": poly_2, "poly_3": poly_3}

    # Process request
    if request.method != 'POST':
        # Get Department:

        # Prepare form
        form = CollectionFullSearchForm(initial={'col_phylum': "Tracheophyta"})

        # Contex
        context.update({"form": form})
        return render(request, "collection_full_search/collection_full_search.html", context)

    else:
        # POST method
        form = CollectionFullSearchForm(request.POST)
        if form.is_valid():
            # Populate CollectionSearchRequestData data clas with form data
            qd = CollectionSearchRequestData(**form.cleaned_data)
            search_data = {
                "saved": False,
                "data": qd.as_dict(),
                "data_readable": qd.readable_filters()
            }
            # Save search data to session
            request.session['search_data'] = search_data
            # Redirect to search results
            return redirect("collection_search_results")

        else:
            # Return form with errors
            context.update({"form": form})
            return render(request, "collection_full_search/collection_full_search.html", context)


@login_required
def collection_report(request, target):
    """
    The view for report rendering.
    Report elements:
    1. + Counts of Classes --> barplot [vertical] (first 10 + others)
    2. + Counts of Orders --> barplot [vertical] (first 10 + others)
    3. + Counts of Families --> barplot [vertical] (first 20 + others)
    4. + Counts of Generas --> barplot [vertical] (first 20 + others)
    5. + Counts of IUCN redlis categories --> barplot [Horizontal] (all)
    6. + Cumulative TU counts by date --> area plot [horizontal] (alive + dead)
    7. + Individuals placement --> leaflet + heatmap
    8. + Counts of cultivation types --> pie-chart
    9. + Individual Origin cities --> d3 map with great circles
    10. + Individuals Origin nature --> leaflet + OSM
    11. + TU counts by department --> barplot [vertical] (species + subspecies + cultivars)
    :param request: GET request object
    :param target: string, possible values ['prev', 'all']
    :return: html response
    """

    # Prepare CollectionSearchRequestData object depending on target parameter
    if target == "prev":  # get data from session
        search_data = request.session.get("search_data")
        if not search_data:
            return
        # Force type = ind
        search_data["data"]["type"] = "ind"
        qd = CollectionSearchRequestData(**search_data.get("data"))
    else:
        qd = CollectionSearchRequestData(type="ind")

    # Prepare ids for individuals of interest
    search_queryset = TaxonomicUnit.objects.full_search(qd)
    # Prefetch related obligatoryinfo = prevent duplicated calls
    prefetch_related_objects(search_queryset, 'obligatoryinfo')
    search_dict = ColIndSearchResultSerializer(search_queryset, many=True).data
    # Check for empty results list
    if not len(search_dict):
        return render(request, 'collection_report/collection_report.html', {'data_present': False})
    # convert search dictionary to Pandas data frame
    search_df = pd.DataFrame(search_dict, columns=search_dict[0].keys())
    # Get individuals ids
    ind_ids = search_df["ind_id"].to_list()
    # Get cities ids for distribution map
    # Here we want to replace NaN with None because NaN cause an error when converted to ::INTEGER in PG
    city_ids = search_df["city_id"].where(pd.notnull(search_df["city_id"]), None).to_list()

    # Get Classes counts
    class_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_class', 9)
    class_count_json = json.dumps(ColTopNCountsSerializer(class_count, many=True).data)

    class_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_class', 9999)
    class_count_tab = json.dumps(ColTopNCountsSerializer(class_count, many=True).data)

    # Get Orders count
    order_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_order', 9)
    order_count_json = json.dumps(ColTopNCountsSerializer(order_count, many=True).data)

    order_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_order', 9999)
    order_count_tab = json.dumps(ColTopNCountsSerializer(order_count, many=True).data)

    # Get family count
    family_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_family', 14)
    family_count_json = json.dumps(ColTopNCountsSerializer(family_count, many=True).data)

    family_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'col_family', 9999)
    family_count_tab = json.dumps(ColTopNCountsSerializer(family_count, many=True).data)

    # Get genera count
    genera_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'genera_name', 14)
    genera_count_json = json.dumps(ColTopNCountsSerializer(genera_count, many=True).data)

    genera_count = TaxonomicUnit.objects.count_top_n_for_tu(qd, 'genera_name', 9999)
    genera_count_tab = json.dumps(ColTopNCountsSerializer(genera_count, many=True).data)

    # IUCN categories
    # Here we use custom query in order to prevent situations when multiple cultivars of same species were counted
    # as separate RedList entries.
    raw_filter_sql, args_list = TaxonomicUnit.objects.full_search_sql(qd)
    iucn_cat = TaxonomicUnit.objects.raw(
        f"""
        SELECT DISTINCT 1 AS id, filter.category AS target_name,
               COUNT(filter.category) AS counts, ROW_NUMBER () OVER (ORDER BY COUNT(filter.category) DESC) AS rn 
        FROM (
            SELECT DISTINCT ON (res.genera_name, res.species_name, res.infra_name, res.author)
                res.category
                FROM ({raw_filter_sql.replace(';', '')}) AS res
            ) AS filter
        WHERE filter.category IS NOT NULL
        GROUP BY filter.category
        ORDER BY counts DESC;
        """,
        params=args_list
    )
    iucn_cat_json = json.dumps(ColTopNCountsSerializer(iucn_cat, many=True).data)

    iucn_cat_tab = json.dumps(ColTopNCountsSerializer(iucn_cat, many=True).data)

    # Cumulative TU counts by date
    # tu_timing = TaxonomicUnit.objects.cumulative_tu_income_outcome(qd)
    # tu_timing_json = json.dumps(ColTuIncomeOutcomeSerializer(tu_timing, many=True).data)
    tu_timing_json = cumulative_tu_income_outcome(search_df)

    # Spatial location of individuals
    ind_queryset = Individual.objects.filter(pk__in=ind_ids, spatial_pos__isnull=False)
    ind_spat_pos_geojoson = serialize('geojson', ind_queryset,
                                      geometry_field='spatial_pos',
                                      fields=('pk',))

    # Cultivation types
    cult_type_json = search_df.groupby('cult_type')["cult_type"].count().to_json()

    # Tu counts by department
    tu_counts_by_dep = search_df.drop_duplicates(subset=["pk", "name"]).\
        groupby("name")[['species_name', 'infra_name', 'cult_name']].count().to_json()

    # Individual Origin cities
    ind_orig_city = PopulatedPlaces.objects.great_circle_to_city(city_ids, 6268)  # Donetsk as destination
    ind_orig_city_json = json.dumps(CitiesGreatCircleSerializer(ind_orig_city, many=True).data)

    # Individuals Origin nature
    ind_orig_nature = Individual.objects.filter(id__in=ind_ids, origin='nature').only("id", "origin_loc")
    ind_orig_nature = serialize('geojson', ind_orig_nature,
                                geometry_field='origin_loc',
                                fields=('id',))

    # Prepare context
    context = {
        'data_present': True,
        'class_count_json': class_count_json,
        'class_count_tab': class_count_tab,
        'order_count_json': order_count_json,
        'order_count_tab': order_count_tab,
        'family_count_json': family_count_json,
        'family_count_tab': family_count_tab,
        'genera_count_json': genera_count_json,
        'genera_count_tab': genera_count_tab,
        'iucn_cat_json': iucn_cat_json,
        'iucn_cat_tab': iucn_cat_tab,
        'tu_timing_json': tu_timing_json,
        'ind_spat_pos_geojoson': ind_spat_pos_geojoson,
        'cult_type_json': cult_type_json,
        'tu_counts_by_dep': tu_counts_by_dep,
        'ind_orig_city_json': ind_orig_city_json,
        'ind_orig_nature': ind_orig_nature,
    }

    return render(request, 'collection_report/collection_report.html', context)


@login_required
def collection_income_outcome_report(request):
    if request.method != 'POST':
        # Get user department
        dep_id = request.user.department_id
        # Get current year
        cur_year = datetime.today().year
        # Prepare income / outcome table for current year and user department
        in_out_query = TaxonomicUnit.objects.\
            tu_income_outcome_by_year(from_year=cur_year, to_year=cur_year, dep_id=dep_id)
        in_out_data = ColTuIncomeOutcomeByYearSerializer(in_out_query, many=True).data
        in_out_json = json.dumps(in_out_data)

        # Prepare empty form
        form = CollectionInOutReportForm(initial={'dep_id': dep_id})
        # Return html response
        context = {'form': form,
                   'in_out_json': in_out_json,
                   'dep_name': request.user.department.name,
                   'from_year': cur_year,
                   'to_year': cur_year,
                   'data_len': len(in_out_data)}
        return render(request, 'collection_in_out_report/collection_in_out_report.html', context)
    else:
        # Check if form is valid
        form = CollectionInOutReportForm(request.POST)
        if form.is_valid():
            cc_data = form.cleaned_data
            # Prepare data for request
            from_year = cc_data.get('from_year')
            to_year = cc_data.get('to_year')
            dep_id = cc_data.get('dep_id').id
            in_out_query = TaxonomicUnit.objects. \
                tu_income_outcome_by_year(from_year=from_year, to_year=to_year, dep_id=dep_id)
            in_out_data = ColTuIncomeOutcomeByYearSerializer(in_out_query, many=True).data
            in_out_json = json.dumps(in_out_data)

            # Prepare context for response
            # Get dep name
            dep_name = Departments.objects.get(pk=dep_id).name
            context = {'form': form,
                       'in_out_json': in_out_json,
                       'dep_name': dep_name,
                       'from_year': from_year,
                       'to_year': to_year,
                       'data_len': len(in_out_data)}
            return render(request, 'collection_in_out_report/collection_in_out_report.html', context)
        else:
            return render(request, 'collection_in_out_report/collection_in_out_report.html', {'form': form})
