// Gulp.js configuration
var gulp = require('gulp'),
    // gulp specific
    gutil = require('gulp-util'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    // include
    include = require("gulp-include"),
    // Newer (process only changed files)
    newer = require("gulp-newer"),
    // js
    streamify = require('gulp-streamify'),
    uglify = require('gulp-uglify-es').default,
    optimizejs = require('gulp-optimize-js'),
    // css
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    cleancss = require('gulp-clean-css'),
    mqpack = require('css-mqpacker'), // optimize media query
    // source map
    sourcemaps = require('gulp-sourcemaps'),
    // run console tasks
    spawn = require('child_process').spawn;
    exec = require('child_process').exec;


var path = require('path'),
    _ = require('lodash'),
    async = require('async'),
    minimist = require('minimist');

// Load configuration file
var config = require('./gulp_config');

// Function for errors during argument parsing handling
function printArgumentsErrorAndExit() {
    gutil.log(gutil.colors.red('You must specify the app or'), gutil.colors.yellow('--all'));
    gutil.log(gutil.colors.red('Available apps:'));
    _.each(config.apps, function(item, i) {
        gutil.log(gutil.colors.yellow('  --app ' + i))
    });

    // break the task on error
    process.exit()
}


// Parse arguments
var argv = {parsed: false};
gulp.task('parseArgs', function(done) {
    // prevent multiple parsing when watching
    if (argv.parsed){
        done();
        return true;
    }

    // check the process arguments
    var options = minimist(process.argv);
    if (_.size(options) === 1) {
        printArgumentsErrorAndExit()
    }

    var apps = [];
    if (options.app && config.apps[options.app]) {
        apps.push(options.app)
    } else if (options.all) {
        apps = _.keys(config.apps)
    }

    if (!apps.length) printArgumentsErrorAndExit();
    argv.apps = apps;

    if (options.dev) argv.dev = true;

    // Check for hard reprocessing of all files
    if (options.reload) argv.reload = true;

    if (options.ip) argv.ip = options.ip;
    if (options.port) argv.port = options.port;

    argv.parsed = true;
    done();
});


// Gulp build task
gulp.task('build', gulp.series('parseArgs', function(cb) {
    var prefix = gutil.colors.yellow('  ->');
    async.each(argv.apps,
        function(app, cb) {
            gutil.log(prefix, 'Building', gutil.colors.cyan(app), '...');
            var conf = config.apps[app];
            if (!conf) return cb(new Error('No conf for app ' + app));

            /** Process js files */
            /** only id path_js is in conf (else do nothing)*/
            if ("path_js" in conf) {
                gulp.src(path.join(conf.path_js, conf.main_js))
                // Skip files older than destination (unprocessed)
                    .pipe(
                        gulpif(!argv.reload,
                            newer(path.join(conf.dest_js, conf.bundle_js))
                        )
                    )
                    // Start plumber to prevent stops of whole process on errors
                    .pipe(plumber())
                    // Process imports
                    .pipe(include()).on('error', gutil.log)
                    // Initiate source map if --dev
                    .pipe(gulpif(argv.dev, sourcemaps.init()))
                    // Uglify if not --dev
                    .pipe(
                        gulpif(!argv.dev,
                            streamify(uglify().on('error', gutil.log))
                        )
                    )
                    // optimize js
                    .pipe(optimizejs())
                    // Rename build
                    .pipe(rename(conf.bundle_js))
                    // Write source map to bundle file
                    .pipe(gulpif(argv.dev, sourcemaps.write()))
                    // Save bundle to destination
                    .pipe(gulp.dest(conf.dest_js))
                    .on('end', function () {
                        if (!("path_css" in conf) && !("fonts" in conf) && !("files" in conf)) {
                            cb()
                        }
                    });
            }

            /** Process css files */
            if ("path_css" in conf) {
                gulp.src(path.join(conf.path_css, conf.main_css))
                // Skip files older than destination (unprocessed)
                    .pipe(
                        gulpif(!argv.reload,
                            newer(path.join(conf.dest_css, conf.bundle_css))
                        )
                    )
                    // Start plumber to prevent stops of whole process on errors
                    .pipe(plumber())
                    // Process imports
                    .pipe(include()).on('error', gutil.log)
                    // Initiate source map if --dev
                    .pipe(gulpif(argv.dev, sourcemaps.init()))
                    // Compile sass files
                    .pipe(sass({
                        includePaths: ['node_modules/', 'bower_components/'],
                        outputStyle: 'compressed'
                    }).on('error', sass.logError))
                    // Add browser prefixes
                    .pipe(postcss([autoprefixer({browsers: ['>1%']}), mqpack]))
                    // Rename bundle
                    .pipe(rename(conf.bundle_css))
                    // Clean and minify css
                    .pipe(gulpif(!argv.dev, cleancss().on('error', gutil.log)))
                    // Write source map to bundle
                    .pipe(gulpif(argv.dev, sourcemaps.write()))
                    // Save bundle to destination
                    .pipe(gulp.dest(conf.dest_css))
                    .on('end', function () {
                        if (!("fonts" in conf) && !("files" in conf)) {
                            cb()
                        }
                    });
            }

            /** Copy fonts */
            if (conf.fonts){
                _.forEach(conf.fonts, function(f_path) {
                    gulp.src(f_path)
                    .pipe(gulpif(!argv.reload, newer(conf.fonts_path)))
                    .pipe(gulp.dest(conf.fonts_path))
                });
                if (!conf.files) {
                    cb()
                }
            }
            /** Copy asserts to media */
            if (conf.files){
                _.forEach(conf.files, function(f_path) {
                    gulp.src(f_path)
                    .pipe(gulpif(!argv.reload, newer(conf.files_path)))
                    .pipe(gulp.dest(conf.files_path));
                });
                cb()
            }
        },
        function(err) {
            cb(err)
        }
    )
}));

// Function for django development server startup
gulp.task('django-runserver', function () {
    if (!process.env['VIRTUAL_ENV']) {
        console.warn("WARNING: To run django you should activate virtual environment")
    } else {
        if (argv.ip && argv.port) {
            var ip = argv.ip;
            var port = argv.port;
        } else {
            var ip = '127.0.0.1';
            var port = 3000;
        }
        var djangoAddress = ip + ":" + port;
        var args = ["-w", "4", "--bind", djangoAddress, "--access-logfile", "-", "--reload", "django_dbg_is.wsgi"];
        var gunicorn = process.env['VIRTUAL_ENV'] + '/bin/gunicorn';
        var runserver = spawn(gunicorn, args, {stdio: "inherit"});
        runserver.on('close', function (code) {
            if (code !== 0) {
                console.error('Django runserver exited with error code: ' + code);
            } else {
                console.log('Django runserver exited normally.');
            }
        });
    }
});

// Function for development tilestach server startup
gulp.task('tilestach-runserver', function () {
    var args = ["--config=spatial/tielstach_config/base_config.cfg"];
    var runserver = spawn("tilestache-server.py", args, {stdio: "inherit"});
    runserver.on('close', function (code) {
        if (code !== 0) {
            console.error('Tilestache development server exited with error code: ' + code);
        } else {
            console.log('Tilestache development server exited normally.');
        }
    });
});

// Function for celery server startup
gulp.task('celery-worker', function () {
    var args = ["--app=django_dbg_is", "worker", "--loglevel=info"];
    var runserver = spawn("celery", args, {stdio: "inherit"});
    runserver.on('close', function (code) {
        if (code !== 0) {
            console.error('Celery worker exited with error code: ' + code);
        } else {
            console.log('Celery worker exited normally.');
        }
    });
});

gulp.task('celery-beat', function () {
    var args = ["--app=django_dbg_is", "beat", "--loglevel=info"];
    var runserver = spawn("celery", args, {stdio: "inherit"});
    runserver.on('close', function (code) {
        spawn("pkill", ["-f", "celery beat"], {stdio: "inherit"})
        if (code !== 0) {
            console.error('Celery-beat worker exited with error code: ' + code);
        } else {
            console.log('Celery-beat worker exited normally.');
        }
    });
});

// Task for watching
gulp.task('watch', function(done) {
    var targets = [];
    _.each(argv.apps, function(app) {
        var conf = config.apps[app];
        if (!conf) return;

        if (conf.watch) {
            if (_.isArray(conf.watch)) {
                targets = _.union(targets, conf.watch)
            } else {
                targets.push(conf.watch)
            }
        }
    });
    targets = _.uniq(targets);
    // start watching files
    var watcher = gulp.watch(targets, gulp.series('build'));
    watcher.on('change', function(path, stats) {
      gutil.log(gutil.colors.yellow('File'), gutil.colors.cyan(path), gutil.colors.yellow(' was changed'));
    });
    done();
});

//Task for initiation dev server and watch simultaneously
gulp.task('devserver',
    gulp.series('build',
        gulp.parallel(
            'watch',
            'django-runserver',
            'celery-worker',
            'celery-beat',
            'tilestach-runserver'
        )
    )
);

// Function for code size report
gulp.task('code_size', function (cb) {
    exec("cloc $(git ls-files -- . ':!:*.png' . ':!:*.json')",
        function (err, stdout, stderr) {
        console.log(stderr);
        console.log(stdout);
        if (err){
            cb(err);
        }
    });
    cb()
});

// Audit
gulp.task("test", function(cb){
    exec("coverage run --source='.'  manage.py test ; coverage html ; coverage report ;  cloc $(git ls-files -- . ':!:*.png' . ':!:*.json')",
        function (err, stdout, stderr) {
        console.log(stderr);
        console.log(stdout);
        if (err){
            cb(err);
        }
    });
    cb()
});


// Rewrite default prompts
gulp.task('default', function(done) {
    console.log();
    console.log('Available tasks:');
    console.log('  ', gutil.colors.cyan('build'), '  ', 'build applications');
    console.log('  ', gutil.colors.cyan('watch'), '  ', 'watch files and rebuild on changes');
    console.log('  ', gutil.colors.cyan('devserver'), '  ', 'start django developmental server and initiate watch');
    console.log('  ', gutil.colors.cyan('test'), '  ', 'code tests and coverage report');
    console.log('  ', gutil.colors.cyan('code_size'), '  ', 'code size report');

    console.log();
    console.log('Parameters:');
    console.log('  ', gutil.colors.cyan('--app [app name]'), '  ', 'build only a specified application');
    console.log('  ', gutil.colors.cyan('--all'), '  ', 'build all applications');
    console.log('  ', gutil.colors.cyan('--dev'), '  ', 'don\'t uglify a bundle');
    console.log('  ', gutil.colors.cyan('--reload'), '  ', 'force to process all files');

    console.log();
    done();
});