# Build assets
gulp build --all

# Collect static
/home/dbg_is/.virtualenvs/django_dbg_is/bin/python manage.py collectstatic --noinput

# Reload services
sudo systemctl restart dbg_is_main_gunicorn.service
sudo systemctl restart nginx.service
sudo systemctl restart celery
sudo systemctl restart celerybeat
sudo systemctl restart postgresql

vacuumdb --analyze -U postgres django_dbg_is