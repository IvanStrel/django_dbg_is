/** Configuration for gulp. It controls which parts for which apps gulp should process **/

module.exports = {
    apps: {
        /** Core =================================================================================================== */
        'leaflet_assets': {
            files: ["./node_modules/leaflet/dist/images/*.png"], // files to be moved into static
            files_path: "./core/static/leaflet_images/", // files destination path
            main_js: "leaflet.js",
            main_css: "leaflet.css",
            path_js: './node_modules/leaflet/dist/', // path to the main js and css files
            path_css: './node_modules/leaflet/dist/', // path to the main js and css files
            bundle_js: 'leaflet.min.js', // name of compiled js file
            bundle_css: 'leaflet.min.css', // name of compiled css file
            dest_js: './core/static/leaflet/',  // path to catalogue with static for js
            dest_css: './core/static/leaflet/',  // path to catalogue with static for css
            watch: ['./node_modules/leaflet/dist/*.js',
                './node_modules/leaflet/dist/*.css'] // paths to watch for changes in js and css
        },

        'leaflet_misc': {
            main_js: "leaflet_misc.js",
            path_js: './core/js/LeafletMisc/', // path to the main js and css files
            bundle_js: 'leaflet_misc.min.js', // name of compiled js file
            dest_js: './core/static/leaflet_misc/',  // path to catalogue with static for js
            watch: ['./core/js/LeafletMisc/*.js']
        },

        'leaflet_heatmap': {
            files: ["./node_modules/heatmap.js/build/heatmap.min.js"],
            files_path: "./core/static/leaflet_plugins/",
            main_js: "leaflet-heatmap.js",
            path_js: './node_modules/heatmap.js/plugins/leaflet-heatmap/', // path to the main js and css files
            bundle_js: 'leaflet_heatmap.min.js', // name of compiled js file
            dest_js: './core/static/leaflet_plugins/'  // path to catalogue with static for js
        },

        'handson_table': {
            files: ["./node_modules/handsontable/dist/languages/*.min.js"], // files to be moved into static
            files_path: "./core/static/handsontable/", // files destination path
            main_js: "handsontable.full.min.js",
            main_css: "handsontable.full.min.css",
            path_js: './node_modules/handsontable/dist/', // path to the main js and css files
            path_css: './node_modules/handsontable/dist/', // path to the main js and css files
            bundle_js: 'handsontable.min.js', // name of compiled js file
            bundle_css: 'handsontable.min.css', // name of compiled css file
            dest_js: './core/static/handsontable/',  // path to catalogue with static for js
            dest_css: './core/static/handsontable/',  // path to catalogue with static for css
            watch: ['./node_modules/handsontable/dist/*.js',
                    './node_modules/handsontable/dist/*.css'] // paths to watch for changes in js and css
        },

        'dropify': {
            main_js: "dropify.js",
            main_css: "dropify.css",
            path_js: './node_modules/dropify/dist/js/', // path to the main js and css files
            path_css: './node_modules/dropify/dist/css/', // path to the main js and css files
            bundle_js: 'dropify.min.js', // name of compiled js file
            bundle_css: 'dropify.min.css', // name of compiled css file
            dest_js: './core/static/dropify/',  // path to catalogue with static for js
            dest_css: './core/static/dropify/',  // path to catalogue with static for css
            watch: ['./node_modules/dropify/dist/js/*.js',
                './node_modules/dropify/dist/css/*.css'] // paths to watch for changes in js and css

        },

        'D3': {
            main_js: "d3.min.js",
            path_js: './node_modules/d3/dist/', // path to the main js and css files
            bundle_js: 'd3.min.js', // name of compiled js file
            dest_js: './core/static/d3/',  // path to catalogue with static for js
            watch: ['./node_modules/d3/dist/*.js']
        },

        'Particles.js': {
            main_js: "particles.js",
            path_js: './node_modules/particles.js/', // path to the main js and css files
            bundle_js: 'particles.min.js', // name of compiled js file
            dest_js: './core/static/particles.js/',  // path to catalogue with static for js
            watch: ['./node_modules/particles.js/*.js']
        },

        'PhotoSwipe': {
            files: ["./node_modules/photoswipe/dist/default-skin/default-skin.png",
                    "./node_modules/photoswipe/dist/default-skin/default-skin.svg",
                    "./node_modules/photoswipe/dist/default-skin/preloader.gif"], // files to be moved into static
            files_path: "./core/static/photoswipe/", // files destination path
            main_js: "main.js",
            main_css: "main.scss",
            path_js: './core/js/PhotoSwipe/', // path to the main js and css files
            path_css: './core/css/PhotoSwipe/', // path to the main js and css files
            bundle_js: 'photoswipe.min.js', // name of compiled js file
            bundle_css: 'photoswipe.min.css', // name of compiled css file
            dest_js: './core/static/photoswipe/',  // path to catalogue with static for js
            dest_css: './core/static/photoswipe/',  // path to catalogue with static for css
            watch: ['./core/js/PhotoSwipe/*.js',
                    './core/scss/PhotoSwipe/*.scss'] // paths to watch for changes in js and css
        },

        'chart.js': {
            files: ["./node_modules/chart.js/dist/Chart.bundle.min.js"], // files to be moved into static
            files_path: "./core/static/chartjs/", // files destination path
            main_css: "Chart.min.css",
            path_css: './node_modules/chart.js/dist/', // path to the main js and css files
            bundle_css: 'Chart.min.css', // name of compiled css file
            dest_css: './core/static/chartjs/',  // path to catalogue with static for css
        },

        'Fonts': {
            fonts: ["./node_modules/material-design-icons/iconfont/MaterialIcons-Regular.*",
                    "./node_modules/dropify/dist/fonts/dropify.*"], // fonts files
            fonts_path: "./core/static/fonts/" // fonts destination
        },

        'Base_style': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './core/js/BaseStyle/', // path to the main js and css files
            path_css: './core/css/BaseStyle/', // path to the main js and css files
            bundle_js: 'common.min.js', // name of compiled js file
            bundle_css: 'common.min.css', // name of compiled css file
            dest_js: './core/static/base_style/js_bundles/',  // path to catalogue with static for js
            dest_css: './core/static/base_style/css_bundles/',  // path to catalogue with static for css
            watch: ['./core/js/BaseStyle/*.js',
                './core/css/BaseStyle/*.scss'] // paths to watch for changes in js and css
        },

        /** HomePage */
        'Home_page': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './core/js/HomePage/', // path to the main js and css files
            path_css: './core/css/HomePage/', // path to the main js and css files
            bundle_js: 'home_page.min.js', // name of compiled js file
            bundle_css: 'home_page.min.css', // name of compiled css file
            dest_js: './core/static/home_page/js_bundles/',  // path to catalogue with static for js
            dest_css: './core/static/home_page/css_bundles/',  // path to catalogue with static for css
            watch: ['./core/js/HomePage/*.js',
                './core/css/HomePage/*.scss'] // paths to watch for changes in js and css
        },

        // 'css-tree': {
        //     main_css: 'css_tree.scss', // name of main css file
        //     path_css: './core/css/miscellaneous/', // path to the main js and css files
        //     bundle_css: 'css_tree.min.css', // name of compiled css file
        //     dest_css: './core/static/miscellaneous/css_bundles/',  // path to catalogue with static for css
        //     watch: ['./core/css/miscellaneous/*.scss'] // paths to watch for changes in js and css
        // },

        /** Collection ============================================================================================== */
        /** Collection Home */
        'Collection_Home': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/CollectionHome/', // path to the main js and css files
            path_css: './collection/css/CollectionHome/', // path to the main js and css files
            bundle_js: 'collection_home.min.js', // name of compiled js file
            bundle_css: 'collection_home.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/CollectionHome/*.js',
                    './collection/css/CollectionHome/*.scss'] // paths to watch for changes in js and css
        },

        /** New TU forms */
        'Collection_NewTuForm1': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuForm1/', // path to the main js and css files
            path_css: './collection/css/NewTuForm1/', // path to the main js and css files
            bundle_js: 'new_tu_form_1.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_1.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuForm1/*.js',
                './collection/css/NewTuForm1/*.scss'] // paths to watch for changes in js and css
        },
        'Collection_NewTuForm2': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuForm2/', // path to the main js and css files
            path_css: './collection/css/NewTuForm2/', // path to the main js and css files
            bundle_js: 'new_tu_form_2.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_2.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuForm2/*.js',
                './collection/css/NewTuForm2/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_NewTuForm3': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuForm3/', // path to the main js and css files
            path_css: './collection/css/NewTuForm3/', // path to the main js and css files
            bundle_js: 'new_tu_form_3.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_3.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuForm3/*.js',
                './collection/css/NewTuForm3/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_NewTuForm4': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuForm4/', // path to the main js and css files
            path_css: './collection/css/NewTuForm4/', // path to the main js and css files
            bundle_js: 'new_tu_form_4.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_4.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuForm4/*.js',
                './collection/css/NewTuForm4/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_NewTuForm5': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuForm5/', // path to the main js and css files
            path_css: './collection/css/NewTuForm5/', // path to the main js and css files
            bundle_js: 'new_tu_form_5.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_5.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuForm5/*.js',
                './collection/css/NewTuForm5/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_NewTuFormConf': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewTuFormConf/', // path to the main js and css files
            path_css: './collection/css/NewTuFormConf/', // path to the main js and css files
            bundle_js: 'new_tu_form_conf.min.js', // name of compiled js file
            bundle_css: 'new_tu_form_conf.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewTuFormConf/*.js',
                './collection/css/NewTuFormConf/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_NewTuFormAcc': { // name of app (i.e. name of django part for which it will be used)
            main_css: 'main.scss', // name of main css file
            path_css: './collection/css/NewTuFormAcc/', // path to the main js and css files
            bundle_css: 'new_tu_form_acc.min.css', // name of compiled css file
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/css/NewTuFormAcc/**/*.scss',
                './collection/css/NewTuFormAcc/*.scss'] // paths to watch for changes in js and css
        },

        /** INDIVIDUALS */
        'Collection_NewIndividual': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NewIndividual/', // path to the main js and css files
            path_css: './collection/css/NewIndividual/', // path to the main js and css files
            bundle_js: 'new_individual.min.js', // name of compiled js file
            bundle_css: 'new_individual.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NewIndividual/*.js',
                './collection/css/NewIndividual/*.scss'] // paths to watch for changes in js and css
        },
        'IndividualProfile': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/IndividualProfile/', // path to the main js and css files
            path_css: './collection/css/IndividualProfile/', // path to the main js and css files
            bundle_js: 'individual_profile.min.js', // name of compiled js file
            bundle_css: 'individual_profile.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/IndividualProfile/*.js',
                './collection/css/IndividualProfile/*.scss'] // paths to watch for changes in js and css
        },

        'EditIndividualProfile': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/EditIndividualProfile/', // path to the main js and css files
            path_css: './collection/css/EditIndividualProfile/', // path to the main js and css files
            bundle_js: 'edit_individual_profile.min.js', // name of compiled js file
            bundle_css: 'edit_individual_profile.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/EditIndividualProfile/*.js',
                './collection/css/EditIndividualProfile/*.scss'] // paths to watch for changes in js and css
        },

        'TuProfile': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/TuProfile/', // path to the main js and css files
            path_css: './collection/css/TuProfile/', // path to the main js and css files
            bundle_js: 'tu_profile.min.js', // name of compiled js file
            bundle_css: 'tu_profile.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/TuProfile/*.js',
                './collection/css/TuProfile/*.scss'] // paths to watch for changes in js and css
        },

        /** VERNACULARS */
        'Vernaculars': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/Vernacular/', // path to the main js and css files
            path_css: './collection/css/Vernacular/', // path to the main js and css files
            bundle_js: 'vernacular_form.min.js', // name of compiled js file
            bundle_css: 'vernacular_form.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/Vernacular/*.js',
                './collection/css/Vernacular/*.scss'] // paths to watch for changes in js and css
        },

        /** NON OBLIGATORY INFO */
        'Non_obligatory_info': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './collection/js/NonObligInfo/', // path to the main js and css files
            path_css: './collection/css/NonObligInfo/', // path to the main js and css files
            bundle_js: 'non_oblig_info_form.min.js', // name of compiled js file
            bundle_css: 'non_oblig_info_form.min.css', // name of compiled css file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/js/NonObligInfo/*.js',
                    './collection/css/NonObligInfo/*.scss'] // paths to watch for changes in js and css
        },

        /** Collection Individual labels */
        'Collection_rect_dark_label': { // name of app (i.e. name of django part for which it will be used)
            main_css: 'rect_dark_label.scss', // name of main css file
            path_css: './collection/css/PdfLabels/', // path to the main js and css files
            bundle_css: 'rect_dark_label.min.css', // name of compiled css file
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/css/PdfLabels/*.scss'] // paths to watch for changes in js and css
        },

        'Collection_narrow_label': { // name of app (i.e. name of django part for which it will be used)
            main_css: 'narrow_label.scss', // name of main css file
            path_css: './collection/css/PdfLabels/', // path to the main js and css files
            bundle_css: 'narrow_label.min.css', // name of compiled css file
            dest_css: './collection/static/collection/css_bundles/',  // path to catalogue with static for css
            watch: ['./collection/css/PdfLabels/*.scss'] // paths to watch for changes in js and css
        },

        /** Images */
        'Collection_image_forms': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'image_forms.js', // name of main js file (it will contain imports of asserts and another parts)
            path_js: './collection/js/Image/', // path to the main js and css files
            bundle_js: 'image_forms.min.js', // name of compiled js file
            dest_js: './collection/static/collection/js_bundles/',  // path to catalogue with static for js
            watch: ['./collection/js/Image/*.js'] // paths to watch for changes in js and css
        },

        /** USERS AND ORGANIZATION ================================================================================= */
        'users_and_org': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './users_and_organization/js/common/', // path to the main js and css files
            path_css: './users_and_organization/css/common/', // path to the main js and css files
            bundle_js: 'common.min.js', // name of compiled js file
            bundle_css: 'common.min.css', // name of compiled css file
            dest_js: './users_and_organization/static/users_and_organization/js_bundles/',  // path to catalogue with static for js
            dest_css: './users_and_organization/static/users_and_organization/css_bundles/',  // path to catalogue with static for css
            watch: ['./users_and_organization/js/common/*.js',
                './users_and_organization/css/common/*.scss'] // paths to watch for changes in js and css
        },

        'users_and_org_sign_up': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'main.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'main.scss', // name of main css file
            path_js: './users_and_organization/js/sign_up/', // path to the main js and css files
            path_css: './users_and_organization/css/sign_up/', // path to the main js and css files
            bundle_js: 'sign_up.min.js', // name of compiled js file
            bundle_css: 'sign_up.min.css', // name of compiled css file
            dest_js: './users_and_organization/static/users_and_organization/js_bundles/',  // path to catalogue with static for js
            dest_css: './users_and_organization/static/users_and_organization/css_bundles/',  // path to catalogue with static for css
            watch: ['./users_and_organization/js/sign_up/*.js',
                './users_and_organization/css/sign_up/*.scss'] // paths to watch for changes in js and css
        },

        'user_profile': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'user_profile.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'user_profile.scss', // name of main css file
            path_js: './users_and_organization/js/user_profile/', // path to the main js and css files
            path_css: './users_and_organization/css/user_profile/', // path to the main js and css files
            bundle_js: 'user_profile.min.js', // name of compiled js file
            bundle_css: 'user_profile.min.css', // name of compiled css file
            dest_js: './users_and_organization/static/users_and_organization/js_bundles/',  // path to catalogue with static for js
            dest_css: './users_and_organization/static/users_and_organization/css_bundles/',  // path to catalogue with static for css
            watch: ['./users_and_organization/js/user_profile/*.js',
                './users_and_organization/css/user_profile/*.scss'] // paths to watch for changes in js and css
        },

        /** SEARCH ================================================================================================= */
        'collection_fast_search': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'fast_search.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'fast_search.scss', // name of main css file
            path_js: './search/js/fast_search/', // path to the main js and css files
            path_css: './search/css/fast_search/', // path to the main js and css files
            bundle_js: 'fast_search.min.js', // name of compiled js file
            bundle_css: 'fast_search.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/fast_search/*.js',
                    './search/css/fast_search/*.scss'] // paths to watch for changes in js and css
        },

        'collection_spatial_search': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'collection_spatial_search.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'collection_spatial_search.scss', // name of main css file
            path_js: './search/js/collection_spatial_search/', // path to the main js and css files
            path_css: './search/css/collection_spatial_search/', // path to the main js and css files
            bundle_js: 'collection_spatial_search.min.js', // name of compiled js file
            bundle_css: 'collection_spatial_search.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/collection_spatial_search/*.js',
                    './search/css/collection_spatial_search/*.scss'] // paths to watch for changes in js and css
        },

        'collection_search_results': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'collection_search_results.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'collection_search_results.scss', // name of main css file
            path_js: './search/js/collection_search_results/', // path to the main js and css files
            path_css: './search/css/collection_search_results/', // path to the main js and css files
            bundle_js: 'collection_search_results.min.js', // name of compiled js file
            bundle_css: 'collection_search_results.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/collection_search_results/*.js',
                    './search/css/collection_search_results/*.scss'] // paths to watch for changes in js and css
        },

        'collection_full_search': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'collection_full_search.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'collection_full_search.scss', // name of main css file
            path_js: './search/js/collection_full_search/', // path to the main js and css files
            path_css: './search/css/collection_full_search/', // path to the main js and css files
            bundle_js: 'collection_full_search.min.js', // name of compiled js file
            bundle_css: 'collection_full_search.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/collection_full_search/*.js',
                    './search/css/collection_full_search/*.scss'] // paths to watch for changes in js and css
        },

        'collection_report': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'collection_report.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'collection_report.scss', // name of main css file
            path_js: './search/js/collection_report/', // path to the main js and css files
            path_css: './search/css/collection_report/', // path to the main js and css files
            bundle_js: 'collection_report.min.js', // name of compiled js file
            bundle_css: 'collection_report.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/collection_report/*.js',
                    './search/css/collection_report/*.scss'] // paths to watch for changes in js and css
        },

        'collection_in_out_report': { // name of app (i.e. name of django part for which it will be used)
            main_js: 'collection_in_out_report.js', // name of main js file (it will contain imports of asserts and another parts)
            main_css: 'collection_in_out_report.scss', // name of main css file
            path_js: './search/js/collection_in_out_report/', // path to the main js and css files
            path_css: './search/css/collection_in_out_report/', // path to the main js and css files
            bundle_js: 'collection_in_out_report.min.js', // name of compiled js file
            bundle_css: 'collection_in_out_report.min.css', // name of compiled css file
            dest_js: './search/static/search/js_bundles/',  // path to catalogue with static for js
            dest_css: './search/static/search/css_bundles/',  // path to catalogue with static for css
            watch: ['./search/js/collection_in_out_report/*.js',
                    './search/css/collection_in_out_report/*.scss'] // paths to watch for changes in js and css
        },

        /** SPATIAL ================================================================================================= */
        'world_atlas and topojson': {
            files: ["./node_modules/world-atlas/countries-110m*"], // files to be moved into static
            files_path: "./spatial/static/world_atlas/", // files destination path
            main_js: 'topojson-client.js', // name of main js file (it will contain imports of asserts and another parts)
            path_js: './node_modules/topojson-client/dist/', // path to the main js and css files
            bundle_js: 'topojson-client.min.js', // name of compiled js file
            dest_js: './spatial/static/world_atlas/'  // path to catalogue with static for js
        }
    }
};