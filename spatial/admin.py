from django.contrib.gis import admin

# Models registration
from .models import PolygonsLevel1
from .models import PolygonsLevel2
from .models import PolygonsLevel3
from .models import Paths
from .models import SovCountries
from .models import AdminRegions

admin.site.register(PolygonsLevel1)
admin.site.register(PolygonsLevel2)
admin.site.register(PolygonsLevel3)
admin.site.register(Paths)
admin.site.register(SovCountries)
admin.site.register(AdminRegions)
