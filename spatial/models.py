from django.contrib.gis.db import models


class PolygonsLevel1(models.Model):
    type = models.CharField(max_length=50, null=False)
    subtype = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=100, null=True)
    poly = models.MultiPolygonField(spatial_index=True)


class PolygonsLevel2(models.Model):
    type = models.CharField(max_length=50, null=False)
    subtype = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=100, null=True)
    poly = models.MultiPolygonField(spatial_index=True)
    parent = models.ForeignKey(PolygonsLevel1, on_delete=models.CASCADE)


class PolygonsLevel3(models.Model):
    type = models.CharField(max_length=50, null=False)
    subtype = models.CharField(max_length=50, null=False)
    name = models.CharField(max_length=100, null=True)
    poly = models.MultiPolygonField(spatial_index=True)
    parent = models.ForeignKey(PolygonsLevel2, on_delete=models.CASCADE)


class Paths(models.Model):
    road_level = models.IntegerField()
    road = models.MultiLineStringField(spatial_index=True)


class SovCountries(models.Model):
    sov_a3 = models.CharField(max_length=3, null=False)
    name = models.CharField(max_length=100, null=False)
    iso_a2 = models.CharField(max_length=2, null=True)
    iso_a3 = models.CharField(max_length=3, null=True)
    continent = models.CharField(max_length=20, null=True)
    subregion = models.CharField(max_length=50, null=True)
    region_wb = models.CharField(max_length=50, null=True)
    name_ru = models.CharField(max_length=100, null=True)
    economy = models.CharField(max_length=50, null=True)
    population = models.IntegerField(null=True)
    poly = models.MultiPolygonField(spatial_index=True)

    def __str__(self):
        return f'{self.name_ru if self.name_ru else self.name}'


class AdminRegions(models.Model):
    sov_id = models.ForeignKey(SovCountries, on_delete=models.CASCADE)
    sov_a3 = models.CharField(max_length=3, null=True)
    name = models.CharField(max_length=100, null=True)
    name_ru = models.CharField(max_length=100, null=True)
    poly = models.MultiPolygonField(spatial_index=True)

    def __str__(self):
        return f'{self.name_ru if self.name_ru else self.name}'


class PopulatedPlacesManager(models.Manager):
    def great_circle_to_city(self, sources_ids, dest_id):
        """
        Comute shortest path between required cities and destination.
        Source cities are provided with list of ids.
        Destination is provided as single id of destination city
        :param sources_ids: list of cities ids
        :param dest_id: id of destination city
        :return: RawQueryset
        """
        raw_sql = """
        SELECT path.id, path.name, path.path, count.count
        FROM
             (
              SELECT city.id, COALESCE(name_ru, name) as name,
                     ST_Segmentize(
                         (ST_makeline(city.point,
                             (SELECT point FROM spatial_populatedplaces WHERE id = %(dest_id)s))::geography)
                         ,50000)::geometry  AS path
              FROM spatial_populatedplaces AS city
              WHERE city.id != %(dest_id)s AND city.id IN %(sources_ids)s
              GROUP BY city.id
              ) AS path
              LEFT JOIN
                 (SELECT id, COUNT(id) AS count
                     FROM UNNEST(%(sources_ids_arr)s::INTEGER[]) AS id
                     GROUP BY id) AS count
              ON path.id = count.id;
        """
        # Prepare parameters for SQL
        args_list = {'sources_ids': tuple(sources_ids), 'sources_ids_arr': sources_ids, 'dest_id': dest_id}
        return self.get_queryset().raw(raw_sql, params=args_list, )


class PopulatedPlaces(models.Model):
    sov_id = models.ForeignKey(SovCountries, on_delete=models.CASCADE)
    admin_id = models.ForeignKey(AdminRegions, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=100, null=True)
    name_ru = models.CharField(max_length=100, null=True)
    point = models.PointField(spatial_index=True)

    # Custom manager
    objects = PopulatedPlacesManager()

    def __str__(self):
        return f'{self.name_ru if self.name_ru else self.name}'
