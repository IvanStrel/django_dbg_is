import os
import getpass
from django.apps import AppConfig
from django_dbg_is import settings

from django.template.loader import render_to_string


class SpatialConfig(AppConfig):
    name = 'spatial'

    def ready(self):
        """
        Process mapnik config file = populate password field
        :return:
        """
        # Run this only if user is not `celery`
        if getpass.getuser() != 'celery':
            # Prepare DB context
            context = {'db_name': settings.DATABASES.get('default').get('NAME'),
                       'db_user': settings.DATABASES.get('default').get('USER'),
                       'db_password': settings.DATABASES.get('default').get('PASSWORD')
                       }
            # Render mapnik style template to string
            mapnik_style = render_to_string('base_mapnik_style_template.xml', context)
            # Save result to file
            with open('./spatial/tielstach_config/mapnik_styles/base_mapnik_style.xml', 'w') as static_file:
                static_file.write(mapnik_style)
