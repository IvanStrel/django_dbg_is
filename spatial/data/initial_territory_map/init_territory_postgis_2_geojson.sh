# Run this to dump spatial data into GeoJSON files

stty -echo
printf "Password for django_dbg_is db user: "
read PASSWORD
stty echo

ogr2ogr -f "GeoJSON" level_1.geojson PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD"  \
-nln "spatial_polygonslevel1" -sql "SELECT id, type, subtype, name, poly FROM spatial_polygonslevel1"

ogr2ogr -f "GeoJSON" level_2.geojson PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD"  \
-nln "spatial_polygonslevel2" \
-sql "SELECT id, type, subtype, name, poly, parent_id AS parent FROM spatial_polygonslevel2"

ogr2ogr -f "GeoJSON" level_3.geojson PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD"  \
-nln "spatial_polygonslevel3" \
-sql "SELECT id, type, subtype, name, poly, parent_id AS parent FROM spatial_polygonslevel3"

ogr2ogr -f "GeoJSON" paths.geojson PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD"  \
-nln "spatial_paths" \
-sql "SELECT id, road_level, road FROM spatial_paths"