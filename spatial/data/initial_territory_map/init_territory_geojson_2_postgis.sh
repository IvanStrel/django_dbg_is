#!/bin/sh
# Run this to populate DB with spatial data from GeoJSON files Natural Earth data

stty -echo
printf "Password for django_dbg_is db user: "
read PASSWORD
stty echo

conf_string_dbg="host=localhost user=django_dbg_is port=5432 password=$PASSWORD dbname=django_dbg_is"

# It is wise to truncate tables before data input. Possible SQL
TRUNCATE spatial_polygonslevel1 RESTART IDENTITY CASCADE;
TRUNCATE spatial_polygonslevel2 RESTART IDENTITY CASCADE;
TRUNCATE spatial_polygonslevel3 RESTART IDENTITY CASCADE;
TRUNCATE spatial_paths RESTART IDENTITY CASCADE;

ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" "level_1.geojson" \
-nln "spatial_polygonslevel1" -sql 'SELECT type, subtype, name FROM spatial_polygonslevel1'

# Import level2
# Currently `parent_id` is empty, therefore we have to provide some initial
ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" "level_2.geojson" \
-nln "spatial_polygonslevel2" -sql 'SELECT type, subtype, 1 AS parent_id, name FROM spatial_polygonslevel2'
# Assign new parents by spatial query (center of level2 belonging to level1 polygons)
psql "$conf_string_dbg" -c '
UPDATE spatial_polygonslevel2
SET parent_id=subquery.id_1
FROM (
  SELECT
    spatial_polygonslevel1.id AS id_1, spatial_polygonslevel2.id AS id_2
  FROM
    spatial_polygonslevel2, spatial_polygonslevel1
  WHERE
    ST_Contains(spatial_polygonslevel1.poly, ST_Centroid(spatial_polygonslevel2.poly))
  ) AS subquery
WHERE spatial_polygonslevel2.id=subquery.id_2;
'

# Import level2
# Currently `parent_id` is empty, therefore we have to provide some initial
ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" "level_3.geojson" \
-nln "spatial_polygonslevel3" -sql "SELECT type, subtype, 1 AS parent_id, name FROM spatial_polygonslevel3"
# Assign new parents by spatial query (center of level2 belonging to level1 polygons)
psql "$conf_string_dbg" -c '
UPDATE spatial_polygonslevel3
SET parent_id=subquery.id_2
FROM (
  SELECT
    spatial_polygonslevel2.id AS id_2, spatial_polygonslevel3.id AS id_3
  FROM
    spatial_polygonslevel3, spatial_polygonslevel2
  WHERE
    ST_Contains(spatial_polygonslevel2.poly, ST_Centroid(spatial_polygonslevel3.poly))
  ) AS subquery
WHERE spatial_polygonslevel3.id=subquery.id_3;
'

ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" "paths.geojson" \
-nln "spatial_paths" -sql "SELECT road_level FROM spatial_paths"
