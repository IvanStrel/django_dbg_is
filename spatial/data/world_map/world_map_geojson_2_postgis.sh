#!/bin/sh
# Run this to populate DB with spatial data from GeoJSON files Natural Earth data

stty -echo
printf "Password for django_dbg_is db user: "
read PASSWORD
stty echo

conf_string_dbg="host=localhost user=django_dbg_is port=5432 password=$PASSWORD dbname=django_dbg_is"

# It is wise to truncate tables before data input. Possible SQL
TRUNCATE spatial_sovcountries RESTART IDENTITY CASCADE;
TRUNCATE spatial_admin_regions RESTART IDENTITY CASCADE;
TRUNCATE spatial_populatedplaces RESTART IDENTITY CASCADE;

# Import Sovereign Countries
ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" \
"ne_10m_admin_0_sovereignty.geojson" \
-nln "spatial_sovcountries" -sql 'SELECT SOV_A3 AS sov_a3, NAME AS name, ISO_A2 as iso_a2,
                                         ISO_A3 AS iso_a3, CONTINENT AS continent, SUBREGION AS subregion,
                                         REGION_WB AS region_wb, NAME_RU AS name_ru, ECONOMY AS economy,
                                         POP_EST AS population
                                  FROM ne_10m_admin_0_sovereignty'
# There is a problem with Israel SOV_A3 it is IS1 and should be ISR
psql "$conf_string_dbg" -c "
UPDATE spatial_sovcountries
SET sov_a3='ISR'
WHERE sov_a3='IS1'
"
# There is a problem with Kazahstan as it called Baikonur kosmodrome
psql "$conf_string_dbg" -c "
UPDATE spatial_sovcountries
SET name_ru='Казахстан'
WHERE name_ru='Байконур'
"

# Import Admin level 1
# Currently `sov_id` is empty, therefore we have to provide some initial
ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" \
"ne_10m_admin_1.geojson" \
-nln "spatial_adminregions" \
-sql 'SELECT 1 AS sov_id_id, name, name_ru, sov_a3 FROM ne_10m_admin_1'

# Assign new sov_id by spatial query (basein on sov_a3 field)
psql "$conf_string_dbg" -c '
UPDATE spatial_adminregions
SET sov_id_id=subquery.sov_id
FROM (
  SELECT
    sov.id AS sov_id, admin.id AS admin_id
  FROM
    spatial_adminregions AS admin
  LEFT JOIN
    spatial_sovcountries as sov ON admin.sov_a3 = sov.sov_a3
  ) AS subquery
WHERE spatial_adminregions.id=subquery.admin_id;
'

# Import Populated places
# Currently `sov_id_id` and `admin_id_id` are empty, therefore we have to provide some initial
ogr2ogr -t_srs EPSG:4326 -f "PostgreSQL" PG:"dbname=django_dbg_is user=django_dbg_is password=$PASSWORD" \
"ne_10m_populated_places.geojson" \
-nln "spatial_populatedplaces" \
-sql 'SELECT 1 AS sov_id_id, name, name_ru FROM ne_10m_populated_places'
# Assign new sov_id by spatial query (position of pop place belonging to sovereign_countries polygons)
psql "$conf_string_dbg" -c '
UPDATE spatial_populatedplaces
SET sov_id_id=subquery.sov_id
FROM (
  SELECT
    spatial_sovcountries.id AS sov_id, spatial_populatedplaces.id AS pop_id
  FROM
    spatial_populatedplaces, spatial_sovcountries
  WHERE
    ST_Contains(spatial_sovcountries.poly, spatial_populatedplaces.point)
  ) AS subquery
WHERE spatial_populatedplaces.id=subquery.pop_id;
'

# As there are some mismatches in sov_a3 make spatial lookup for populated places with remained sov_id = 1
psql "$conf_string_dbg" -c '
UPDATE spatial_populatedplaces
SET sov_id_id=subquery.sov_id
FROM (
  SELECT
    spatial_sovcountries.id AS sov_id, spatial_populatedplaces.id AS pop_id
  FROM
    spatial_populatedplaces, spatial_sovcountries
  WHERE
    ST_Contains(spatial_sovcountries.poly, spatial_populatedplaces.point)
  ) AS subquery
WHERE spatial_populatedplaces.id=subquery.pop_id AND spatial_populatedplaces.sov_id_id=1;
'


# Assign new admin_id by spatial query (position of pop place belonging to admin_regions polygons)
psql "$conf_string_dbg" -c '
UPDATE spatial_populatedplaces
SET admin_id_id=subquery.admin_id
FROM (
  SELECT
    spatial_adminregions.id AS admin_id, spatial_populatedplaces.id AS pop_id
  FROM
    spatial_populatedplaces, spatial_adminregions
  WHERE
    ST_Contains(spatial_adminregions.poly, spatial_populatedplaces.point)
  ) AS subquery
WHERE spatial_populatedplaces.id=subquery.pop_id;
'
