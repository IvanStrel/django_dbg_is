from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers

from django.contrib.gis.geos import GEOSGeometry

from .models import PopulatedPlaces


class CitiesGreatCircleSerializer(gis_serializers.GeoFeatureModelSerializer):
    path = gis_serializers.GeometrySerializerMethodField(allow_null=True)
    count = serializers.IntegerField(required=True, allow_null=True)

    def get_path(self, obj):
        """
        A custom method for geometry field retrieval
        As `path` is a custom field, absent form initial model, it return `str` instead of `Line`
        therefore we need to cast it to Geometry type manually
        :param obj: RawQueryset raw
        :return: GEOSGeometry object
        """
        return GEOSGeometry(obj.path)

    class Meta:
        model = PopulatedPlaces
        geo_field = "path"

        fields = ('id', 'name', 'count', 'path')
