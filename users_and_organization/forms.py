from allauth.account.forms import SignupForm
from mptt.forms import TreeNodeChoiceField
from django import forms

from users_and_organization.models import Departments
from users_and_organization.models import User


class MyCustomSignupForm(SignupForm):
    first_name = forms.CharField(max_length=100, label="Имя", required=True)
    second_name = forms.CharField(max_length=100, label="Отчество", required=True)
    last_name = forms.CharField(max_length=100, label="Фамилия", required=True)
    is_dbg_staff = forms.BooleanField(initial=False, label="Сотрудник ДБС?", required=False)
    department_id = TreeNodeChoiceField(queryset=Departments.objects.all(), label="Подразделение", required=False)
    role = forms.CharField(max_length=300, label="Должность", required=False)
    acad_degree = forms.ChoiceField(choices=User.acad_degree_choices, initial=User.none, required=True,
                                    label="Ученая степнь")
    acad_title = forms.ChoiceField(choices=User.acad_title_choices, initial=User.none, required=True,
                                   label="Ученое звание")

    def clean(self):
        cleaned_data = super(SignupForm, self).clean()

        # check for duplicates in combination of first, second and last names
        names_comb = User.objects.filter(
            first_name=cleaned_data.get('first_name'),
            second_name=cleaned_data.get('second_name'),
            last_name=cleaned_data.get('last_name')
        ).exists()
        if names_comb:
            raise forms.ValidationError(
                "ОШИБКА: пользователь с такой комбинацией имени, отчества и фамилии уже существует"
            )

        # Check for DBGStaff model data if 'is_dbg_staff' is True
        # all values required for dbg staff member
        if cleaned_data.get("is_dbg_staff"):
            # Prepare message
            msg = "Поле обязательно для сотрудников"
            # Check all fields
            if not cleaned_data.get("department_id"):
                self.add_error('department_id', msg)
            if not cleaned_data.get("role"):
                self.add_error('role', msg)
            if not cleaned_data.get("acad_degree"):
                self.add_error('acad_degree', msg)
            if not cleaned_data.get("acad_title"):
                self.add_error('acad_title', msg)

        return self.cleaned_data

    def save(self, request):

        # Ensure you call the parent classes save.
        # .save() returns a User object.
        user = super(MyCustomSignupForm, self).save(request)
        cleaned_data = self.cleaned_data
        # Process additional data
        user.second_name = self.cleaned_data['second_name']
        user.is_dbg_staff = self.cleaned_data['is_dbg_staff']

        # Process DBGStaff model if user is in staff
        if self.cleaned_data['is_dbg_staff']:
            user.department = self.cleaned_data['department_id']
            user.role = self.cleaned_data['role']
            user.acad_degree = self.cleaned_data['acad_degree']
            user.acad_title = self.cleaned_data['acad_title']
            user.save()

        user.save()

        # Return the original result.
        return user
