from mptt.models import MPTTModel, TreeForeignKey
from django.db import models
from allauth.account.models import EmailAddress
from django.contrib.auth.models import AbstractUser
from django.db.models import JSONField


class User(AbstractUser):
    """
        Additional user data as well as DBG staff member info
        """
    none = "not"
    candidate_biol = "cand_bio"
    candidate_med = "cand_med"
    candidate_agr = "cand_agr"
    candidate_math = "cand_math"
    phd = "phd"
    doctor_biol = "doc_bio"
    doctor_med = "doc_med"
    doctor_agr = "doc_agr"
    doctor_math = "doc_math"

    acad_degree_choices = (
        (none, "нет"),
        (candidate_biol, "кандидат биологических наук"),
        (candidate_med, "кандидат медицинских наук"),
        (candidate_agr, "кандидат сельскохозяйственных наук"),
        (candidate_math, "кандидат физико-математических наук"),
        (phd, "phd"),
        (doctor_biol, "доктор биологических наук"),
        (doctor_med, "доктор медицинских наук"),
        (doctor_agr, "доктор сельскохозяйственных наук"),
        (doctor_math, "доктор физико-математических наук"),
    )

    acad_degree_abbr = {
        "кандидат биологических наук": "к.б.н.",
        "кандидат медицинских наук": "к.м.н.",
        "кандидат сельскохозяйственных наук": "к.с.-х.н.",
        "кандидат физико-математических наук": "к.ф.-м.н.",
        "phd": "phd",
        "доктор биологических наук": "д.б.н.",
        "доктор медицинских наук": "д.м.н.",
        "доктор сельскохозяйственных наук": "д.с.-х.н.",
        "доктор физико-математических наук": "д.ф.-м.н.",
    }

    assoc_prof = "as_prof"
    professor = "prof"
    corresp_acad_nasu = "c_acad_nasu"
    acad_nasu = "r_acad_nasu"
    corresp_acad_ras = "c_acad_ras"
    acad_ras = "r_acad_ras"

    acad_title_choices = (
        (none, "нет"),
        (assoc_prof, "доцент"),
        (professor, "профессор"),
        (corresp_acad_nasu, "член-корреспондент НАНУ"),
        (acad_nasu, "действительный член НАНУ"),
        (corresp_acad_ras, "член-корреспондент РАН"),
        (acad_ras, "действительный член РАН"),
    )

    second_name = models.CharField(max_length=100, verbose_name="Отчество", null=True)

    # dbg_staff
    is_dbg_staff = models.BooleanField(default=False, verbose_name="сотрудник ДБС")

    department = TreeForeignKey("Departments", on_delete=models.CASCADE, verbose_name="Подразделение ДБС", null=True)
    role = models.CharField(max_length=300, null=True, verbose_name="Должность")
    acad_degree = models.CharField(max_length=50, null=True, choices=acad_degree_choices,
                                   verbose_name="Ученая степень")
    acad_title = models.CharField(max_length=50, null=True, choices=acad_title_choices, verbose_name="Ученое звание")

    def get_name_abbr(self):
        """
        :return: string with full First Second (if exists) and Last names for dbg_staff and name for others
        """
        if not self.is_dbg_staff:
            return self.username
        # Replace None with ""
        last_name = self.last_name if self.last_name else ""
        second_abbr = f'{self.second_name[0].capitalize() + "." if self.second_name else ""}'

        return f"{self.first_name[0].capitalize()}.{second_abbr} {last_name}"

    def get_user_reference(self):
        # Get related
        names = [self.first_name, self.second_name, self.last_name]
        # Replace None with "" or add space
        names = [x + " " if x else "" for x in names]
        # Check academy degree
        acad = self.acad_degree_abbr.get(self.get_acad_degree_display()) if self.acad_degree else ""
        acad = acad + " " if acad else ""
        dep = "(" + self.department.name + ")" if self.department else ""
        dep = dep if self.is_dbg_staff else "(не является сотрудником)"
        # Check department
        return f"{names[0]}{names[1]}{names[2]}{acad}{dep}"


class Departments(MPTTModel):
    """
    Model for handling departments names and hierarchy
    """
    name = models.CharField(max_length=100, unique=True)
    description = models.CharField(max_length=500, null=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True,
                            related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return str(self.name)


class SavedSearchLists(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=7, null=False)
    name = models.CharField(max_length=150, null=False)
    search_query = JSONField()
    created = models.DateField(auto_now_add=True)