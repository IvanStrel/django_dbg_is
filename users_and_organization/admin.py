from django.contrib import admin
from mptt.admin import MPTTModelAdmin

# Models registration
from .models import User
from .models import Departments
from .models import SavedSearchLists

admin.site.register(User)
admin.site.register(Departments, MPTTModelAdmin)
admin.site.register(SavedSearchLists)
