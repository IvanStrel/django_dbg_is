from django.shortcuts import render

from django.contrib.auth.decorators import login_required

# Load models
from .models import Departments
from .models import SavedSearchLists


# Create your views here.
def rights_required(request):
    return render(request, "rights_required/rights_required.html")


@login_required
def user_profile(request):
    # Get department data here we now that user is logged in
    if request.user.is_dbg_staff:
        dep = request.user.department
    else:
        dep = None

    # Get save search lists
    seach_lists = SavedSearchLists.objects.filter(user=request.user)

    context = {'dep': dep, 'seach_lists': seach_lists}

    return render(request, "user_profile/user_profile.html", context)
