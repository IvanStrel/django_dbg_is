document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    M.FormSelect.init(elems);

    // ==Move helper text
    $(elems).each(function(){
        $(this).parent().append($(this).parent().parent().find("span.helper-text"));
        console.log(this, this.checkValidity())
    });
});

$("button").on("click");


// Show staff part if staff_checkbox checked
$(document).ready(function(){
    var staff_check = $("#id_is_dbg_staff");
    var staff_wrapp = $("#is-staff-wrapper");
    /** Switch between enter and select options for family input */

    staff_check.change(function(){
        if (!this.checked){
            staff_wrapp.slideUp(300);
        } else {
            staff_wrapp.slideDown(300);
        }
    });
});


