from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('rights-required/', views.rights_required, name='rights_required'),
    path('profile/', views.user_profile , name='user_profile')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
