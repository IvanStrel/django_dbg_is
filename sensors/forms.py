import pytz
from datetime import datetime
from datetime import timedelta

from django import forms
from django.core.exceptions import ValidationError

from .models import Sensors
from .models import IndoorUnit
from .models import IndoorReading


class IndoorSensorReading(forms.Form):
    sensor_name = forms.CharField(max_length=100, required=True)
    unit_name = forms.CharField(max_length=100, required=True)
    time_epoch = forms.IntegerField(required=True)
    raw_value = forms.FloatField(required=True)
    time_delta = forms.IntegerField(required=True)

    def clean(self):
        cleaned_data = super(IndoorSensorReading, self).clean()
        # Get sensor instance
        if Sensors.objects.filter(name=cleaned_data.get("sensor_name")).exists():
            sensor = Sensors.objects.get(name=cleaned_data.get("sensor_name"))
        else:
            raise ValidationError('Invalid sensor name: %(sensor)',
                                  params={'sensor': cleaned_data.get("sensor_name")},
                                  code='invalid')

        # Check and get the unit instance
        if IndoorUnit.objects.filter(name=cleaned_data.get("unit_name")).exists():
            unit = IndoorUnit.objects.get(name=cleaned_data.get("unit_name"))
        else:
            raise ValidationError('Invalid unit name: %(unit)',
                                  params={'unit': cleaned_data.get("unit")},
                                  code='invalid')

        # Convert value
        value = cleaned_data.get("raw_value") * sensor.conversion

        # Convert epoch to datetime
        date_time = datetime.fromtimestamp(cleaned_data.get("time_epoch"), tz=pytz.timezone("Europe/Moscow"))

        # Append time with timedelta
        date_time += timedelta(microseconds=cleaned_data.get("time_delta"))

        # Check if time is unique
        while IndoorReading.objects.filter(time=date_time).exists():
            date_time += timedelta(microseconds=1)

        # Populate cleaned data
        cleaned_data["parameter"] = sensor.parameter
        cleaned_data["value"] = value
        cleaned_data["time"] = date_time
        cleaned_data["sensor"] = sensor
        cleaned_data["unit"] = unit

    def save(self):
        data = self.cleaned_data
        reading_obj = IndoorReading(
            unit=data["unit"],
            time=data["time"],
            sensor=data["sensor"],
            parameter=data["parameter"],
            value=data["value"],
        )
        reading_obj.save()
