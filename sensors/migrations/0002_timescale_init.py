from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("sensors", "0001_initial"),
    ]

    operations = [
        migrations.RunSQL("SELECT create_hypertable('sensors_indoorreading', 'time');")
    ]