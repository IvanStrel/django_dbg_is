from django.db import migrations
from django.core.management import call_command


def populate_parameters(apps, schema_editor):
    print("Populating parameters table with initial data")
    call_command("loaddata", "init_parameters_fixture", verbosity=2)


def populate_sensors(apps, schema_editor):
    print("Populating sensors table with initial data")
    call_command("loaddata", "init_sensors_fixture", verbosity=2)


def populate_units(apps, schema_editor):
    print("Populating IndoorUnits table with initial data")
    call_command("loaddata", "init_units_fixture", verbosity=2)


class Migration(migrations.Migration):

    dependencies = [
        ("sensors", "0002_timescale_init"),
    ]

    operations = [
        migrations.RunPython(populate_parameters),
        migrations.RunPython(populate_sensors),
        migrations.RunPython(populate_units)
    ]
