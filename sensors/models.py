from django.contrib.gis.db import models
from django.contrib.gis.db.models import PointField
from django.db import IntegrityError
from django.utils.dateparse import parse_datetime

from datetime import datetime
from datetime import timedelta


# ================================== Sensors models ==================================================================
#   The Idea:
#   1. The incoming data will contain unit name {"sensor": "gr3"} = unit "gr3"
#   2. The sensor readings will contain sensor description and value
#       for example: {"data":[{"h_am23_4_100": 4028}]}
#       where:  h = humidity
#               am23 = sensor module AM2301 (dht22 in casing)
#               4 = number of averaged samples
#               100 = multiplier, i.e. real value is 4028 / 100 = 40.28 [units a rh%]
#   3. The package received shod be parsed as:
#       - get unit name
#       - get time {"time": 1619097611} ==> convert to datetime
#       - for each sensor reading:
#           . get value, and convert according to conversion factor from Sensors table
#           . save value with a link to
# ====================================================================================================================
class IndoorUnit(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    location = PointField(null=False, blank=False)


class Parameters(models.Model):
    name = models.CharField(max_length=50)
    name_ru = models.CharField(max_length=50)
    description = models.CharField(max_length=300)
    units = models.CharField(max_length=50)


class Sensors(models.Model):
    parameter = models.ForeignKey(Parameters, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500)
    conversion = models.FloatField()
    error = models.FloatField()


class IndoorReading(models.Model):
    time = models.DateTimeField(primary_key=True, default=datetime.now)
    unit = models.ForeignKey(IndoorUnit, on_delete=models.CASCADE)
    sensor = models.ForeignKey(Sensors, on_delete=models.CASCADE)
    parameter = models.ForeignKey(Parameters, on_delete=models.CASCADE)
    value = models.FloatField()

    def save(self, *args, **kwargs):
        self.save_and_smear_timestamp(*args, **kwargs)

    def save_and_smear_timestamp(self, *args, **kwargs):
        """Recursivly try to save by incrementing the timestamp on duplicate error"""
        try:
            super().save(*args, **kwargs)
        except IntegrityError as exception:
            # Only handle the error:
            #   psycopg2.errors.UniqueViolation: duplicate key value violates
            if all(k in exception.args[0] for k in ("Key", "time", "already exists")):
                # Increment the timestamp by 1 µs and try again
                self.time = self.time + timedelta(microseconds=1)
                self.save_and_smear_timestamp(*args, **kwargs)
