import json

from django.http import HttpResponse
from django.db import transaction
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings

from .forms import IndoorSensorReading

from .utils import reading_to_list


class WrongFormException(Exception):
    pass


@csrf_exempt
def indoor_reading(request):
    response_false = HttpResponse(status=200)
    response_false['Content-Length'] = str(len(b'false'))
    response_false['Content-Type'] = 'text/plain'
    response_false.write(b'false')

    if request.method != 'POST':
        return HttpResponse("GET")

    # Get JSON
    sensor_reading = json.loads(request.body.decode("utf-8"))
    # Check key
    if sensor_reading.get("key") != settings.SENSOR_API_KEY:
        return response_false
    # Check if all data present
    if not sensor_reading.get("sensor") or not sensor_reading.get("data"):
        return response_false

    sensor_data = sensor_reading.get("data")

    unit = sensor_reading.get("sensor")

    reads_list = [x for read in sensor_data for x in reading_to_list(read, unit=unit)]

    # Try to save each data point
    try:
        # Initiate atomic transaction
        with transaction.atomic():
            for dat in reads_list:
                # Populate form
                form = IndoorSensorReading(dat)
                if not form.is_valid():
                    raise WrongFormException
                form.clean()
                form.save()
    except WrongFormException:
        return response_false

    response = HttpResponse(status=200)
    response['Content-Length'] = str(len(b'true'))
    response['Content-Type'] = 'text/plain'
    response.write(b'true')
    return response
