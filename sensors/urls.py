from django.urls import path

from . import views

urlpatterns = [
    path('indoor-reading/', views.indoor_reading, name='indoor_reading')
]
