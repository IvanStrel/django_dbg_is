from typing import List
from typing import Dict


def reading_to_list(read: Dict, unit: str) -> List[Dict]:
    """
    Get time variable, from a list and loop over other variables (sensors names)
    Prepare a list of dicts
    In each dict: {"time": ..., "sensor_name": ..., "unit": ..., "", "raw_value": ...}
    :param unit: unit name
    :param read: readings dict {"time": ..., "sensor_1": ..., "sensor_2": ...}
    :return: List[Dict]
    """
    # Here we need to check if time exists
    time = read.pop("time", None)

    out = []
    # As there is only one time for n readings, we want to add
    # some timedelta for each observation to prevent unique time constraint
    time_delta = 0  # Initiate time delta
    for k, v in read.items():
        out.append({"sensor_name": k,
                    "time_epoch": time,
                    "unit_name": unit,
                    "raw_value": v,
                    "time_delta": time_delta})
        time_delta += 1
    return out