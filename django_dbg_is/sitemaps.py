from django.contrib import sitemaps
from django.urls import reverse

from collection.models import TaxonomicUnit
from collection.models import Individual


class CommonElementsSiteMap(sitemaps.Sitemap):
    """
    Common elements as 'Home page' to include in sitemap
    """
    priority = 0.5
    changefreq = 'monthly'

    def items(self):
        return ['home_page', 'collection_home']

    def location(self, item):
        return reverse(item)


class TuProfilesSiteMap(sitemaps.Sitemap):
    """
    Sitemap elements for Taxonomic Units Profiles
    """
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return TaxonomicUnit.objects.all().order_by('id')

    def lastmod(self, obj):
        return obj.input_date

    def location(self, obj):
        return reverse("tu_profile", args=[obj.id])


class IndividualProfilesSiteMap(sitemaps.Sitemap):
    """
    Sitemap elements for Individual Profiles
    """
    changefreq = "monthly"
    priority = 0.5

    def items(self):
        return Individual.objects.all().order_by('id')

    def lastmod(self, obj):
        return obj.input_date

    def location(self, obj):
        return reverse("individual_profile", kwargs={'tu_id': obj.tu_id.id, 'ind_id': obj.id})