from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    # Collections paths
    path('<int:tu_id>/<int:ind_id>/pdf_label/<int:type>/', views.LabelPdfGenerator.as_view(), name='label_pdf_gen'),
    path('<int:tu_id>/edit-additional-info/<int:info_id>/', views.edit_non_oblig_info, name='edit_non_oblig_info'),
    path('<int:tu_id>/add-additional-info/', views.add_non_oblig_info, name='add_non_oblig_info'),
    path('<int:tu_id>/edit-vernacular/', views.edit_vernacular, name='edit_vernacular'),
    path('<int:tu_id>/add-vernacular/', views.add_vernacular, name='add_vernacular'),
    path('<int:tu_id>/add-image/', views.add_image, name='add_image'),
    path('<int:tu_id>/<int:ind_id>/', views.individual_profile, name='individual_profile'),
    path('<int:tu_id>/', views.tu_profile, name='tu_profile'),
    path('new-tu-1/', views.new_tu_form_1, name='new_tu_form_1'),
    path('new-tu-2/', views.new_tu_form_2, name='new_tu_form_2'),
    path('new-tu-3/', views.new_tu_form_3, name='new_tu_form_3'),
    path('new-tu-4/', views.new_tu_form_4, name='new_tu_form_4'),
    path('new-tu-5/', views.new_tu_form_5, name='new_tu_form_5'),
    path('new-tu-conf/', views.new_tu_form_conf, name='new_tu_form_conf'),
    path('new-tu-accept/', views.new_tu_accept, name='new_tu_accept'),
    path('new-individual/<int:tu_id>/', views.new_individual, name='new_individual'),
    path('new-ind-accept/', views.new_ind_accept, name='new_ind_accept'),
    path('edit-ind/<int:ind_id>/', views.edit_individual, name='edit_individual'),
    path('', views.collection_home, name='collection_home'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
