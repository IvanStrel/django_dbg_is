import requests
import time
import datetime
import pycountry
import gettext
import os
import re
import urllib.request
import json
import html
from django_dbg_is import settings
import matplotlib.pyplot as plt
from descartes import PolygonPatch
import matplotlib.patches as mpatches
from PIL import Image
from io import BytesIO
import pathlib

import pandas as pd
import numpy as np

from dataclasses import dataclass
from dataclasses import field
from dataclasses import asdict
from typing import List, Set, Dict, Tuple, Optional, Union

from django.utils.dateparse import parse_datetime
from django.conf import settings
from django.contrib.postgres.indexes import GistIndex

'''
STRUCTURE:
1. Miscellaneous functions:
  1.1. country_to_ru -- countries names translation
  1.2. none_to_empty -- replace None with "" (empty string)
  1.3. full_choice_from_abbr -- helper for working with choices fields
  1.4. proxy_list_from_free_proxy_list_net -- get proxyes from https://free-proxy-list.net/
  1.5. validate_proxy -- check if proxy ip is working
  1.6. find_working_proxy -- test proxyes form https://free-proxy-list.net/ against provided url
  1.7. check_site_access -- test if site url return 200 code
  1.8. request_proxy_check -- repeated code from Tropicos requests
  1.9. RedListCatConverter Red list categories converter
  1.10 remove_emty_choices -- remove '' choices from Model.choices object
2. External API callers:
  2.1. col_query
  2.2. tnrs_query
  2.3. redlist_query
  2.4. tropicos_search_query
  2.5. tropicos_distrib_query
  2.6. tropicos_chrom_query
  
3. Mapping functions
  3.1. redlist_distr_map
  
4. DB function
  4.1 Trigramm GIST index

5. Form utilities
  5.1 NewTU from clean session
  5.2 Process uploaded image
  
6. Data classes
  6.1 Search_request_data
  
7. Data processing
  7.1 TU cumulative income/outcome by years
'''


'''
MISCELLANEOUS FUNCTIONS
'''


def country_to_ru(country_name_eng):
    """
    Translates country's name from English to Russian
    :param country_name_eng: string with country name in English iso3166
    :return: string country name in Russian
    """
    russian = gettext.translation('iso3166', pycountry.LOCALES_DIR, languages=['ru'])
    russian.install()
    return russian.gettext(country_name_eng)


def none_to_empty(s):
    """
    Function which replace None with "" (empty string)
    :param s: input string
    :return: "" if s is None and s if s is not None
    """
    if s is None:
        return ''
    return str(s)


def normalize_string(*args, to_none=True):
    """
    Replace empty string with None
    :param to_none: convert empty string to None (bool)
    :param args: variable number of strings
    :return: strings or None for empty strings
    """
    if to_none:
        return [v.lower().strip() if v else None for v in args]
    else:
        return [v.lower().strip() if v else v for v in args]


def full_choice_from_abbr(choices_tuple, abbreviation):
    """
    Returns choice string from its abbreviation.
    :param choices_tuple: str
    :param abbreviation: str
    :return: str
    """
    return [res[1] for res in choices_tuple if res[0] == abbreviation][0]


def proxy_list_from_free_proxy_list_net():
    """
    will connect to https://free-proxy-list.net/
    and will get the proxy list from that page
    proxy format : ip address, port
    """
    link = 'https://free-proxy-list.net/'
    regex = '<td>(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})<\/td><td>(?P<port>\d{1,5})<\/td>'
    req = urllib.request.Request(link)
    req.add_header('User-Agent', 'Mozilla/5.0')
    content = urllib.request.urlopen(req).read().decode('ascii')
    res = re.findall(regex, content)
    return res


def validate_proxy(ip, port, link):
    """
    use a link url header to validate that proxy works
    we check only a http proxy
    :param ip: proxy ip
    :param port: proxy port
    :param link: url for testing agains. For example link = 'http://www.tropicos.org'
    return: True/False if the proxy is working
    """
    # Prepare url connection
    proxy_handler = urllib.request.ProxyHandler({'http': 'http://{}:{}/'.format(ip, port)})
    opener = urllib.request.build_opener(proxy_handler)

    try:
        res = opener.open(link, timeout=2)
        if res.getcode() == 200:
            return True
    except Exception as e:
        pass

    return False


def find_working_proxy(link):
    """
    Loops over proxyes list and validate each of them against link url.
    Returns first working proxy
    :param link: url to check proxyes against
    :return: working proxy in format "ip:port"
    """
    # Prepare proxy list
    proxy_list = proxy_list_from_free_proxy_list_net()
    # Initial empty proxy string
    proxy = None
    # Main proxy finding loop
    for ip, port in proxy_list:
        res = validate_proxy(ip, port, link)
        if res:
            proxy = ip + ":" + port
            # Stop loop if working proxy was found
            break

    return proxy


def check_site_access(link: str) -> bool:
    req = requests.get(link)
    if req.status_code == 200:
        return True
    else:
        return False


def request_proxy_check(link: str, timeout: int, proxy: str) -> requests.Response:
    if proxy is not None:
        resp = requests.get(link, timeout=timeout, proxies={"http": proxy})
    else:
        resp = requests.get(link, timeout=timeout)
    return resp


# 1.9 RedListCatConverter Red list categories converter ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class RedListCatConverter:
    def __init__(self, cat=None):
        self.cat_dict = {
            "cr": "Находящиеся на грани полного исчезновения",
            "en": "Исчезающие",
            "vu": "Уязвимые",
            "ex": "Исчезнувшие",
            "ew": "Исчезнувшие в дикой природе",
            "nt": "Находящиеся в состоянии, близком к угрожаемому",
            "lc": "Вызывающе наименьшее опасение",
            "dd": "Недостаток данных",
            "ne": "Неоцененные",
            "lr": "Низкий риcк",
            "lr/cd": "Низкий риск (зависящие от заповедных мероприятий)",
            "lr/nt": "Низкий риск (близкие к угрожаемому состоянию)",
            "lr/lc": "Низкий риск (вызывающе наименьшее опасение)"
        }
        self.cat = cat

    def normalize_cat(self, lw_name):
        """
        Returns normalized version of category name. For example `lR/LC` ==> `LR/lc`
        :param lw_name: Red List category abbreviation
        :return: Normalized category abbreviation
        """
        lw_split = lw_name.lower().split('/')
        lw_split[0] = lw_split[0].upper()
        return '/'.join(lw_split)

    def cat_choices(self):
        """
        Return Red List categories as choices tuple, suitable for forms.ChoiceField
        :return: list of tuples
        """
        return [(k, f"{self.normalize_cat(k)}: {v}") for k, v in self.cat_dict.items()]

    def cat_interp(self):
        """
        Returns an interpritation of category abbreviation in Russian
        :return: str
        """
        return self.cat_dict.get(self.cat.lower().strip())


# 1.10 remove_emty_choices -- remove '' choices from Model.choices object ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def remove_empty_choices(choices):
    return [(k, v) for k, v in choices if v]


'''
EXTERNAL API CALLERS
'''


def col_query(genera, species, infra_name, infra_marker, author):
    """
    Function to make a query to COL API and to parse results
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :param infra_marker: infraspecies marker
    :param author: string with taxon author abbreviation
    :return: dictionary with results. Contains elements 'parser status' and 'parser_error'
        and, possibly, resulting elemets from COL response
    """
    # Convert all params to lowercase and remove whitespaces
    genera = none_to_empty(genera).lower().replace(" ", "")
    species = none_to_empty(species).lower().replace(" ", "")
    infra_name = none_to_empty(infra_name).lower().replace(" ", "")
    infra_marker = none_to_empty(infra_marker).lower().replace(" ", "")
    author = none_to_empty(author)

    # Prepare set of keys to return in results
    keys = ["parser_error", "parser_status", "id", "genus", "species", "infraspecies_marker",
            "infraspecies", "author", "url", "references", "classification", "synonyms", "common_names",
            "bibliographic_citation"]

    # Capitalize genera
    genera = genera.capitalize()

    # Add + sign before infra name
    infra_name_p = ""
    if len(infra_name) > 0:
        infra_name_p = "+" + infra_name

    # Prepare url for query
    url_query = "http://www.catalogueoflife.org/col/webservice?name={}+{}{}&format=json&response=full"
    url_query = url_query.format(genera, species, infra_name_p)

    # Make API query
    try:
        resp = requests.get(url_query, timeout=15)

        resp_text = resp.text

        # convert utf escaped characters into utf8 (I is not necessary as json.loads handles it right)
        # resp_clean = bytes(resp_text, "utf-8").decode("unicode_escape")

        # un escape html
        resp_clean = html.unescape(resp_text)
        resp_j = json.loads(resp_clean)

    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    '''
    Response parsing procedure:
        1. Check for "error_message" in response (j["error_message"])
            if true: return error_message
        2. Check for full match
            if true:
                if accepted: return result (status = Accepted)
                if synonym: return result (status = Synonym)
        3. Check for match without Author 
            if true: return variants for replacement (status = MissedAuthor)
        4. Check for match without Infrspecies marker
            if true: return variants for replacement (status = MissedInfraMark)
        5. Check for match without both Infrspecies marker and Author
            if true: return variants for replacement  (status = MissedAuthorAndInfraMark)
        6. Else
            return Error (status = Error)
    '''

    # Check for error_message in response
    if len(resp_j["error_message"]) > 0:
        # Case when matched names were not found
        if resp_j["error_message"] == "No names found":
            result = {"parser_status": "NoNamesFound", "parser_error": ""}

        # Other cases
        else:
            result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    # ~~~~~~~~~~~~~~~~~~~~
    #   Main parsing
    # ~~~~~~~~~~~~~~~~~~~~

    # Prepare list of names (input and COL answers) for comparison
    target_str = [genera, species, infra_name, infra_marker, author]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"],
                x["infraspecies_marker"],
                x["author"]] for
               x in resp_j['results']]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Case 2-3. Check for full match
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if target_str in res_str:
        idx = res_str.index(target_str)
        result = resp_j['results'][idx]

        # Check if name is Accepted or Synonym
        if result["name_status"] in ["accepted name", "provisionally accepted name"]:
            result.update({"parser_status": "Accepted", "parser_error": ""})
            return {k: result[k] for k in result if k in keys}
        else:
            result = result["accepted_name"]
            result.update({"parser_status": "Synonym", "parser_error": ""})
            return {k: result[k] for k in result if k in keys}

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Case 4. Check for match without Author
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Here full match not found. Hence, look for partial match without author
    target_str = [genera, species, infra_name, infra_marker]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"],
                x["infraspecies_marker"]] for
               x in resp_j['results']]

    # Get indices of elements in res_str matching target_str
    occurrences = [i for i, x in enumerate(res_str) if x == target_str]

    # Proceed if at least 1 occurrence was found
    if len(occurrences):
        result = {"parser_status": "AuthorMismatch", "parser_error": "",
                  "variants": [resp_j['results'][x] for x in occurrences]}
        return result

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Case 5. Check for match without infra_mark
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    target_str = [genera, species, infra_name, author]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"],
                x["author"]] for
               x in resp_j['results']]

    # Get indices of elements in res_str matching target_str
    occurrences = [i for i, x in enumerate(res_str) if x == target_str]

    # Proceed if at least 1 occurrence was found
    if len(occurrences):
        result = {"parser_status": "InfraMarkMismatch", "parser_error": "",
                  "variants": [resp_j['results'][x] for x in occurrences]}
        return result

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #   Case 5. Check for match without both infra_mark and author
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    target_str = [genera, species, infra_name]
    res_str = [[x["genus"],
                x["species"],
                x["infraspecies"]] for
               x in resp_j['results']]

    # Get indices of elements in res_str matching target_str
    occurrences = [i for i, x in enumerate(res_str) if x == target_str]

    # Proceed if at least 1 occurrence was found
    if len(occurrences):
        result = {"parser_status": "InfraMarkAndAuthorMismatch", "parser_error": "",
                  "variants": [resp_j['results'][x] for x in occurrences]}
        return result
    else:
        result = {"parser_status": "Error", "parser_error": "SomethingWrongInCOL"}
        return result


def tnrs_query(genera, species, infra_name):
    """
    Function to make a query to TNRS API and to parse results
    Provided species name will be checked for match with TRNS db
        function will return only accepted names
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :return: dictionary with results. Contains elements 'parser status' and 'parser_error'
        and, possibly, resulting elemets from TRNS API response
    """
    # Convert all params to lowercase and remove whitespaces
    genera = genera.lower().replace(" ", "")
    species = species.lower().replace(" ", "")
    infra_name = none_to_empty(infra_name).lower().replace(" ", "")

    # Add + sign before infra name
    if len(infra_name) > 0:
        infra_name = " " + infra_name

    # Prepare url for species query
    url_query = "http://tnrs.iplantc.org/tnrsm-svc/matchNames?retrieve=all&names={} {}{}"
    url_query = url_query.format(genera, species, infra_name)

    try:
        resp = requests.get(url_query, timeout=5)
        resp_j = json.loads(resp.text)
    except requests.ConnectionError:
        result = {"parser_status": "Error", "parser_error": "ConnectionError"}
        return result
    except requests.HTTPError:
        result = {"parser_status": "Error", "parser_error": "HTTPError"}
        return result
    except requests.Timeout:
        result = {"parser_status": "Error", "parser_error": "Timeout"}
        return result
    except requests.TooManyRedirects:
        result = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
        return result
    except:
        result = {"parser_status": "Error", "parser_error": "UnexpectedError"}
        return result

    # Get all accepted names from the result
    items = []

    for obj in resp_j["items"]:
        # Check if accepted name is not empty
        # i.e. matches were found
        if len(obj["acceptedName"]):
            res = {"tnrs_family": obj["family"],
                   "tnrs_genus": obj["genus"],
                   "tnrs_species": obj["speciesMatched"],
                   "tnrs_infra_name": obj["infraspecific1Epithet"],
                   "tnrs_author": obj["acceptedAuthor"] if obj["acceptedAuthor"] else obj["authorAttributed"],
                   "tnrs_url": obj["acceptedNameUrl"],
                   }
            # Populate items with results
            items.append(res)

    # Check if any result was found
    if len(items) > 0:
        result = {"items": items}

        # add parser information to result
        result.update({"parser_status": "FoundMatchs", "parser_error": ""})
        return result
    else:
        result = {"parser_status": "Error", "parser_error": "NoMatchedNames"}
        return result


def redlist_query(genera, species, infra_name):
    """
    Function to make a query to Red List API and to parse results
    :param genera: genera scientific name
    :param species: species scientific name
    :param infra_name: infraspecies scientific name
    :return: dictionary
    """
    # Convert all params to lowercase and remove whitespaces
    genera = none_to_empty(genera).lower().replace(" ", "")
    species = none_to_empty(species).lower().replace(" ", "")
    infra_name = none_to_empty(infra_name).lower().replace(" ", "")

    # Add + sign before infra name
    if len(infra_name) > 0:
        infra_name = " " + infra_name

    # Red_list access token
    token = settings.API_TOKENS.get("red_list")

    # Prepare url for species query
    url_spec_query = "http://apiv3.iucnredlist.org/api/v3/species/{} {}{}?token={}"
    url_spec_query = url_spec_query.format(genera, species, infra_name, token)

    def make_request(url):
        try:
            resp = requests.get(url, timeout=15)
            resp_j = json.loads(resp.text)
        except requests.ConnectionError:
            res = {"parser_status": "Error", "parser_error": "ConnectionError"}
            return res
        except requests.HTTPError:
            res = {"parser_status": "Error", "parser_error": "HTTPError"}
            return res
        except requests.Timeout:
            res = {"parser_status": "Error", "parser_error": "Timeout"}
            return res
        except requests.TooManyRedirects:
            res = {"parser_status": "Error", "parser_error": "TooManyRedirects"}
            return res
        except:
            res = {"parser_status": "Error", "parser_error": "UnexpectedError"}
            return res

        resp_j.update({"parser_status": ""})
        return resp_j

    '''
    Get information about species
    '''
    resp_sp = make_request(url_spec_query)
    # Check no errors and retry
    if resp_sp["parser_status"] == "Error":
        time.sleep(1.5)
        resp_sp = make_request(url_spec_query)

    # If still contains error, return
    if resp_sp["parser_status"] == "Error":
        return resp_sp

    # Hot fix! Some problem with absent "result" key in `resp_j` dict
    try:
        resp_sp["result"]
    except KeyError:
        result = {"parser_status": "Error", "parser_error": "BadResponse"}
        return result

    # Parse results and prepare dictionary
    if len(resp_sp["result"]) > 0:  # I.e. species is in red list
        # ========================
        # Parse species data
        # ========================
        result = {
            "common_name": resp_sp["result"][0]["main_common_name"],
            "category": resp_sp["result"][0]["category"],
            "criteria": resp_sp["result"][0]["criteria"],
            "published_year": resp_sp["result"][0]["published_year"]
        }

        taxid = resp_sp["result"][0]["taxonid"]

        # ========================
        # Citation query
        # ========================
        # Prepare url for citation query
        url_cit_query = "http://apiv3.iucnredlist.org/api/v3/species/citation/id/{}?token={}"
        url_cit_query = url_cit_query.format(taxid, token)

        # Get citation data
        resp_cit = make_request(url_cit_query)
        # Check no errors and retry
        if resp_cit["parser_status"] == "Error":
            time.sleep(1.5)
            resp_cit = make_request(url_cit_query)

        if "result" in resp_cit.keys():
            if len(resp_cit["result"]) > 0:
                result["citation"] = resp_cit["result"][0]["citation"]
                result["cit_error"] = ""
            else:
                result["cit_error"] = "Error"
        else:
            result["cit_error"] = "Error"

        # ========================
        # Distribution query
        # ========================
        # Prepare url for distribution
        url_dist_query = "http://apiv3.iucnredlist.org/api/v3/species/countries/id/{}?token={}"
        url_dist_query = url_dist_query.format(taxid, token)

        # Get distribution data
        resp_dist = make_request(url_dist_query)
        # Check no errors and retry
        if resp_dist["parser_status"] == "Error":
            time.sleep(1.5)
            resp_dist = make_request(url_dist_query)

        if "result" in resp_dist.keys():
            if len(resp_dist["result"]) > 0:
                result["distribution"] = resp_dist["result"]
                result["distr_error"] = ""
            else:
                result["distr_error"] = "Error"
        else:
            result["distr_error"] = "Error"

        # ================================
        # Perform translation to russian
        # ================================

        # add category translation
        categories = {
            "cr": "Находящиеся на грани полного исчезновения",
            "en": "Исчезающие",
            "vu": "Уязвимые",
            "ex": "Исчезнувшие",
            "ew": "Исчезнувшие в дикой природе",
            "nt": "Находящиеся в состоянии, близком к угрожаемому",
            "lc": "Вызывающе наименьшее опасение",
            "dd": "Недостаток данных",
            "ne": "Неоцененные",
            "lr": "Низкий риcк",
            "lr/cd": "Низкий риск (зависящие от заповедных мероприятий)",
            "lr/nt": "Низкий риск (близкие к угрожаемому состоянию)",
            "lr/lc": "Низкий риск (вызывающе наименьшее опасение)"
        }

        # Check if result contains category as key
        if "category" in result.keys():
            if result["category"].lower() in categories:
                result.update(
                    {"category_trans": categories[result["category"].lower()]}
                )
            else:
                result.update(
                    {"category_trans": "Нераспознанная категория"}
                )

        origin_categories = {
            "Native": "Абориген",
            "Reintroduced": "Реинтродуцирован",
            "Introduced": "Интродуцирован",
            "Vagrant": "Редкий, не абориген",
            "Origin Uncertain": "Происхождение не известно",
            "Assisted Colonisation": "Целенаправленное поддержание"
        }
        # add countries names translation if distribution info is present
        # add distribution origin translation
        if "distribution" in result.keys():
            for dat in result["distribution"]:
                # Check if country is a key in dat obj
                # if true, add country name translation
                if "country" in dat.keys():
                    dat.update(
                        {"country_ru": country_to_ru(dat["country"])}
                    )

                # Check if country is a key in dat obj
                # if true, add country name translation
                if "origin" in dat.keys():
                    # Check if origin string is in origin_categories
                    if dat["origin"] in origin_categories.keys():
                        dat.update(
                            {"origin_ru": origin_categories[dat["origin"]]}
                        )
                    else:
                        dat.update(
                            {"origin_ru": "Неизвестно"}
                        )

        # add parser information to result
        result.update({"parser_status": "InRedList", "parser_error": ""})

        # convert utf escaped characters into utf8 and unescape html
        res_text = json.dumps(result)
        res_clean = bytes(res_text, "utf-8").decode("unicode_escape")
        res_clean = html.unescape(res_clean)
        result = json.loads(res_clean)

        return result
    else:
        result = {"parser_status": "NotInRedList", "parser_error": ""}
        return result


def tropicos_search_query(genera, species, infra_mark, infra_name, author, proxy):
    """
    The function to make a request to Tropicos API.
    Response is parsed and (if positive) taxonomic unit ID returned.
    Work flow:
    1. Check all parameters and prepare scientific name according to Tropicos notation
    2. Prepare and make request.
    3. Check response variant against scientific name
      3.1. If only one name were found and it is not illegitimate use it
      3.2. If multiple names were found use first legitimate name
      3.3. If multiple and no legitimate, use first non illegitimate name
      3.4. In all other cases return error

    !!! Important remark !!!
    Current script does not check the author, so it possibly
    can give wrong results.
    """
    api = settings.API_TOKENS.get("tropicos")
    # Check parameters
    # 1. infra_name without infra_mark or infra_mark without infra_name
    if infra_mark == "" and infra_name:
        return {"parser_status": "Error", "parser_error": "InfraMarkMissed"}
    if infra_name == "" and infra_mark:
        return {"parser_status": "Error", "parser_error": "InfraNameMissed"}

    # Convert all params to lowercase and remove white-spaces
    genera = none_to_empty(genera).lower().replace(" ", "")
    species = none_to_empty(species).lower().replace(" ", "")
    infra_name = none_to_empty(infra_name).lower().replace(" ", "")
    infra_mark = none_to_empty(infra_mark).lower().replace(" ", "")
    author = none_to_empty(author)

    ''' Clean data
    Tropicos uses different notation for forms and subforms (fo. and subfo.)
    additionally, Tropicos use sp. notation for clean species.
    We have to change infra_mark accordingly
    '''
    if infra_mark.lower() == "f.":
        infra_mark = "fo."
    if infra_mark.lower() == "subf.":
        infra_mark = "subfo."
    if not infra_mark:  # infra names consistency was checked previously
        infra_mark = "sp."

    # Construct Scientific name according to Tropicos notation
    # i.e. genera species --> for clean species
    # and genera species infra_mark infra_name --> for subspecies
    if not infra_name:
        sci_name = genera + " " + species
    else:
        sci_name = "{} {} {} {}".format(genera, species, infra_mark, infra_name)

    # Prepare url for query
    url_query = "http://services.tropicos.org/Name/Search?name={} {}{}&type=wildcard&apikey={}&format=json"

    # Add whitespace before infra_name if exists
    if infra_name:
        infra_name_p = " " + infra_name
    else:
        infra_name_p = ""

    # Populate url template
    url_query = url_query.format(genera, species, infra_name_p, api)

    # Make API query
    try:
        # Check if proxy required
        resp = request_proxy_check(url_query, timeout=15, proxy=proxy)

        resp_text = resp.text

        # unescape html
        resp_clean = html.unescape(resp_text)
        resp_j = json.loads(resp_clean)

    except requests.exceptions.RequestException as e:
        result = {"parser_status": "Error", "parser_error": e}
        return result

    except json.decoder.JSONDecodeError as e:
        result = {"parser_status": "Error", "parser_error": e}
        return result

    # ==========================================================================
    # Parse results
    # ==========================================================================
    # If result is empty
    if len(resp_j) == 0:
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}
    # If no names were found
    if resp_j[0].get("Error") == "No names were found":
        return {"parser_status": "Error", "parser_error": "NoNamesFound"}

    # Get only results with matching scientific name
    res_parsed = []
    for var in resp_j:
        if var.get("ScientificName").lower().strip() == sci_name:
            res_parsed.append(var)

    # If 0 matches found
    if len(res_parsed) == 0:
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}

    # If only one result was found
    if len(res_parsed) == 1:
        # And it is not Illegitimate
        if res_parsed[0].get("NomenclatureStatusName").lower() != "illegitimate":
            return ({"parser_status": "Success",
                     "parser_error": "",
                     "id": res_parsed[0].get("NameId")})

    # Now we know that there are more than 1 variant with matched sci_name
    # Return first Legitimate name id
    # # Consider check against Author
    for var in res_parsed:
        if var.get("NomenclatureStatusName").lower() == "legitimate":
            return ({"parser_status": "Success",
                     "parser_error": "",
                     "id": var.get("NameId")})

    # Now we know that there are not any Legitimate name
    # so return first non Illegitimate, non Invalid, non Nom.rej.
    for var in res_parsed:
        var_status = var.get("NomenclatureStatusName").lower()
        if var_status not in ["illegitimate", "invalid", "nom. rej."]:
            return ({"parser_status": "Success",
                     "parser_error": "",
                     "id": var.get("NameId")})

    # At this point we have already checked all plausible variants
    # and have not got any appropriate result, so return error
    return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}


def tropicos_distrib_query(tropicos_id, proxy):
    """
    Will request Tropicos Distribution API (need Tropicos name_id)
    Work flow:
    1. Construct url
    2. Make a request
    3. Retain Country name, Sub country name and full citation
    4. Return results list
    """
    api = settings.API_TOKENS.get("tropicos")

    # Prepare url
    url_query = "http://services.tropicos.org/Name/{}/Distributions?apikey={}&format=json"
    # Populate url
    url_query = url_query.format(tropicos_id, api)

    # Make API query
    try:
        # Check if proxy required
        resp = request_proxy_check(url_query, timeout=15, proxy=proxy)

        resp_text = resp.text
        # unescape html
        resp_clean = html.unescape(resp_text)
        resp_j = json.loads(resp_clean)

    except requests.exceptions.RequestException as e:
        result = {"parser_status": "Error", "parser_error": e}
        return result

    # ==========================================================================
    # Parse results
    # ==========================================================================
    # If result is empty
    if len(resp_j) == 0:
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}
    # If no names were found
    if resp_j[0].get("Error") == "No distribution records were found":
        return {"parser_status": "Error", "parser_error": "NoNamesFound"}

    # Retain country name (CountryName), sub country name (UpperName) and FullCitation
    res = []
    for var in resp_j:
        # Here we loop over each row in response
        # each variant contains two sub dictionaries with
        # Location info and Reference info
        # it is possible that one of them is absent,
        # therefore we need to check their presence before
        # querying sub dictionaries
        var_ret = {}
        # Check for Location and pass if absent
        if "Location" in var.keys():
            # Country name and translation
            var_ret.update({"CountryName": var.get("Location").get("CountryName"),
                            "CountryNameRu": country_to_ru(var.get("Location").get("CountryName"))})
            # Sub country name and translation
            var_ret.update({"UpperName": var.get("Location").get("UpperName"),
                            "UpperNameRu": country_to_ru(var.get("Location").get("UpperName"))})

            # Check for Reference sub directory presence
            if "Reference" in var.keys():
                var_ret.update({"Citation": var.get("Reference").get("FullCitation")})
            else:
                # Reference is empty --> add blank Citation
                var_ret.update({"Citation": ""})

            # Append parsing result to output object
            res.append(var_ret)

    # If res is empty return error
    if not len(res):
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}

    # Here we know that res contains something --> return it
    return ({"parser_status": "Success",
             "parser_error": "",
             "Distribution": res})


def tropicos_chrom_query(tropicos_id, proxy):
    """
    Will request Tropicos ChromosomeCounts API (need Tropicos name_id)
    Work flow:
    1. Construct url
    2. Make a request
    3. Retain gametophyte and sporophyte counts and citation
    4. Return results list
    """
    api = settings.API_TOKENS.get("tropicos")

    # Prepare url
    url_query = "http://services.tropicos.org/Name/{}/ChromosomeCounts?apikey={}&format=json"
    # Populate url
    url_query = url_query.format(tropicos_id, api)

    # Make API query
    try:
        # Check if proxy required
        resp = request_proxy_check(url_query, timeout=15, proxy=proxy)

        resp_text = resp.text
        # unescape html
        resp_clean = html.unescape(resp_text)
        resp_j = json.loads(resp_clean)

    except requests.exceptions.RequestException as e:
        result = {"parser_status": "Error", "parser_error": e}
        return result

    # ==========================================================================
    # Parse results
    # ==========================================================================
    # If result is empty
    if len(resp_j) == 0:
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}
    # If no names were found
    if resp_j[0].get("Error") == "No names were found":
        return {"parser_status": "Error", "parser_error": "NoNamesFound"}

    # Retain country name (CountryName), sub country name (UpperName) and FullCitation
    res = []
    for var in resp_j:
        # Here we want to retain only entries with
        # SporophyticCount or GametophyticCount
        # also we need to check if Reference sub directory
        # is in var
        if "SporophyticCount" in var.keys() or "GametophyticCount" in var.keys():
            var_ret = {"SporophyticCount": var.get("SporophyticCount"),
                       "GametophyticCount": var.get("GametophyticCount")}
            # Check if Reference sub dictionary is present
            if "Reference" in var.keys():
                var_ret.update({"Citation": var.get("Reference").get("FullCitation")})

            res.append(var_ret)

    # If res is empty return error
    if not len(res):
        return {"parser_status": "Error", "parser_error": "SomethingWentWrong"}

    # Here we know that res contains something --> return it
    return ({"parser_status": "Success",
             "parser_error": "",
             "ChromosomeCounts": res})


'''
MAPPING FUNCTIONS
'''


def redlist_distr_map(redlist_distr_list, species_name, out_path, map_geojson):
    """
    Creates SVG distribution map (worldwide) basing on distribution data from IUCN red list.
    Requires a Json World map from https://github.com/johan/world.geo.json
    Countries with assigned species occurrence are plotted with different colors according
    to Red List's origin info (look color code dictionary)
    :param redlist_distr_list: "distribution: object from redlist_query result (list of dicts)
    :param species_name: full name of the species (used map title)
    :param out_path: output path
    :param map_geojson: path to initial geojson map with countries geometries
    :return: none
    """
    # Prepare color codes for origin category
    color_code = {
        "Native": {"color": "#228B22", "name": "Абориген"},
        "Reintroduced": {"color": "#00FF00", "name": "Реинтродуцирован"},
        "Introduced": {"color": "#FF00FF", "name": "Интродуцирован"},
        "Vagrant": {"color": "#9932CC", "name": "Редкий, не абориген"},
        "Origin Uncertain": {"color": "#FFFF00", "name": "Происхождение не известно"},
        "Assisted Colonisation": {"color": "#FFA500", "name": "Целенаправленное поддержание"},
    }

    # Create list with countries names from redlist_distr_list
    alpha_2_codes = [country.alpha_2 for country in list(pycountry.countries)]
    c_names = [pycountry.countries.get(alpha_2=distr["code"]).alpha_3
               for distr in redlist_distr_list
               if distr["code"] in alpha_2_codes]  # Check for real alpha 2 code

    # Prepare variable for legend
    legend_var = {}

    # Read geojson map
    json_path = os.path.join(settings.BASE_DIR, map_geojson)
    with open(json_path) as json_file:
        json_data = json.load(json_file)  # or geojson.load(json_file)

    # Remove antarctica
    json_data["features"] = [f for f in json_data["features"] if f["properties"]["name"] != "Antarctica"]

    gray = '#A9A9A9'
    fig = plt.figure(figsize=(22, 12))
    fig.suptitle('Распространение ' + species_name, fontsize=30, y=.95)
    ax = fig.add_subplot(111, frame_on=False)
    for feature in json_data["features"]:
        # Prepare color for country if it is in redlist_distr_list
        # color according to color_code
        color = gray
        alpha = 0.5
        if feature["id"] in c_names:
            # find index of country name in c_names
            idx = c_names.index(feature["id"])

            # Get origin value
            origin_val = redlist_distr_list[idx]["origin"]

            # Get color value
            if origin_val in color_code.keys():
                color = color_code[origin_val]["color"]
                alpha = 1

                # Add color and name to legend variables
                if origin_val not in legend_var.keys():
                    legend_var.update({origin_val: color_code[origin_val]})
            else:
                print(origin_val)

        ax.add_patch(PolygonPatch(feature["geometry"], fc=color, ec=gray, alpha=alpha, zorder=2))

    plt.axis('off')
    plt.ylim((-90, 90))
    plt.xlim((-180, 180))

    # Construct legend patches
    patches = [mpatches.Patch(color=v["color"], label=v["name"]) for k, v in legend_var.items()]

    # Calculate number of columns
    n_col = 2
    if len(legend_var) > 4:
        n_col = 3

    # Add legend
    plt.legend(handles=patches, loc=8, ncol=n_col, fontsize="xx-large")

    # save image
    plt.savefig(out_path, bbox_inches='tight')
    # Change permissions
    pathlib.Path(out_path).chmod(0o777)


def img_convert_to_buffer(path=None, image=None, otype="JPEG", size="3x"):
    assert path is not None or image is not None
    # Prepare maximum sizes dict (max image dimension should be less or equal to max size)
    sizes = {"3x": [3456, 2592],
             "2x": [2304, 1728],
             "1x": [1024, 768],
             "th": [500, 500]}

    # Read initial image from disk if path provide
    if path:
        # The case of initial images addition through `new_tu_5` form
        img_init = Image.open(path)
    else:
        # The case when new (single) images is added via `add_image` form
        img_init = image

    # Get image initial size
    width, height = img_init.size
    # Compute new size
    target_size = sizes.get(size)
    if max([width, height]) <= target_size[0]:
        pass
    elif width >= height:
        new_width = target_size[0]
        new_height = target_size[0] * height / width
        img_init.thumbnail((int(new_width), int(new_height)), Image.ANTIALIAS)
    else:
        new_height = target_size[0]
        new_width = target_size[0] * width / height
        img_init.thumbnail((int(new_width), int(new_height)), Image.ANTIALIAS)

    # Prepare output buffer
    out_io_stream = BytesIO()
    # Save resized image in desired format
    if otype == "JPEG":
        img_init.save(out_io_stream, format='JPEG', quality=95, optimize=True, progressive=True)
    else:
        img_init.save(out_io_stream, format='WebP', lossless=False, quality=95)

    return out_io_stream


""" 
===================================================================================================================
    4. DB functions
=================================================================================================================== 
"""
# 4.1 Trigramm GIST index ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class GistIndexTrgrmOps(GistIndex):
    """
    The code taken from
    https://vxlabs.com/2018/01/31/creating-a-django-migration-for-a-gist-gin-index-with-a-special-index-operator/
    """
    def create_sql(self, model, schema_editor, using='', **kwargs):
        # - this Statement is instantiated by the _create_index_sql()
        #   method of django.db.backends.base.schema.BaseDatabaseSchemaEditor.
        #   using sql_create_index template from
        #   django.db.backends.postgresql.schema.DatabaseSchemaEditor
        # - the template has original value:
        #   "CREATE INDEX %(name)s ON %(table)s%(using)s (%(columns)s)%(extra)s"
        statement = super().create_sql(model, schema_editor)
        # - however, we want to use a GIST index to accelerate trigram
        #   matching, so we want to add the gist_trgm_ops index operator
        #   class
        # - so we replace the template with:
        #   "CREATE INDEX %(name)s ON %(table)s%(using)s (%(columns)s gist_trgrm_ops)%(extra)s"
        statement.template =\
            "CREATE INDEX %(name)s ON %(table)s%(using)s (%(columns)s gist_trgm_ops)%(extra)s"

        return statement


""" 
===================================================================================================================
    5. Form utilities
=================================================================================================================== 
"""
# 5.1 NewTU from clean session ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def new_tu_form_clean_session(request):
    if "new_tu_form_1_cleaned_data" in request.session.keys():
        del request.session["new_tu_form_1_cleaned_data"]

    if "new_tu_form_2_cleaned_data" in request.session.keys():
        del request.session["new_tu_form_2_cleaned_data"]

    if "new_tu_form_3_cleaned_data" in request.session.keys():
        del request.session["new_tu_form_3_cleaned_data"]

    if "new_tu_form_4_cleaned_data" in request.session.keys():
        del request.session["new_tu_form_4_cleaned_data"]

    if "new_tu_form_5_cleaned_data" in request.session.keys():
        del request.session["new_tu_form_5_cleaned_data"]


# 5.2 Taxonomy to dict ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def taxonomy_obj_to_dict(col_tax_object):
    if col_tax_object:
        taxonomy = {"col_kingdom": col_tax_object.col_kingdom,
                    "col_phylum": col_tax_object.col_phylum,
                    "col_class": col_tax_object.col_class,
                    "col_order": col_tax_object.col_order,
                    "col_family": col_tax_object.col_family,
                    "col_family_id": col_tax_object.col_family_id}
        return taxonomy
    else:
        return None


""" 
===================================================================================================================
    5. Data classes
=================================================================================================================== 
"""
# 6.1 Search_request_data ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


@dataclass
class CollectionSearchRequestData:
    type: str = field(default='tu')  # required (`tu` or `ind`)
    alive_only: bool = field(default=False)

    # ID lists
    tu_ids: Union[List[int], None] = field(default=None)  # list of taxonomic units id to return
    ind_ids: Union[List[int], None] = field(default=None)  # list of individuals id to return

    # In col
    in_col: bool = field(default=True)
    not_in_col: bool = field(default=True)
    hybrid_only: bool = field(default=False)

    # Time of individuals arrival (should be a string, for example: "2020-04-09")
    after_time: Union[str, None] = field(default=None)
    before_time: Union[str, None] = field(default=None)

    # Taxonomy lookup
    col_phylum: Union[List[str], None] = field(default=None)
    col_class: Union[List[str], None] = field(default=None)
    col_order: Union[List[str], None] = field(default=None)
    col_family: Union[List[str], None] = field(default=None)
    genera_name: Union[List[str], None] = field(default=None)

    # Obligatory info
    raunc_form: Union[List[str], None] = field(default=None)  # List of life forms
    moisture: Union[List[str], None] = field(default=None)  # List of eco groups in relation to moisture
    light: Union[List[str], None] = field(default=None)  # List of eco groups in relation to light
    fertility: Union[List[str], None] = field(default=None)  # List of eco groups in relation to soil fertility
    salinity: Union[List[str], None] = field(default=None)  # List of eco groups in relation to soil salinity
    acidity: Union[List[str], None] = field(default=None)  # List of eco groups in relation to soil acidity
    phenotype: Union[List[str], None] = field(default=None)  # List of phenorythms
    cenotype: Union[List[str], None] = field(default=None)  # List of cenotypes

    # Red list lookup
    in_red_list: Union[bool, None] = field(default=None)  # True = in RedList, False = not in RedList, None = Any
    red_list_cat: Union[List[str], None] = field(default=None)  # Red list category

    # Images
    with_images: bool = field(default=False)  # Look for tu with assigned photos

    # Departments
    departments_ids: Union[List[int], None] = field(default=None)  # list of departments for filtering

    # Spatial lookup
    sp_level_1: Union[List[int], None] = field(default=None)  # list of spatial polygons level 1 ids
    sp_level_2: Union[List[int], None] = field(default=None)  # list of spatial polygons level 2 ids
    sp_level_3: Union[List[int], None] = field(default=None)  # list of spatial polygons level 3 ids

    def as_dict(self):
        """
        Convert self to dict. Useful for serialization.
        Deserialization could be achieved with `SearchRequestData(**dict)`
        :return: dict
        """
        return asdict(self)

    def prepared_dict(self):
        """
        Return dict usable as raw SQL mapping dict, i.e. lists converted to tuples [1, 2, 3] ==> (1, 2, 3).
        Example usage: Obj.objects.raw("some_SQL", translations = CollectionSearchRequestData.prepared_dict())
        :return: dict
        """
        raw_dict = asdict(self)
        return {k: tuple(v) if type(v) is list else v for k, v in raw_dict.items()}

    def readable_filters(self):
        """
        Returns filters in readable format = convert dict keys from latin abbreviation to cyrillic representation.
        :return: dict
        """
        # If there are a lists of ids, return only number of requested tus or uds
        out_dict = {}
        if self.ind_ids or self.tu_ids:
            if self.ind_ids:
                data_dict = {f'Сохраненная выборка особей': f"всего особей: {len(self.ind_ids)}"}
            else:
                data_dict = {f'Сохраненная выборка таксонов': f"всего научных названий: {len(self.tu_ids)}"}
            out_dict.update({'type': "ids", 'data': data_dict})
            return out_dict

        # Else, translate keys
        # Prepare translation dict:
        from collection.models import ObligatoryInfo
        trans_dict = {'type': {'tu': "Таксоны", 'ind': "Особи"},
                      'alive_only': {True: "Нет", False: "Да"},  # The question is 'do we include dead items'
                      'raunc_form': {k: v for k, v in ObligatoryInfo.raunc_form_choices if v},
                      'light': {k: v for k, v in ObligatoryInfo.light_choices if v},
                      'moisture': {k: v for k, v in ObligatoryInfo.moisture_choices if v},
                      'fertility': {k: v for k, v in ObligatoryInfo.fertil_choices if v},
                      'salinity': {k: v for k, v in ObligatoryInfo.salin_choices if v},
                      'acidity': {k: v for k, v in ObligatoryInfo.acidity_choices if v},
                      'phenotype': {k: v for k, v in ObligatoryInfo.pheno_choices if v},
                      'cenotype': {k: v for k, v in ObligatoryInfo.cenotype_choices if v},
                      'in_red_list': {True: "Да", False: "Нет"},
                      }

        # Populate data dict
        data_dict = {'Цель поиска': trans_dict.get('type').get(self.type),
                     'Включая выпавшие': trans_dict.get('alive_only').get(self.alive_only)}
        # COL lookup
        if self.in_col:
            data_dict.update({'Сверенные с Catalogue of Life': 'Да'})
        if self.not_in_col:
            data_dict.update({'Не сверенные с Catalogue of Life': 'Да'})
        if self.hybrid_only:
            data_dict.update({'Только гибриды': 'Да'})

        # After, before filter
        if self.after_time:
            data_dict.update({'Поступившие после': self.after_time})
        if self.after_time:
            data_dict.update({'Поступившие до': self.before_time})

        # Taxonomic filters
        if self.col_phylum:
            data_dict.update({'Отделы': ', '.join(self.col_phylum)})
        if self.col_class:
            data_dict.update({'Классы': ', '.join(self.col_class)})
        if self.col_order:
            data_dict.update({'Порядки': ', '.join(self.col_order)})
        if self.col_family:
            data_dict.update({'Семейства': ', '.join(self.col_family)})
        if self.genera_name:
            data_dict.update({'Роды': ', '.join(self.genera_name)})

        # Obligatory info lookups:
        if self.raunc_form:
            dat_list = [trans_dict.get('raunc_form').get(x) for x in self.raunc_form]
            data_dict.update({'Жизненные формы по Раункиеру': ', '.join(dat_list)})

        if self.moisture:
            dat_list = [trans_dict.get('moisture').get(x) for x in self.moisture]
            data_dict.update({'Эко. группы по увлажнению': ', '.join(dat_list)})

        if self.light:
            dat_list = [trans_dict.get('light').get(x) for x in self.light]
            data_dict.update({'Эко. группы по освещению': ', '.join(dat_list)})

        if self.fertility:
            dat_list = [trans_dict.get('fertility').get(x) for x in self.fertility]
            data_dict.update({'Эко. группы по плодородию почвы': ', '.join(dat_list)})

        if self.salinity:
            dat_list = [trans_dict.get('salinity').get(x) for x in self.salinity]
            data_dict.update({'Эко. группы по засоленности': ', '.join(dat_list)})

        if self.acidity:
            dat_list = [trans_dict.get('acidity').get(x) for x in self.acidity]
            data_dict.update({'Эко. группы по кислотности': ', '.join(dat_list)})

        if self.phenotype:
            dat_list = [trans_dict.get('phenotype').get(x) for x in self.phenotype]
            data_dict.update({'Феноритмотипы': ', '.join(dat_list)})

        if self.cenotype:
            dat_list = [trans_dict.get('cenotype').get(x) for x in self.cenotype]
            data_dict.update({'Ценотипы': ', '.join(dat_list)})

        # Red list data:
        if self.in_red_list:
            data_dict.update({'Включенные в красную книгу МСОП': trans_dict.get('in_red_list').get(self.in_red_list)})
        if self.red_list_cat:
            data_dict.update({'Категории МСОП': ', '.join(self.red_list_cat)})

        # Images lookup
        if self.with_images:
            data_dict.update({'Только с изображениями': 'Да'})

        # Departments lookup
        if self.departments_ids:
            from users_and_organization.models import Departments
            dep_names = Departments.objects.filter(pk__in=self.departments_ids).values_list('name')
            dat_list = [x[0] for x in dep_names]
            data_dict.update({'Подразделения ДБС': ', '.join(dat_list)})

        # Spatial lookup
        if self.sp_level_1:
            data_dict.update({'Территориальные участки уровня 1': f"Всего учасктов: {len(self.sp_level_1)}"})
        if self.sp_level_2:
            data_dict.update({'Территориальные участки уровня 2': f"Всего учасктов: {len(self.sp_level_2)}"})
        if self.sp_level_3:
            data_dict.update({'Территориальные участки уровня 3': f"Всего учасктов: {len(self.sp_level_3)}"})

        out_dict.update({'type': "filters", 'data': data_dict})
        return out_dict


""" 
===================================================================================================================
    7. Data processing
=================================================================================================================== 
"""
# 7.1 TU cumulative income/outcome by years


def cumulative_tu_income_outcome(ind_pd):
    # Create years data frame
    from_year = '1964-01-01'
    to_year = datetime.datetime.today()

    years_ser = pd.date_range(start=pd.to_datetime(from_year),
                              end=pd.to_datetime(to_year) + pd.DateOffset(years=1), freq='AS')
    years = pd.DataFrame(years_ser, columns=['year'])

    # From input df retain only tu_id, ind_id, income, outcome
    dat = ind_pd[['ind_id', 'pk', 'income_date', 'out_date']].copy()
    dat['income_date'] = pd.to_datetime(dat['income_date'])
    dat['out_date'] = pd.to_datetime(dat['out_date'])

    # Left join individuals to years
    ind_year = pd.merge(years.assign(key='x'), dat.assign(key='x'), on='key').drop('key', 1)

    # Get alive/dead status for each individual
    conditions = [
        np.isnan(ind_year['income_date']),
        ind_year['income_date'] > ind_year['year'],
        (ind_year['income_date'] <= ind_year['year']) & ((ind_year['out_date'] > ind_year['year']) | np.isnan(ind_year['out_date'])),
        (ind_year['income_date'] <= ind_year['year']) & (ind_year['out_date'] <= ind_year['year'])
    ]

    outputs = [np.NaN, np.NaN, np.True_, np.False_]

    res_np = np.select(conditions, outputs, np.False_)

    ind_year["alive"] = res_np

    # Count alive and dead
    counts = ind_year.groupby(['year', 'pk'], as_index=False)['alive']\
        .agg(np.nanmax)\
        .groupby('year', as_index=True)\
        .agg(alive=pd.NamedAgg('alive', lambda x: np.count_nonzero(x[~np.isnan(x)])),
             dead=pd.NamedAgg('alive', lambda y: np.nansum(y[~np.isnan(y)] == 0)))\
        .reset_index()

    # Return json
    return counts.to_json(orient='records')
