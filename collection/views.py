from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import HttpResponseNotFound
from django.forms import formset_factory
from django.forms.models import model_to_dict
from django.conf import settings
from django.urls import reverse
from django.urls import reverse_lazy
from django.core.files import File
from django.shortcuts import get_object_or_404
from django.core.serializers import serialize
from django.db.models import Count
from django.db.models import Q
from django.views.generic import DetailView
from django.utils.text import slugify
from django.db import transaction
from django.db import IntegrityError

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from django.db.models import Case
from django.db.models import When
from django.db.models import F

import json
import os
import pathlib
import shutil
import re
from io import BytesIO
import qrcode
import qrcode.image.svg
from datetime import date
import reversion

# Manipulations with files
from django.core.files.storage import default_storage
from PIL import Image
from PIL import ImageOps

# Celery imports
from django_dbg_is.celery import app
from collection.tasks import redlist_query_async
from collection.tasks import tropicos_query_async

# Import forms
from .forms import NewTuForm1
from .forms import NewTuForm2
from .forms import NewTuForm3
from .forms import NewTuForm4
from .forms import NewTuForm5
from .forms import BaseVernacularFormSet
from .forms import BaseObligInfoFormSet
from .forms import BaseTuImagesFormSet
from .forms import NewIndividualForm

# Import models
from .models import Taxonomy
from .models import ColTaxonomy
from .models import ColSpecies
from .models import TaxonomicUnit
# from .models import Synonyms
# from .models import ColData
from .models import NonObligatoryInfo
from .models import ObligatoryInfo
from .models import Vernacular
from .models import RedListIUCN
from .models import TropicosInfo
from .models import TaxonomicUnitImages
from .models import Individual
from spatial.models import SovCountries
from spatial.models import AdminRegions
from spatial.models import PopulatedPlaces

# Import utils
from .utils import taxonomy_obj_to_dict
from .utils import new_tu_form_clean_session
from .utils import full_choice_from_abbr


# Import serializers
from .serializers import CollectionHomeTableSerializer

# Import weasy print
from django_weasyprint import WeasyTemplateResponseMixin


def collection_home(request):
    """
    The view for Collection home page which contains a table with all data about TU
    :param request: GET request
    :return: html response
    """
    # Prepare data (TU, alive only)
    tax = TaxonomicUnit.objects.collection_home_table()
    tax_serialized = CollectionHomeTableSerializer(tax, many=True)
    data = {
        "tax_json": json.dumps(tax_serialized.data),
    }

    return render(request, 'collection_home/collection_home.html', data)


@login_required
@permission_required('collection.add_taxonomicunit', login_url=reverse_lazy("rights_required"))
def new_tu_form_1(request):
    """
    First page of the multi page form for new TU entering
    It asks for the taxonomic data
    :param request: GET or POST request
    """
    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':
        # Prepare form object for template filling
        form = NewTuForm1()

        # Prepare class and families values for choice field
        family_choices = ColTaxonomy.objects \
            .values('col_family').distinct().order_by('col_family')
        class_choices = ColTaxonomy.objects \
            .values('col_class').distinct().order_by('col_class')

        data_render = {'form': form, 'family_choices': family_choices, 'class_choices': class_choices}

        # Check if form 1 data is in session (i.e. user returned to this form)
        if "new_tu_form_1_cleaned_data" in request.session.keys():
            data = request.session["new_tu_form_1_cleaned_data"]

            # Check if data is filled
            if any(v is not None for v in data.values()):
                data_render["prev_data"] = data

        return render(request, 'new_tu_forms/new_tu_form_1.html', data_render)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #                   POST request on first form
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    else:
        form = NewTuForm1(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #  Process Valid form
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #  Check if TU is already in DB
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            exists = TaxonomicUnit.objects.tax_unit_exact_match(genera=form.cleaned_data["tu_genera"],
                                                                species=form.cleaned_data["tu_species"],
                                                                infra_mark=form.cleaned_data["tu_infra_mark"],
                                                                infra_name=form.cleaned_data["tu_infra_name"],
                                                                cult_mark=form.cleaned_data["tu_cult_mark"],
                                                                cult_name=form.cleaned_data["tu_cult_name"],
                                                                author=form.cleaned_data["tu_author_name"])
            if exists:
                # Prepare html for modal dialogue "Unit is already exists"
                template = loader.get_template("new_tu_forms/new_tu_form_1_exists.html", None)
                content = {"tu": exists[0], "type": "exists"}
                content = template.render(content)
                # Convert to json
                data = json.dumps({"type": "exists",
                                   "content": content})
                # Send response
                return HttpResponse(data, content_type='application/json')

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #    Check if TU is in Synonyms of exisitng
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            exists = ColSpecies.objects.col_species_is_synonym(table="collection_taxonomicunit",
                                                               genera=form.cleaned_data["tu_genera"],
                                                               species=form.cleaned_data["tu_species"],
                                                               infra_mark=form.cleaned_data["tu_infra_mark"],
                                                               infra_name=form.cleaned_data["tu_infra_name"],
                                                               author=form.cleaned_data["tu_author_name"], )
            if exists and form.cleaned_data["col_check"]:
                # Prepare tu object
                # Be aware there could be multiple TU linked with single ColSpecies object because of cultivars
                # Therefore filter by existing ColSpecies, and return first entry with infra name = None
                tu = TaxonomicUnit.objects.filter(col=exists).order_by(F('infra_name').asc(nulls_first=True))[0]
                # Prepare html for modal dialogue
                template = loader.get_template("new_tu_forms/new_tu_form_1_exists.html", None)
                content = {"tu": tu, "type": "exists_syn", "form": form.cleaned_data}
                content = template.render(content)

                # Prepare data for populating form fields with basionym in js
                data = model_to_dict(tu, fields=['genera_name', 'species_name', 'infra_mark', 'infra_name', 'author'])

                # Convert to json
                resp = json.dumps({"type": "exists_syn",
                                   "content": content,
                                   "data": data})
                # Send response
                return HttpResponse(resp, content_type='application/json')

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            # Case when COL check is not required or Hybrid
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (not form.cleaned_data["col_check"]) or form.cleaned_data["is_hybrid"]:

                c_data = form.cleaned_data

                # Prepare above genera taxonomy dictionary
                family = form.cleaned_data.get("col_family")

                taxonomy = None
                if family:
                    try:
                        taxonomy_q = ColTaxonomy.objects.get(col_family__iexact=family)
                    except Taxonomy.DoesNotExist:
                        return HttpResponse(json.dumps({"type": "wrong_family"}), content_type='application/json')

                    # Construct taxonomy dict
                    taxonomy = taxonomy_obj_to_dict(taxonomy_q)
                else:
                    return HttpResponse(json.dumps({"type": "wrong_family"}), content_type='application/json')

                # Add taxonomy data to form data
                c_data.update(taxonomy)

                # Save cleaned data to session
                # if cleaned data is already in session delete it before saving
                new_tu_form_clean_session(request)

                request.session["new_tu_form_1_cleaned_data"] = c_data
                request.session["col_type"] = "Not_col_or_hybrid"
                return HttpResponse(json.dumps({"type": "not_col_or_hybrid"}), content_type='application/json')

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #    Check TU against COL (Exact Match)
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            col_res = ColSpecies.objects.col_species_exact_match(genera=form.cleaned_data["tu_genera"],
                                                                 species=form.cleaned_data["tu_species"],
                                                                 infra_name=form.cleaned_data["tu_infra_name"],
                                                                 infra_mark=form.cleaned_data["tu_infra_mark"],
                                                                 author=form.cleaned_data["tu_author_name"])

            """
            At this time we know that inputted name is not in main database (not in CollectionTU)
            also it is not a synonym of an existing TU. Therefor we treat it as new entry.
            There could be 2 possibilities:
            
            1. The name is in COL and is accepted ==> save data to session and go to next form
            2. The names is in COL and is synonym ==> propose to replace synonym with basionym 
            """

            if col_res:  # == name is in COL
                # ========================================
                # Case 1. The name is in COL and accepted
                # ========================================
                if col_res.col_status == 'accepted':
                    # Clean session from other previous form data
                    new_tu_form_clean_session(request)

                    # Get taxonomy above genera from col_res foreign key
                    taxonomy = taxonomy_obj_to_dict(col_res.col_family)

                    # Save ColSpecies object id to session
                    request.session["col_res"] = col_res.col_id
                    request.session["col_type"] = "Accepted"

                    # Save Form cleaned data for persistent re rendering of a form if user will return to it.
                    c_data = form.cleaned_data
                    c_data.update(taxonomy)
                    request.session["new_tu_form_1_cleaned_data"] = c_data

                    # Initiate RedList and Tropicos queries
                    # Clean session from previous red_list and tropicos data
                    if "red_list" in request.session.keys():
                        del request.session["red_list"]

                    if "tropicos" in request.session.keys():
                        del request.session["tropicos"]

                    # !!! Important. We have to save session explicitly before
                    # RedList asynchronous call
                    request.session.save()

                    # Construct species name
                    name = "{} {} {} {} {}".format(
                        form.cleaned_data["tu_genera"].capitalize(),
                        form.cleaned_data["tu_species"],
                        form.cleaned_data["tu_infra_mark"],
                        form.cleaned_data["tu_infra_name"],
                        form.cleaned_data["tu_author_name"]
                    )

                    # Remove None in name if exists
                    name = re.sub('None', ' ', name)
                    name = re.sub(' +', ' ', name)

                    # Make red list api query
                    # check if it is not running already
                    if "red_list" in request.session.keys():
                        if request.session["red_list"]["active_status"] == "Active":
                            # cancel task
                            task_id = request.session["red_list"]["task_id"]
                            app.control.revoke(task_id, terminate=True)
                        del request.session['red_list']
                        request.session.save()

                    redlist_query_async.delay(request.session.session_key,
                                              genera=form.cleaned_data["tu_genera"],
                                              species=form.cleaned_data["tu_species"],
                                              subspecies=form.cleaned_data["tu_infra_name"],
                                              full_name=name
                                              )

                    # Prepare asynchronous Tropicos api call
                    # check if it is not running already
                    if "tropicos" in request.session.keys():
                        if request.session["tropicos"]["active_status"] == "Active":
                            # cancel task
                            task_id = request.session["tropicos"]["task_id"]
                            app.control.revoke(task_id, terminate=True)
                        del request.session['tropicos']
                        request.session.save()

                    # Make Tropicos api call
                    tropicos_query_async.delay(request.session.session_key,
                                               genera=form.cleaned_data["tu_genera"],
                                               species=form.cleaned_data["tu_species"],
                                               infra_name=form.cleaned_data["tu_infra_name"],
                                               infra_mark=form.cleaned_data["tu_infra_mark"],
                                               author=form.cleaned_data["tu_author_name"]
                                               )

                    # Prepare response with type = Accepted
                    content = {"type": "Accepted"}
                    return HttpResponse(json.dumps(content), content_type='application/json')

                # ========================================
                # Case 2. The name is in COL but synonym
                # ========================================
                # Prevent synonyms match for input without specified author ==> force full text search
                if col_res.col_status == 'synonym' and form.cleaned_data["tu_author_name"]:
                    '''Return modal with prompt about synonym'''
                    # Get accepted name (basionym)
                    col_bas = col_res.col_accepted
                    col_res_dict = model_to_dict(col_bas,
                                                 fields=['col_genera', 'col_species_name',
                                                         'col_infra_mark', 'col_infra_name', 'col_author'])
                    context = {"form": form.cleaned_data, "col": col_bas}

                    # Load template and render it
                    template = loader.get_template(
                        "new_tu_forms/new_tu_form_1_col_synonym.html", None)
                    content_render = template.render(context)

                    # Prepare json object with content and send it as response
                    content = json.dumps({"type": "col_syn",
                                          "content": content_render,
                                          "data": col_res_dict})
                    return HttpResponse(content, content_type='application/json')

            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #    Check TU against COL (Fuzzy Match)
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            '''
            Now we checked that given scientific name is absent in main table `TaxonomicUnit` and in COL `ColSpecies`
            Therefore we assume, that name contains typos or it is illegal name.
            So we need to perform fuzzy search across `ColSpeceis.col_full_name` and to provide possible replacements
            or typos fixes.
            '''
            col_res = ColSpecies.objects.search_alike(genera=form.cleaned_data["tu_genera"],
                                                      species=form.cleaned_data["tu_species"],
                                                      infra_name=form.cleaned_data["tu_infra_name"],
                                                      infra_mark=form.cleaned_data["tu_infra_mark"],
                                                      author=form.cleaned_data["tu_author_name"])\
                .prefetch_related("col_accepted")

            # Prepare data for js script (to populate the from with accepted variant)
            data = {}
            for i in range(len(col_res)):
                data.update({str(i): {"syn": None, "bas": None}})
                row = col_res[i]
                if row.col_status == "synonym":
                    data[str(i)]["syn"] = model_to_dict(row, fields=['col_genera', 'col_species_name',
                                                                     'col_infra_mark', 'col_infra_name', 'col_author'])
                    data[str(i)]["bas"] = model_to_dict(row.col_accepted, fields=['col_genera', 'col_species_name',
                                                                                  'col_infra_mark', 'col_infra_name',
                                                                                  'col_author'])
                else:
                    data[str(i)]["bas"] = model_to_dict(row, fields=['col_genera', 'col_species_name',
                                                                     'col_infra_mark', 'col_infra_name', 'col_author'])

            context = {"form": form.cleaned_data, "data": data}

            # Load template and render it
            template = loader.get_template(
                "new_tu_forms/new_tu_form_1_col_fuzzy.html", None)
            content_render = template.render(context)

            # Prepare json object with content and send it as response
            content = json.dumps({"type": "col_fuzzy",
                                  "content": content_render,
                                  "data": data})

            return HttpResponse(content, content_type='application/json')

        else:
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            #  Process invalid form
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            return HttpResponse(form, content_type='application/json')


@login_required
@permission_required(['collection.add_obligatoryinfo', 'collection.add_vernacular'],
                     login_url=reverse_lazy("rights_required"))
def new_tu_form_2(request):
    """
    Third from for new TU entering
    Obligatory Info
    :param request: 
    :return: 
    """""

    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':

        # Check if NewTuForm 1 was completed and is in session
        # and is not empty
        # if ino redirect to NewTuForm 1
        if "new_tu_form_1_cleaned_data" not in request.session.keys():
            return HttpResponseRedirect(reverse("new_tu_form_1"))
        # session contains NewTuForm1 data object, now check if it is not empty
        elif not any(v is not None for v in request.session["new_tu_form_1_cleaned_data"]):
            return HttpResponseRedirect(reverse("new_tu_form_1"))

        # Prepare data for rendering if session contains info about previous attempt

        if "new_tu_form_2_cleaned_data" in request.session.keys():
            data = request.session["new_tu_form_2_cleaned_data"]

            # Check if data is filled
            if any(v is not None for dic in data for v in dic.values()):
                for i in range(len(data)):
                    data[i].update({"form_id": "form-" + str(i)})
                    data[i].update({"vern_id": "id_form-" + str(i) + "-tu_vernacular"})
                    data[i].update({"lang_id": "id_form-" + str(i) + "-tu_vern_lang"})
                    # Set names
                    data[i].update({"vern_name": "form-" + str(i) + "-tu_vernacular"})
                    data[i].update({"lang_name": "form-" + str(i) + "-tu_vern_lang"})
                tot_n = len(data)
            else:
                # Data in session is empty
                data = []
                tot_n = 0
        else:
            # Session does not contain data from NewTuForm2
            data = []
            tot_n = 0

        # Prepare data for rendering and render
        data_render = {"data": data,
                       "tot_n": tot_n,
                       "form": NewTuForm2()}
        return render(request, 'new_tu_forms/new_tu_form_2.html', data_render)

    # Request is POST
    else:
        # Prepare data for formset
        post_data = request.POST.get("form_data", None)
        post_data = json.loads(post_data)

        data = {
            'form-TOTAL_FORMS': str(len(post_data)),
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
        }
        for dat in post_data:
            data.update(dat)

        # Create formset
        form2_form_factor = formset_factory(NewTuForm2, formset=BaseVernacularFormSet)
        formset = form2_form_factor(data)
        formset.is_valid()

        # Check if formset is valid
        if not formset.is_valid():
            return HttpResponse(json.dumps({"type": "invalid"}),
                                content_type='application/json')

        # Now we know that formset is valid
        # save cleaned data to session and send confirmation
        request.session["new_tu_form_2_cleaned_data"] = formset.cleaned_data
        return HttpResponse(json.dumps({"type": "accepted"}), content_type='application/json')


@login_required
@permission_required(['collection.add_taxonomicunit', 'collection.add_vernacular'],
                     login_url=reverse_lazy("rights_required"))
def new_tu_form_3(request):
    """
    Second from for new TU entering
    Vernacular data (could be multiple vernaculars for one TU)
    :param request: 
    :return: 
    """""

    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':

        # Check if NewTuForm 1 was completed and is in session
        # and is not empty
        # if ino redirect to NewTuForm 1
        if "new_tu_form_1_cleaned_data" not in request.session.keys():
            return HttpResponseRedirect(reverse("new_tu_form_1"))
        # session contains NewTuForm1 data object, now check if it is not empty
        elif not any(v is not None for v in request.session["new_tu_form_1_cleaned_data"]):
            return HttpResponseRedirect(reverse("new_tu_form_1"))

        # Prepare data for rendering if session contains info about previous attempt
        if "new_tu_form_3_cleaned_data" in request.session.keys():
            data = request.session["new_tu_form_3_cleaned_data"]

            # Check if data is filled
            if any(v is not None for v in data.values()):
                form = NewTuForm3(data)
            else:
                form = NewTuForm3()
        else:
            form = NewTuForm3()
        return render(request, 'new_tu_forms/new_tu_form_3.html', {"form": form})

    # Request is POST
    else:
        # Prepare data for formset
        post_data = request.POST.get("form_data", None)
        post_data = json.loads(post_data)
        # Create formset
        form = NewTuForm3(post_data)

        # Check if formset is valid
        if not form.is_valid():
            return HttpResponse(json.dumps({"type": "invalid"}),
                                content_type='application/json')

        # Now we know that formset is valid
        # save cleaned data to session and send confirmation
        request.session["new_tu_form_3_cleaned_data"] = form.cleaned_data

        # add full categories names to new_tu_form_3_cleaned_data (now just abbreviations)
        # as cenotype is a list we have to prepare full choices beforehand
        cen_choice_full = []
        for ch in form.cleaned_data["tu_cenotype"]:
            ch_full = (full_choice_from_abbr(ObligatoryInfo.cenotype_choices, ch))
            cen_choice_full.append(ch_full)

        # Prepare dictionary with full choices names
        cat_full_names = {
            "tu_salinity_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.salin_choices,
                                                                         form.cleaned_data["tu_salinity"]),
            "tu_light_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.light_choices,
                                                                         form.cleaned_data["tu_light"]),
            "tu_fertility_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.fertil_choices,
                                                                         form.cleaned_data["tu_fertility"]),
            "tu_cenotype_full": cen_choice_full,
            "tu_moisture_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.moisture_choices,
                                                                         form.cleaned_data["tu_moisture"]),
            "tu_raunc_form_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.raunc_form_choices,
                                                                         form.cleaned_data["tu_raunc_form"]),
            "tu_acidity_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.acidity_choices,
                                                                         form.cleaned_data["tu_acidity"]),
            "tu_phenotype_full":
                "" if form.cleaned_data == "" else full_choice_from_abbr(ObligatoryInfo.pheno_choices,
                                                                         form.cleaned_data["tu_phenotype"]),
        }

        # Count total filled choices to show percentage
        filled_choices = [v for k, v in cat_full_names.items() if v not in ["", []]]
        filled_percent = round(len(filled_choices) / len(cat_full_names) * 100)

        # Update session form data with cat_full_names
        request.session["new_tu_form_3_cleaned_data"].update(cat_full_names)
        request.session["new_tu_form_3_cleaned_data"].update({"filled_percent": filled_percent})

        return HttpResponse(json.dumps({"type": "accepted"}), content_type='application/json')


@login_required
@permission_required(['collection.add_taxonomicunit', 'collection.add_nonobligatoryinfo'],
                     login_url=reverse_lazy("rights_required"))
def new_tu_form_4(request):
    """
    Forth from for new TU entering
    Non obligatory info (could be multiple info for one TU)
    :param request: 
    :return: 
    """""

    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':

        # Check if NewTuForm 1 was completed and is in session
        # and is not empty
        # if ino redirect to NewTuForm 1
        if "new_tu_form_1_cleaned_data" not in request.session.keys():
            return HttpResponseRedirect(reverse("new_tu_form_1"))
        # session contains NewTuForm1 data object, now check if it is not empty
        elif not any(v is not None for v in request.session["new_tu_form_1_cleaned_data"]):
            return HttpResponseRedirect(reverse("new_tu_form_1"))

        # Get all unique categories
        categories = NonObligatoryInfo.objects \
            .values('category').distinct().order_by('category')

        # Check if there are previous data in session
        if "new_tu_form_4_cleaned_data" in request.session.keys():
            data = request.session["new_tu_form_4_cleaned_data"]

            # Check if data is filled
            if any(v is not None for dic in data for v in dic.values()):
                for i in range(len(data)):
                    # Set id's
                    data[i].update({"form_id": "form-" + str(i)})
                    data[i].update({"cat_id": "id_form-" + str(i) + "-category"})
                    data[i].update({"ref_id": "id_form-" + str(i) + "-reference"})
                    data[i].update({"info_id": "id_form-" + str(i) + "-info"})
                    # Set names
                    data[i].update({"cat_name": "form-" + str(i) + "-category"})
                    data[i].update({"ref_name": "form-" + str(i) + "-reference"})
                tot_n = len(data)
            else:
                # Data in session is empty
                data = []
                tot_n = 0
        else:
            # Session does not contain data from NewTuForm2
            data = []
            tot_n = 0

        # Prepare data for rendering and render
        data_render = {"data": data,
                       "tot_n": tot_n,
                       "form": NewTuForm4(),
                       "categories": categories}

        return render(request, 'new_tu_forms/new_tu_form_4.html', data_render)

    # Request is POST
    else:
        # Prepare data for formset
        post_data = request.POST.get("form_data", None)
        post_data = json.loads(post_data)

        data = {
            'form-TOTAL_FORMS': str(len(post_data)),
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
        }
        for dat in post_data:
            data.update(dat)

        # Create formset
        form4_form_factor = formset_factory(NewTuForm4, formset=BaseObligInfoFormSet)
        formset = form4_form_factor(data)
        formset.is_valid()

        # Check if formset is valid
        if not formset.is_valid():
            return HttpResponse(json.dumps({"type": "invalid"}),
                                content_type='application/json')

        # Now we know that formset is valid
        # save cleaned data to session and send confirmation
        request.session["new_tu_form_4_cleaned_data"] = formset.cleaned_data
        return HttpResponse(json.dumps({"type": "accepted"}), content_type='application/json')


@login_required
@permission_required(['collection.add_taxonomicunit', 'collection.add_taxonomicunitimages'],
                     login_url=reverse_lazy("rights_required"))
def new_tu_form_5(request):
    """
    Fifth from for new TU entering
    Images and data (could be multiple images for one TU)
    :param request: 
    :return: 
    """""

    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':

        # Check if NewTuForm 1 was completed and is in session
        # and is not empty
        # if ino redirect to NewTuForm 1
        if "new_tu_form_1_cleaned_data" not in request.session.keys():
            return HttpResponseRedirect(reverse("new_tu_form_1"))
        # session contains NewTuForm1 data object, now check if it is not empty
        elif not any(v is not None for v in request.session["new_tu_form_1_cleaned_data"]):
            return HttpResponseRedirect(reverse("new_tu_form_1"))

        # Check if there are previous data in session
        if "new_tu_form_5_cleaned_data" in request.session.keys():
            data = request.session["new_tu_form_5_cleaned_data"]

            # Check if data is filled
            if any(v is not None for dic in data for v in dic.values()):
                for i in range(len(data)):
                    # Set id's
                    data[i].update({"form_id": "form-" + str(i)})
                    data[i].update({"author_id": "id_form-" + str(i) + "-author"})
                    data[i].update({"date_id": "id_form-" + str(i) + "-date"})
                    data[i].update({"caption_id": "id_form-" + str(i) + "-caption"})
                    data[i].update({"description_id": "id_form-" + str(i) + "-description"})
                    data[i].update({"img_id": "id_form-" + str(i) + "-img"})
                    data[i].update({"rate_id": "id_form-" + str(i) + "-rate"})
                    data[i].update({"main_scr_id": "id_form-" + str(i) + "-main_scr"})
                    # Set names
                    data[i].update({"author_name": "form-" + str(i) + "-author"})
                    data[i].update({"date_name": "form-" + str(i) + "-date"})
                    data[i].update({"caption_name": "form-" + str(i) + "-caption"})
                    data[i].update({"description_name": "form-" + str(i) + "-description"})
                    data[i].update({"img_name": "form-" + str(i) + "-img"})
                    data[i].update({"rate_name": "form-" + str(i) + "-rate"})
                    data[i].update({"main_scr_name": "form-" + str(i) + "-main_scr"})
                tot_n = len(data)
            else:
                # Data in session is empty
                data = []
                tot_n = 0
        else:
            # Session does not contain data from NewTuForm2
            data = []
            tot_n = 0

        # Prepare data for rendering and render
        data_render = {"data": data,
                       "tot_n": tot_n,
                       "form": NewTuForm5(),
                       }

        return render(request, 'new_tu_forms/new_tu_form_5.html', data_render)
    else:
        # Get data from request and prepare dictionary for formset
        post_data = request.POST.get("form_data", None)
        post_data = json.loads(post_data)

        data = {
            'form-TOTAL_FORMS': str(len(post_data)),
            'form-INITIAL_FORMS': '0',
            'form-MAX_NUM_FORMS': '',
        }

        for dat in post_data:
            data.update(dat)

        '''
        Now we need to process previously uploaded files
        the point is, if the form was rendered with default images,
        the request will not contain those files (just imitations). In such case request data
        for this sub form will contain changed = False and init_file_path attributes.
        So, we need to loop over request data, if changed is false,
        we will construct uploaded file object from local file and then
        add it to request.FILES
        '''
        files = request.FILES
        for datum in post_data:
            if not datum["changed"]:
                # Get file pat and name
                prev_im_path = datum["prev_im_path"]
                img_id = datum["img_id"]

                # Read file from disc and add replace file mock in files
                uploaded_file = default_storage.open(prev_im_path)
                files[img_id] = uploaded_file

        # Create formset
        form5_form_factor = formset_factory(NewTuForm5, formset=BaseTuImagesFormSet)
        formset = form5_form_factor(data, files)

        # Check if formset is valid
        if not formset.is_valid():
            return HttpResponse(json.dumps({"type": "invalid"}),
                                content_type='application/json')

        '''
        Next code will run if formset is valid
        1. we need to save each file into session specific folder in media/tmp/<session_id>
        2. create a thumbnail of this image
        3. add paths of each file into cleaned data
        4. remove file objects from cleaned data
        5. serialize date object
        6. save altered cleaned data to session
        *** There should be a Celery task for regular media/tmp/ cleaning. It should check folders
            in media/tmp/ against active sessions and if session is absent delete corresponding folder.
            So, each time when we want to save files we will create temporary folder with name = session_id
            each hour or two hours Celery will check if the session with that id is active and if not it will
            delete that folder
        '''
        cleaned_data = formset.cleaned_data

        # Get session key
        sk = request.session.session_key

        # Construct tmp path
        tmp_path = "tmp/new_tu_forms/" + sk

        # Remove tmp folder if already exists
        # and create empty
        tmp_path_full = os.path.join(settings.MEDIA_ROOT, tmp_path)
        if os.path.exists(tmp_path_full):
            shutil.rmtree(tmp_path_full)
            pathlib.Path(tmp_path_full).mkdir(mode=0o777, parents=True, exist_ok=True)
        else:
            pathlib.Path(tmp_path_full).mkdir(mode=0o777, parents=True, exist_ok=True)

        for i in range(len(cleaned_data)):
            # Get uploaded file object
            upload = cleaned_data[i]["img"]

            # Construct file_name and path
            f_name_init = "init_size_%03d.jpg" % i
            f_name_thumb = "thumb_%03d.jpg" % i
            f_path_init = os.path.join(tmp_path, f_name_init)
            f_path_thumb = os.path.join(tmp_path, f_name_thumb)

            # ==========================
            # Save initial image
            # ==========================

            # Prepare full path
            img_init_path_full = os.path.join(settings.MEDIA_ROOT, f_path_init)

            # Read image and convert to rgb
            try:
                im = Image.open(upload.file)
                # Reorient image according to EXIF
                im = ImageOps.exif_transpose(im)

                im = im.convert('RGB')

                # Save image
                im.save(img_init_path_full, quality=95)
                # Set permissions (include group or celery will not be able to delete this file afterward)
                pathlib.Path(img_init_path_full).chmod(0o777)
            except IOError:
                # Return
                return HttpResponse(json.dumps({"type": "img_error",
                                                "message": "Произошла ошибка при обработке изображения № %d" % i}),
                                    content_type='application/json')

            cleaned_data[i]["img_init_path_full"] = img_init_path_full
            cleaned_data[i]["img_init_url"] = default_storage.url(f_path_init)

            # ==========================
            # Save thumbnail image
            # ==========================
            # Get image size
            width, height = im.size

            # Calculate new height and width (max dim = 500 px)
            if width > height:
                w_new = 500
                h_new = 500 / width * height
            else:
                h_new = 500
                w_new = 500 / height * width

            try:
                # Resize image
                im.thumbnail((int(w_new), int(h_new)), Image.ANTIALIAS)

                # Prepare full path
                img_thumb_path_full = os.path.join(settings.MEDIA_ROOT, f_path_thumb)

                # Save image
                im.save(img_thumb_path_full, quality=80)
                # Set permissions (include group or celery will not be able to delete this file afterward)
                pathlib.Path(img_thumb_path_full).chmod(0o777)
            except IOError:
                # Return
                return HttpResponse(json.dumps({"type": "img_error",
                                                "message": "Произошла ошибка при обработке изображения № %d" % i}),
                                    content_type='application/json')

            cleaned_data[i]["img_thumb_path_full"] = img_thumb_path_full
            cleaned_data[i]["img_thumb_url"] = default_storage.url(f_path_thumb)

            # =============================================
            # Remove file object and convert date to string
            # =============================================
            cleaned_data[i]["date"] = str(cleaned_data[i]["date"])
            del cleaned_data[i]["img"]

        # save cleaned data to session and send confirmation
        request.session["new_tu_form_5_cleaned_data"] = cleaned_data
        return HttpResponse(json.dumps({"type": "accepted"}), content_type='application/json')


@login_required
@permission_required(['collection.add_taxonomicunit', 'collection.add_obligatoryinfo',
                      'collection.add_nonobligatoryinfo', 'collection.add_vernacular',
                      'collection.add_taxonomicunitimages'], login_url=reverse_lazy("rights_required"))
def new_tu_form_conf(request):
    """
    The form for confirmation of information in previous
    forms
    :param request:
    :return:
    """
    # if a GET (or any other method) we'll create a blank form
    if request.method != 'POST':
        '''
        1. Check if NewTuForm1 data is in session
        2. Prepare dict with information from forms 1 - 5, red_list query and other
        3. Render response 
        '''
        # Check if NewTuForm 1 was completed and is in session
        # and is not empty
        # if ino redirect to NewTuForm 1
        if "new_tu_form_1_cleaned_data" not in request.session.keys():
            return HttpResponseRedirect(reverse("new_tu_form_1"))
        # session contains NewTuForm1 data object, now check if it is not empty
        elif not any(v is not None for v in request.session["new_tu_form_1_cleaned_data"]):
            return HttpResponseRedirect(reverse("new_tu_form_1"))

        # ===========================================
        # Collect data from forms and async queries
        # ===========================================
        # Get data from form 1 (we know that it is in session)
        data_render = {
            "f1": request.session["new_tu_form_1_cleaned_data"]
        }

        # Check if data from forms 2-5 is in session (could be not)
        # add it if true
        # NewTuForm2
        if "new_tu_form_2_cleaned_data" in request.session.keys():
            data_render.update({"f2": request.session["new_tu_form_2_cleaned_data"]})
        # NewTuForm3
        if "new_tu_form_3_cleaned_data" in request.session.keys():
            data_render.update({"f3": request.session["new_tu_form_3_cleaned_data"]})
        # NewTuForm4
        if "new_tu_form_4_cleaned_data" in request.session.keys():
            data_render.update({"f4": request.session["new_tu_form_4_cleaned_data"]})
        # NewTuForm5
        if "new_tu_form_5_cleaned_data" in request.session.keys():
            data_render.update({"f5": request.session["new_tu_form_5_cleaned_data"]})
        # red_list
        if "red_list" in request.session.keys():
            data_render.update({"red_list": request.session["red_list"]})
        # tropicos
        if "tropicos" in request.session.keys():
            data_render.update({"tropicos": request.session["tropicos"]})

        # Here could be other additional data (for example from Tropicos)

        # COL data
        # first check if col res is in session
        if "col_res" in request.session.keys():
            # Get ColSpecies queryset
            col_res = ColSpecies.objects.get(pk=request.session["col_res"])
            data_render.update({"col_res": col_res})

            # Get all synonyms as separate queryset
            col_syn = ColSpecies.objects.filter(col_accepted=col_res)
            data_render.update({"col_syn": col_syn})

        # second add col type
        data_render.update({"col_type": request.session["col_type"]})

        # Return rendered template
        return render(request, 'new_tu_forms/new_tu_from_conf.html', data_render)

    else:
        '''
        The request is POST
        1. Process taxonomic data (get link to COL family etc.)
        2. Process images (different sizes and types)
        3. Save all taxonomic, info and media data to tables
        4. Save date and author of entry
        5. Clean session if saving was successful
        6. Redirect to "thank you page"
        '''

        # =================================================================================
        # Prepare taxonomic unit model entry and save
        # =================================================================================
        tax_unit_data = request.session["new_tu_form_1_cleaned_data"]
        # Get reference to ColTaxonomy id
        if "col_family_id" in tax_unit_data:
            family_id = ColTaxonomy.objects.get(col_family_id=tax_unit_data["col_family_id"])
        else:
            raise Exception('Something wrong with COL Taxonomy family id. It is not in session data after all checks')

        # Prepare ColSpecies object for foreign key
        if "col_res" in request.session.keys():
            col_id = ColSpecies.objects.get(pk=request.session["col_res"])
        else:
            col_id = None

        # Initiate transaction
        try:
            with transaction.atomic():
                # Populate TaxonomicUnit object
                tax_unit_mod = TaxonomicUnit(genera_name=tax_unit_data["tu_genera"],
                                             species_name=tax_unit_data["tu_species"],
                                             infra_mark=tax_unit_data["tu_infra_mark"],
                                             infra_name=tax_unit_data["tu_infra_name"],
                                             cult_mark=tax_unit_data["tu_cult_mark"],
                                             cult_name=tax_unit_data["tu_cult_name"],
                                             hybrid_mark=tax_unit_data["is_hybrid"],
                                             author=tax_unit_data["tu_author_name"],
                                             family_col=family_id,
                                             input_author=request.user,
                                             col=col_id,
                                             )
                with reversion.create_revision():
                    tax_unit_mod.save()
                    reversion.set_user(request.user)
                    reversion.set_comment("Initial")
                tax_unit_id = tax_unit_mod.id

                # =================================================================================
                # Prepare Vernacular info model entry and save
                # =================================================================================
                vern_info = request.session.get("new_tu_form_2_cleaned_data")
                if vern_info is not None:
                    for vern_entry in vern_info:
                        vern_info_mod = Vernacular(tu_id=tax_unit_mod,
                                                   vernacular=vern_entry.get('tu_vernacular'),
                                                   language=vern_entry.get('tu_vern_lan')
                                                   )
                        with reversion.create_revision():
                            vern_info_mod.save()
                            reversion.set_user(request.user)
                            reversion.set_comment("Initial")

                # =================================================================================
                # Prepare Obligatory info model entry and save
                # =================================================================================
                obl_info = request.session.get("new_tu_form_3_cleaned_data")
                if obl_info is not None:
                    obl_info_mod = ObligatoryInfo(tu_id=tax_unit_mod,
                                                  raunc_form=obl_info.get('tu_raunc_form'),
                                                  moisture=obl_info.get('tu_moisture'),
                                                  light=obl_info.get('tu_light'),
                                                  fertility=obl_info.get('tu_fertility'),
                                                  salinity=obl_info.get('tu_salinity'),
                                                  acidity=obl_info.get('tu_acidity'),
                                                  phenotype=obl_info.get('tu_phenotype'),
                                                  cenotype=obl_info.get('tu_cenotype'),
                                                  )
                    with reversion.create_revision():
                        obl_info_mod.save()
                        reversion.set_user(request.user)
                        reversion.set_comment("Initial")
                # =================================================================================
                # Prepare NonObligatory info model entry and save
                # =================================================================================
                nonobl_info = request.session.get("new_tu_form_4_cleaned_data")
                if nonobl_info is not None:
                    for entry in nonobl_info:
                        nonobl_info_mod = NonObligatoryInfo(tu_id=tax_unit_mod,
                                                            input_date=date.today(),
                                                            input_auth=request.user,
                                                            reference=entry.get("reference"),
                                                            category=entry.get("category"),
                                                            info=entry.get("info")
                                                            )
                        with reversion.create_revision():
                            nonobl_info_mod.save()
                            reversion.set_user(request.user)
                            reversion.set_comment("Initial")

                # =================================================================================
                # Prepare RedListIUCN info model entry and save
                # =================================================================================
                redlist_info = request.session.get("red_list")

                if redlist_info is not None:
                    # Construct errors
                    if redlist_info.get("active_status") != "Done":
                        errors = RedListIUCN.ERROR
                    elif redlist_info.get("parser_error"):
                        errors = RedListIUCN.ERROR
                    elif redlist_info.get("distr_error") and redlist_info.get("cit_error"):
                        errors = RedListIUCN.FULL
                    elif redlist_info.get("distr_error") or redlist_info.get("cit_error"):
                        errors = RedListIUCN.PARTIAL
                    else:
                        errors = ""

                    # If redList query returned error
                    if errors == RedListIUCN.ERROR:
                        redlist_mod = RedListIUCN(tu_id=tax_unit_mod,
                                                  parsing_errors=errors,
                                                  last_check=date.today())
                        redlist_mod.save()
                    else:
                        # There were not fatal errors, so we can proceed
                        redlist_mod = RedListIUCN(tu_id=tax_unit_mod,
                                                  in_red_list=True if redlist_info.get("parser_status") ==
                                                                      "InRedList" else False,
                                                  parsing_errors=errors,
                                                  common_name=redlist_info.get("common_name"),
                                                  category=redlist_info.get("category"),
                                                  cat_interp=redlist_info.get("category_trans"),
                                                  criteria=redlist_info.get("criteria"),
                                                  citation=redlist_info.get("citation"),
                                                  distribution=redlist_info.get("distribution"),
                                                  pub_year=redlist_info.get("published_year"),
                                                  last_check=date.today())

                        with reversion.create_revision():
                            redlist_mod.save()
                            reversion.set_user(request.user)
                            reversion.set_comment("Initial")

                        # Add map file if exists
                        if redlist_info.get("map_url") is not None:
                            f_path = os.path.join(settings.BASE_DIR, redlist_info.get("map_url")[1:])
                            f = open(f_path)
                            map_file = File(f)
                            map_name = f"{tax_unit_mod.genera_name}_{tax_unit_mod.species_name}_{tax_unit_mod.id}.svg"
                            redlist_mod.distribution_map.save(map_name, map_file)

                        # If common name is present in saved model, add it to Vernaculars
                        if redlist_mod.common_name is not None:
                            Vernacular(tu_id=tax_unit_mod,
                                       vernacular=redlist_mod.common_name,
                                       language=Vernacular.ENGLISH
                                       ).save()
                # =================================================================================
                # Prepare Tropicos info model entry and save
                # =================================================================================
                trop_info = request.session.get("tropicos")
                if trop_info is not None:
                    trop_mod = TropicosInfo()
                    with reversion.create_revision():
                        trop_mod.populate(tax_unit_mod, trop_info)
                        reversion.set_user(request.user)
                        reversion.set_comment("Initial")

                # =================================================================================
                # Prepare TaxonomicUnitImages info entry and save
                # =================================================================================
                tu_img = request.session.get("new_tu_form_5_cleaned_data")
                if tu_img is not None:
                    for entry in tu_img:
                        tu_img_mod = TaxonomicUnitImages()

                        with reversion.create_revision():
                            tu_img_mod.add_image(tax_unit=tax_unit_mod, cleaned_data=entry, image=None)
                            reversion.set_user(request.user)
                            reversion.set_comment("Initial")

                # =================================================================================
                # Clean session data
                # =================================================================================
                session_keys = ["new_tu_form_1_cleaned_data", "new_tu_form_2_cleaned_data",
                                "new_tu_form_3_cleaned_data", "new_tu_form_4_cleaned_data",
                                "new_tu_form_5_cleaned_data", "tropicos", "red_list",
                                "col_type", "col_res"]

                for key in session_keys:
                    if key in request.session.keys():
                        del request.session[key]

                # Finally redirect to conformation page
                # As we will need to know accepted TU id in `new_tu_accept` view,
                # we will pass it as GET parameter (?name-<value>)
                return HttpResponseRedirect(f"%s?tu_id={tax_unit_mod.id}" % reverse("new_tu_accept"))

        except IntegrityError:
            return HttpResponseRedirect(reverse("new_tu_form_conf"))


@login_required
def new_tu_accept(request):
    # get tu_id parameter from GET
    tu_id = request.GET.get("tu_id")
    # Get full taxonomic name
    tu_mod = TaxonomicUnit.objects.get(pk=tu_id)
    return render(request, 'new_tu_forms/new_tu_accept.html', {"tu_mod": tu_mod})


@login_required
@permission_required('collection.add_individual', login_url=reverse_lazy("rights_required"))
def new_individual(request, tu_id):
    """Input new individual"""
    # Get user object or redirect to login
    if request.user.is_authenticated:
        user_obj = request.user
    else:
        return HttpResponseRedirect(reverse("account_login"))

    # Get individual object
    tu_obj = get_object_or_404(TaxonomicUnit, pk=tu_id)

    # Get coordinates of existent individuals
    exist_ind = serialize('geojson', Individual.objects.all().filter(tu_id=tu_obj, is_alive=True),
                          geometry_field='spatial_pos',
                          fields=('pk', 'field_number'))

    if request.method != 'POST':
        # Create a from
        form = NewIndividualForm(initial={"tu_id": tu_id, "input_author": user_obj,
                                          "department_id": user_obj.department.pk},
                                 admin_choices=None, city_choices=None)

        data = {"form": form,
                "tu": tu_obj,
                "exist_ind": exist_ind}

        # Render output and send
        return render(request, 'new_individual_forms/new_individual_input.html', data)

    else:
        # If origin city is selected request POST, prepare initial choices for form
        if request.POST.get("city"):
            city_id = request.POST.get("city")
            city_obj = PopulatedPlaces.objects.get(id=city_id)
            country_obj = SovCountries.objects.get(populatedplaces=city_obj)
            city_choices = PopulatedPlaces.objects.filter(sov_id=country_obj). \
                annotate(name_res=(Case(When(name_ru__isnull=False, then=F('name_ru')), default=F('name')))). \
                values_list('id', 'name_res').order_by('name_res')
            admin_choices = AdminRegions.objects.filter(sov_id=country_obj). \
                annotate(name_res=(Case(When(name_ru__isnull=False, then=F('name_ru')), default=F('name')))). \
                values_list('id', 'name_res').order_by('name_res')
        else:
            admin_choices = None
            city_choices = None

        # Create from with prepopulated tu_id
        form = NewIndividualForm(request.POST, admin_choices=admin_choices, city_choices=city_choices)

        if form.is_valid():
            with reversion.create_revision():
                form_data = form.save()
                reversion.set_user(request.user)
                reversion.set_comment("Initial")
            new_individual_id = form_data.id
            # Return redirect to conformation form
            return HttpResponseRedirect(f"%s?ind_id={new_individual_id}" % reverse("new_ind_accept"))
        else:
            # I do not know why, but normal flow of the filled form rendering does not work for
            # the spatial field. I.e. we can expect, that spatial field (which is hidden text input)
            # will contain spatial position. It is not so. The spatial input in rendered form is empty.
            # Therefore we will create `exist_ind` variable from cleaned data and provide it as a context.
            if form.cleaned_data.get("spatial_pos") is not None:
                initial_pos = form.cleaned_data.get("spatial_pos").geojson
            else:
                initial_pos = False
            if form.cleaned_data.get("origin_loc") is not None:
                initial_loc = form.cleaned_data.get("origin_loc").geojson
            else:
                initial_loc = False

            data = {"form": form,
                    "tu": tu_obj,
                    "exist_ind": exist_ind,
                    "initial_pos": initial_pos,
                    "initial_loc": initial_loc}

            return render(request, 'new_individual_forms/new_individual_input.html', data)


@login_required
def new_ind_accept(request):
    # get ind_id parameter from GET
    ind_id = request.GET.get("ind_id")
    ind_mod = get_object_or_404(Individual, pk=ind_id)
    data = {"ind_mod": ind_mod}
    return render(request, 'new_individual_forms/new_ind_accept.html', data)


def individual_profile(request, tu_id, ind_id):
    """
    The view for individual profile
    """
    # Get tu and individual objects
    tu_obj = get_object_or_404(TaxonomicUnit, pk=tu_id)
    ind_obj = get_object_or_404(Individual, tu_id=tu_obj, pk=ind_id)

    data = {
        "tu_obj": tu_obj,
        "ind_obj": ind_obj
    }

    return render(request, 'individual_profile/individual_profile.html', data)


@login_required
@permission_required('collection.change_individual', login_url=reverse_lazy("rights_required"))
def edit_individual(request, ind_id):
    # TODO There are a lot of duplicated SQL queries, maybe this view could be optimized
    # Get instance of Individual model and Taxonomic Unit
    ind_obj = get_object_or_404(Individual.objects.select_related().prefetch_related(), pk=ind_id)

    # Get existent individuals of the same TU
    exist_ind = serialize('geojson',
                          Individual.objects.all().filter(tu_id=ind_obj.tu_id, is_alive=True).exclude(pk=ind_obj.id),
                          geometry_field='spatial_pos',
                          fields=('pk', 'field_number'))
    spatial_pos_for_map = serialize('geojson',
                                    Individual.objects.all().filter(pk=ind_obj.id),
                                    geometry_field='spatial_pos',
                                    fields=('pk', 'field_number'))
    initial_origin_loc = serialize('geojson',
                                   Individual.objects.all().filter(pk=ind_obj.id),
                                   geometry_field='origin_loc',
                                   fields=('pk', 'field_number'))

    # If origin city is selected prepare city and admin choices for the form
    if ind_obj.city:
        city_choices = PopulatedPlaces.objects.filter(sov_id=ind_obj.city.sov_id).\
            annotate(name_res=(Case(When(name_ru__isnull=False, then=F('name_ru')), default=F('name')))).\
            values_list('id', 'name_res').order_by('name_res')
        admin_choices = AdminRegions.objects.filter(sov_id=ind_obj.city.sov_id).\
            annotate(name_res=(Case(When(name_ru__isnull=False, then=F('name_ru')), default=F('name')))).\
            values_list('id', 'name_res').order_by('name_res')
    else:
        admin_choices = None
        city_choices = None

    if request.method != 'POST':
        # Construct data list for form initial population
        init_data = {
            "tu_id": ind_obj.tu_id,
            "field_number": ind_obj.field_number,
            "is_alive": ind_obj.is_alive,
            "income_date": ind_obj.income_date,
            "income_material": ind_obj.income_material,
            "source": ind_obj.source,
            "out_date": ind_obj.out_date,
            "department_id": ind_obj.department_id_id,  # Here we need numeric value
            "cult_type": ind_obj.cult_type,
            "spatial_pos": ind_obj.spatial_pos,
            "input_author": ind_obj.input_author,
            "origin": ind_obj.origin,
            "city": ind_obj.city.id if ind_obj.city else 6268,
            "admin": ind_obj.city.admin_id.id if ind_obj.city else 976,
            "country": ind_obj.city.sov_id.id if ind_obj.city else 34
        }
        form = NewIndividualForm(init_data, admin_choices=admin_choices, city_choices=city_choices)

        data = {"form": form,
                "ind_obj": ind_obj,
                "exist_ind": exist_ind,
                "spatial_pos_for_map": spatial_pos_for_map,
                "initial_origin_loc": initial_origin_loc}

        # Render output and send
        return render(request, 'individual_profile/edit_profile_form.html', data)

    else:
        # Create from with prepopulated tu_id
        form = NewIndividualForm(request.POST, instance=ind_obj, admin_choices=admin_choices, city_choices=city_choices)
        if form.is_valid():
            with reversion.create_revision():
                form.save()
                reversion.set_user(request.user)
                reversion.set_comment("Edit")
            # Return redirect to conformation form
            return HttpResponseRedirect(reverse("individual_profile",
                                                kwargs={"tu_id": ind_obj.tu_id.id, "ind_id": ind_obj.id}))
        else:
            data = {"form": form,
                    "ind_obj": get_object_or_404(Individual, pk=ind_id),
                    "exist_ind": exist_ind,
                    "spatial_pos_for_map": spatial_pos_for_map,
                    "initial_origin_loc": initial_origin_loc}
            return render(request, 'individual_profile/edit_profile_form.html', data)


def tu_profile(request, tu_id):
    # Get TU object
    tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related().prefetch_related(), pk=tu_id)

    # Get vernaculars
    vernaculars = tu_obj.vernacular_set.all()

    # COL data is not required as it is one-to-one field (i.e. simply accessible from template)

    # Get images
    images = None
    if tu_obj.has_images():
        images = tu_obj.taxonomicunitimages_set.order_by("-img_rate").all()

    # Get individuals
    individuals = Individual.objects.filter(tu_id=tu_obj).order_by("income_date", "out_date")
    # Prepare individuals in json format for D3 plotting
    ind_json = serialize("json", individuals, fields=('pk', 'income_date', 'out_date', 'input_date'))
    # Count alive and dead individuals
    alive_counts = individuals.order_by('is_alive').values('is_alive').annotate(total=Count('is_alive'))
    # Create geojson with locations
    exist_ind = serialize('geojson',
                          individuals,
                          geometry_field='spatial_pos',
                          fields=('pk', 'field_number'))

    # Get obligatory info
    oblig_info_q = ObligatoryInfo.objects.filter(tu_id=tu_obj)
    if oblig_info_q.exists():
        oblig_info = oblig_info_q.get(tu_id=tu_obj)
    else:
        oblig_info = None

    # Get nonobligatory info
    non_oblig_info = tu_obj.nonobligatoryinfo_set.prefetch_related().order_by("category")
    # Get list of categories
    non_oblig_cats = non_oblig_info.values('category').distinct()

    # Get catalogue of life data (check if tu has relation with ColSpecies i.e. the COL check was done)
    if tu_obj.col:
        syns = ColSpecies.objects.filter(col_accepted=tu_obj.col)
    else:
        syns = None

    data = {
        "tu_obj": tu_obj,
        "images": images,
        "vernaculars": vernaculars,
        "individuals": individuals,
        "ind_json": ind_json,
        "alive_counts": alive_counts,
        "exist_ind": exist_ind,
        "oblig_info": oblig_info,
        "non_oblig_info": non_oblig_info,
        "non_oblig_cats": non_oblig_cats,
        "syns": syns,
    }
    return render(request, 'tu_profile/tu_profile.html', data)


@login_required
@permission_required('collection.add_vernacular', login_url=reverse_lazy("rights_required"))
def add_vernacular(request, tu_id):
    tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related(), pk=tu_id)
    prev_vernaculars = tu_obj.vernacular_set.all()

    if request.method != 'POST':
        vern_form = NewTuForm2(initial={"tu_vern_lan": Vernacular.RUSSIAN})
        data = {
            "tu_obj": tu_obj,
            "vern_form": vern_form,
            "prev_vernaculars": prev_vernaculars,
        }

        return render(request, 'vernacular_forms/add_vernacular.html', data)

    else:
        # POST request
        tu_id = request.POST.get("tu_id")
        tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related(), pk=tu_id)
        vern_form = NewTuForm2(request.POST)

        # Initiate dummy validation and check for duplicated names
        if vern_form.is_valid():
            new_vern_name = vern_form.cleaned_data.get("tu_vernacular")
            if (new_vern_name,) in prev_vernaculars.values_list("vernacular"):
                # Add arror to form (such vernacular already exists)
                vern_form.add_error(None, "Вводимое вернакулярное название дублирует ранее внесенные варианты")
                # Here form should become invalid so next validity check will throw False

        # Here we will check validity once more, it will fail if there were duplicates
        if vern_form.is_valid():
            # The form is valid => save the data
            new_vern = Vernacular(tu_id=tu_obj,
                                  vernacular=vern_form.cleaned_data.get("tu_vernacular"),
                                  language=vern_form.cleaned_data.get("tu_vern_lan"))
            with reversion.create_revision():
                new_vern.save()
                reversion.set_user(request.user)
                reversion.set_comment("Initial")

            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": tu_id}))
        else:
            # The form contains erros
            data = {
                "tu_obj": tu_obj,
                "vern_form": vern_form,
                "prev_vernaculars": prev_vernaculars,
            }

            return render(request, 'vernacular_forms/add_vernacular.html', data)


@login_required
@permission_required('collection.change_vernacular', login_url=reverse_lazy("rights_required"))
def edit_vernacular(request, tu_id):
    tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related(), pk=tu_id)
    vern_data = tu_obj.vernacular_set.all().values()

    # Redirect to `add_vernacular` if vern_data is empty
    if not len(vern_data):
        return HttpResponseRedirect(reverse("add_vernacular", kwargs={"tu_id": tu_id}))

    # Prepare initial data for formset
    form_init_data = [{"vern_id": x.get("id"),
                       "tu_vernacular": x.get("vernacular"),
                       "tu_vern_lan": x.get("language"),
                       "DELETE": False} for x in vern_data]
    # Prepare the formset
    vern_forms_factory = formset_factory(NewTuForm2, formset=BaseVernacularFormSet, extra=0, can_delete=True)

    if request.method != "POST":
        vern_forms = vern_forms_factory(initial=form_init_data)

        data = {
            "tu_obj": tu_obj,
            "vern_forms": vern_forms,
        }

        return render(request, 'vernacular_forms/edit_vernacular.html', data)
    else:
        # POST request
        tu_id = request.POST.get("tu_id")
        tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related(), pk=tu_id)

        # Prepare data for formset
        vern_formset = vern_forms_factory(request.POST, initial=form_init_data)

        if vern_formset.is_valid():
            cleaned_data = vern_formset.cleaned_data
            # Change form keys
            forms_data = [{"vernacular": x.get("tu_vernacular"),
                           "language": x.get("tu_vern_lan"),
                           "vern_id": x.get("vern_id"),
                           "DELETE": x.get("DELETE")} for x in cleaned_data]
            for dat, init_dat in zip(forms_data, form_init_data):
                # Check for deletion or update
                if dat.get("DELETE"):
                    # Delete object
                    get_object_or_404(Vernacular, pk=dat.get("vern_id"), tu_id=tu_obj).delete()
                else:
                    # Update object if data was changed. Here we can not use `has changed` method because
                    # our `clean` method alter values. Therefore we have to compare data directly.
                    if (dat.get("vernacular") != init_dat.get("tu_vernacular") or
                            dat.get("language") != init_dat.get("tu_vern_lan")):
                        with reversion.create_revision():
                            Vernacular(tu_id=tu_obj,
                                       id=dat.get("vern_id"),
                                       vernacular=dat.get("vernacular"),
                                       language=dat.get("language")).save()
                            reversion.set_user(request.user)
                            reversion.set_comment("Edit")

            # Than redirect to Taxonomic unit profile
            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": tu_id}))
        else:
            data = {
                "tu_obj": tu_obj,
                "vern_forms": vern_formset,
            }
            return render(request, 'vernacular_forms/edit_vernacular.html', data)


@login_required
@permission_required('collection.add_nonobligatoryinfo', login_url=reverse_lazy("rights_required"))
def add_non_oblig_info(request, tu_id):
    # Get tu object
    tu_obj = get_object_or_404(TaxonomicUnit.objects, pk=tu_id)
    # Get distinct categories
    non_oblig_cats = NonObligatoryInfo.objects.values('category').distinct()

    if request.method != "POST":
        non_oblig_form = NewTuForm4()
        data = {
            "tu_obj": tu_obj,
            "non_oblig_form": non_oblig_form,
            "non_oblig_cats": non_oblig_cats,
        }
        return render(request, 'non_obligatory_info_forms/add_non_oblig_info.html', data)
    else:
        form = NewTuForm4(request.POST)

        if form.is_valid():
            # Save new entry to database
            cleaned_data = form.cleaned_data
            new_entry = NonObligatoryInfo(tu_id=tu_obj,
                                          input_auth=request.user,
                                          input_date=date.today(),
                                          reference=cleaned_data.get("reference"),
                                          category=cleaned_data.get("category"),
                                          info=cleaned_data.get("info"),
                                          )
            with reversion.create_revision():
                new_entry.save()
                reversion.set_user(request.user)
                reversion.set_comment("Initial")
            # Redirect to TU profile
            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": request.POST.get("tu_id")}))
        else:
            data = {
                "tu_obj": tu_obj,
                "non_oblig_form": form,
                "non_oblig_cats": non_oblig_cats,
            }
            return render(request, 'non_obligatory_info_forms/add_non_oblig_info.html', data)


@login_required
@permission_required('collection.change_nonobligatoryinfo', login_url=reverse_lazy("rights_required"))
def edit_non_oblig_info(request, tu_id, info_id):
    tu_obj = get_object_or_404(TaxonomicUnit.objects.select_related(), pk=tu_id)
    # Check an entry with such id exists for given tu
    info_obj = get_object_or_404(NonObligatoryInfo, tu_id=tu_obj, pk=info_id)
    # Prepare init data
    info_init = {
        "reference": info_obj.reference,
        "category": info_obj.category,
        "info": info_obj.info,}

    if request.method != "POST":
        non_oblig_form = NewTuForm4(initial=info_init)
        data = {
            "tu_obj": tu_obj,
            "info_obj": info_obj,
            "non_oblig_form": non_oblig_form,
        }
        return render(request, 'non_obligatory_info_forms/edit_non_oblig_info.html', data)
    else:
        # POST method
        # Check if DELETE was requested
        if int(request.POST.get("DELETE")):
            # Delete entry and return redirect
            with reversion.create_revision():
                info_obj.delete()
                reversion.set_user(request.user)
                reversion.set_comment("Delete")
            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": request.POST.get("tu_id")}))

        # Else proceed with saving or error handling
        non_oblig_form = NewTuForm4(request.POST, initial=info_init)
        if non_oblig_form.is_valid():
            # Check if form has changed
            if non_oblig_form.has_changed():
                # Save data to model
                cleaned_data = non_oblig_form.cleaned_data

                # Update fields
                info_obj.reference = cleaned_data.get("reference")
                info_obj.category = cleaned_data.get("category")
                info_obj.info = cleaned_data.get("info")

                with reversion.create_revision():
                    info_obj.save()
                    reversion.set_user(request.user)
                    reversion.set_comment("Edit")
            # Redirect to TU profile
            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": request.POST.get("tu_id")}))
        else:
            # The form contains errors
            data = {
                "tu_obj": tu_obj,
                "info_obj": info_obj,
                "non_oblig_form": non_oblig_form,
            }
            return render(request, 'non_obligatory_info_forms/edit_non_oblig_info.html', data)


class LabelDetailView(DetailView):
    model = Individual
    pk_url_kwarg = "ind_id"
    css = {
        "1": "file://" + settings.BASE_DIR + "/collection/static/collection/css_bundles/rect_dark_label.min.css",
        "2": "file://" + settings.BASE_DIR + "/collection/static/collection/css_bundles/narrow_label.min.css",
    }

    def get_template_names(self):
        if self.kwargs.get("type") == 1:
            return ['pdf_labels/base_label_pdf.html']
        if self.kwargs.get("type") == 2:
            return ['pdf_labels/narrow_label_pdf.html']
        else:
            pass

    def get_object(self, queryset=None):
        pk = self.kwargs.get("ind_id")
        # Check if Individual with provided ID exists
        qs = get_object_or_404(self.model.objects, id=pk)
        # Prepare a query
        qs = self.model.objects.prefetch_related().get(id=pk)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # Get Ru vernacular or prepare transliteration
        ru_vern_set = context.get("object").tu_id.vernacular_set.filter(language="rus")
        if ru_vern_set.exists():
            context['ru_vern'] = ru_vern_set[0].vernacular
        else:
            # Prepare transliteration of Genera and Species names
            context['ru_vern'] = context.get("object").tu_id.name_translit_ru()

        # Create QR code with base64
        qr_in_memory = BytesIO()
        qr = qrcode.QRCode(
            version=None,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            image_factory=qrcode.image.svg.SvgPathFillImage,
            border=1,
        )
        # Construct path for QR code
        tu_id = self.kwargs.get("tu_id")
        ind_id = self.kwargs.get("ind_id")
        rel_path = reverse('individual_profile', kwargs={"tu_id": tu_id, "ind_id": ind_id})
        abs_path = self.request.build_absolute_uri(rel_path)
        qr.add_data(abs_path)
        qr.make(fit=True)
        qr.make_image(fill_color="black", back_color="white").save(qr_in_memory)
        context['qr'] = qr_in_memory.getvalue().decode('ascii')
        context['logo'] = "file://" + settings.BASE_DIR + "/collection/static/misc_images/dbg_logo_bw_white.min.svg"
        # Get proper css basing on GET type parameter
        context['css'] = self.css.get(str(self.kwargs.get("type")))

        # Construct name for narrow label
        # The point is we will need to separate base name and author name and find the length of base name
        tu = context.get("object").tu_id
        base_name = f'{tu.genera_name} {tu.species_name}{" " if tu.cult_mark else ""}'\
                    f'{" " if tu.infra_mark else ""}{tu.infra_mark or ""}'\
                    f'{" " if tu.infra_name else ""}{tu.infra_name or ""}'
        base_name_html = f'<b><i>{tu.genera_name.capitalize()} {tu.species_name}{" " if tu.cult_mark else ""}</i>'\
                         f'{" " if tu.infra_mark else ""}{tu.infra_mark or ""}'\
                         f'<i>{" " if tu.infra_name else ""}{tu.infra_name or ""}</i></b>'
        context['base_name_size'] = len(base_name)
        context['base_name_html'] = base_name_html
        return context


class LabelPdfGenerator(LoginRequiredMixin, WeasyTemplateResponseMixin, LabelDetailView):
    # show pdf in-line (default: True, show download dialog)
    pdf_attachment = True

    def get_pdf_filename(self):
        tu_full_name = TaxonomicUnit.objects.get(pk=self.kwargs.get("tu_id")).full_name
        tu_slug = slugify(tu_full_name).replace("-", "_")
        pdf_name = f"{tu_slug}_{self.kwargs.get('ind_id')}.pdf"
        return pdf_name


@login_required
@permission_required(['collection.add_taxonomicunit', 'collection.add_taxonomicunitimages'],
                     login_url=reverse_lazy("rights_required"))
def add_image(request, tu_id):
    # Get tu object
    tu_obj = get_object_or_404(TaxonomicUnit, pk=tu_id)

    if request.method != "POST":
        form = NewTuForm5()
        context = {'tu_obj': tu_obj, 'form': form}
        return render(request, "image_forms/add_image.html", context)
    else:
        form = NewTuForm5(request.POST, request.FILES)
        tu_obj = get_object_or_404(TaxonomicUnit, pk=request.POST.get("tu_id"))

        if form.is_valid():
            # Save image to disk and process it with save
            TaxonomicUnitImages().add_image(tax_unit=tu_obj, cleaned_data=form.cleaned_data,
                                            image=form.cleaned_data.get('img'))
            return HttpResponseRedirect(reverse("tu_profile", kwargs={"tu_id": tu_obj.id}))
        else:
            context = {'tu_obj': tu_obj, 'form': form}
            return render(request, "image_forms/add_image.html", context)
