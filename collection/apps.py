from django.apps import AppConfig
from django.db.models.signals import post_save
from .signals import tu_is_syn_of_col_sp


class MainAppConfig(AppConfig):
    name = 'collection'

    def ready(self):
        # registering signals with the model's string label
        post_save.connect(tu_is_syn_of_col_sp, sender='collection.TaxonomicUnit')
