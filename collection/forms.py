import re

from django import forms
from django.forms import ModelForm
from django.contrib.gis.forms import BaseGeometryWidget
from mptt.forms import TreeNodeChoiceField

# import models
from .models import TaxonomicUnit
from .models import Vernacular
from .models import ObligatoryInfo
from .models import Individual
from spatial.models import SovCountries
from spatial.models import AdminRegions
from spatial.models import PopulatedPlaces
from users_and_organization.models import Departments

from django.forms import BaseFormSet
from ckeditor.widgets import CKEditorWidget

from core.forms import ChoiceFieldNoValidation


# Actually forms
class NewTuForm1(forms.Form):
    """
    First page of TU input form
    """
    # Selecting or entering family (should much family name from Taxonomy table)
    # TODO next 3 fields must have cross filtering
    col_class = forms.CharField(max_length=100, label="Класс", required=False)
    col_order = forms.CharField(max_length=100, label="Порядок", required=False)
    col_family = forms.CharField(max_length=100, label="Семейство", required=False)

    # Entering Genera, Species, Infraspecies info and Cultivar info
    tu_genera = forms.CharField(max_length=100, required=True, label="Род")
    tu_species = forms.CharField(max_length=100, required=True, label="Видовой эпитет")
    tu_infra_mark = forms.ChoiceField(choices=TaxonomicUnit.infra_mark_choices, initial=None, required=False,
                                      label="Внутривидовая метка")
    tu_infra_name = forms.CharField(max_length=100, required=False, label="Внутривидовой эпитет")
    tu_cult_mark = forms.ChoiceField(choices=TaxonomicUnit.cult_mark_choices, initial=None, required=False,
                                     label="Тип культивара")
    tu_cult_name = forms.CharField(max_length=100, required=False, label="Название культивара")

    # Author name
    tu_author_name = forms.CharField(max_length=100, required=False, label="Имя автора")

    # Is hybrid?
    is_hybrid = forms.BooleanField(required=False, initial=False, label="Является гибридом?")

    # Need COL check
    col_check = forms.BooleanField(required=False, initial=True, label="Сверить COL?")

    # Change _genera, _species and _infra_name to lowercase
    def clean_tu_genera(self):
        return self.cleaned_data["tu_genera"].lower()

    def clean_tu_species(self):
        tu_species = self.cleaned_data["tu_species"].lower()
        # Replace X_ or x_ in the beginning of hydride names with ×
        tu_species = re.sub(r"^\s*[xXхХ]\s+", "× ", tu_species)
        return tu_species

    def clean_tu_infra_name(self):
        return self.cleaned_data["tu_infra_name"].lower()

    def clean_tu_cult_name(self):
        tu_cult_name = self.cleaned_data["tu_cult_name"]
        # Remove any possible quotation marks
        tu_cult_name = re.sub(r'''^\s*["'`ʺ“”˝]\s*''', "", tu_cult_name)
        tu_cult_name = re.sub(r'''\s*["'`ʺ“”˝]\s*$''', "", tu_cult_name)
        return tu_cult_name

    def clean(self):
        cleaned_data = super(NewTuForm1, self).clean()
        for key, value in cleaned_data.items():
            if cleaned_data[key] == "" or cleaned_data[key] == "None":
                cleaned_data[key] = None
        return cleaned_data


class NewTuForm2(forms.Form):
    """
    Second page of TU input form: Vernaculars
    """
    tu_vernacular = forms.CharField(max_length=100, label="Вернакуляр",
                                    required=True, error_messages={'required': "Поле обязательно для ввода"})
    tu_vern_lan = forms.ChoiceField(choices=Vernacular.lang_choices,
                                    label="Язык", required=True)
    # During editing we need vernacular id, so we will create a hidden non required field for it
    vern_id = forms.CharField(max_length=10, required=False, widget=forms.HiddenInput)

    def clean(self):
        cleaned_data = super(NewTuForm2, self).clean()
        # Replace empty string with None
        for key, value in cleaned_data.items():
            if cleaned_data[key] == "" or cleaned_data[key] == "None":
                cleaned_data[key] = None

        # Clean vernacular string
        vern = cleaned_data["tu_vernacular"].strip()  # Lower case and strip spaces
        vern = re.sub(' +', ' ', vern)  # Remove multiple whitespaces
        cleaned_data["tu_vernacular"] = vern

        return cleaned_data


class BaseVernacularFormSet(BaseFormSet):
    def clean(self):
        """Checks that no two entries have the same verancular."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        vernaculars = []
        for form in self.forms:
            vern = [form.cleaned_data['tu_vernacular'], form.cleaned_data['tu_vern_lan']]
            if vern in vernaculars:
                form.add_error(None, "Такое название уже присутствует")
                raise forms.ValidationError("Дублирующиеся названия недопустимы")
            vernaculars.append(vern)


class NewTuForm3(forms.Form):
    """
    Third page of TU input form: ObligatoryInfo
    """
    tu_raunc_form = forms.ChoiceField(required=False, choices=ObligatoryInfo.raunc_form_choices,
                                      label="ЖФ по Раункиеру")
    tu_moisture = forms.ChoiceField(required=False, choices=ObligatoryInfo.moisture_choices,
                                    label="Увлажнение")
    tu_light = forms.ChoiceField(required=False, choices=ObligatoryInfo.light_choices,
                                 label="Освещенность")
    tu_fertility = forms.ChoiceField(required=False, choices=ObligatoryInfo.fertil_choices,
                                     label="Плодородие почв")
    tu_salinity = forms.ChoiceField(required=False, choices=ObligatoryInfo.salin_choices,
                                    label="Засоление")
    tu_acidity = forms.ChoiceField(required=False, choices=ObligatoryInfo.acidity_choices,
                                   label="pH почвы")
    tu_phenotype = forms.ChoiceField(required=False, choices=ObligatoryInfo.pheno_choices,
                                     label="Феноритм")
    tu_cenotype = forms.MultipleChoiceField(choices=ObligatoryInfo.cenotype_choices,
                                            widget=forms.CheckboxSelectMultiple,
                                            label="Ценотип",
                                            required=False)


class NewTuForm4(forms.Form):
    reference = forms.CharField(max_length=500, required=True, label="Источник")
    category = forms.CharField(max_length=100, required=True, label="Категория")
    info = forms.CharField(max_length=10000, required=True,
                           widget=CKEditorWidget, label="Информация")

    def clean(self):
        cleaned_data = super(NewTuForm4, self).clean()
        # Replace empty string with None
        for key, value in cleaned_data.items():
            if cleaned_data[key] == "" or cleaned_data[key] == "None":
                cleaned_data[key] = None

        # Check if entry is complete (all three fields are not null)
        if any([cleaned_data.get("category") is None,
               cleaned_data.get("reference") is None,
               cleaned_data.get("info") is None]):
            raise forms.ValidationError("Only full entries are allowed")

        # Clean category and category string
        category = cleaned_data["category"].lower().strip()  # Lower case and strip spaces
        category = re.sub(' +', ' ', category)  # Remove multiple whitespaces
        cleaned_data["category"] = category

        return cleaned_data


class BaseObligInfoFormSet(BaseFormSet):
    def clean(self):
        """Checks for unique only entries"""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        info_fields = []
        for form in self.forms:
            info = [form.cleaned_data['info']]
            if info in info_fields:
                raise forms.ValidationError("Duplicated entries are not allowed")
            info_fields.append(info)


class NewTuForm5(forms.Form):
    """
    Third page of TU input form: TaxonomicUnitImages
    """
    author = forms.CharField(max_length=500, required=True, label="Автор изображения")
    date = forms.DateField(required=True, label="Когда сделано")
    caption = forms.CharField(max_length=100, required=True, label="Подпись (макс. 100 знаков)")
    description = forms.CharField(max_length=300, required=True, label="Описание (макс. 300 знаков)")
    img = forms.ImageField(required=True)
    rate = forms.IntegerField(initial=3, required=True, label="Оценка изображения")
    main_scr = forms.BooleanField(initial=False, required=False, label="Показывать на главной странице")


class BaseTuImagesFormSet(BaseFormSet):
    def clean(self):
        """Checks for unique only images"""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        info_fields = []
        for form in self.forms:
            info = [form.cleaned_data['img']]
            if info in info_fields:
                raise forms.ValidationError("Duplicated images are not allowed")
            info_fields.append(info)


class NewIndividualForm(ModelForm):
    """
    The form for new individual input
    """
    def __init__(self, *args, **kwargs):
        """
        Here we will alter init method for adopting custom admin and city choices as additional kwargs
        """
        # Look for admin and city choices in arguments
        if 'admin_choices' in kwargs:
            self.admin_choices = kwargs.pop('admin_choices')
        if 'city_choices' in kwargs:
            self.city_choices = kwargs.pop('city_choices')
        super(NewIndividualForm, self).__init__(*args, **kwargs)
        if self.admin_choices:
            self.fields["admin"].choices = self.admin_choices
        else:
            # Prepare initial
            self.fields["admin"].choices = AdminRegions.objects.filter(sov_id_id=34).\
                order_by("name_ru").values_list('id', 'name_ru')
        if self.city_choices:
            self.fields["city"].choices = self.city_choices
        else:
            # Prepare initial
            self.fields["city"].choices = PopulatedPlaces.objects.filter(sov_id_id=34, admin_id_id=976)\
                .order_by("name_ru").values_list('id', 'name_ru')

    # Set additional fields for country and region
    country = forms.ModelChoiceField(queryset=SovCountries.objects.order_by("name_ru").all(), empty_label=None,
                                     label="Страна происхождения", required=False, initial=34)

    # Prepare fields for Admin and city
    admin = ChoiceFieldNoValidation(choices=[('', '')], label='Админ. регион происхождения', required=False, initial=976)
    city = ChoiceFieldNoValidation(choices=[('', '')], label='Город происхождения', required=False, initial=6268)

    origin = forms.ChoiceField(label='Происхождение', choices=Individual.origin_choices,
                               widget=forms.RadioSelect, initial=Individual.EMPTY_LOC)
    # Prepare field for department_id
    department_id = TreeNodeChoiceField(Departments.objects.filter(children__isnull=True))

    class Meta:
        model = Individual
        fields = ["tu_id", "field_number", "is_alive", "income_date", "income_material",
                  "source", "out_date", "department_id", "cult_type", "spatial_pos", "input_author",
                  "origin_loc"]  # city is exluded
        widgets = {'tu_id': forms.HiddenInput(),
                   'input_author': forms.HiddenInput(),
                   'spatial_pos': forms.Textarea(),
                   'origin_loc': forms.Textarea()
                   }

    def clean_city(self):
        cleaned_data = super(NewIndividualForm, self).clean()
        city = cleaned_data.get('city')
        if city:
            if PopulatedPlaces.objects.filter(pk=city).exists():
                return PopulatedPlaces.objects.get(pk=city)
            else:
                self.add_error('city', 'Такого города нет в нашей базе')
        else:
            return city

    def clean(self):
        """
        Custom clean method for NewIndividual form
        There some constraints for the data:
        1. The individual could not be live if there is a death date ==> mark is_alive as False
        2. If the individual is marked as not alive, there should be a  date of death ==> raise an error
        3. The death date should be after income date ==> rise an error
        :return:
        """
        cleaned_data = super(NewIndividualForm, self).clean()
        # Case 1 (death date provided, but is alive)
        if cleaned_data.get("out_date") is not None:
            # Force is_alive field to be False
            cleaned_data.update({"is_alive": False})
        # Case 2 (not alive without death date)
        if not cleaned_data.get("is_alive") and cleaned_data.get("out_date") is None:
            # Return an error
            raise forms.ValidationError("Особь укзана, как выпавшая (погибшая), " +
                                        "но дата выпада не указана. Укажите дату выпада")
        # Case 3 (death date before income date)
        income = cleaned_data.get("income_date")
        out = cleaned_data.get("out_date")
        if income is not None and out is not None:
            if income > out:
                raise forms.ValidationError("Указанная дата выпада предшествует дате постуления. Проверьте даты")

        return cleaned_data

    def save(self, commit=True):
        origin = self.cleaned_data['origin']
        self.instance.origin = origin

        # Save city or location only if such origin was selected
        if origin == Individual.CITY:
            self.instance.city = self.cleaned_data['city']
            # Ensure location is null
            self.instance.origin_loc = None
        elif origin == Individual.NATURE:
            self.instance.origin_loc = self.cleaned_data['origin_loc']
            # Ensure city is null
            self.instance.city = None
        elif origin == Individual.EMPTY_LOC or origin == Individual.LOCAL:
            # Ensure both city and location are null
            self.instance.city = None
            self.instance.origin_loc = None

        return super(NewIndividualForm, self).save(commit=commit)

