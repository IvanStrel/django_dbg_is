from django.contrib import admin
from reversion.admin import VersionAdmin
from leaflet.admin import LeafletGeoAdmin
from django.utils.safestring import mark_safe

# Models registration
from .models import ColTaxonomy
from .models import ColSpecies
from .models import TaxonomicUnit
from .models import Vernacular
from .models import RedListIUCN
from .models import Individual
from .models import ObligatoryInfo
from .models import NonObligatoryInfo
from .models import TaxonomicUnitImages


@admin.register(ColTaxonomy)
class ColTaxonomyAdmin(VersionAdmin):
    search_fields = ['col_family']


@admin.register(ColSpecies)
class ColSpeciesAdmin(VersionAdmin):
    search_fields = ['col_full_name']


@admin.register(Individual)
class IndividualAdmin(VersionAdmin, LeafletGeoAdmin):
    pass


class IndividualInline(admin.TabularInline):
    model = Individual
    fields = ['id', 'field_number', 'is_alive', 'income_date', 'department_id', 'cult_type']
    show_change_link = True


@admin.register(Vernacular)
class VernacularAdmin(VersionAdmin):
    pass


@admin.register(RedListIUCN)
class RedListIUCNAdmin(VersionAdmin):
    pass


@admin.register(ObligatoryInfo)
class ObligatoryInfoAdmin(VersionAdmin):
    pass


@admin.register(NonObligatoryInfo)
class NonObligatoryInfoAdmin(VersionAdmin):
    pass


@admin.register(TaxonomicUnitImages)
class TaxonomicUnitImagesAdmin(VersionAdmin):
    readonly_fields = ["preview"]

    def preview(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.img_thunb_webp.url,
            width=obj.img_thunb_webp.width,
            height=obj.img_thunb_webp.height,
            )
        )


class TaxonomicUnitImagesInline(admin.TabularInline):
    model = TaxonomicUnitImages
    fields = ['id', 'img_caption', 'img_description', 'input_date', 'input_auth', 'image_preview']
    readonly_fields = ['image_preview', ]
    show_change_link = True

    def image_preview(self, obj):
        return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
            url=obj.img_thunb_webp.url,
            width=obj.img_thunb_webp.width,
            height=obj.img_thunb_webp.height,
            )
        )


@admin.register(TaxonomicUnit)
class TaxonomicUnitAdmin(VersionAdmin):
    list_display = ['id', 'genera_name', 'species_name', 'infra_mark', 'infra_name',
                    'author', 'cult_name', 'col']
    search_fields = ['full_name']
    raw_id_fields = ['col']
    inlines = [
        IndividualInline,
        TaxonomicUnitImagesInline,
    ]
