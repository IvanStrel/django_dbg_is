from rest_framework import serializers
from rest_framework_gis import serializers as gis_serializers

# models
from .models import TaxonomicUnit
from .models import ObligatoryInfo
from .models import Individual


class CollectionHomeTableSerializer(serializers.ModelSerializer):
    col = serializers.BooleanField()
    iucn = serializers.BooleanField()
    family_name = serializers.CharField()

    class Meta:
        model = TaxonomicUnit
        fields = ("id", "family_name", "genera_name", "species_name", "infra_mark",
                  "infra_name", "author", "cult_name", "hybrid_mark", "col", "iucn")


class ColTuSearchResultSerializer(serializers.ModelSerializer):
    """
    Serializer for RawSQL queryset from TaxonomicUnit.objects.full_serach(qd) method.
    """
    col_check = serializers.BooleanField(allow_null=False)
    col_family = serializers.CharField(max_length=100, required=True, allow_null=False)
    col_order = serializers.CharField(max_length=100, required=True, allow_null=False)
    col_class = serializers.CharField(max_length=100, required=True, allow_null=False)
    col_phylum = serializers.CharField(max_length=100, required=True, allow_null=False)
    in_red_list = serializers.BooleanField(allow_null=False)
    category = serializers.CharField(max_length=100, allow_null=True)
    name = serializers.CharField(max_length=100, required=True, allow_null=False)
    raunc_form = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_raunc_form_display')
    moisture = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_moisture_display')
    light = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_light_display')
    fertility = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_fertility_display')
    salinity = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_salinity_display')
    acidity = serializers.CharField(max_length=30, allow_null=True, source='obligatoryinfo.get_acidity_display')
    phenotype = serializers.CharField(max_length=100, allow_null=True, source='obligatoryinfo.get_phenotype_display')
    cenotype = serializers.CharField(max_length=500, allow_null=True, source='obligatoryinfo.get_cenotype_display')

    class Meta:
        model = TaxonomicUnit
        fields = ('pk', 'genera_name', 'species_name', 'infra_mark', 'infra_name', 'author', 'cult_mark', 'cult_name',
                  'hybrid_mark', 'col_check', 'input_date', 'full_html_name', 'col_family', 'col_order', 'col_class',
                  'col_phylum', 'in_red_list', 'category', 'name', 'raunc_form', 'moisture', 'light', 'fertility',
                  'salinity', 'acidity', 'phenotype', 'cenotype')


class ColIndSearchResultSerializer(ColTuSearchResultSerializer):
    ind_id = serializers.IntegerField(required=True, allow_null=False)
    is_alive = serializers.BooleanField(required=True, allow_null=False)
    income_date = serializers.DateField(required=True, allow_null=True)
    out_date = serializers.DateField(required=True, allow_null=True)
    cult_type = serializers.CharField(max_length=100, allow_null=True)
    source = serializers.CharField(max_length=300, allow_null=True)
    # Can not get proper choice label
    # origin = serializers.CharField(max_length=300, allow_null=True, source='get_origin_display')
    city_id = serializers.IntegerField(required=True, allow_null=True)
    name_ru = serializers.CharField(max_length=300, allow_null=True)

    class Meta:
        model = TaxonomicUnit
        fields = ('ind_id', 'is_alive', 'income_date', 'out_date', 'cult_type', 'source',
                  'pk', 'genera_name', 'species_name', 'infra_mark', 'infra_name', 'author', 'cult_mark', 'cult_name',
                  'hybrid_mark', 'col_check', 'input_date', 'full_html_name', 'col_family', 'col_order', 'col_class',
                  'col_phylum', 'in_red_list', 'category', 'name', 'raunc_form', 'moisture', 'light', 'fertility',
                  'salinity', 'acidity', 'phenotype', 'cenotype', 'city_id', 'name_ru')


class ColTopNCountsSerializer(serializers.ModelSerializer):
    """
    Serializer for raw query output of TaxonomicUnit().objects.count_top_n_for_tu
    """
    target_name = serializers.CharField(required=False, max_length=300, allow_null=True)
    counts = serializers.IntegerField(required=False, allow_null=False)
    rn = serializers.IntegerField(required=False, allow_null=False)

    class Meta:
        model = TaxonomicUnit
        fields = ('target_name', 'counts', 'rn')


class ColTuIncomeOutcomeSerializer(serializers.ModelSerializer):
    """
    Serializer for raw query output of TaxonomicUnit().objects.cumulative_ind_income_outcome
    """
    year = serializers.DateTimeField(required=True)
    alive = serializers.IntegerField(required=False, allow_null=False)
    dead = serializers.IntegerField(required=False, allow_null=False)

    class Meta:
        model = TaxonomicUnit
        fields = ('year', 'alive', 'dead')


class ColTuIncomeOutcomeByYearSerializer(serializers.ModelSerializer):
    """
    Serializer for raw query output of TaxonomicUnit().objects.tu_income_outcome_by_year
    """
    year = serializers.IntegerField(required=True)
    type = serializers.CharField(required=False, allow_null=False)

    class Meta:
        model = TaxonomicUnit
        fields = ('year', 'type', 'id', 'genera_name', 'species_name', 'infra_mark', 'infra_name',
                  'cult_name', 'author')


class ColIndSpatialPosSerializer(gis_serializers.GeoFeatureModelSerializer):
    ind_id = serializers.IntegerField(required=False, allow_null=False)

    class Meta:
        model = TaxonomicUnit
        geo_field = "spatial_pos"

        fields = ('id', 'ind_id')
