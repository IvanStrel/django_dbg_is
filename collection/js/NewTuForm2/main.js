var url = "/collections/new-tu-2/";
var form_n;
var form_id;

// Get template elements
var form_temp_btn;

// Get all select elements
var selects;

// Count all forms and alter selects
$(document).ready(function(){
    // Get all selects
    selects = $("select");
    // Replace material select with browser select
    // if any dimension less or  equal to 320
    if (($( document ).width() <= 320) || ($( document ).height() <= 320)) {
        selects.each(function(){
            // Detach label
            var lab = $(this).parent().find("label");
            lab.detach();
            // Add browser default class to select element
            $(this).addClass("browser-default");
            // Attach label before select element
            lab.insertBefore( $(this).parent() );
        })
    }
    // Add event listener to each select element
    // it should delete .invalid on select state change
    /**
    selects.each(function() {
        $(this).parent().find("input").on("click", function () {
            $(this).removeClass("invalid valid")
        });
        // Fix hidden error message
        $(this).parent().append($(this).parent().parent().find("span.helper-text"));
    });*/

    form_n = $('.form-row').length - 1; // Two forms for template;
    form_id = JSON.parse(JSON.stringify(form_n));

    form_temp_btn = $('#form-templ-btn');

    // Activate selects and tooltips
    // Init select
    var elems_select = document.querySelectorAll('.form-row:not(#form-templ-btn) select');
    M.FormSelect.init(elems_select);
    // Init tooltips
    var elems_tooltips = document.querySelectorAll('.tooltipped:not(#form-templ-btn)');
    M.Tooltip.init(elems_tooltips);
});



function add_form() {
    /** Place additional form to the page
     * id and name number will be counted from initial form_n (count of forms)
     * each new from will have id = form_id (first form has 0) (even if some forms will be deleted)
     * in such way, there will not be a case with duplicated id*/
    // If there was only one form before function run add del button to it
    if (form_n === 1){
        var del_btn = form_temp_btn.find(".del-btn-wrapper").parent().clone();
        var init_form = $(".form-row:not(#form-templ-btn)");

        // Alter col classes
        init_form.find(".vern-text").removeClass("s12 m8 l8");
        init_form.find(".vern-text").addClass("s12 m7 l7");
        init_form.find(".lang-text").removeClass("s12 m4 l4");
        init_form.find(".lang-text").addClass("s9 m4 l4");

        // Add button
        init_form.append(del_btn);

        // Activate tooltip on the button
        del_btn.find('.tooltipped').tooltip();
    }

    // Construct form fields names and id
    var f_vern_id = "id_form-" + form_id + "-tu_vernacular";
    var f_vern_lang_id = "id_form-" + form_id + "-tu_vern_lang";
    var f_vern_name = "form-" + form_id + "-tu_vernacular";
    var f_vern_lang_name = "form-" + form_id + "-tu_vern_lang";

    // Place new form to the page
    var template = form_temp_btn.clone();
    // change template id
    template.attr("id", "form-" + form_id);
    template.insertBefore($("#add-btn-wrapper"));

    // Assign id-s and names
    template.find(".vern-text").find("input").attr("id", f_vern_id); // ID
    template.find(".vern-text").find("input").attr("name", f_vern_name); // NAME
    template.find(".vern-text").find("label").attr("for", f_vern_id); // Label FOR
    template.find(".lang-select").find("select").attr("id", f_vern_lang_id); // ID
    template.find(".lang-select").find("select").attr("name", f_vern_lang_name); // NAME
    template.find(".lang-select").find("label").attr("for", f_vern_lang_id); // Label FOR


    // Make visible
    template.slideDown(300);

    // increment form_n and from id
    form_id += 1;
    form_n += 1;

    // Update tooltip on button
    template.find('.tooltipped').tooltip();
    template.find('select').formSelect();
}


function del_form(event) {
    /** Remove the form on which button was clicked
     * last form could not be removed
     * if there is only one form remains after deleting remove the delete button
     * alter form_n on each deleting */

    // Check if there are more than one forms on the page
    if (form_n < 2){
        // Do not do nothing
        return
    }

    // Get form container and remove it
    // first remove tooltip to prevent permanent its permanent placement
    var cur_form = event.target.closest(".form-row");

    var tooltip_inst = M.Tooltip.getInstance(cur_form.querySelector(".tooltipped"));
    tooltip_inst.destroy();

    cur_form.remove();

    // Decrement form_n
    form_n -= 1;

    // Remove delete button if only one form remains
    if (form_n === 1){
        // get form
        var last_from = $('.form-row:not(#form-templ-btn)');

        // delete button
        var doomed_btn = last_from.find(".del-btn-col");
        doomed_btn.remove();

        // Alter grid column classes
        last_from.find(".vern-text").removeClass("s12 m7 l7");
        last_from.find(".vern-text").addClass("s12 m8 l8");
        last_from.find(".lang-text").removeClass("s9 m4 l4");
        last_from.find(".lang-text").addClass("s12 m4 l4");

    }
}

function submit_button() {
    /** Submit entered information as post request
     * 1. verify data (all fields entered and now duplicated entries)
     * 2. prepare data for submission
     * 3. handle response*/

    // Read all data
    var form = $('.form-row:not(#form-templ-btn)');
    var form_data = [];
    var form_id_for_valid = [];
    var is_valid = true;
    var iter = 0;

    /** Validation */

    form.each(function (index) {
        // Get fields and values from forms
        var vern_field = $(this).find("input");
        var lang_field = $(this).find("select");
        var vern = $.trim(vern_field.val()).toLowerCase().replace(/\s\s+/g, ' ');
        var lang = lang_field.find(":selected").val();

        if (lang === ""){
            lang = null
        }
        if (vern === ""){
            vern = null
        }

        // Validate for single field (validation for duplicates will be outside this loop)
        // first remove previous validation classes
        vern_field.removeClass("invalid valid");
        lang_field.parent().find("input").removeClass("invalid valid");

        // replace vern field error message with requred
        $(this).find("vern-text").find(".helper-text").attr('data-error', "Поле обязательно для ввода");
        $(this).find("lang-text").find(".helper-text").attr('data-error', "Нужно выбрать язык");

        // Check if field is properly filled (both empty or both filled)
        if ((vern === null) && (lang !== null)){
            // Language = Yes Name = Not (show error message)
            vern_field.addClass("invalid");
            lang_field.parent().find("input").addClass("valid");
            is_valid = false
        } else if ((vern !== null) && (lang === null)) {
            // Language = Not Name = Yes (show error message)
            vern_field.addClass("valid");
            lang_field.parent().find("input").addClass("invalid");

            // Alter helper text
            lang_field.parent().append(lang_field.parent().parent().find("span.helper-text"));

            is_valid = false
        } else {
            vern_field.addClass("valid");
            lang_field.parent().find("input").addClass("valid")
        }

        // Append data to from_data
        if (vern !== null && lang !== null) {
            var vern_id = "form-" + iter + "-tu_vernacular";
            var lang_id = "form-" + iter + "-tu_vern_lan";
            var obj = {};
            obj[vern_id] = vern;
            obj[lang_id] = lang;
            form_data[iter] = obj;

            // Increment iterator
            iter += 1
        }

        // Append form_id_for_valid with id
        form_id_for_valid.push($(this).attr('id'))
    });

    // If invalid do nothing
    if (!is_valid){
        return
    }

    // Else validate possible duplicates
    // find duplicates: compare each element with each and count matches
    // if element is unique it will match only itself
    // otherwise number of matches will be greater
    // --> forms under index with matches > 1 are invalid due to duplicates
    var dup_ind = [];
    dup_ind.length = form_data.length;
    dup_ind.fill(0);

    form_data.forEach(function(x, index_x){
        form_data.forEach(function(y) {
            if (JSON.stringify(Object.values(x)) === JSON.stringify(Object.values(y))) {
                dup_ind[index_x] += 1
            }
        })
    });

    // Place invalid state to invalid forms
    // iterate over dup_ind for values > 2
    // forms id in same order as dup_ind are in form_id_for_valid
    dup_ind.forEach(function(value, index) {
        if (value > 1) {
            is_valid = false;

            // find form
            var form_field = $('#' + form_id_for_valid[index]);

            // add error message
            var placement = form_field.find(".helper-text");
            $(placement).attr('data-error', "Повторяющиеся названия");

            // add class invalid
            form_field.find("input").removeClass("valid invalid").addClass("invalid")
        }
    });

    // If invalid do nothing
    if (!is_valid){
        return
    }

    /** SEnding data to server */
    // Prepare and send POST request
    $.ajax({
        url: url,
        type: "POST", //http method
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            form_data: JSON.stringify(form_data)
        },
        //beforeSend: function() {          // Loader overlay spinner
        //    $(".loader-overlay").show();
        //},
        success: response_handling
    })
}

function response_handling(data) {
    // Check for errors
    if (data.type === "invalid") {
        $('#errors').slideDown(300);
    }

    // If accepted -- redirect to form 3
    if (data.type === "accepted"){
        window.location.href = "/collections/new-tu-3/";
    }
}

function pass_button() {
    // Redirect ot next form without saving
    window.location.href = "/collections/new-tu-3/";
}