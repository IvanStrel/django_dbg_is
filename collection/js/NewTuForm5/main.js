/** Prepare data and activate material components */

var url = "/collections/new-tu-5/";
var form_n;
var form_id;

// Get template elements
var form_temp_btn;

// Count all forms and alter selects
$(document).ready(function(){
    // Count forms
    form_n = $('.form-row').length - 1; // Two forms for template;
    form_id = JSON.parse(JSON.stringify(form_n));

    form_temp_btn = $('#form-templ-btn');
});

/** Initiate page elements */
// prepare function for initiation
function datepicker_init(elems) {
    M.Datepicker.init(elems, {
        firstDay: true,
        format: 'yyyy-mm-dd',
        minDate: new Date(1964, 1, 1),
        maxDate: new Date(Date.now()),
        showClearBtn: true,
        yearRange: [1964, new Date().getFullYear()],
        i18n: {
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                     "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            weekdays: ["Воскресенье","Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            weekdaysShort: ["Вск","Пон", "Втр", "Срд", "Чет", "Пят", "Суб"],
            weekdaysAbbrev: ["В","По", "В", "Ср", "Ч", "Пя", "Су"],
            cancel: "Отменить",
            clear: "Очистить",
            done: "Готово"
        }
    });
}

function dropify_init(elems) {
    elems.dropify({
        messages: {
            'default': 'Перетащите файл сюда или кликните мышью',
            'replace': 'Чтобы изменить перетащите новый файл или кликните мышью',
            'remove':  'Удалить',
            'error':   'Что-то пошло не так'
        },
        error: {
            'fileSize': 'Файл слишком большой (допустимый максимум: {{ value }}).',
            'minWidth': 'Ширина изображения слишком маленькая (допустимый минимум{{ value }}}px).',
            'minHeight': 'Высота изображения слишком маленькая (допустимый минимум{{ value }}}px).',
            'imageFormat': 'Недопустимый формат изображения ({{ value }} только)',
            'fileExtension': 'Недопустимый тип изображения ({{ value }} только)'
        }
    });

    // Initiate watcher for changed status
    elems.each(function(){
        var obj = $(this);
        // Add change listener
        obj.on("change", function(){
            $(this).attr("data-changed", "changed");
            obj.parent().find(".dropify-error").text("Что-то пошло не так")
        });
        // Add after delete listener
        obj.on('dropify.afterClear', function(){
            $(this).attr("data-changed", "changed");
            // Hide error message
            obj.parent().find(".dropify-error").text("Что-то пошло не так")
        });

    })
}

function star_rating_init(elems) {
    elems.find('.star').on("click", function () {
        var obj = $( this );
        var value = obj.attr("data-value");
        var parent = obj.parent().parent();

        // Change obj class value-*
        parent.attr('class', function(i, c){
            return c.replace(/\d+/, value);
        });
        parent.find("input").attr('value', value)
    })
}

// Init elements
document.addEventListener('DOMContentLoaded', function() {
    // Init tooltips
    var elems_tooltips = document.querySelectorAll(".form-row:not(#form-templ-btn) .tooltipped");
    M.Tooltip.init(elems_tooltips);

    // init data pickers
    var elems = document.querySelectorAll(".form-row:not(#form-templ-btn) .datepicker");
    datepicker_init(elems);

    // init characters counter on text inputs
    $('[data-length]').characterCounter();

    // init dropify
    dropify_init($(".form-row:not(#form-templ-btn)").find(".dropify"));

    // init star container
    star_rating_init($(".form-row:not(#form-templ-btn)").find(".star-container"));
});

/** Make columns height equal */
$(document).ready(function() {
    var rows = $(".form-row");
    rows.each(function () {
        var row = $(this);
        var info_h = row.find(".info-col").height();
        row.find(".img-col").height(info_h)
    });
});


function add_form() {
    /** Place additional form to the page
     * id and name number will be counted from initial form_n (count of forms)
     * each new from will have id = form_id (first form has 0) (even if some forms will be deleted)
     * in such way, there will not be a case with duplicated id*/
    // If there was only one form before function add del button to it
    if (form_n === 1){
        var del_btn = form_temp_btn.find(".del-btn-wrapper").parent().clone();
        var init_form = $(".form-row:not(#form-templ-btn)");

        // Add button
        init_form.append(del_btn);

        // Activate tooltip on the button
        del_btn.find('.tooltipped').tooltip();
    }

    // Construct form fields names and id
    var f_auth_id = "id_form-" + form_id + "-author";
    var f_date_id = "id_form-" + form_id + "-date";
    var f_cap_id = "id_form-" + form_id + "-caption";
    var f_descr_id = "id_form-" + form_id + "-description";
    var f_img_id = "id_form-" + form_id + "-img";
    var f_rate_id = "id_form-" + form_id + "-rate";
    var f_main_scr_id = "id_form-" + form_id + "-main_scr";

    var f_auth_name = "form-" + form_id + "-author";
    var f_date_name = "form-" + form_id + "-date";
    var f_cap_name = "form-" + form_id + "-caption";
    var f_descr_name = "form-" + form_id + "-description";
    var f_img_name = "form-" + form_id + "-img";
    var f_rate_name = "form-" + form_id + "-rate";
    var f_main_scr_name = "form-" + form_id + "-main_scr";

    // Place new form to the page
    var template = form_temp_btn.clone();
    // change template id
    template.attr("id", "form-" + form_id);
    template.insertBefore($("#add-btn-wrapper"));

    // Assign id-s and names
    //author
    template.find(".author-text").find("input").attr("id", f_auth_id); // ID
    template.find(".author-text").find("input").attr("name", f_auth_name); // NAME
    template.find(".author-text").find("label").attr("for", f_auth_id); // Label FOR
    // date
    template.find(".date-text").find("input").attr("id", f_date_id); // ID
    template.find(".date-text").find("input").attr("name", f_date_name); // NAME
    template.find(".date-text").find("label").attr("for", f_date_id); // Label FOR
    // caption
    template.find(".caption-text").find("input").attr("id", f_cap_id); // ID
    template.find(".caption-text").find("input").attr("name", f_cap_name); // NAME
    template.find(".caption-text").find("label").attr("for", f_cap_id); // Label FOR
    // description
    template.find(".description-text").find("input").attr("id", f_descr_id); // ID
    template.find(".description-text").find("input").attr("name", f_descr_name); // NAME
    template.find(".description-text").find("label").attr("for", f_descr_id); // Label FOR
    // rate
    template.find(".rating-text").find("input").attr("id", f_rate_id); // ID
    template.find(".rating-text").find("input").attr("name", f_rate_name); // NAME
    template.find(".rating-text").find("label").attr("for", f_rate_id); // Label FOR
    // img
    template.find(".img-col").find("input").attr("id", f_img_id); // ID
    template.find(".img-col").find("input").attr("name", f_img_name); // NAME
    template.find(".img-col").find("label").attr("for", f_img_id); // Label FOR
    //main_scr
    template.find(".main_scr-wrapper").find("input").attr("id", f_main_scr_id); // ID
    template.find(".main_scr-wrapper").find("input").attr("name", f_main_scr_name); // NAME

    // init star container
    star_rating_init(template.find('.star-container'));

    // init data picker
    datepicker_init(template.find(".datepicker"));

    // init characters counter on text inputs
    template.find('[data-length]').characterCounter();

    // Make visible
    template.slideDown(300);

    // Adjust dropify region height
    var info_h = template.find(".info-col").height();
    template.find(".img-col").height(info_h);

    // Initiate Dropify region
    dropify_init(template.find(".dropify"));

    // increment form_n and from id
    form_id += 1;
    form_n += 1;

    // Update tooltip on button
    template.find('.tooltipped').tooltip();
    template.find('select').formSelect();
}


function del_form(event) {
    /** Remove the form on which button was clicked
     * last form could not be removed
     * if there is only one form remains after deleting remove the delete button
     * alter form_n on each deleting */

    // Check if there are more than one forms on the page
    if (form_n < 2){
        // Do not do nothing
        return
    }

    // Get form container and remove it
    // first remove tooltip to prevent permanent its permanent placement
    var cur_form = event.target.closest(".form-row");

    var tooltip_inst = M.Tooltip.getInstance(cur_form.querySelector(".tooltipped"));
    tooltip_inst.destroy();

    cur_form.remove();

    // Decrement form_n
    form_n -= 1;

    // Remove delete button if only one form remains
    if (form_n === 1){
        // get form
        var last_from = $('.form-row:not(#form-templ-btn)');

        // delete button
        var doomed_btn = last_from.find(".del-btn-col");
        doomed_btn.remove();
    }
}


function submit_button() {
    /** Submit entered information as post request
     * 1. verify data (all fields entered and now duplicated entries)
     * 2. prepare data for submission
     * 3. handle response*/

    // Read all data
    var forms = $('.form-row:not(#form-templ-btn)');
    var form_data = [];
    var formData_class = new FormData();
    var is_valid = true;
    var iter = 0;

    /** Validation */

    forms.each(function () {
        var form = $(this);
        // Get fields
        var auth_field = form.find(".author-text").find("input");
        var date_field = form.find(".date-text").find(".datepicker");
        var cap_field = form.find(".caption-text").find("input");
        var descr_field = form.find(".description-text").find("textarea");
        var rate_field = form.find(".rating-text").find("input");
        var img_field = form.find(".img-col").find("input");
        var main_scr_field = form.find(".main_scr-wrapper").find("input");

        var field_list = [auth_field, date_field, cap_field, descr_field, rate_field, img_field, main_scr_field];

        // Get values
        var auth = $.trim(auth_field.val()).replace(/\s\s+/g, ' ');
        var date = date_field.val();
        var cap = $.trim(cap_field.val().replace(/\s\s+/g, ' '));
        var descr = $.trim(descr_field.val().replace(/\s\s+/g, ' '));
        var rate = rate_field.val();
        var img = img_field.get(0).files[0];
        var main_scr = main_scr_field.prop( "checked" );
        var changed = true; // the variable which shows that image was changed
        var prev_im_path = null;

        /** When dropify is initiated with default image it does not return it as file.
         * As a result, we get img==null. In order to prevent this we will assign it a
         * string "not_changed" */

        if (img == null && img_field.attr("data-default-file") && img_field.attr("data-changed") === ""){
            img = "unchanged";
            changed = false;
            prev_im_path = img_field.attr("data-prev-image-path")
        }

        var val_list = [auth, date, cap, descr, rate, img, main_scr];

        // replace fields error message with required
        $(this).find(".helper-text").attr('data-error', "Поле обязательно для ввода");

        // Repalce empty values with null
        // Validate for single field (validation for duplicates will be outside this loop)
        // here wee will use the fact that orders of field_list and val_list are the same
        $.each(val_list, function(i, x){
            // remove previous validation class from field
            field_list[i].removeClass("invalid valid");
            // Check if value is not empty
            if (x === "" || x == null) {
                val_list[i] = null;
                // Add invalid class to the field
                field_list[i].addClass("invalid");
                // change form wide is_valid variable
                is_valid = false
            } else {
                // value is not empty, assign valid class
                field_list[i].addClass("valid");
            }
        });

        // Show dropify error
        if (img_field.hasClass("invalid")) {
            var dropify_error = img_field.parent().find(".dropify-error");
            dropify_error.show();
            dropify_error.text("Нужно добавить файл")
        }

        // Append variables id according to current form n
        var auth_id = "form-" + iter + "-author";
        var date_id = "form-" + iter + "-date";
        var cap_id = "form-" + iter + "-caption";
        var descr_id = "form-" + iter + "-description";
        var rate_id = "form-" + iter + "-rate";
        var main_scr_id = "form-" + iter + "-main_scr";
        var img_id = "form-" + iter + "-img";

        // Prepare object
        var obj = {};
        obj[auth_id] = auth;
        obj[date_id] = date;
        obj[cap_id] = cap;
        obj[rate_id] = rate;
        obj[descr_id] = descr;
        obj[main_scr_id] = main_scr;
        obj["changed"] = changed;
        obj["prev_im_path"] = prev_im_path;
        obj["img_id"] = img_id;

        form_data[iter] = obj;

        // Append image
        if (img !== "unchanged"){
            formData_class.append(img_id, img);
        } else {
            // append some Blob object to imitate image
            // it will be replaced with previously uploaded image on server side
            var blob = new Blob([img_id], {type: 'text/plain'});
            formData_class.append(img_id, blob);
        }

        // Increment iterator
        iter += 1;

    });

    // If invalid do nothing
    if (!is_valid) {
        return
    }

    // TODO think about checking for possible duplicated images

    // If invalid do nothing
    if (!is_valid){
        return
    }

    /** SEnding data to server */
    // Prepare and send POST request
    form_data = JSON.stringify(form_data);
    formData_class.append("form_data", form_data);
    formData_class.append("csrfmiddlewaretoken", document.getElementsByName('csrfmiddlewaretoken')[0].value);

    /** Deactivate button i.e. prevent second click */
    let subm_button = document.getElementById("submit-button");
    subm_button.disabled = true;
    console.log(subm_button.disabled);

    $.ajax({
        url: url,
        type: "POST", //http method
        data: formData_class,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function() {          // Loader overlay spinner
           $("#loader").show();
        },
        success: response_handling
    })
}


function response_handling(data) {
    let subm_button = document.getElementById("submit-button");

    switch (data.type) {
        // If invalid show form errors
        case "invalid":
            $('#errors').slideDown(300);
            break;

        // If accepted -- redirect to form 5
        case "accepted":
            window.location.href = "/collections/new-tu-conf/";
            break;

        // If errors in image handling show alert
        case "img_error":
            alert(data.message);
            subm_button.disabled = false;
            break;
    }

}

function pass_button() {
    // Redirect ot next form without saving
    window.location.href = "/collections/new-tu-conf/";
}