let container = document.getElementById('taxons-table');
let hnd_table;
let emoji_check = '✅';
let emoji_false = '❌';

document.addEventListener('DOMContentLoaded', function() {
    // Process data
    // add links url
    let link_base = link_template.slice(0, -2);
    tax.forEach(function (element) {
        let id = element.id;
        let link = '<a href=' + link_base + id + '/><i class="material-icons"> link </i></a>';
        element.link = link;

        // Replace true/false with material icons Done Clear
        // Catalogue of life
        if (element.col) {
            element.col_html = emoji_check;
        } else {
            element.col_html = emoji_false;
        }
        // IUCN Red List
        if (element.iucn) {
            element.iucn_html = emoji_check;
        } else {
            element.iucn_html = emoji_false;
        }

        // Hybrid
        if (element.hybrid_mark) {
            element.hybrid_mark_html = emoji_check;
        } else {
            element.hybrid_mark_html = ''
        }
    });
    hnd_table = new Handsontable(container, {
        data: tax,
        minSpareRows: 0,
        colHeaders: ['Ссылка', 'ID', 'Семейство', 'Род', 'Видовой эпитет', 'Внутривид. метка', 'Внутривид. эпитет',
                     'Культивар', 'Автор', 'Гибрид', 'Сверено с COL', 'В красном списке'],
        columns: [
            {data: 'link', editor: false, renderer: "html", className: "htCenter"},
            {data: 'id', editor: false},
            {data: 'family_name', editor: false},
            {data: 'genera_name', editor: false},
            {data: 'species_name', editor: false},
            {data: 'infra_mark', editor: false},
            {data: 'infra_name', editor: false},
            {data: 'cult_name', editor: false},
            {data: 'author', editor: false},
            {data: 'hybrid_mark_html', editor: false, className: "htCenter"},
            {data: 'col_html', editor: false, className: "htCenter"},
            {data: 'iucn_html', editor: false, className: "htCenter"}
        ],
        contextMenu: false,
        rowHeaders: true,
        dropdownMenu: ['filter_by_condition2', 'filter_by_value', 'filter_action_bar'],
        // dropdownMenu: true,
        filters: true,
        // preventOverflow: 'horizontal',
        fillHandle: false,
        columnSorting: true,
        autoColumnSize: {useHeaders: true},
        language: 'ru-Ru',
        licenseKey: 'non-commercial-and-evaluation'
    });

    /** The logic for data download (CSV currently) */
    var button = document.getElementById("export-table-btn");
    var export_plugin = hnd_table.getPlugin('exportFile');

    button.addEventListener('click', function() {
        export_plugin.downloadFile('csv', {
            bom: false,
            columnDelimiter: ',',
            columnHeaders: true,
            exportHiddenColumns: true,
            exportHiddenRows: true,
            fileExtension: 'csv',
            filename: 'Коллекционный_фонд_[YYYY]-[MM]-[DD]',
            mimeType: 'text/csv',
            rowDelimiter: '\r\n',
            rowHeaders: false
        });
    });
});
