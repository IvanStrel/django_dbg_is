var url = "/collections/new-tu-3/";

// Get all select elements
var selects;
var checkboxes;

// Count all forms and alter selects
$(document).ready(function(){
    // Get all selects
    selects = $("select");

    // Replace material select with browser select
    // if any dimension less or  equal to 320
    if (($( document ).width() <= 320) || ($( document ).height() <= 320)) {
        selects.each(function(){
            // Detach label
            var lab = $(this).parent().find("label");
            lab.detach();
            // Add browser default class to select element
            $(this).addClass("browser-default");
            // Attach label before select element
            lab.insertBefore( $(this).parent() );
        })
    }

    // Add event listener to each select element
    // it should delete .invalid on select state change
    selects.each(function() {
        $(this).parent().find("input").on("click", function () {
            $(this).removeClass("invalid valid")
        });
        // Fix hidden error message
        $(this).parent().append($(this).parent().parent().find("span.helper-text"));
    });

    // Initiate select inputs
    selects.each(function () {
        $(this).formSelect()
    });

    // Get all checkboxes
    checkboxes = $("#cenotype-group").find("input")
});


function submit_button() {
    /** Submits form data to server
     * 1. loop over each select element
     *    get field name and value
     * 2. collect data-values attributes from selected checkboxes*/

    // Prepare obj fro all form data
    var form_data = {};

    // Get all values from select
    selects.each(function () {
        var select_name = $(this).attr("name");
        var select_val = $(this).find(":selected").val();

        if (select_val === ""){
            select_val = null
        }

        // Populate form data
        form_data[select_name] = select_val
    });

    // Get values of checked checkboxes
    var tu_cenotype = [];
    console.log(checkboxes);
    checkboxes.each(function () {
        if ($(this).prop("checked")){
            tu_cenotype.push($(this).attr("data-value"))
        }
    });

    // Populate form_data with cenotype values
    form_data["tu_cenotype"] = tu_cenotype;

    /** SEnding data to server */
    // Prepare and send POST request
    $.ajax({
        url: url,
        type: "POST", //http method
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            form_data: JSON.stringify(form_data)
        },
        //beforeSend: function() {          // Loader overlay spinner
        //    $(".loader-overlay").show();
        //},
        success: response_handling
    })
}

function response_handling(data) {
    // Check for errors
    if (data.type === "invalid") {
        $('#errors').slideDown(300);
    }

    // If accepted -- redirect to form 4
    if (data.type === "accepted"){
        window.location.href = "/collections/new-tu-4/";
    }
}

function pass_button() {
    // Redirect ot next form without saving
    window.location.href = "/collections/new-tu-4/";
}