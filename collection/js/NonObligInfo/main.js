document.addEventListener('DOMContentLoaded', function() {
    // Tooltips
    var tooltips = document.querySelectorAll('.tooltipped');
    var tooltips_inst = M.Tooltip.init(tooltips, null);

    // Delete modal
    var del_modal = document.getElementById("modal-delete");
    var del_modal_inst = M.Modal.init(del_modal, null);
});

/** Initiate CKeditor */
var ckeditor_area = document.getElementById("ckeditor-col").querySelector("textarea");
console.log(ckeditor_area);
CKEDITOR.replace( ckeditor_area, {
    language: 'ru',
    customConfig: '',
    autoGrow_onStartup: false,
    resize_enabled: true,
    resize_maxHeight: "15em",
    height:"15em",
    toolbar: [
        { name: 'clipboard', items: ['Paste', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'basicstyles', items: [ 'Bold', 'Italic', "Underline"]},
        { name: 'paragraph', items: ["NumberedList", "BulletedList", '-', "Outdent", "Indent" , '-', "Blockquote",
            '-', "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-", "Format"]}
    ]
});


/** Deletion button action */
var del_function = function() {
    // Get form and delete input
    var form = document.forms["add_nonoblig_form"];
    var del_inp = document.getElementById("delete-input");
    // Set del_input value to 1
    del_inp.setAttribute("value", "1");
    // Trigger form submission
    form.submit();
};