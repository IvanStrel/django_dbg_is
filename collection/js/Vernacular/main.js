document.addEventListener('DOMContentLoaded', function() {
    var select_elems = document.querySelectorAll('select');
    var select_inst = M.FormSelect.init(select_elems, null);

    // Tooltips
    var tooltips = document.querySelectorAll('.tooltipped');
    var tooltips_inst = M.Tooltip.init(tooltips, null);
});

/** Actions for deletion */
// Get all del-buttons
var del_btn = document.querySelectorAll('.del-btn');

// Attach action
del_btn.forEach(function (btn) {
   // attach action
   btn.addEventListener('click', function(event) {
       // Get id
       var id = btn.getAttribute("data");
       // Get parent `card-content`
       var card = btn.closest(".card-content");
       var dell_message = card.querySelector(".del-message");
       // Prepare dell-message text
       var del_message_text = document.createElement("p")
           .appendChild(document.createTextNode("Будет удалено после сохранения"));
       // Read Del tag from associated input (could be 1 if was sated for deletion and 0 otherwise)
       var del_input = btn.parentNode.querySelector("input");
       var del_tag = del_input.getAttribute("value");
       if (del_tag === "on") {
           del_input.setAttribute("value", "");
           // Remove color
           card.classList.remove("deleted");
           // Remove any children from delete message div
           while (dell_message.firstChild) {
               dell_message.removeChild(dell_message.firstChild);
           }
       } else {
           del_input.setAttribute("value", "on");
           // Add color and delete text
           card.classList.add("deleted");
           dell_message.appendChild(del_message_text);
       }
       // Change background color
   });
});