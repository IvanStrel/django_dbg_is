document.addEventListener('DOMContentLoaded', function() {
    let elems = document.querySelectorAll('.materialboxed');
    M.Materialbox.init(elems);

    elems = document.querySelectorAll('.collapsible');
    let instances = M.Collapsible.init(elems);
});

function confirm() {
    //elem.disabled = true;
    // Show loader
    let loader_obj = document.getElementById("loader")
    loader_obj.style.display = 'block';
    return true;
}