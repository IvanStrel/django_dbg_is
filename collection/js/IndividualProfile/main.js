/** Materialize */
document.addEventListener('DOMContentLoaded', function() {
    let elems = document.querySelectorAll('.fixed-action-btn');
    let instances = M.FloatingActionButton.init(elems);
  });

document.addEventListener('DOMContentLoaded', function() {
    let elems = document.querySelectorAll('.tooltipped');
    let instances = M.Tooltip.init(elems);
  });

// Modal for printing
document.addEventListener('DOMContentLoaded', function() {
    let modal_elems = document.querySelectorAll('.modal');
    let modal_insts = M.Modal.init(modal_elems);
});

/** Leaflet for territory position */
var territory_map_div = document.getElementById('map');
if (territory_map_div) {
    var dbg_ter_map = new LeafletWithMarkers(territory_map_div, {
        tiles: '/territory-map/{z}/{x}/{y}.png',
        init_zoom: 17,
        mobile: true,
        center: [48.010, 37.88],
        interactive: false,
        perm_marker_point_geojson: [spat_pos]
    });
}

/** Leaflet for origin locatio */
// Check if location map div is on page
var origin_map_div = document.getElementById('location-map');
if (origin_map_div) {
    var origin_loaction_map = new LeafletWithMarkers(origin_map_div, {
        tiles: 'osm',
        init_zoom: 9,
        mobile: true,
        center: [origin_location.coordinates[1], origin_location.coordinates[0]],
        interactive: false,
        perm_marker_point_geojson: [origin_location]
    });
}

/** D3 map for city location */
var city_map_div = document.getElementById('city-globe');
if (city_map_div) {
    var svg = d3.select("#city-globe")
        .append("svg")
        .attr("class", "map");

    // Get #city-globe height and width
    var map_container = document.getElementById('city-globe');
    var height = map_container.offsetHeight;
    var width = map_container.offsetWidth;
    var projection;

    // Function for pulsing circle
    function pulsing_circle(json, projection) {
        console.log(projection);
        var timeCircle = svg.append("circle")
        .attr("cx", projection(json.coordinates)[0])
        .attr("cy", projection(json.coordinates)[0])
        .attr("r", "5px")
        .attr("fill", "red");

        repeat();

        function repeat() {
            timeCircle
                .transition()
                .duration(2000)
                .attr("stroke-width", 4)
                .attr("r", 4)
                .transition()
                .duration(2000)
                .attr('stroke-width', 0.5)
                .attr("r", 14)
                .ease('sine')
                .on("end", repeat);
        }
    }

    Promise.all([d3.json(country_110_topo)]).then(function(data) {
        var world = data[0];
        var countries = topojson.feature(world, world.objects.countries);

        // Fit map size
        projection = d3.geoNaturalEarth1();
        projection.fitSize([width, height], countries);

        var path = d3.geoPath().projection(projection);
        var graticule = d3.geoGraticule();

        // Add Background and graticule
        svg.append("defs").append("path")
            .datum({type: "Sphere"})
            .attr("id", "sphere")
            .attr("d", path);
        svg.append("use")
            .attr("class", "map-stroke")
            .attr("xlink:href", "#sphere");

        svg.append("use")
            .attr("class", "map-fill")
            .attr("xlink:href", "#sphere");

        svg.append("path")
            .datum(graticule)
            .attr("class", "map-graticule")
            .attr("d", path);

        // Add countries
        svg.append("g")
            .attr("class", "map-country")
            .selectAll("path")
            .data(countries.features)
            .enter()
            .append("path")
            .attr("d", path);

        // Add circle
        var timeCircle = svg.append("circle")
            .attr("class", "map-circle")
            .attr("cx", projection(city_location.coordinates)[0])
            .attr("cy", projection(city_location.coordinates)[1])
            .attr("r", "5px")
            .attr("stroke", "red")
            .attr("stroke-width", "3")
            .attr("fill", "none");

        timeCircle.append("animate")
            .attr("attributeName", "r")
            .attr("begin", "0s")
            .attr("dur", "1s")
            .attr("repeatCount", "indefinite")
            .attr("from", "0px")
            .attr("to", "18px");
        timeCircle.append("animate")
            .attr("attributeName", "stroke-width")
            .attr("begin", "0s")
            .attr("dur", "1s")
            .attr("repeatCount", "indefinite")
            .attr("from", "8px")
            .attr("to", "0px");
    });
}