// Materialize initiations
function datepicker_init(elems) {
    M.Datepicker.init(elems, {
        firstDay: true,
        format: 'yyyy-mm-dd',
        minDate: new Date(1964, 1, 1),
        maxDate: new Date(Date.now()),
        showClearBtn: true,
        yearRange: [1964, new Date().getFullYear()],
        i18n: {
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                     "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            weekdays: ["Воскресенье","Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            weekdaysShort: ["Вск","Пон", "Втр", "Срд", "Чет", "Пят", "Суб"],
            weekdaysAbbrev: ["В","По", "В", "Ср", "Ч", "Пя", "Су"],
            cancel: "Отменить",
            clear: "Очистить",
            done: "Готово"
        }
    });
}
document.addEventListener('DOMContentLoaded', function() {
    var date_elems = document.querySelectorAll('.datepicker');
    datepicker_init(date_elems);

    var select_elems = document.querySelectorAll('select');
    M.FormSelect.init(select_elems);
});

/** ====================================================================================
 * Hide / Show tabs depending on radio selects
 * ===================================================================================**/
var city_tab = document.getElementById("city-tab");
var origin_local = document.getElementById("location-tab");

var radios = {
    origin_none: document.getElementById("id_origin_0"),
    origin_local: document.getElementById("id_origin_1"),
    origin_city: document.getElementById("id_origin_3"),
    origin_nature: document.getElementById("id_origin_2")
};

function get_radio_name(radios, radio) {
    var name = Object.entries(radios).find(function (obj) {
        return obj[1] === radio;
    });
    return name[0]
}

Object.entries(radios).forEach(function(obj) {
    obj[1].addEventListener('change', function (e) {
        var radio = e.target;
        // Get selected radio name
        var name = get_radio_name(radios, radio);

        // Change visibility classes depending on current radio
        switch (name) {
            case 'origin_none':
                city_tab.classList.add("invisible");
                origin_local.classList.add("invisible");
                break;
            case 'origin_local':
                city_tab.classList.add("invisible");
                origin_local.classList.add("invisible");
                break;
            case 'origin_city':
                city_tab.classList.remove("invisible");
                origin_local.classList.add("invisible");
                break;
            case 'origin_nature':
                city_tab.classList.add("invisible");
                origin_local.classList.remove("invisible");
                // Invalidate map size
                origin_location_map.invalidate_size();
                break;
            default:
                break;
        }
    })
});

/** ====================================================================================
 * City name select dynamic filters
 * ===================================================================================**/
var city_inputs = {
    country: document.getElementById('id_country'),
    admin: document.getElementById('id_admin'),
    city: document.getElementById('id_city')
};

// Prepare query object
var query_obj = {
    country: null,
    admin: null
};

// Function for city update
function options_update(element, arr, selected_val) {
    // Options list
    var opt_list = element.options;
    // remove all existing options
    while (element.options.length) {
        element.remove(0);
    }
    // Create empty option
    // var new_opt = new Option('----', '0');
    // opt_list.add(new_opt);

    // Populate options form list
    for (var i = 0; i < arr.length; i++) {
        new_opt = new Option(arr[i][1], arr[i][0]);
        // Set selected
        if (arr[i][0] === parseInt(selected_val)) {
            new_opt.selected = true;
        }
        opt_list.add(new_opt)
    }
}

function city_options_update(city_inp, city_arr) {
    // Prepare cities array
    var city_list = city_arr.map(function(entry){
        return [entry[0], entry[1].name_ru]
    });

    options_update(city_inp, city_list)
}

// Function to perform AJAX on select change
function on_city_input_change(e) {
    // Get selected option
    var obj = e.target;

    // Get current selected country and admin
    var cur_admin = city_inputs.admin.options[city_inputs.admin.selectedIndex].value;
    var cur_country = city_inputs.country.options[city_inputs.country.selectedIndex].value;

    // Get input name (country or admin)
    var inp_name = Object.entries(city_inputs).find(function(entry) {
        return entry[1] === obj;
    })[0];

    // Reinitiate query object
    if (inp_name === 'country') {
        // The case when new country was selected (remove selected admin)
        query_obj = {country: cur_country, admin: null};
        cur_admin = null;
    } else {
        query_obj = {country: cur_country, admin: cur_admin};
    }

    // Preform AJAX call
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        if (request.readyState === XMLHttpRequest.DONE) {
            if(request.status === 200) {
                var resp = JSON.parse(request.responseText);
                var admin_arr = resp.admin;
                var city_arr = resp.city;

                // Update lists
                options_update(city_inputs.admin, admin_arr, cur_admin);
                city_options_update(city_inputs.city, city_arr);

                // Reinitiate Materialize select elements
                M.FormSelect.init(city_inputs.admin);
                M.FormSelect.init(city_inputs.city)
            }
        }
    };
    request.open("POST", city_search_url);
    // Add crsf token
    request.setRequestHeader("Content-type", "application/json");
    request.setRequestHeader("X-CSRFToken",
        document.getElementsByName('csrfmiddlewaretoken')[0].value);
    request.send(JSON.stringify(query_obj));
}
city_inputs.country.addEventListener('change', on_city_input_change);
city_inputs.admin.addEventListener('change', on_city_input_change);


/*** ====================================================================================
 * LEAFLET MAP FOR DBG TERRITORY
 * ===================================================================================**/
// Initiate map
var dbg_ter_map = new LeafletWithMarkers(document.getElementById('map'), {
    tiles: '/territory-map/{z}/{x}/{y}.png',
    init_zoom: 18,
    mobile: true,
    center: [48.010, 37.88],
    interactive: true,
    initial_marker_json: initial_ind_pos,
    coord_input: document.getElementById('id_spatial_pos'),
    perm_markers_json: [exist_ind],
    perm_marker_style: [red_marker_style]
});

/*** ====================================================================================
 * LEAFLET MAP FOR LOCAL ORIGIN
 * ===================================================================================**/

var origin_location_map = new LeafletWithMarkers(document.getElementById('location-map'), {
    tiles: 'osm',
    mobile: true,
    init_zoom: 9,
    center: [48.010, 37.88],
    interactive: true,
    initial_marker_json: initial_origin_loc,
    coord_input: document.getElementById('id_origin_loc')
});