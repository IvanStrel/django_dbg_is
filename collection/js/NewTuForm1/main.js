var url = "/collections/new-tu-1/";
var basionym_info = null; // data about basionim if TU is a synonym

// Get all select elements
var selects = $("select");

// Replace material select with browser select
// if any dimension less or  equal to 320
if (($( document ).width() <= 320) || ($( document ).height() <= 320)) {
    selects.each(function(){
        // Detach label
        var lab = $(this).parent().find("label");
        lab.detach();
        // Add browser default class to select element
        $(this).addClass("browser-default");
        // Attach label before select element
        lab.insertBefore( $(this).parent() );
    })
}

// Initiate select inputs
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('select');
    var instances = M.FormSelect.init(elems);
});

// Initialize modals
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelector('.modal');
    var mod_instance = M.Modal.init(elems, {'onCloseEnd': function () {
            var mod_content = document.querySelector(".modal-content");
            var fuzzy_button = document.querySelector("#col-fuzzy-accept-btn");
            var syn_to_bas_button = document.querySelector("#synonym-to-basionym-button");

            if (mod_content) {mod_content.remove()}
            if (fuzzy_button) {fuzzy_button.remove()}
            if (syn_to_bas_button) {syn_to_bas_button.remove()}
        }});
  });

// Add event listener to each select element
// it should delete .invalid on select state change
$(document).ready(function() {
    selects.each(function(){
        $(this).parent().find("input").on("click", function(){
            $(this).removeClass("invalid valid")
        })
    })
});

// Div-s exchange on family switch changes
$(document).ready(function(){
    /** Switch between enter and select options for family input */
    var $family_switch = $('#family-switch');
    // Set col_check state to checked
    $family_switch.prop('checked', false);

    $family_switch.change(function(){
        if (!this.checked){
            $('#family_enter').slideDown(300);
            $('#family_choose').slideUp(300);
        } else {
            $('#family_enter').slideUp(300);
            $('#family_choose').slideDown(300);
        }
    });
});


$(document).ready(function() {
    /** The function for handling family entering by choosing taxonomy from dropdown
     *  Should watch for selection of Class than send POST query to get all
     *  Orders of selected Class and populate choices list for Order field.
     *  Tan repeat the same for Order and Family fields.
     */

    var select_class_field = $('#select_class');
    var select_order_field = $('#select_order');
    var select_family_field = $('#select_family');

    select_class_field.change(function () {
        /** Start watching for select_class choice field and perform POST request on changes*/
        var class_choice = select_class_field.find(':selected').text();

        // Prepare POST request
        $.ajax({
            url: "/search/simple-tax-filter/",
            type: "POST", //http method
            data: {
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                filter: "{'col_class':'" + class_choice + "'}",
                target: "col_order"
            },
            success: function (data) {
                // If POST return success, get data and populate order choices
                // Remove previous Order choices if exist
                select_order_field.find("option").remove();

                // Remove previous Family choices if exist and disable field
                select_family_field.find("option").remove();
                select_family_field.prop( "disabled", true );

                // Populate choices from POST response data
                var items = [];
                items.push('<option value="" disabled selected>Выберите порядок</option>');

                $.each(data, function (i, item) {
                    items.push('<option value="">' + item + '</option>');
                });  // close each()
                // append options to select field
                select_order_field.append(items.join(''));
                // remove disabled
                select_order_field.prop( "disabled", false );

                // update select fields
                //M.FormSelect.init(select_order_field);
                //M.FormSelect.init(select_order_field)
                select_order_field.formSelect();
                select_order_field.parent().find("input").on("click", function(){
                    $(this).removeClass("invalid valid")
                });
                select_family_field.formSelect();
                select_family_field.parent().find("input").on("click", function(){
                    $(this).removeClass("invalid valid")
                })
            }
        });
    });

    select_order_field.change(function() {
        /** Start watching for select_order choice field and perform POST request on changes*/
        var order_choice = select_order_field.find(':selected').text();
        var url = "/search/simple-tax-filter/";


        // Prepare POST request
        $.ajax({
            url: url,
            type: "POST", //http method
            data: {
                csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
                filter: JSON.stringify({'col_class': select_class_field.find(':selected').text(),
                         'col_order': order_choice}),
                target: "col_family"
            },
            success: function (data) {
                // If POST return success, get data and populate family choices
                // Remove previous family choices if exist
                select_family_field.find("option").remove();

                // Populate choices from POST response data
                var items = [];
                items.push('<option value="" disabled selected>Выберите семейство</option>');

                $.each(data, function (i, item) {
                    items.push('<option value="">' + item + '</option>');
                });  // close each()
                // append li's to ul
                select_family_field.append(items.join(''));
                // remove disabled
                select_family_field.prop( "disabled", false );

                // update select fields
                select_family_field.formSelect();
                select_family_field.parent().find("input").on("click", function(){
                    $(this).removeClass("invalid valid")
                })
            }
        });
    });
});


function submit_button() {
    /** The function for form submitting.
     *  First, it performs fields validation, i.e. it looks for missing fields.
     *  Than it composes and sends POST request
     *  Finally it parses POST response and redirect to next form or rises
     *  modal dialogue for clarification */
    // Get all fields values
    var select_family_field = null;
    var family_name = null;
    var family_switch = $('#family-switch');

    var class_name_field = $("#select_class");
    var class_name = null;
    var order_name_field = $("#select_order");
    var order_name = null;

    if (! family_switch.prop("checked")) {
        // Get family name, depending on family-switch position
        select_family_field = $('#id_col_family');
        family_name = select_family_field.val();
        if (family_name === "") {
            family_name = null
        }
    } else {
        select_family_field = $('#select_family');
        family_name = select_family_field.find(":selected").text();
        if (family_name === "Выберите семейство" || family_name === ""){
            family_name = null
        }
        class_name = class_name_field.find(":selected").text();
        if (class_name === "Выберите класс" || class_name === ""){
            class_name = null
        }
        order_name = order_name_field.find(":selected").text();
        if (order_name === "Выберите порядок" || order_name === ""){
            order_name = null
        }
    }

    // Get Genera and Species values
    var genera_field = $('#id_tu_genera');
    var genera_name = genera_field.val();
    if (genera_name === ""){
        genera_name = null
    }

    var species_field = $('#id_tu_species');
    var species_name = species_field.val();
    if (species_name === ""){
        species_name = null
    }

    // Get Infra mark and Infra name
    var infra_mark_field = $('#id_tu_infra_mark');
    var infra_mark = infra_mark_field.find(":selected").text();
    if (infra_mark === "" || infra_mark === "Выберите внутривидовую метку"){
        infra_mark = null
    }

    var infra_name_field = $('#id_tu_infra_name');
    var infra_name = infra_name_field.val();
    if (infra_name === ""){
        infra_name = null
    }

    // Get cultural type and name
    var cult_mark_field = $('#id_tu_cult_mark');
    var cult_mark = cult_mark_field.find(":selected").text();
    if (cult_mark === "" || cult_mark === "Выберите тип культивара"){
        cult_mark = null
    }

    var cult_name_field = $('#id_tu_cult_name');
    var cult_name = cult_name_field.val();
    if (cult_name === ""){
        cult_name = null
    }

    // Get author name
    var author_name_field = $("#id_tu_author_name");
    var author_name = author_name_field.val();
    if (author_name === ""){
        author_name = null
    }

    // Get checkboxes
    var col_choice = $('#id_col_check').prop("checked");
    var hybrid_choice = $('#id_is_hybrid').prop("checked");

    // Check validity
    var is_valid = function(){
        /** The function for fields validation.
         *  It check each field and
         *  return true if all fields valid
         *  and false if at least one field not valid
         */
        // Prepare output valid tag
        var out = false;

        // Remove invalid classes
        genera_field.removeClass("invalid valid");
        species_field.removeClass("invalid valid");
        select_family_field.removeClass("invalid valid");
        infra_mark_field.parent().find("input").removeClass("invalid valid");
        infra_name_field.removeClass("invalid valid");
        cult_mark_field.parent().find("input").removeClass("invalid valid");
        cult_name_field.removeClass("invalid valid");
        author_name_field.removeClass("invalid valid");

        // Check _genera and _name fields
        if (!genera_name){
            genera_field.addClass("invalid");
            out = true;
        } else {
            genera_field.addClass("valid");
        }
        if (!species_name){
            species_field.addClass("invalid");
            out = true;
        } else {
            species_field.addClass("valid");
        }

        // Check _family field if Check with COL check box is unchecked
        if (!col_choice){
            if (!family_name){
                select_family_field.parent().find("input").addClass("invalid");
                select_family_field.parent()
                    .append(select_family_field.parent().parent().find("span.helper-text"));
                out = true;
            } else {
                select_family_field.parent().find("input").addClass("valid");
            }
        } else {
            select_family_field.parent().find("input").addClass("valid");
        }

        // Check that infra species mark is selected if infra name was entered
        if (infra_name && !infra_mark){
            infra_mark_field.parent().find("input").addClass("invalid");
            infra_mark_field.parent().append(cult_mark_field.parent().parent().find("span.helper-text"));
            out = true;
        } else {
            infra_mark_field.parent().find("input").addClass("valid");
        }
        if (!infra_name && infra_mark){
            infra_name_field.addClass("invalid");
            out = true;
        } else {
            infra_name_field.addClass("valid");
        }

        // Check that cultural type is selected if cultural name was entered
        if (cult_name && !cult_mark){
            cult_mark_field.parent().find("input").addClass("invalid");
            cult_mark_field.parent().append(cult_mark_field.parent().parent().find("span.helper-text"));
            out = true;
        } else {
            cult_mark_field.parent().find("input").addClass("valid");
        }
        if (!cult_name && cult_mark){
            cult_name_field.addClass("invalid");
            out = true;
        } else {
            cult_name_field.addClass("valid");
        }

        /* Check author name
        if (!author_name){
            author_name_field.addClass("invalid");
            out = true
        } else {
            author_name_field.addClass("valid");
        }*/

        //$("select").formSelect();
        return(out);
    }();

    // Exit if invalid
    if (is_valid){
        return
    }

    // Prepare and send POST request
    $.ajax({
        url: url,
        type: "POST", //http method
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            type: 'new_tu_1',
            col_class: class_name,
            col_order: order_name,
            col_family: family_name,
            tu_genera: genera_name,
            tu_species: species_name,
            tu_infra_mark: infra_mark_field.find(":selected").val(),
            tu_infra_name: infra_name,
            tu_cult_mark: cult_mark_field.find(":selected").val(),
            tu_cult_name: cult_name,
            tu_author_name: author_name,
            is_hybrid: hybrid_choice,
            col_check: col_choice
        },
        beforeSend: function() {
            $(".loader-overlay").show();
        },
        success: response_handling
        })
}

function response_handling(data){
    /** Function for handling AJAX POST response
     *  1. Rise modal if TU is already exists
     *  2. Rise modal if TU is nor in COL
     *  2.x Different variants of previous
     */

    // Hide loader
    $(".loader-overlay").hide();

    // Process response
    var resp_mod = $("#resp_modal");
    switch (data.type){
        case "exists":
        case 'tnrs_parser_error':
        case 'col_parser_error':
        case "tnrs_no_matched":
            resp_mod.append(data.content);
            resp_mod.modal('open');
            break;
        case "exists_syn":
            resp_mod.append(data.content);

            // suggest to replace appropriate fields with a basionim information
            // Create global variable with basionym info
            basionym_info = data.data;

            // Append button
            resp_mod.find(".modal-footer").append(
                '<a href="#" id="synonym-to-basionym-button" ' +
                'class="modal-action waves-effect waves-green btn-flat" ' +
                'onclick="use_basionym()">Использовать базионим</a>'
            )
            resp_mod.modal('open');
            break;
        case "col_syn":
            // Append html from response to modal body
            resp_mod.append(data.content);

            // Save data about basionym to global variable
            basionym_info = data.data;

            // Append button
            resp_mod.find(".modal-footer").append(
                    '<a href="#" id="synonym-to-basionym-button" ' +
                    'class="modal-action waves-effect waves-green btn-flat" ' +
                    'onclick="col_use_basionym()">Использовать базионим</a>'
                );

            // Show modal
            resp_mod.modal('open');
            break;
        case 'col_fuzzy':
            // Append html from response to modal body
            resp_mod.append(data.content);

            // Save data about basionym to global variable
            basionym_info = data.data;

            // Append button
            resp_mod.find(".modal-footer").append(
                    '<a href="#" id="col-fuzzy-accept-btn" ' +
                    'class="modal-action waves-effect waves-green btn-flat" ' +
                    'onclick="col_use_replace()">Использовать замену</a>'
                );
            resp_mod.modal('open');
            break;
        case "not_col_or_hybrid":
            window.location.href = "/collections/new-tu-2/";
            break;
        case "Accepted":
            // Redirect ro second form page
            window.location.href = "/collections/new-tu-2/";
    }
}

function modals_close(){
    var mod_inst = M.Modal.getInstance(document.querySelector('.modal'));
    mod_inst.close();
    var mod_content = document.querySelector(".modal-content");
    var fuzzy_button = document.querySelector("#col-fuzzy-accept-btn");
    var syn_to_bas_button = document.querySelector("#synonym-to-basionym-button");

    if (mod_content) {mod_content.remove()}
    if (fuzzy_button) {fuzzy_button.remove()}
    if (syn_to_bas_button) {syn_to_bas_button.remove()}
}

function use_basionym() {
    /**
     * Function fires when button on synonym modal is clicked
     * it replaces old values in from (with synonym info) into new values
     * with basionym info
     * @type {*|jQuery|HTMLElement}
     */
    var mod = $('.modal');

    // Prepare variables to change information
    var genera_field = $('#id_tu_genera');
    var species_field = $('#id_tu_species');
    var infra_mark_field = $('#id_tu_infra_mark');
    var infra_name_field = $('#id_tu_infra_name');
    var author_name_field = $("#id_tu_author_name");

    // replace values
    genera_field.val(basionym_info.genera_name);
    species_field.val(basionym_info.species_name);
    infra_mark_field.val(basionym_info.infra_mark);
    infra_mark_field.formSelect();
    infra_name_field.val(basionym_info.infra_name);
    author_name_field.val(basionym_info.author);

    mod.modal('close');
    mod.find(".modal-content").remove();
    mod.find("#synonym-to-basionym-button").remove();

    // Launch form submitting
    //submit_button()
}

function col_use_basionym() {
    var mod = $('.modal');

    // Prepare variables to change information
    var genera_field = $('#id_tu_genera');
    var species_field = $('#id_tu_species');
    var infra_mark_field = $('#id_tu_infra_mark');
    var infra_name_field = $('#id_tu_infra_name');
    var author_name_field = $("#id_tu_author_name");

    // replace values
    genera_field.val(basionym_info.col_genera);
    species_field.val(basionym_info.col_species_name);
    infra_mark_field.val(basionym_info.col_infra_mark);
    infra_mark_field.formSelect();
    infra_name_field.val(basionym_info.col_infra_name);
    author_name_field.val(basionym_info.col_author);

    mod.modal('close');
    mod.find(".modal-content").remove();
    mod.find("#synonym-to-basionym-button").remove();

    // Launch form submitting
    //submit_button()
}


function col_use_replace() {
    // Check if any radio button was selected
    var selector = document.querySelector('input[name="variants"]:checked');
    if(selector === null) {
        alert("Для вненсения изменений сначала нужно выбрать один из предложенных вариантов");
        return;
    }
    // Get index of selected radio button
    var radios = document.getElementsByName('variants');
    var variant_ind;
    var variant_type;

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            // do whatever you want with the checked radio
            variant_ind = radios[i].dataset.id;
            variant_type = radios[i].dataset.type;

            // only one radio can be logically checked, don't check the rest
            break;
        }
    }

    // Prepare variables to change info on form
    var genera_field = document.getElementById('id_tu_genera');
    var species_field = document.getElementById('id_tu_species');
    var infra_mark_field = document.getElementById('id_tu_infra_mark');
    var infra_name_field = document.getElementById('id_tu_infra_name');
    var author_name_field = document.getElementById('id_tu_author_name');

    // Replace values on form
    var checked_dat = basionym_info[variant_ind][variant_type];
    genera_field.value = checked_dat.col_genera;
    species_field.value = checked_dat.col_species_name;
    infra_mark_field.value = checked_dat.col_infra_mark;
    infra_name_field.value = checked_dat.col_infra_name;
    author_name_field.value = checked_dat.col_author;

    // reinitialize infra mark select
    var infra_mark_field_inst = M.FormSelect.init(infra_mark_field, null);
    M.updateTextFields();


    // Get modal instance
    // var mod_inst = M.Modal.init(document.querySelector('.modal'));
    var mod_inst = M.Modal.getInstance(document.querySelector('.modal'));
    mod_inst.close();
    document.querySelector(".modal-content").remove();
    document.querySelector("#col-fuzzy-accept-btn").remove();
}