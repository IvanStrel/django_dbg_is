/** Initiate first CKeditor */
CKEDITOR.disableAutoInline = true;

function init_ckedit_inline(id) {
    CKEDITOR.inline( id, {
        language: 'ru',
        customConfig: '',
        autoGrow_onStartup: false,
        resize_enabled: true,
        resize_maxHeight: "15em",
        height:"15em",
        toolbar: [
            { name: 'clipboard', items: ['Paste', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { name: 'basicstyles', items: [ 'Bold', 'Italic', "Underline"]},
            { name: 'paragraph', items: ["NumberedList", "BulletedList", '-', "Outdent", "Indent" , '-', "Blockquote",
                '-', "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "-", "Format"]}
        ]
    });
}

$(document).ready(function(){
    var forms = $(".form-row:not(#form-templ-btn)").find(".info_text_area");
    forms.each(function () {
        init_ckedit_inline(this)
    });
});

/** Prepare data and activate material components */

var url = "/collections/new-tu-4/";
var form_n;
var form_id;

// Get template elements
var form_temp_btn;

// Count all forms and alter selects
$(document).ready(function(){
    // Get all selects
    form_n = $('.form-row').length - 1; // Two forms for template;
    form_id = JSON.parse(JSON.stringify(form_n));

    form_temp_btn = $('#form-templ-btn');

    // Autoresize text areas
    //M.textareaAutoResize($('#id_form-0-info'));
});

// Initiate buttons
document.addEventListener('DOMContentLoaded', function() {
    // Init tooltips
    var elems_tooltips = document.querySelectorAll('.tooltipped:not(#form-templ-btn)');
    M.Tooltip.init(elems_tooltips);
});


function add_form() {
    /** Place additional form to the page
     * id and name number will be counted from initial form_n (count of forms)
     * each new from will have id = form_id (first form has 0) (even if some forms will be deleted)
     * in such way, there will not be a case with duplicated id*/
    // If there was only one form before function run add del button to it
    if (form_n === 1){
        var del_btn = form_temp_btn.find(".del-btn-wrapper").parent().clone();
        var init_form = $(".form-row:not(#form-templ-btn)");

        // Alter col classes
        init_form.find(".info-text").removeClass("s12 m12 l12");
        init_form.find(".info-text").addClass("s12 m10 l11");

        // Add button
        init_form.append(del_btn);

        // Activate tooltip on the button
        del_btn.find('.tooltipped').tooltip();
    }

    // Construct form fields names and id
    var f_cat_id = "id_form-" + form_id + "-category";
    var f_ref_id = "id_form-" + form_id + "-reference";
    var f_info_id = "id_form-" + form_id + "-info";
    var f_cat_name = "form-" + form_id + "-category";
    var f_ref_name = "form-" + form_id + "-reference";
    var f_info_name = "form-" + form_id + "-info";

    // Place new form to the page
    var template = form_temp_btn.clone();
    // change template id
    template.attr("id", "form-" + form_id);
    template.insertBefore($("#add-btn-wrapper"));

    // Assign id-s and names
    //category
    template.find(".category-text").find("input").attr("id", f_cat_id); // ID
    template.find(".category-text").find("input").attr("name", f_cat_name); // NAME
    template.find(".category-text").find("label").attr("for", f_cat_id); // Label FOR
    // reference
    template.find(".reference-text").find("input").attr("id", f_ref_id); // ID
    template.find(".reference-text").find("input").attr("name", f_ref_name); // NAME
    template.find(".reference-text").find("label").attr("for", f_ref_id); // Label FOR
    // info
    template.find(".info_text_area").attr("id", f_info_id); // ID
    template.find(".info_text_area").attr("name", f_info_name); // NAME

    // Initiate CKeditor
    init_ckedit_inline(f_info_id);

    // Make visible
    template.slideDown(300);

    // increment form_n and from id
    form_id += 1;
    form_n += 1;

    // Update tooltip on button
    template.find('.tooltipped').tooltip();
    template.find('select').formSelect();
}


function del_form(event) {
    /** Remove the form on which button was clicked
     * last form could not be removed
     * if there is only one form remains after deleting remove the delete button
     * alter form_n on each deleting */

    // Check if there are more than one forms on the page
    if (form_n < 2){
        // Do not do nothing
        return
    }

    // Get form container and remove it
    // first remove tooltip to prevent permanent its permanent placement
    var cur_form = event.target.closest(".form-row");

    var tooltip_inst = M.Tooltip.getInstance(cur_form.querySelector(".tooltipped"));
    tooltip_inst.destroy();

    cur_form.remove();

    // Decrement form_n
    form_n -= 1;

    // Remove delete button if only one form remains
    if (form_n === 1){
        // get form
        var last_from = $('.form-row:not(#form-templ-btn)');

        // delete button
        var doomed_btn = last_from.find(".del-btn-col");
        doomed_btn.remove();

        // Alter grid column classes
        last_from.find(".info-text").addClass("s12 m12 l12");
        last_from.find(".info-text").removeClass("s12 m10 l11");
    }
}


function submit_button() {
    /** Submit entered information as post request
     * 1. verify data (all fields entered and now duplicated entries)
     * 2. prepare data for submission
     * 3. handle response*/

    // Read all data
    var form = $('.form-row:not(#form-templ-btn)');
    var form_data = [];
    var form_id_for_valid = [];
    var is_valid = true;
    var iter = 0;

    /** Validation */

    form.each(function () {
        // Get fields
        var cat_field = $(this).find(".category-text").find("input");
        var ref_field = $(this).find(".materialize-textarea");
        var info_field = $(this).find(".info_text_area");

        // Get values
        var cat = $.trim(cat_field.val()).toLowerCase().replace(/\s\s+/g, ' ');
        var ref = ref_field.val().replace(/\s\s+/g, ' ');
        var info = CKEDITOR.instances[info_field.attr("id")].getData();

        // Prepare empty values
        if (cat === ""){
            cat = null
        }
        if (ref === ""){
            ref = null
        }
        if (info.toLowerCase().replace(/\s\s+/g, ' ').includes('введите информацию в этом поле') ||
            info.toLowerCase().replace(/\s\s+/g, ' ')  === "") {
            info = null
        }


        // Validate for single field (validation for duplicates will be outside this loop)
        // first remove previous validation classes
        cat_field.removeClass("invalid valid");
        ref_field.removeClass("invalid valid");
        info_field.removeClass("invalid valid");

        // replace fields error message with required
        $(this).find(".helper-text").attr('data-error', "Поле обязательно для ввода");

        // Check if field is properly filled (all empty or all filled)
        // If all empty
        if ([cat, ref, info].every(function(x) {return(x === null)})) {
            cat_field.addClass("invalid");
            ref_field.addClass("invalid");
            info_field.addClass("invalid");
            is_valid = false
        } else if ([cat, ref, info].some(function(x) {return(x === null)})) { // some null
            if (cat === null) {
                cat_field.addClass("invalid");
            }
            if (ref === null) {
                ref_field.addClass("invalid")
            }
            if (info === null) {
                info_field.addClass("invalid")
            }
            is_valid = false
        } else { // all are entered
            cat_field.addClass("valid");
            ref_field.addClass("valid");
            info_field.addClass("valid");
        }

        // Append data to form_data
        var cat_id = "form-" + iter + "-category";
        var ref_id = "form-" + iter + "-reference";
        var info_id = "form-" + iter + "-info";
        var obj = {};
        obj[cat_id] = cat;
        obj[ref_id] = ref;
        obj[info_id] = info;
        form_data[iter] = obj;

        // Increment iterator
        iter += 1;

        // Append form_id_for_valid with id
        // it looks like I pretended to highlight invalid forms??? but form_id_for_valid never used
        form_id_for_valid.push($(this).attr('id'))
    });

    // If invalid do nothing
    if (!is_valid){
        return
    }

    // Else validate possible duplicates
    // find duplicates: compare each element with each and count matches
    // if element is unique it will match only itself
    // otherwise number of matches will be greater
    // --> forms under index with matches > 1 are invalid due to duplicates
    var dup_ind = [];
    dup_ind.length = form_data.length;
    dup_ind.fill(0);

    form_data.forEach(function(x, index_x){
        form_data.forEach(function(y) {
            if (JSON.stringify(Object.values(x)) === JSON.stringify(Object.values(y))) {
                dup_ind[index_x] += 1
            }
        })
    });

    // Place invalid state to invalid forms
    // iterate over dup_ind for values > 2
    // forms id in same order as dup_ind are in form_id_for_valid
    dup_ind.forEach(function(value, index) {
        if (value > 1) {
            is_valid = false;

            // find form
            var form_field = $('#' + form_id_for_valid[index]);

            // add error message to text and text area
            var placement = form_field.find(".helper-text");
            $(placement).attr('data-error', "Повторяющаяся информация");

            // add class invalid
            form_field.find("input").removeClass("valid invalid").addClass("invalid");
            form_field.find(".materialize-textarea").removeClass("valid invalid").addClass("invalid");
            form_field.find(".info_text_area").removeClass("valid invalid").addClass("invalid");
        }
    });

    // If invalid do nothing
    if (!is_valid){
        return
    }

    /** SEnding data to server */
    // Prepare and send POST request
    $.ajax({
        url: url,
        type: "POST", //http method
        data: {
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
            form_data: JSON.stringify(form_data)
        },
        //beforeSend: function() {          // Loader overlay spinner
        //    $(".loader-overlay").show();
        //},
        success: response_handling
    })
}

function response_handling(data) {
    // Check for errors
    if (data.type === "invalid") {
        $('#errors').slideDown(300);
    }

    // If accepted -- redirect to form 5
    if (data.type === "accepted"){
        window.location.href = "/collections/new-tu-5/";
    }
}

function pass_button() {
    // Redirect ot next form without saving
    window.location.href = "/collections/new-tu-5/";
}