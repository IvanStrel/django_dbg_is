/** Nav bar initialization */
document.addEventListener('DOMContentLoaded', function() {
    // Tabs initialization
    var tabs_elems = document.querySelectorAll('.tabs');
    var tab_instance = M.Tabs.init(tabs_elems, null, null);

    // Tooltips
    var tooltips = document.querySelectorAll('.tooltipped');
    var tooltips_inst = M.Tooltip.init(tooltips, null);

    // Vernacular modal
    var vern_modal = document.getElementById("modalvern");
    var vern_modal_inst = M.Modal.init(vern_modal, null);

    // Collapsible
    var collaps = document.querySelectorAll('.collapsible');
    var collaps_inst = M.Collapsible.init(collaps, null);

    // Distribution map
    var dist_map = document.getElementById('distribution-map-country');
    var dist_map_inst = M.Materialbox.init(dist_map, null);
});


/** Individuals timeline graph ====================================================================================== */
// All this code should work only if there are non empty `ind_json` object
if (ind_json.length) {
    // Prepare data variables
    var min_dat = Math.min.apply(Math, ind_json.map(function (o) {
        var income = Date.parse(o.fields.income_date);
        // Check for cases with empty income date,
        // if such, return input date.
        // There is a problem with 1970-01-01 as it return 0, so all next `if` statements return False
        // Therefore we will check for income == 0
        if (!income && (income !== 0)) {
            return Date.parse(o.fields.input_date);
        }
        return income;
    }));
    // If there were not any
    var max_dat = Date.now();

    var ind_number = ind_json.length;

    // set the dimensions and margins of the graph
    d3_container = document.getElementById("ind-timeline");
    var parent_w = d3_container.offsetWidth;
    var margin = {top: 20, right: 30, bottom: 40, left: 40},
        width = parent_w - margin.left - margin.right,
        height = ind_number * 20;

// append the svg object to the body of the page
    var svg = d3.select("#ind-timeline")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    // Parse the Data
    // Add X axis ======================================================================================================
    // Prepare time forma (with days or years only)
    var time_format;
    if ((max_dat - min_dat) > (365 * 24 * 60 * 60 * 1000)) {
        time_format = d3.timeFormat("%Y");
    } else {
        time_format = d3.timeFormat("%Y-%m");
    }
    var x = d3.scaleTime()
        .domain([min_dat, max_dat])
        .range([0, width]);
    // Use the custom scale
    var xAxis = d3.axisBottom()
        .scale(x)
        .tickFormat(time_format);
    // Get max width
    var max_width = x(max_dat) - x(min_dat);
    var time_delta = max_dat - min_dat;

    // Calcualte number of ticks
    var ticks_n = 10;
    if (parent_w < 800) {
        ticks_n = 7;
    }
    if (parent_w < 600) {
        ticks_n = 5;
    }
    if (parent_w < 400) {
        ticks_n = 3;
    }

    // Append x scale
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        // Add custom axis
        .call(xAxis.ticks(ticks_n))
        // .call(d3.axisBottom(x))
        .selectAll("text")
        // .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "center");

    // Add Y axis ======================================================================================================
    var y = d3.scaleBand()
        .range([0, height])
        .domain(ind_json.map(function (d) {
            return d.pk;
        }))
        .padding(.0);

    svg.append("g")
        .call(d3.axisLeft(y));

    // Add Bars ========================================================================================================
    svg.selectAll("myRect")
        .data(ind_json)
        .enter()
        .append("rect")
        .attr("x", function (d) {
            var income = d.fields.income_date;
            if (income) {
                return x(Date.parse(income));
            } else {
                return x(0);
            }
        })
        .attr("y", function (d) {
            return y(d.pk);
        })
        .attr("width", function (d) {
            var income = Date.parse(d.fields.income_date);
            // If income absent, replace it with input
            if (!income && (income !== 0)) {
                income = Date.parse(d.fields.input_date);
            }
            var out = Date.parse(d.fields.out_date);
            if (out) {
                return max_width * ((out - income) / time_delta);
            } else {
                return max_width * ((max_dat - income) / time_delta);
            }
        })
        .attr("height", y.bandwidth())
        .attr("fill", function (d) {
            var out = d.fields.out_date;
            if (out) {
                return ("#dc7963")
            } else {
                return "#69b3a2";
            }
        })
        // Append tooltip to each rectangle (bar)
        .append("svg:title").text(function(d) {
            var text = "Особь № ";
            text += d.pk;
            return text;
        });

    // Add Labels ======================================================================================================
    // Add X axis label:
    svg.append("text")
        .attr("text-anchor", "end")
        .attr("x", width)
        .attr("y", height + margin.top + 10)
        .text("Дата");

    // Y axis label:
    svg.append("text")
        .attr("text-anchor", "end")
        .attr("transform", "rotate(-90)")
        .attr("y", -margin.left + 15)
        .attr("x", -margin.top + 10)
        .text("ID");
}

/** Individuals map ================================================================================================ */
// Next should work only if existing individuals are present
if (ind_json.length) {
    var map = L.map('ind-map').setView([48.010, 37.88], 17);
    L.tileLayer('/territory-map/{z}/{x}/{y}.png', {
        maxZoom: 22,
        attribution: '&copy; ГУ "ДБС"',
        drag_options: {dragging: !L.Browser.mobile, tap: !L.Browser.mobile}
    }).addTo(map);


    /** Create points layer for existent individuals
     * Here `exist_ind` is a json object, created in html
     */
    var exist_ind_lyr = L.geoJSON(exist_ind.features, {
        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, {
                radius: 5,
                fillColor: "red",
                color: "black",
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8
            }).bindTooltip(feature.properties.pk, {
                permanent: true,
                direction: 'auto',
                opacity: 0.9,
                className: 'leaflet-tooltip-style'
            });
        },
        onEachFeature: function (feature, layer) {
            layer.bindPopup('<h6>' + tu_name + '</h6>' +
                            '<p> номер в базе данных: ' +feature.properties.pk+ '</p>' +
                            '<p> полевой номер:' +((feature.properties.field_number)? feature.properties.field_number : ' нет')+ '</p>' +
                            '<h6><a href="' + ind_prof_path_templ + feature.properties.pk + '">Профайл</a></h6>');
        }
    });

    exist_ind_lyr.addTo(map);
}

/** TU gallery ===================================================================================================== */
// check_webp_feature:
//   'feature' can be one of 'lossy', 'lossless', 'alpha' or 'animation'.
//   'callback(feature, isSupported)' will be passed back the detection result (in an asynchronous way!)
function check_webp_feature(feature, callback) {
    var kTestImages = {
        lossy: "UklGRiIAAABXRUJQVlA4IBYAAAAwAQCdASoBAAEADsD+JaQAA3AAAAAA",
        lossless: "UklGRhoAAABXRUJQVlA4TA0AAAAvAAAAEAcQERGIiP4HAA==",
        alpha: "UklGRkoAAABXRUJQVlA4WAoAAAAQAAAAAAAAAAAAQUxQSAwAAAARBxAR/Q9ERP8DAABWUDggGAAAABQBAJ0BKgEAAQAAAP4AAA3AAP7mtQAAAA==",
        animation: "UklGRlIAAABXRUJQVlA4WAoAAAASAAAAAAAAAAAAQU5JTQYAAAD/////AABBTk1GJgAAAAAAAAAAAAAAAAAAAGQAAABWUDhMDQAAAC8AAAAQBxAREYiI/gcA"
    };
    var img = new Image();
    img.onload = function () {
        var result = (img.width > 0) && (img.height > 0);
        callback(feature, result);
    };
    img.onerror = function () {
        callback(feature, false);
    };
    img.src = "data:image/webp;base64," + kTestImages[feature];
}

// Check webp support
var webp = false;
check_webp_feature('lossy', function (feature, isSupported) {
    if (isSupported) {
        webp = true;
    }
});

// Photoswipe
var pswpElement = document.querySelectorAll('.pswp')[0];
var options = {
        // optionName: 'option value'
        // for example:
        index: 0, // start at first slide
        shareButtons: [
            // {id:'facebook', label:'Share on Facebook', url:'https://www.facebook.com/sharer/sharer.php?u={{url}}'},
            // {id:'twitter', label:'Tweet', url:'https://twitter.com/intent/tweet?text={{text}}&url={{url}}'},
            // {id:'pinterest', label:'Pin it', url:'http://www.pinterest.com/pin/create/button/?url={{url}}&media={{image_url}}&description={{text}}'},
            {id:'download', label:'Download image', url:'', download:true}
        ]
    };

// Bound click listener for each image thumbnail
img_instanec = document.querySelectorAll(".gallery-img");
Array.from(img_instanec).forEach(function (elem) {
    elem.addEventListener("click", function () {
        var index = Number.parseInt(this.getAttribute("data-id"));
        options.index = index;
        // Set download url
        options.shareButtons[0].url = slides_jpg[index].largeImage.src;

        // Create gallery object with appropriate image type
        var slides;
        if (webp) {
            slides = slides_webp;
        } else {
            slides = slides_jpg;
        }
        var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, slides, options);

        // Responsive images
        // create variable that will store real size of viewport
        var realViewportWidth,
            useLargeImages = false,
            useMediumImages = false,
            useSamllImages = false,
            firstResize = true,
            imageSrcWillChange;

        gallery.listen('beforeResize', function() {
            // calculate real pixels when size changes
            realViewportWidth = gallery.viewportSize.x * window.devicePixelRatio;

            // Code below is needed if you want image to switch dynamically on window.resize

            // Find out if current images need to be changed
            if(useLargeImages && realViewportWidth < 2304) {
                useLargeImages = false;
                imageSrcWillChange = true;
            } else if(useMediumImages && realViewportWidth < 1024) {
                useMediumImages = false;
                imageSrcWillChange = true;
            } else if(useSamllImages && realViewportWidth < 500) {
                useSamllImages = false;
                imageSrcWillChange = true;
            } else if(!useLargeImages && realViewportWidth >= 2304) {
                useLargeImages = true;
                imageSrcWillChange = true;
            } else if(!useMediumImages && realViewportWidth >= 1024) {
                useMediumImages = true;
                imageSrcWillChange = true;
            } else if(!useSamllImages && realViewportWidth >= 500) {
                useSamllImages = true;
                imageSrcWillChange = true;
            }

            // Invalidate items only when source is changed and when it's not the first update
            if(imageSrcWillChange && !firstResize) {
                // invalidateCurrItems sets a flag on slides that are in DOM,
                // which will force update of content (image) on window.resize.
                gallery.invalidateCurrItems();
            }

            if(firstResize) {
                firstResize = false;
            }

            imageSrcWillChange = false;

        });

        // gettingData event fires each time PhotoSwipe retrieves image source & size
        gallery.listen('gettingData', function(index, item) {

            // Set image source & size based on real viewport width
            if ( useLargeImages ) {
                item.src = item.largeImage.src;
                item.w = item.largeImage.w;
                item.h = item.largeImage.h;
            } else if ( useMediumImages ) {
                item.src = item.mediumImage.src;
                item.w = item.mediumImage.w;
                item.h = item.mediumImage.h;
            } else if ( useSamllImages ) {
                item.src = item.smallImage.src;
                item.w = item.smallImage.w;
                item.h = item.smallImage.h;
            } else {
                item.src = item.thumbImage.src;
                item.w = item.thumbImage.w;
                item.h = item.thumbImage.h;
            }
        });

        gallery.init();
    });
});