/** Initiate page elements */
// prepare function for initiation
function datepicker_init(elems) {
    M.Datepicker.init(elems, {
        firstDay: true,
        format: 'yyyy-mm-dd',
        minDate: new Date(1964, 1, 1),
        maxDate: new Date(Date.now()),
        showClearBtn: true,
        yearRange: [1964, new Date().getFullYear()],
        i18n: {
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь",
                     "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            weekdays: ["Воскресенье","Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
            weekdaysShort: ["Вск","Пон", "Втр", "Срд", "Чет", "Пят", "Суб"],
            weekdaysAbbrev: ["В","По", "В", "Ср", "Ч", "Пя", "Су"],
            cancel: "Отменить",
            clear: "Очистить",
            done: "Готово"
        }
    });
}

function dropify_init(elems) {
    elems.dropify({
        messages: {
            'default': 'Перетащите файл сюда или кликните мышью',
            'replace': 'Чтобы изменить перетащите новый файл или кликните мышью',
            'remove':  'Удалить',
            'error':   'Что-то пошло не так'
        },
        error: {
            'fileSize': 'Файл слишком большой (допустимый максимум: {{ value }}).',
            'minWidth': 'Ширина изображения слишком маленькая (допустимый минимум {{ value }} px).',
            'minHeight': 'Высота изображения слишком маленькая (допустимый минимум {{ value }} px).',
            'imageFormat': 'Недопустимый формат изображения ({{ value }} только)',
            'fileExtension': 'Недопустимый тип изображения ({{ value }} только)'
        }
    });

    // Initiate watcher for changed status
    elems.each(function(){
        let obj = $(this);
        // Add change listener
        obj.on("change", function(){
            $(this).attr("data-changed", "changed");
            obj.parent().find(".dropify-error").text("Что-то пошло не так")
        });
        // Add after delete listener
        obj.on('dropify.afterClear', function(){
            $(this).attr("data-changed", "changed");
            // Hide error message
            obj.parent().find(".dropify-error").text("Что-то пошло не так")
        });

    })
}

function star_rating_init(elems) {
    elems.find('.star').on("click", function () {
        let obj = $( this );
        let value = obj.attr("data-value");
        let parent = obj.parent().parent();

        // Change obj class value-*
        parent.attr('class', function(i, c){
            return c.replace(/\d+/, value);
        });
        parent.find("input").attr('value', value)
    })
}

// Init elements
document.addEventListener('DOMContentLoaded', function() {
    // Init tooltips
    let elems_tooltips = document.querySelectorAll(".tooltipped");
    M.Tooltip.init(elems_tooltips);

    // init data pickers
    let elems = document.querySelectorAll(".datepicker");
    datepicker_init(elems);

    // init characters counter on text inputs
    $('[data-length]').characterCounter();

    // init dropify
    dropify_init($("#id_img"));

    // init star container
    star_rating_init($("#star-input"));

    /** Make columns height equal */
    const info_h = document.getElementById('info-col').offsetHeight;
    document.getElementById("img-col").style.height = String(info_h) + 'px';
});

function confirm(elem) {
    elem.disabled = true;
    // Show loader
    let loader_obj = document.getElementById("loader")
    loader_obj.style.display = 'block';
    // Submit by default
    //elem.form.submit();
}