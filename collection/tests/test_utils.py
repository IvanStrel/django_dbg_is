from django.test import TestCase
from collection.utils import col_query
from collection.utils import redlist_query
from unittest import mock
import requests
import json


class TestColQueryBaseCases(TestCase):

    def test_col_query_accepted(self):
        """
        Test for base case with full name acceptance
        :return: none
        """
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_status"], "Accepted")
        self.assertEqual(res["genus"], "Ficus")
        self.assertEqual(res["species"], "benjamina")
        self.assertEqual(res["infraspecies"], "")
        self.assertEqual(res["parser_error"], "")

    def test_col_query_synonym(self):
        """
        Test for base case with full name match but as synonym
        Should be an accepted name (basionym) for current synonym
        :return: none
        """
        res = col_query("ficus", "benghalensis", "krishnae", "var.", "(C. DC.) C. DC.")
        self.assertEqual(res["parser_status"], "Synonym")
        self.assertEqual(res["genus"], "Ficus")
        self.assertEqual(res["species"], "benghalensis")
        self.assertEqual(res["infraspecies"], "")  # Because it is basionym and not a synonym
        self.assertEqual(res["parser_error"], "")

    def test_col_query_wrong_author(self):
        """
        Test for case with mismatched author name
        Should return 'parser_status' = "AuthorMismatch" and few variants
        :return: none
        """
        res = col_query("ficus", "benjamina", "", "", "Linnaeus.")
        self.assertEqual(res["parser_status"], "AuthorMismatch")

        # The length of variants should be at least 1
        self.assertGreaterEqual(1, len(res["variants"]))
        self.assertEqual(res["parser_error"], "")

    def test_col_query_wrong_infra_mark(self):
        """
        Test for case with mismatched infra_mark
        Should return 'parser_status' = "InfraMarkMismatch" and few variants
        :return: none
        """
        res = col_query("ficus", "benghalensis", "krishnae", "f.", "(C. DC.) C. DC.")
        self.assertEqual(res["parser_status"], "InfraMarkMismatch")

        # The length of variants should be at least 1
        self.assertGreaterEqual(1, len(res["variants"]))
        self.assertEqual(res["parser_error"], "")

    def test_col_query_wrong_infra_mark_and_author(self):
        """
        Test for case with mismatched both infra_mark and author name
        Should return 'parser_status' = "InfraMarkAndAuthorMismatch" and few variants
        :return: none
        """
        res = col_query("ficus", "benghalensis", "krishnae", "f.", "L.")
        self.assertEqual(res["parser_status"], "InfraMarkAndAuthorMismatch")

        # The length of variants should be at least 1
        self.assertGreaterEqual(1, len(res["variants"]))
        self.assertEqual(res["parser_error"], "")

    def test_col_query_no_names_found(self):
        """
        Test for case with mismatched both infra_mark and author name
        Should return 'parser_status' = "InfraMarkAndAuthorMismatch" and few variants
        :return: none
        """
        res = col_query("foo", "bar", "baz", "qux", "quux")
        self.assertEqual(res["parser_status"], "NoNamesFound")
        self.assertEqual(res["parser_error"], "")


# Testing different variants of errors handling in 'test_col_query'
class TestColQueryErrors(TestCase):

    @mock.patch('requests.get')
    def test_col_connection_error(self, mock_get):
        """
        Test for base case with full name acceptance
        :return: none
        """
        # ConnectionError case
        def connection_error(*args, **kwargs):
            raise requests.exceptions.ConnectionError

        mock_get.side_effect = connection_error
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "ConnectionError")
        self.assertEqual(res["parser_status"], "Error")

        # HTTPError case
        def http_error(*args, **kwargs):
            raise requests.exceptions.HTTPError

        mock_get.side_effect = http_error
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "HTTPError")
        self.assertEqual(res["parser_status"], "Error")

        # Timeout case
        def timeout_error(*args, **kwargs):
            raise requests.exceptions.Timeout

        mock_get.side_effect = timeout_error
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "Timeout")
        self.assertEqual(res["parser_status"], "Error")

        # TooManyRedirects case
        def too_many_redirects_error(*args, **kwargs):
            raise requests.exceptions.TooManyRedirects

        mock_get.side_effect = too_many_redirects_error
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "TooManyRedirects")
        self.assertEqual(res["parser_status"], "Error")

        # TooManyRedirects case
        def unexpected_error(*args, **kwargs):
            raise requests.exceptions.RequestException

        mock_get.side_effect = unexpected_error
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "UnexpectedError")
        self.assertEqual(res["parser_status"], "Error")

        # Test case, with unexpected 'error message' in json response
        def unexpected_error_in_json(*args, **kwargs):
            class Response:
                pass

            resp = Response()
            resp.text = '{"error_message": "Some weird message"}'
            return resp

        mock_get.side_effect = unexpected_error_in_json
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "UnexpectedError")
        self.assertEqual(res["parser_status"], "Error")

        # Test case when no "NoNamesFound" error was raised and
        # check without Author and InfraMark did not bring any suitable matches
        def weird_names_in_json(*args, **kwargs):
            class Response:
                pass

            resp = Response()
            resp.text = '''{"error_message": "",
                            "results": [
                                        {
                                            "genus": "Some genus",
                                            "species": "some species",
                                            "infraspecies": "",
                                            "infraspecies_marker": "",
                                            "author": "Some Author"
                                        }
                                       ]}'''
            return resp

        mock_get.side_effect = weird_names_in_json
        res = col_query("ficus", "benjamina", "", "", "L.")
        self.assertEqual(res["parser_error"], "SomethingWrongInCOL")
        self.assertEqual(res["parser_status"], "Error")


class TestRedListQueryBaseCases(TestCase):

    def test_red_list_query_in_red_list(self):
        """
        Test for base case with species is in red list
        :return: none
        """
        res = redlist_query("Acer", "skutchii", "")
        self.assertEqual(res["parser_status"], "InRedList")
        self.assertEqual(res["common_name"], 'Álamo plateado')
        self.assertEqual(res["category"], "CR")
        self.assertEqual(res["criteria"], "B2ab(iii,v)")
        self.assertEqual(res["parser_error"], "")
        self.assertEqual(len(res["distribution"]), 2)

    def test_red_list_query_not_in_red_list(self):
        """
        Test for base case with some weird name (not in red list)
        :return: none
        """
        res = redlist_query("some", "species", "subspecies")
        self.assertEqual(res["parser_status"], "NotInRedList")
        self.assertEqual(res["parser_error"], "")