from django.test import TestCase
from django.test import Client
import re
import codecs


class NewTuFirstTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #           Test first tu form POST request
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The case where TU is already exists
    def test_frst_tu_exists(self):
        c = Client()
        response = c.post('/collections/new-tu-1/', {'type': 'new_tu_1',
                                                     'itis_class': '',
                                                     'itis_order': '',
                                                     'isti_family': '',
                                                     'tu_genera': 'ficus',
                                                     'tu_species': 'benjamina',
                                                     'tu_infra_mark': '',
                                                     'tu_infra_name': '',
                                                     'tu_cult_mark': 'cultivar',
                                                     'tu_cult_name': 'variegata',
                                                     'tu_author_name': 'L.',
                                                     'is_hybrid': False,
                                                     'col_check': True})
        resp_text = codecs.decode(response.content, 'unicode_escape')
        self.assertEqual(bool(re.search('Введенная ТЕ уже существует', resp_text)), True)
        self.assertEqual(bool(re.search('Введенная ТЕ является синонимом', resp_text)), False)

    # The case where TU is exists as synonym
    def test_frst_tu_exists_synonym(self):
        c = Client()
        response = c.post('/collections/new-tu-1/', {'type': 'new_tu_1',
                                                     'itis_class': '',
                                                     'itis_order': '',
                                                     'isti_family': '',
                                                     'tu_genera': 'ficus',
                                                     'tu_species': 'nitida',
                                                     'tu_infra_mark': '',
                                                     'tu_infra_name': '',
                                                     'tu_cult_mark': '',
                                                     'tu_cult_name': '',
                                                     'tu_author_name': 'Thunb.',
                                                     'is_hybrid': False,
                                                     'col_check': True})
        resp_text = codecs.decode(response.content, 'unicode_escape')
        self.assertEqual(bool(re.search('Введенная ТЕ является синонимом', resp_text)), True)
        self.assertEqual(bool(re.search('<i>Ficus</i>', resp_text)), True)
        self.assertEqual(bool(re.search('<i>nitida</i>', resp_text)), True)

    # The case where TU is a synonym in COL
    def test_frst_tu_col_synonym(self):
        c = Client()
        response = c.post('/collections/new-tu-1/', {'type': 'new_tu_1',
                                                     'itis_class': '',
                                                     'itis_order': '',
                                                     'isti_family': '',
                                                     'tu_genera': 'ficus',
                                                     'tu_species': 'comosa',
                                                     'tu_infra_mark': '',
                                                     'tu_infra_name': '',
                                                     'tu_cult_mark': '',
                                                     'tu_cult_name': '',
                                                     'tu_author_name': 'Roxb.',
                                                     'is_hybrid': False,
                                                     'col_check': True})
        resp_text = codecs.decode(response.content, 'unicode_escape')
        self.assertEqual(bool(re.search('Вводимая ТУ является синонимом', resp_text)), True)
        self.assertEqual(bool(re.search('<i>Ficus</i>', resp_text)), True)
        self.assertEqual(bool(re.search('<i>comosa</i>', resp_text)), True)
        self.assertEqual(bool(re.search('<i>benjamina</i>', resp_text)), True)

    def test_new_tu_form1_get_request(self):
        c = Client()
        response = c.get("/collections/new-tu-1/")

        resp_text = response.content.decode()
        self.assertEqual(bool(re.search('Новая единица, шаг 1', resp_text)), True)