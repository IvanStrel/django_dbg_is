import re
from django.test import TestCase
from datetime import date
from django.utils.dateparse import parse_date

from collection.models import TaxonomicUnit
from collection.models import Taxonomy
from collection.models import ObligatoryInfo
from collection.models import ColData


class BaseTestCase(TestCase):
    fixtures = ["fixture_for_test.json"]

    def setUp(self):
        pass

    def test_taxonomic_lookup(self):
        taxon = TaxonomicUnit.objects.get(cult_name__iexact="variegata")
        self.assertEqual(taxon.species_name, "benjamina")
        family = Taxonomy.objects.filter(taxonomicunit__cult_name="variegata").get()
        self.assertEqual(family.itis_family, "Moraceae")


# Test Taxonomic Unit model
class TaxonomicUnitTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    def setUp(self):
        # Create some test species
        TaxonomicUnit.objects.get_or_create(
            genera_name="Ficus",
            species_name="benjamina",
            infra_mark="f.",
            infra_name="some_form",
            author="L.",
            cult_mark="cultivar",
            cult_name="something",
            hybrid_mark=False,
            family_tsn=Taxonomy.objects.filter(itis_family__iexact="Moraceae").get(),
            input_author="somebody",
            input_date=date.today()
        )

    def test_TaxonomicUnit_set_up(self):
        obj = TaxonomicUnit.objects.filter(genera_name__iexact="Ficus", input_author__iexact="somebody").get()

        # Test if dates are equal (simple test to check if entry was created properly)
        self.assertEqual(date.today(), obj.input_date)

        # Test if cult_mark "cultivar" is equal to that expected from choice field
        self.assertEqual(obj.cult_mark, TaxonomicUnit.CULTIVAR)

        # Test if infra_mark ".f" is equal to that expected from choice field
        self.assertEqual(obj.infra_mark, TaxonomicUnit.FORM)

    def test_TaxonomicUnit_str(self):
        """
        Test for right __str__ representation of TaxonomicUnit model object
        """
        obj = TaxonomicUnit.objects.filter(genera_name__iexact="Ficus", input_author__iexact="somebody").get()

        obj_string = str(obj)
        test_string = "F. " + "benjamina" + " f. " + "some_form"
        self.assertEqual(test_string, obj_string)


class ObligatoryInfoTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    def setUp(self):
        # Add obligatory info for Ficus benjamina L. 'variegata'
        ObligatoryInfo.objects.get_or_create(
            tu_id=TaxonomicUnit.objects.filter(genera_name__iexact="ficus", cult_name__iexact="variegata").get(),
            # Now add some random choices
            raunc_form="geo",
            moisture="hygro",
            light="scihel",
            fertility="meso",
            salinity="halo",
            acidity="neutro",
            phenotype="f1",
            cenotype="g"
        )

    def test_OblygatoryInfo_set_up(self):
        obj = ObligatoryInfo.objects.filter(tu_id__cult_mark__iexact="cultivar",
                                            tu_id__cult_name__iexact="variegata").get()

        # Test if choices are equal to models properties
        self.assertEqual(obj.raunc_form, ObligatoryInfo.GEOPHYTES)
        self.assertEqual(obj.moisture, ObligatoryInfo.HYGROPHYTE)
        self.assertEqual(obj.light, ObligatoryInfo.SCIOHELIOPHYTE)
        self.assertEqual(obj.fertility, ObligatoryInfo.MESOTROPH)
        self.assertEqual(obj.salinity, ObligatoryInfo.HALOPHYTE)
        self.assertEqual(obj.acidity, ObligatoryInfo.NEUTROPHYTE)
        self.assertEqual(obj.phenotype, ObligatoryInfo.PHENO_F1)
        self.assertEqual(obj.cenotype, [ObligatoryInfo.CENO_G])

    def test_OblygatoryInfo_str(self):
        """
        Test for right __str__ representation of ObligatoryInfo model object
        """
        obj = ObligatoryInfo.objects.filter(tu_id__cult_mark__iexact="cultivar",
                                            tu_id__cult_name__iexact="variegata").get()

        self.assertEqual(bool(re.search("Additional info, " + str(8) + " columns filled", str(obj))), True)


class TaxonomyTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    def setUp(self):
        # Create a Taxonomy object
        Taxonomy.objects.get_or_create(
            itis_kingdom="Plantae",
            itis_subkingdom="Viridiplantae",
            itis_infrakingdom="Streptophyta",
            itis_superdivision="Embryophyta",
            itis_division="Tracheophyta",
            itis_subdivision="Spermatophytina",
            itis_infradivision=None,
            itis_superclass=None,
            itis_class="Magnoliopsida",
            itis_subclass=None,
            itis_infraclass=None,
            itis_superorder="Rosanae",
            itis_order="Rosales",
            itis_suborder=None,
            itis_family="Moraceae",
            itis_family_id=2222
        )

    def test_Taxonomy_str(self):
        """
        Test for right __str__ representation of Taxonomy model object
        """

        obj = Taxonomy.objects.filter(itis_family_id=2222).get()

        target_str = 'Magnoliopsida' + " " + 'Rosales' + " " + "Moraceae" + "_" + str(2222)
        obj_str = str(obj)

        self.assertEqual(target_str, obj_str)


class ColDataTest(TestCase):
    fixtures = ["fixture_for_test.json"]

    def setUp(self):
        # Create object with fake infra mark and infra name
        ColData.objects.get_or_create(
            tu_id=TaxonomicUnit.objects.filter(pk=2).get(),
            col_id=22221,
            col_url="some/url/",
            col_ref="some/col/ref",
            genera_name="ficus",
            species_name="benjamina",
            infra_mark=".f",
            infra_name="some_form",
            author="L.",
            ref_author="Some author",
            ref_year="2018",
            ref_title="Some ref title",
            ref_res="Some ref res",
            last_check=parse_date("2018-06-12"),
            check_need=False
        )

    def test_ColData_str(self):
        """
        Test for right __str__ representation of Taxonomy model object
        """

        obj = ColData.objects.filter(col_id=22221).get()

        target_str = "COL " + "F" + ". " + "benjamina" + " "\
                     + ".f" + " " + "some_form" + " ColID = " + str(22221)
        obj_str = str(obj)

        self.assertEqual(target_str, obj_str)
