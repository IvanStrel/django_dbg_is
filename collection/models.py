from django.contrib.gis.db import models
from django.contrib.gis.db.models import PointField
from django.db.models import JSONField
from multiselectfield import MultiSelectField
from ckeditor.fields import RichTextField

from slugify import slugify
from datetime import date, datetime

from .utils import img_convert_to_buffer
from .utils import normalize_string

from users_and_organization.models import Departments
from users_and_organization.models import User

from django.db.models import F
from django.db.models import Q
from django.db.models import Count
from django.db.models import When
from django.db.models import Case

from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.indexes import GistIndex

from spatial.models import PopulatedPlaces

from transliterate import translit

from PIL import Image
from PIL import ImageOps


class ColTaxonomy(models.Model):
    """
    The table which will contain taxonomic data from Catalogue Of Life.
    This data must be obtained from DB dump through `col_preparation.sh` script in /taxonomic_data folder.
    """
    col_family_id = models.IntegerField(primary_key=True)
    col_family = models.CharField(max_length=100, null=True)
    col_order = models.CharField(max_length=100, null=True)
    col_class = models.CharField(max_length=100, null=True)
    col_phylum = models.CharField(max_length=100, null=True)
    col_kingdom = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name = "Catalogue Of Life taxonomy"
        verbose_name_plural = "Catalogue Of Life taxonomy"

    def __str__(self):
        out_str = f"The family: {self.col_family.capitalize()}"
        return out_str


class Taxonomy(models.Model):
    """
    The table which will contain taxonomic data from ITIS.
    This data must be obtained from DB dump and transformed into wide format.
    Further, it could be updated according to ITIS updates.
    """
    itis_kingdom = models.CharField(max_length=100, null=True)
    itis_subkingdom = models.CharField(max_length=100, null=True)
    itis_infrakingdom = models.CharField(max_length=100, null=True)
    itis_superdivision = models.CharField(max_length=100, null=True)
    itis_division = models.CharField(max_length=100, null=True)
    itis_subdivision = models.CharField(max_length=100, null=True)
    itis_infradivision = models.CharField(max_length=100, null=True)
    itis_superclass = models.CharField(max_length=100, null=True)
    itis_class = models.CharField(max_length=100, null=True)
    itis_subclass = models.CharField(max_length=100, null=True)
    itis_infraclass = models.CharField(max_length=100, null=True)
    itis_superorder = models.CharField(max_length=100, null=True)
    itis_order = models.CharField(max_length=100, null=True)
    itis_suborder = models.CharField(max_length=100, null=True)
    itis_family = models.CharField(max_length=100, null=True)
    itis_family_id = models.IntegerField(primary_key=True)
    itis_name_usage = models.CharField(max_length=100, null=True)

    class Meta:
        verbose_name = "ITIS taxonomy"
        verbose_name_plural = "ITIS taxonomy"

    def __str__(self):
        itis_class, itis_order = ' -- ', ' -- '
        if self.itis_class is not None:
            itis_class = self.itis_class

        if self.itis_order is not None:
            itis_order = self.itis_order

        return itis_class + " " + itis_order + " " + self.itis_family.strip() + "_" + str(self.itis_family_id)


#  ==================================================================================================================
#  COL Species data model and managers
#  ==================================================================================================================
class ColSpeciesManager(models.Manager):

    def col_species_exact_match(self, genera, species, infra_mark, infra_name, author):
        """
        Check for exact match with entry in catalogue of life species data
        :param genera: sting genera name
        :param species: string species epithet
        :param infra_mark: string infraspecies marker
        :param infra_name: string infraspecies epithet
        :param author: string author
        :return: ColSpecies object or None
        """
        # Replace empty strings with None
        genera, species, infra_mark, infra_name, author = normalize_string(genera, species, infra_mark,
                                                                           infra_name, author, to_none=True)

        '''
        In order to utilize indexes we have to use raw sql. The problem is there should be
        different SQL depending on parameters: ILIKE 'param' if param is string and `IS NULL` if param is `None`;
        Therefore we will create a template with %s placeholders where needed and populate it in `raw()` with
        not `None` parameters
        Here we want to pass any user defined author if col_author is NULL.
        Also we want to return first accepted name if request return two or more names (rare case when the name with
        author is accepted, but the same name without author is a synonym of an another name)
        '''
        sql_template = f'''SELECT * 
                           FROM "collection_colspecies"
                           WHERE "col_genera" {'ILIKE %(genera)s' if genera else 'IS NULL'}
                                AND "col_species_name" {'ILIKE %(species)s' if species else 'IS NULL'}
                                AND "col_infra_mark" {'ILIKE %(infra_mark)s' if infra_mark else 'IS NULL'}
                                AND "col_infra_name" {'ILIKE %(infra_name)s' if infra_name else 'IS NULL'}
                                AND ({'"col_author" ILIKE %(author)s OR' if author else ''} "col_author" IS NULL)
                           ORDER BY "col_status";
                        '''
        # Prepare parameters dict
        sql_params = {
            'genera': genera,
            'species': species,
            'infra_mark': infra_mark,
            'infra_name': infra_name,
            'author': author,
        }
        # Perform raw SQL query
        exist_obj = self.get_queryset().raw(sql_template, params=sql_params,)
        if len(exist_obj):
            return exist_obj[0]  # Should be only one
        else:
            return None

    def col_species_is_synonym(self, table, genera, species, infra_mark, infra_name, author):
        """
        Check if provided name is a synonym of existing TaxonomicUnit entry
        :param genera: sting genera name
        :param species: string species epithet
        :param infra_mark: string infraspecies marker
        :param infra_name: string infraspecies epithet
        :param author: string author
        :return: TaxonomicUnit query objects or None
        """
        # Replace empty strings with None
        genera, species, infra_mark, infra_name, author = normalize_string(genera, species, infra_mark,
                                                                           infra_name, author, to_none=True)

        '''
        Here again we use raw SQL for indexes utilisation. (see explanation in `col_species_exact_match` method)
        '''
        sql_template = f'''SELECT
                             ct.*
                           FROM {table} as ct
                           LEFT JOIN collection_colspecies cc_ac on ct.col_id = cc_ac.col_id
                           LEFT JOIN collection_colspecies cc_syn on cc_ac.col_id = cc_syn.col_accepted_id
                           WHERE cc_syn."col_genera" {'ILIKE %(genera)s' if genera else 'IS NULL'}
                                AND cc_syn."col_species_name" {'ILIKE %(species)s' if species else 'IS NULL'}
                                AND cc_syn."col_infra_mark" {'ILIKE %(infra_mark)s' if infra_mark else 'IS NULL'}
                                AND cc_syn."col_infra_name" {'ILIKE %(infra_name)s' if infra_name else 'IS NULL'}
                                AND cc_syn."col_author" {'ILIKE %(author)s' if author else 'IS NULL'}
                                AND cc_syn."used_by_tu_id" IS NULL;
                        '''
        # Prepare parameters dict
        sql_params = {
            'genera': genera,
            'species': species,
            'infra_mark': infra_mark,
            'infra_name': infra_name,
            'author': author,
        }
        # Perform raw SQL query
        exist_obj = self.get_queryset().raw(sql_template, params=sql_params, )
        if len(exist_obj):
            return exist_obj[0]  # Should be only one
        else:
            return None

    def search_alike(self, genera, species, infra_mark, infra_name, author):
        """
        Get alike scientific names as solutions for typos fixing or misused names replacement.
        Similar scientific names are searched with trigrams
        :param genera: sting genera name
        :param species: string species epithet
        :param infra_mark: string infraspecies marker
        :param infra_name: string infraspecies epithet
        :param author: string author
        :return: ColSpecies objects or None
        """
        # Replace empty strings with None and normalize
        genera, species, infra_mark, infra_name, author = normalize_string(genera, species, infra_mark,
                                                                           infra_name, author, to_none=True)

        # Construct scientific name string
        spec_name = f'{genera} {species}'
        infra_name = f'{" " if infra_mark else ""}{infra_mark or ""}{" " if infra_name else ""}{infra_name or ""}'
        full_name = f'{spec_name}{infra_name}{" " if author else ""}{author or ""}'

        # Perform trigram search
        matches = self.get_queryset().raw('''
                            SELECT *
                            FROM "collection_colspecies"
                            WHERE "collection_colspecies"."col_full_name" %% %s
                            ORDER BY "collection_colspecies"."col_full_name" <-> %s
                            LIMIT 5;
                            ''', [full_name, full_name])
        return matches


class ColSpecies(models.Model):
    col_id = models.IntegerField(unique=True, primary_key=True, null=False)
    col_identifier = models.CharField(max_length=255, null=True)
    col_dataset = models.CharField(max_length=255, null=True)
    col_dataset_id = models.CharField(max_length=255, null=True)
    col_status = models.CharField(max_length=8, null=True)
    col_accepted = models.ForeignKey("ColSpecies", on_delete=models.CASCADE, null=True, blank=True)
    col_family = models.ForeignKey(ColTaxonomy, on_delete=models.SET_NULL, null=True, blank=True)
    col_genera = models.CharField(max_length=255, null=True)
    col_species_name = models.CharField(max_length=255, null=True)
    col_infra_name = models.CharField(max_length=255, null=True)
    col_infra_mark = models.CharField(max_length=20, null=True)
    col_author = models.CharField(max_length=100, null=True)
    col_db_id = models.CharField(max_length=255, null=True)
    col_link = models.CharField(max_length=255, null=True)
    col_full_name = models.CharField(max_length=512, null=True)
    col_name_ref = models.CharField(max_length=1024, null=True)
    col_full_name_html = models.CharField(max_length=512, null=True)
    used_by_tu = models.ForeignKey("TaxonomicUnit", on_delete=models.SET_NULL, null=True, blank=True)

    # Attach managers
    objects = ColSpeciesManager()

    class Meta:
        verbose_name = "COL species taxonomic datum"
        verbose_name_plural = "COL species taxonomic data"
        indexes = [GistIndex(name="col_sp_gist_trgm_idx", fields=("col_full_name",), opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_genera_gist_trgm_idx", fields=("col_genera",), opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_species_gist_trgm_idx", fields=("col_species_name",),
                             opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_infra_mark_gist_trgm_idx", fields=("col_infra_mark",),
                             opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_infra_name_gist_trgm_idx", fields=("col_infra_name",),
                             opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_author_gist_trgm_idx", fields=("col_author",),
                             opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_status_gist_trgm_idx", fields=("col_status",),
                             opclasses=("gist_trgm_ops",)),
                   GistIndex(name="col_sp_multy_name_gist_trgm_idx",
                             fields=("col_species_name", "col_infra_name", "col_infra_mark",
                                     "col_infra_name", "col_author", "col_genera"),
                             opclasses=("gist_trgm_ops", "gist_trgm_ops", "gist_trgm_ops",
                                        "gist_trgm_ops", "gist_trgm_ops", "gist_trgm_ops")), ]

    def __str__(self):
        lat_name = f'{self.col_genera.capitalize()} {self.col_species_name}' \
                   f'{" " if self.col_infra_mark else ""}' \
                   f'{self.col_infra_mark or ""}{" " if self.col_infra_name else ""}' \
                   f'{self.col_infra_name or ""} {self.col_author} [{self.col_status}]'
        return f'COL: {lat_name}'

    def html_str(self):
        """
        Pretty HTML formatted taxonomic unit full name
        :return: string
        """
        name_html = f"<i>{self.col_genera.capitalize()} {self.col_species_name} </i>"
        if self.col_infra_mark is not None and self.col_infra_name is not None:
            name_html = f"{name_html} {str(self.col_infra_mark)} <i>{self.col_infra_name}</i>"
        name_html = f"{name_html} {self.col_author}"
        return name_html


#  ==================================================================================================================
# Taxonomic Unit model and managers
#  ==================================================================================================================

class TaxonomicUnitManager(models.Manager):
    def collection_home_table(self):
        queryset = self.get_queryset().prefetch_related().annotate(alive_count=Count("individual__is_alive")) \
            .annotate(family_name=F("family_col__col_family")) \
            .annotate(in_col=Case(
            When(col__isnull=False, then=True),
            default=False,
            output_field=models.BooleanField()
        )
        ) \
            .annotate(iucn=F("redlistiucn__in_red_list")).filter(alive_count__gt=0).order_by("full_name")
        return queryset

    def tax_unit_exact_match(self, genera, species, infra_mark, infra_name, cult_mark, cult_name, author):
        """
        Check if taxonomic unit is already in database
        :param genera: sting genera name
        :param species: string species epithet
        :param infra_mark: string infraspecies marker
        :param infra_name: string infraspecies epithet
        :param cult_mark: string cultural mark
        :param cult_name: string cultural name
        :param author: string author
        :return: TaxonomicUnit query object or None
        """
        # Replace empty strings with None
        genera, species, infra_mark, infra_name, cult_mark, cult_name, author = \
            normalize_string(genera, species, infra_mark, infra_name, cult_mark, cult_name, author, to_none=True)

        exist_obj = self.get_queryset().filter(genera_name__iexact=genera,
                                               species_name__iexact=species,
                                               infra_mark__iexact=infra_mark,
                                               infra_name__iexact=infra_name,
                                               cult_mark=cult_mark,
                                               cult_name=cult_name) \
            .filter(Q(author__iexact=author) | Q(author=None)).prefetch_related()

        if exist_obj.exists():
            return exist_obj  # Should be only one
        else:
            return None

    def full_search_sql(self, qd):
        """
        The model manager method to return SQL and parameters for extended search query with a set of
        filters provided with `CollectionSearchRequestData` instance.

        The output could be directly used in `raw` queries:

        #Example:
        from .utils import CollectionSearchRequestData
        qd = CollectionSearchRequestData(type='ind', ind_ids=[1, 2, 3])
        raw_sql, args_list = TaxonomicUnit.full_search_sql(qd)
        # Get query:
        TaxonomicUnit.objects.raw(raw_sql, params=args_list)

        :param qd: `CollectionSearchRequestData` dataclass with values for filters
        :return: (str: raw SQL, dict: parameters for raw SQL)
        """
        '''
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        Construct SQL query
        ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        '''
        args_list = qd.prepared_dict()
        where_cond = 'WHERE '  # WHERE clauses
        where_nuber = 0  # Number of added WHERE clauses

        tu_cols = "tu.id, tu.genera_name, tu.species_name, tu.infra_mark, tu.infra_name, tu.author, tu.cult_mark, "\
                  "tu.cult_name, tu.hybrid_mark, tu.input_date, tu.full_html_name, "\
                  "CASE WHEN col_id IS NULL THEN False ELSE True END AS col_check, "
        ind_cols = "ind.id AS ind_id, ind.is_alive, ind.income_date, ind.out_date, ind.cult_type, ind.source,"\
                   "ind.origin, city.id AS city_id, city.name_ru, ind.spatial_pos, "
        fam_cols = "fam.col_family, fam.col_order, fam.col_class, fam.col_phylum, "
        red_cols = "red.in_red_list, red.category, "
        dep_cols = "dep.name, "
        obl_cols = "obl.raunc_form, obl.moisture, obl.light, obl.fertility, obl.salinity, obl.acidity, "\
                   "obl.phenotype, obl.cenotype"
        extra = fam_cols + red_cols + dep_cols + obl_cols

        # Construct raw SQL `SELECT ... JOIN` part
        raw_sql = f'''SELECT DISTINCT {tu_cols + extra if qd.type == "tu" else tu_cols + ind_cols + extra}
                      FROM collection_taxonomicunit AS tu
                      LEFT JOIN collection_individual AS ind ON ind.tu_id_id = tu.id
                      LEFT JOIN spatial_populatedplaces AS city ON ind.city_id = city.id
                      LEFT JOIN collection_coltaxonomy AS fam on tu.family_col_id = fam.col_family_id
                      LEFT JOIN collection_redlistiucn AS red ON red.tu_id_id=tu.id 
                      LEFT JOIN collection_obligatoryinfo AS obl ON obl.tu_id_id=tu.id
                      LEFT JOIN collection_taxonomicunitimages img on tu.id = img.tu_id_id '''
        # JOIN departments if needed
        if qd.departments_ids:
            # Filter out only individuals belonging to leaf node departments for provided departments ids
            raw_sql += """
                INNER JOIN (SELECT
                       CASE
                           WHEN t3.name IS NOT NULL THEN t3.name
                           WHEN t2.name IS NOT NULL THEN t2.name
                           WHEN t1.name IS NOT NULL THEN t1.name
                       ELSE t4.name END AS name,
                       CASE
                           WHEN t3.name IS NOT NULL THEN t3.id
                           WHEN t2.name IS NOT NULL THEN t2.id
                           WHEN t1.name IS NOT NULL THEN t1.id
                       ELSE t4.id END AS id
                FROM users_and_organization_departments AS t1
                LEFT JOIN users_and_organization_departments AS t2 ON t2.parent_id = t1.id
                LEFT JOIN users_and_organization_departments AS t3 ON t3.parent_id = t2.id
                LEFT JOIN users_and_organization_departments AS t4 ON t4.parent_id = t3.id
                WHERE t1.id IN %(departments_ids)s) AS dep ON ind.department_id_id=dep.id """
        else:
            raw_sql += "LEFT JOIN users_and_organization_departments AS dep ON ind.department_id_id=dep.id "
        # Spatial lookup ========================================================================================
        # populate raw sql
        raw_sql += 'LEFT JOIN spatial_polygonslevel1 AS sp1 ON ST_Intersects(sp1.poly, ind.spatial_pos) '\
            if qd.sp_level_1 else ''
        raw_sql += 'LEFT JOIN spatial_polygonslevel2 AS sp2 ON ST_Intersects(sp2.poly, ind.spatial_pos) ' \
            if qd.sp_level_2 else ''
        raw_sql += 'LEFT JOIN spatial_polygonslevel3 AS sp3 ON ST_Intersects(sp3.poly, ind.spatial_pos) ' \
            if qd.sp_level_3 else ''

        # populate WHERE clauses
        if qd.sp_level_1:
            where_cond += '( sp1.id IN %(sp_level_1)s '
            where_nuber += 1
        if qd.sp_level_2:
            where_cond += 'OR sp2.id IN %(sp_level_2)s ' if where_nuber else '( sp2.id IN %(sp_level_2)s '
            where_nuber += 1
        if qd.sp_level_3:
            where_cond += 'OR sp3.id IN %(sp_level_3)s ' if where_nuber else '( sp3.id IN %(sp_level_3)s '
            where_nuber += 1
        # Close bracket if needed
        if qd.sp_level_1 or qd.sp_level_2 or qd.sp_level_3:
            where_cond += ') '

        '''
        Now we will add a bunch of where clauses for all not None fields in `qd` dataclass
        for each new clause we have to decide if we need to prepend it with AND or not (i.e. is this clause is first one
        in the list or not).
        For this we can check `where_nuber` on each not None filed, and prepend AND if `where_nuber` is > 0 (i.e. there
        were WHERE clauses before this one) and increment `where_nuber`.
        Instead we will insert hardcoded condition (not from qd dataclass), so all further WHERE clauses will
        require AND statement.
        '''
        where_cond += 'AND ind.id IS NOT NULL ' if where_nuber else 'ind.id IS NOT NULL '
        '''
        As tu.id is a prime key it could not be NULL, therefore this statement will not change the output.
        On the other hand, now we are sure that there was at least one WHERE clause, therefore each next clause will
        be with AND statemnt.
        '''

        # COL lookup and hybrid  ======================================================================================
        # Here we need to use OR if both `in_col` and `not_in_col` were selected
        if qd.in_col and qd.not_in_col:
            # Do nothing (it means we need all of entries)
            pass
        elif qd.in_col:
            # Checked with COL only
            where_cond += 'AND tu.col_id IS NOT NULL '
        else:
            # Not checked with COL only
            where_cond += 'AND tu.col_id IS NULL '
        if qd.hybrid_only:
            where_cond += 'AND tu.hybrid_mark = %(hybrid_only)s '

        # Filter Alive only  ==========================================================================================
        if qd.alive_only:
            where_cond += 'AND ind.is_alive=%(alive_only)s '

        # Filter by TU and Individual ids =============================================================================
        if qd.tu_ids:
            where_cond += 'AND tu.id IN %(tu_ids)s '
        if qd.ind_ids:
            where_cond += 'AND ind.id IN %(ind_ids)s '

        # Filter images only  =========================================================================================
        if qd.with_images:
            where_cond += 'AND img.id IS NOT NULL '

        # Filter by RedList data ======================================================================================
        if qd.in_red_list:
            where_cond += 'AND red.in_red_list=%(in_red_list)s '
        if qd.red_list_cat:
            where_cond += 'AND LOWER(red.category) IN %(red_list_cat)s '

        # Filter by income data =======================================================================================
        if qd.after_time:
            where_cond += 'AND ind.income_date > %(after_time)s '
        if qd.before_time:
            where_cond += 'AND ind.income_date <= %(before_time)s '

        # Taxonomy lookup =============================================================================================
        if qd.col_phylum:
            where_cond += 'AND fam.col_phylum IN %(col_phylum)s '
        if qd.col_class:
            where_cond += 'AND fam.col_class IN %(col_class)s '
        if qd.col_order:
            where_cond += 'AND fam.col_order IN %(col_order)s '
        if qd.col_family:
            where_cond += 'AND fam.col_family IN %(col_family)s '
        if qd.genera_name:
            where_cond += 'AND tu.genera_name IN %(genera_name)s '

        # Ecology lookup ==============================================================================================
        if qd.raunc_form:
            where_cond += 'AND obl.raunc_form IN %(raunc_form)s '
        if qd.moisture:
            where_cond += 'AND obl.moisture IN %(moisture)s '
        if qd.light:
            where_cond += 'AND obl.light IN %(light)s '
        if qd.fertility:
            where_cond += 'AND obl.fertility IN %(fertility)s '
        if qd.salinity:
            where_cond += 'AND obl.salinity IN %(salinity)s '
        if qd.acidity:
            where_cond += 'AND obl.acidity IN %(acidity)s '
        if qd.phenotype:
            where_cond += 'AND obl.phenotype IN %(phenotype)s '
        if qd.cenotype:
            '''
            Cenotype is stored as string where variants are separated with commas.
            For example 'a,j'. Therefore we need to convert such string into an array
            and check if it intersects with an input array. The Input is given as a tuple ('a', 'j')
            but to be passed into SQL as ARRAY it should be a list -- ['a', 'b']. It means,
            we need to convert tuple into list.
            '''
            args_list["cenotype"] = list(qd.cenotype)
            where_cond += "AND string_to_array(obl.cenotype, ',') && %(cenotype)s::text[] "

        # Add ; at the end of SQL
        raw_sql += where_cond + ";"
        # Return SQL string and parameters Dict
        return raw_sql, args_list

    def full_search(self, qd):
        """
        Direct implementation of TaxonomicUnit.full_search_sql ==> RawQuerySet
        :param qd: `CollectionSearchRequestData` dataclass with values for filters
        :return: RawQuerySet
        """
        raw_sql, args_list = self.full_search_sql(qd)
        return self.get_queryset().raw(raw_sql, params=args_list, )

    def count_top_n_for_tu(self, qd, target, n):
        """
        Given qd object, count occurances of target for unique TU.id
        Targets could be any column in `self.full_search_sql` query
        For example: target = 'col_order' and n = 5,
        will return counts for top 5 most abundant `orders` and sum count of other orders
        :param qd: `CollectionSearchRequestData` dataclass with values for filters
        :param target: target field
        :param n: number of top most abundant items to return (other will be counted as `Others`)
        :return: RawQuerySet [TaxonomicUnit object with additional columns: `target_name` and `counts`]
        """
        # Preapre raw sql for
        raw_filter_sql, args_list = self.full_search_sql(qd)

        # Prepare raw sql for counting
        # !!! Here I use direct insertion of `target` into SQL, be aware. This value should
        # not come from user. Otherwise it could be a target for SQL INJECTION ATTACK
        raw_sql = f"""
                    WITH cte AS (
                       SELECT MIN(id) AS id, {target} AS target_name, count(*) AS counts
                            , row_number() OVER (ORDER BY count(*) DESC, {target}, MIN(id)) AS rn
                       FROM   (SELECT DISTINCT ON (filter.id)
                                      filter.id, filter.{target}
                               FROM ({raw_filter_sql.replace(';', '')}) AS filter) AS res
                       GROUP BY res.{target}
                    )
                    (  -- parentheses required again
                    SELECT id, target_name, counts, rn
                    FROM   cte
                    WHERE  rn <= {n}
                    ORDER  BY rn
                    )
                    UNION ALL
                    SELECT null, 'Прочие x10', sum(counts) / 10, {n + 1}
                    FROM   cte
                    WHERE  rn > {n}
                    HAVING count(*) > 0;
                    """
        # Populate args list
        args_list.update({'target': target, 'n': n})
        return self.get_queryset().raw(raw_sql, params=args_list, )

    def cumulative_tu_income_outcome(self, qd):
        """
        Returns cumulative count of alive and dead TU by years from 1964 to today year + 1
        :param qd: `CollectionSearchRequestData` dataclass with values for filters
        :return: RawQuerySet
        """
        # Get bound dates
        from_year = '1964-01-01'
        to_year = f'{datetime.today().year + 1}-01-01'

        raw_filter_sql, args_list = self.full_search_sql(qd)
        # TODO this SQL is awful. Consider to optimize it. Still it works.
        raw_sql = f"""
                    SELECT (SELECT MIN(id) FROM collection_taxonomicunit) AS id, t.year,
                           (SELECT SUM(CASE
                                       WHEN inner_res.alive > 0 THEN 1
                                       ELSE 0
                                       END) AS alive
                           FROM (
                                SELECT t.year,
                                       tu.id,
                                       SUM(CASE
                                               WHEN ind.income_date <= t.year
                                                   AND (ind.out_date > t.year
                                                       OR ind.out_date IS NULL) THEN 1
                                               ELSE 0
                                           END) AS alive
                                FROM collection_taxonomicunit AS tu
                                         LEFT JOIN ({raw_filter_sql.replace(';', '')}) AS ind ON ind.id = tu.id
                                WHERE ind.out_date IS NOT NULL
                                   OR ind.income_date IS NOT NULL
                                GROUP BY tu.id
                            ) AS inner_res
                           ) AS alive,
                           (SELECT SUM(CASE
                                       WHEN inner_res.dead > 0 AND inner_res.alive = 0 THEN 1
                                       ELSE 0
                                       END) AS dead
                           FROM (
                                SELECT t.year,
                                       tu.id,
                                       SUM(CASE
                                               WHEN ind.income_date <= t.year
                                                   AND (ind.out_date > t.year
                                                       OR ind.out_date IS NULL) THEN 1
                                               ELSE 0
                                           END) AS alive,
                                       SUM(CASE
                                               WHEN ind.out_date <= t.year THEN 1
                                               ELSE 0
                                           END) AS dead
                                FROM collection_taxonomicunit AS tu
                                         LEFT JOIN ({raw_filter_sql.replace(';', '')}) AS ind ON ind.id = tu.id
                                WHERE ind.out_date IS NOT NULL
                                   OR ind.income_date IS NOT NULL
                                GROUP BY tu.id
                            ) AS inner_res
                           ) AS dead
                    FROM generate_series(timestamp '{from_year}'
                                         ,timestamp '{to_year}'
                                         ,interval  '1 year') AS t(year);
                    """
        # Populate args_list
        return self.get_queryset().raw(raw_sql, params=args_list, )

    def tu_income_outcome_by_year(self, dep_id, from_year, to_year):
        """
        Returns taxonomic units which were added or lost from `from_year` to present
        each TU is accomplished with year of status change and type:
            ↗ = income, ↘️ = outcome
        :param dep_id: the department id
        :param from_year: from which year to return counts
        :return: RawQueryset
        """
        # Check that `from year is in range from 1964 to present`
        from_year = int(from_year)
        to_year = int(to_year)
        assert to_year >= from_year >= 1964, f'`from_year` should be in range 1964-{to_year}'
        raw_sql = """
            SELECT tu.id, INITCAP(tu.genera_name) AS genera_name, tu.species_name,
                   tu.infra_mark, tu.infra_name, tu.cult_name, tu.author,
                   -- Convert alive_now alive_before into `income` or `outcome`
                   CASE
                       WHEN res.alive_now = 1 AND res.alive_before = 0 THEN '↗'
                       ELSE '↘️'
                       END AS type,
                   EXTRACT(YEAR FROM res.year) AS year
            FROM collection_taxonomicunit AS tu
            -- JOIN alive dead markers for each year
            LEFT JOIN (
                SELECT ct.id, t.year, ci.department_id_id,
                   max(CASE
                       WHEN ci.out_date <= (t.year + interval '1 year')
                                AND ci.out_date is not null
                                OR ci.income_date > (t.year + interval '1 year')
                            THEN 0
                       ELSE 1
                       END) AS alive_now,
                   max(CASE
                       WHEN ci.out_date <= (t.year)
                                AND ci.out_date is not null
                                OR ci.income_date > (t.year)
                            THEN 0
                       ELSE 1
                       END) AS alive_before
                FROM collection_taxonomicunit AS ct
                LEFT JOIN collection_individual ci on ct.id = ci.tu_id_id,
                     generate_series(timestamp %(from_year)s,
                                     timestamp %(to_year)s,
                                     interval '1 year') AS t(year)
                WHERE ci.income_date is not null
                GROUP BY ct.id, t.year, ci.department_id_id
                ) AS res ON tu.id = res.id
            -- JOIN (filter by) departments
            INNER JOIN (SELECT
                   CASE
                       WHEN t3.name IS NOT NULL THEN t3.name
                       WHEN t2.name IS NOT NULL THEN t2.name
                       WHEN t1.name IS NOT NULL THEN t1.name
                   ELSE t4.name END AS name,
                   CASE
                       WHEN t3.name IS NOT NULL THEN t3.id
                       WHEN t2.name IS NOT NULL THEN t2.id
                       WHEN t1.name IS NOT NULL THEN t1.id
                   ELSE t4.id END AS id
            FROM users_and_organization_departments AS t1
            LEFT JOIN users_and_organization_departments AS t2 ON t2.parent_id = t1.id
            LEFT JOIN users_and_organization_departments AS t3 ON t3.parent_id = t2.id
            LEFT JOIN users_and_organization_departments AS t4 ON t4.parent_id = t3.id
            WHERE t1.id = %(dep_id)s) AS dep ON res.department_id_id=dep.id
            -- Filter out tu without changes i.e. alive_now == alive_before
            WHERE (res.alive_now != res.alive_before)
            ORDER BY res.year, genera_name;
        """

        # Populate args_list
        args_list = {'to_year': f"{to_year}-01-01",
                     'from_year': f"{from_year}-01-01",
                     'dep_id': dep_id}

        return self.get_queryset().raw(raw_sql, params=args_list, )


class TaxonomicUnit(models.Model):
    """
    The table which will contain data bout taxonomic units.
    """
    # Prepare choices for infra_mark
    EMPTY = ""
    SUBSPECIES = "subsp."
    VARIATION = "var."
    SUBVARIATION = "subvar."
    FORM = "f."
    SUBFORM = "subf."

    infra_mark_choices = (
        (EMPTY, ""),
        (SUBSPECIES, "подвид"),
        (VARIATION, "вариация"),
        (SUBVARIATION, "подвариация"),
        (FORM, "форма"),
        (SUBFORM, "подформа"),
    )

    # Prepare choices for infra_mark cult_mark
    CULTIVAR = "cultivar"
    GROUP = "group"
    GRAFT_CHIMERA = "graft-chimera"
    GREX = "grex"

    cult_mark_choices = (
        (EMPTY, ""),
        (CULTIVAR, "культивар"),
        (GROUP, "группа культиваров"),
        (GRAFT_CHIMERA, "трансплантационная химера"),
        (GREX, "грекс"),
    )

    genera_name = models.CharField(max_length=100, null=False, blank=False)
    species_name = models.CharField(max_length=100, null=False, blank=False)
    infra_mark = models.CharField(max_length=10, null=True, choices=infra_mark_choices, blank=True)
    infra_name = models.CharField(max_length=100, null=True, blank=True)
    author = models.CharField(max_length=100, null=True, blank=True)
    cult_mark = models.CharField(max_length=13, null=True, choices=cult_mark_choices, blank=True)
    cult_name = models.CharField(max_length=100, null=True, blank=True)
    hybrid_mark = models.BooleanField(null=False)
    # Foreign key from COL Taxonomy
    family_col = models.ForeignKey(ColTaxonomy, null=True, on_delete=models.SET_NULL, blank=False)
    # Foreign key from COL Species
    col = models.ForeignKey(ColSpecies, null=True, on_delete=models.SET_NULL, blank=True)
    # Input history
    input_author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=False, null=True)
    input_date = models.DateField(null=True, auto_now_add=True, blank=True)
    # Full name for trigram search
    full_name = models.CharField(max_length=523, null=True, blank=True)
    full_html_name = models.CharField(max_length=523, null=True, blank=True)

    # Custom manager
    objects = TaxonomicUnitManager()

    class Meta:
        verbose_name = "Taxonomic unit"
        verbose_name_plural = "Taxonomic units"
        indexes = [GinIndex(name="tu_gin_trgm_idx", fields=("full_name",), opclasses=("gin_trgm_ops",))]

    def save(self, *args, **kwargs):
        # Populate full name field
        self.full_name = self.__str__().lower()
        self.full_html_name = self.html_str()
        super(TaxonomicUnit, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.genera_name.capitalize()} {self.species_name}' \
               f'{" " if self.infra_mark else ""}{self.infra_mark or ""}{" " if self.infra_name else ""}' \
               f'{self.infra_name or ""}{" " if self.cult_mark else ""}' \
               f'{self.cult_mark or ""}{" " if self.cult_name else ""}{self.cult_name or ""}'\
               f'{" " if self.author else ""}{self.author or ""}'

    def html_str(self):
        """
        Pretty HTML formatted taxonomic unit full name
        According to ICNCP author of parent taxon is not listed

        :return: string
        """
        name_html = f"<i>{self.genera_name.capitalize()} {self.species_name} </i>"
        if self.infra_mark is not None and self.infra_name is not None:
            name_html = f"{name_html} {str(self.infra_mark)} <i>{self.infra_name}</i>"
        if self.cult_name is not None:
            name_html = f"{name_html} \'{self.cult_name}\'"
        elif self.author:
            if self.author != "None":
                name_html = f"{name_html} {self.author}"

        return name_html

    def has_alive(self):
        """
        Check if taxonomic unit has alive individuals
        :return: bool
        """
        return self.individual_set.filter(is_alive=True).exists()

    def has_images(self):
        """
        Check if taxonomic unit has attached images
        :return: bool
        """
        return self.taxonomicunitimages_set.exists()

    def get_one_good_img(self):
        """
        Get one image with highest rating
        :return: TaxonomicUnitImages object
        """
        return self.taxonomicunitimages_set.order_by("img_rate").first()

    def name_translit_ru(self):
        """
        Performs transliteration of scientific name into cyrillic
        :return: string
        """
        # Prepare cultural name
        cult_name = self.cult_name
        if cult_name:
            cult_name = f'"{cult_name.capitalize()}"'
        lat_name = f'{self.genera_name.capitalize()} {self.species_name}' \
                   f'{" " if self.infra_mark else ""}{self.infra_mark or ""}{" " if self.infra_name else ""}' \
                   f'{self.infra_name or ""}{" " if self.cult_mark else ""}' \
                   f'{self.cult_mark or ""}{" " if self.cult_name else ""}{cult_name or ""}'
        ru_name = translit(lat_name, language_code='ru')
        ru_name = ru_name.replace("цултивар", "культивар")
        # Replace infraspecies markers
        ru_name = ru_name.replace("субсп.", "подвид")
        ru_name = ru_name.replace("вар.", "вариация")
        ru_name = ru_name.replace("субвар.", "подвариация")
        ru_name = ru_name.replace("ф.", "форма")
        ru_name = ru_name.replace("субф.", "подформа")
        return ru_name


class ColData(models.Model):
    """
    The table which represent data from Catalogue Of Life
    for appropriate TU from TaxonomicUnit table
    """
    tu_id = models.OneToOneField(TaxonomicUnit, on_delete=models.CASCADE)
    col_id = models.CharField(max_length=100, null=False)
    col_url = models.URLField(null=False)
    col_ref = models.CharField(max_length=1000, null=False)
    genera_name = models.CharField(max_length=100, null=False)
    species_name = models.CharField(max_length=100, null=False)
    infra_mark = models.CharField(max_length=10, null=True)
    infra_name = models.CharField(max_length=100, null=True)
    author = models.CharField(max_length=100, null=True)
    ref_author = models.CharField(max_length=100, null=True)
    ref_year = models.CharField(max_length=100, null=True)
    ref_title = models.CharField(max_length=300, null=True)
    ref_res = models.CharField(max_length=300, null=True)
    last_check = models.DateField(null=False)
    check_need = models.BooleanField(null=False)

    class Meta:
        verbose_name = "Catalogue of Life datum"
        verbose_name_plural = "Catalogue of Life data"

    def __str__(self):
        infra_mark, infra_name = "", ""
        if self.infra_mark is not None:
            infra_mark = self.infra_mark

        if self.infra_name is not None:
            infra_name = self.infra_name

        return "COL " + self.genera_name[0].upper() + ". " + self.species_name + " " \
               + infra_mark + " " + infra_name + " ColID = " + str(self.col_id)


class Synonyms(models.Model):
    """
    The table for synonyms handling.
    Contains same fields as ColData table + some specific columns.
    """
    tu_id = models.ForeignKey(TaxonomicUnit, on_delete=models.CASCADE)
    col_id = models.CharField(max_length=100, null=False)
    col_url = models.URLField(null=False)
    genera_name = models.CharField(max_length=100, null=False)
    species_name = models.CharField(max_length=100, null=False)
    infra_mark = models.CharField(max_length=10, null=True)
    infra_name = models.CharField(max_length=100, null=True)
    author = models.CharField(max_length=100, null=True)
    ref_author = models.CharField(max_length=100, null=True)
    ref_year = models.CharField(max_length=100, null=True)
    ref_title = models.CharField(max_length=300, null=True)
    ref_res = models.CharField(max_length=300, null=True)
    last_check = models.DateField(null=False)
    check_need = models.BooleanField(null=False)
    # Full name for trigram search
    full_name = models.CharField(max_length=523, null=True)
    full_html_name = models.CharField(max_length=523, null=True)

    class Meta:
        verbose_name = "Synonym"
        verbose_name_plural = "Synonyms"
        indexes = [GinIndex(name="syn_gin_trgm_idx", fields=("full_name",), opclasses=("gin_trgm_ops",))]

    def save(self, *args, **kwargs):
        # Populate full name field
        self.full_name = f'{self.genera_name.capitalize()} {self.species_name}{" " if self.infra_mark else ""}' \
                         f'{self.infra_mark or ""}{" " if self.infra_name else ""}' \
                         f'{self.infra_name or ""} {self.author}'
        self.full_html_name = self.html_str()
        super(Synonyms, self).save(*args, **kwargs)

    def __str__(self):
        return "Syn " + str(self.genera_name[0].upper()) + ". " + str(self.species_name) + " " \
               + str(self.infra_mark) + " " + str(self.infra_name)

    def html_str(self):
        """
        Pretty HTML formatted synonym full name
        :return: string
        """
        name_html = f"<i>{self.genera_name.capitalize()} {self.species_name} </i>"
        if self.infra_mark is not None and self.infra_name is not None:
            name_html = f"{name_html} {str(self.infra_mark)} <i>{self.infra_name}</i>"
        name_html = f"{name_html} {self.author}"
        return name_html


class Vernacular(models.Model):
    """
    The table for handling common names of plants
    """
    # Prepare choices for language
    ENGLISH = "eng"
    RUSSIAN = "rus"
    UKRAINIAN = "ukr"

    lang_choices = (
        (ENGLISH, "Английский"),
        (RUSSIAN, "Русский"),
        (UKRAINIAN, "Украинский"),
    )

    tu_id = models.ForeignKey(TaxonomicUnit, on_delete=models.CASCADE)
    vernacular = models.CharField(max_length=100, null=False)
    language = models.CharField(max_length=3, null=False, choices=lang_choices)

    class Meta:
        verbose_name = "Vernacular name"
        verbose_name_plural = "Vernacular names"
        indexes = [GinIndex(name="vern_gin_trgm_idx", fields=("vernacular",), opclasses=("gin_trgm_ops",))]

    def __str__(self):
        return "Vernacular: " + self.vernacular + "-- lang: " + self.language

    def html_str(self):
        return f"{self.vernacular} <i>({self.language})</i>"


class RedListIUCN(models.Model):
    """
    The table with data from IUCN Red List
    """
    # Prepare choices for parsing_errors
    FULL = "full"
    PARTIAL = "part"
    ERROR = "err"

    error_choices = (
        (FULL, "No errors"),
        (PARTIAL, "Some errors"),
        (ERROR, "All errors"),
    )

    tu_id = models.OneToOneField(TaxonomicUnit, on_delete=models.CASCADE)
    in_red_list = models.BooleanField(null=True)
    parsing_errors = models.CharField(max_length=5, null=False, choices=error_choices)
    common_name = models.CharField(max_length=100, null=True)
    category = models.CharField(max_length=10, null=True)
    cat_interp = models.CharField(max_length=100, null=True)
    criteria = models.CharField(max_length=50, null=True)
    citation = models.CharField(max_length=500, null=True)
    distribution = JSONField(null=True)
    distribution_map = models.ImageField(upload_to="red_list_distr_maps", null=True)
    pub_year = models.IntegerField(null=True)
    last_check = models.DateField(null=False)

    class Meta:
        verbose_name = "Red list (IUCN) datum"
        verbose_name_plural = "Red list (IUCN) data"

    def __str__(self):
        return f"IUCN RedList category: {self.category if self.category is not None else ''}"


class Individual(models.Model):
    """
    The table with information on separate plant individuals
    """
    # Prepare choices for income_material field
    EMPTY = ""
    SEED = "seed"
    PART = "part"
    PLANT = "plant"

    inc_mat_choices = (
        (EMPTY, ""),
        (SEED, "Семена"),
        (PART, "Часть растения"),
        (PLANT, "Целое растение")
    )

    # Prepare choices for cultivation types
    CONTAINER_OPEN = "cont_og"
    CONTAINER_PROTECTED = "cont_pg"
    GROUND_OPEN = "grd_og"
    GROUND_PROTECTED = "grd_pg"

    cult_type_choices = (
        (EMPTY, ""),
        (CONTAINER_OPEN, "Контейнер отк.гр."),
        (CONTAINER_PROTECTED, "Контейнер закр.гр."),
        (GROUND_OPEN, "Грунт откр.гр."),
        (GROUND_PROTECTED, "Грунт закр.гр.")
    )

    # Prepare choices for origin
    EMPTY_LOC = 'none'
    LOCAL = 'local'
    NATURE = 'nature'
    CITY = 'city'

    origin_choices = (
        (EMPTY_LOC, 'неизвестно'),
        (LOCAL, 'автохтонное'),
        (NATURE, 'из природы'),
        (CITY, 'получено из города')
    )

    tu_id = models.ForeignKey(TaxonomicUnit, on_delete=models.CASCADE)
    field_number = models.IntegerField(null=True, blank=True)
    temp_id = models.IntegerField(null=True, unique=True, blank=True)
    is_alive = models.BooleanField(null=False, default=True)
    income_date = models.DateField(null=True, blank=True)
    income_material = models.CharField(max_length=30, null=True, choices=inc_mat_choices, blank=True)
    source = models.CharField(max_length=300, null=True, blank=True)
    out_date = models.DateField(null=True, blank=True)
    department_id = models.ForeignKey(Departments, null=True, blank=False, on_delete=models.SET_NULL)
    cult_type = models.CharField(max_length=50, null=True, blank=True, choices=cult_type_choices)
    spatial_pos = PointField(null=True, blank=True)
    input_author = models.ForeignKey(User, on_delete=models.SET_NULL, blank=False, null=True)
    input_date = models.DateField(null=True, blank=True, auto_now_add=True)
    origin = models.CharField(max_length=6, null=True, choices=origin_choices)
    city = models.ForeignKey(PopulatedPlaces, on_delete=models.SET_NULL, null=True)
    origin_loc = PointField(null=True, blank=True)

    class Meta:
        verbose_name = "Individual"
        verbose_name_plural = "Individuals"
        indexes = [
            models.Index(fields=['income_date']),
            models.Index(fields=['out_date']),
            models.Index(fields=['income_date', 'out_date']),
            models.Index(fields=['is_alive']),
            models.Index(fields=['origin']),
        ]

    def __str__(self):
        return f"Individual: {str(self.pk)} of TU: {str(self.tu_id)}"


class ObligatoryInfo(models.Model):
    """
    The table with obligatory additional info for Taxonomic Units
    """
    # Prepare choices for life form
    EMPTY = ""
    PHANEROPHYTE = "pha"
    CHAMAEPHYTE = "ham"
    POTOHEMICRYPTOPHYTE = "prohem"
    PARTIALROSETTE = "partroset"
    ROSETTE = "roset"
    GEOPHYTES = "geo"
    HELOPHYTES = "hel"
    HYDROPHYTES = "hyd"
    THEROPHYTES = "tero"
    EPIPHYTES = "epi"

    raunc_form_choices = (
        (EMPTY, ""),
        (PHANEROPHYTE, "Фанерофит"),
        (CHAMAEPHYTE, "Хамефит"),
        (POTOHEMICRYPTOPHYTE, "Протогемикриптофит"),
        (PARTIALROSETTE, "Частично розеточное"),
        (ROSETTE, "Розеточное"),
        (GEOPHYTES, "Геофит"),
        (HELOPHYTES, "Гелофит"),
        (HYDROPHYTES, "Гидрофит"),
        (THEROPHYTES, "Терофит"),
        (EPIPHYTES, "Эпифит")
    )

    # Prepare choices for ecological groups with respect to moisture factor
    HYDATOPHYTE = "hyda"
    HYDROPHYTE = "hydro"
    HYGROPHYTE = "hygro"
    MESOPHYTE = "meso"
    SUCCULENT = "succ"
    SCLEROPHYTE = "sclero"

    moisture_choices = (
        (EMPTY, ""),
        (HYDATOPHYTE, "Гидатофит"),
        (HYDROPHYTE, "Гидрофит"),
        (HYGROPHYTE, "Гигрофит"),
        (MESOPHYTE, "Мезофит"),
        (SUCCULENT, "Суккулент"),
        (SCLEROPHYTE, "Склерофит")
    )

    # Prepare choices for ecological groups with respect to light factor
    SCIOPHYTE = "scio"
    SCIOHELIOPHYTE = "scihel"
    HELIOPHYTE = "hel"

    light_choices = (
        (EMPTY, ""),
        (SCIOPHYTE, "Сциофит"),
        (SCIOHELIOPHYTE, "Сциогелиофит"),
        (HELIOPHYTE, "Гелиофит")
    )

    # Prepare choices for ecological groups with respect to soil fertility
    OLIGOTROPH = "olygo"
    MESOTROPH = "meso"
    EUTROPH = "eu"

    fertil_choices = (
        (EMPTY, ""),
        (OLIGOTROPH, "Олиготроф"),
        (MESOTROPH, "Мезотроф"),
        (EUTROPH, "Эутроф")
    )

    # Prepare choices for ecological groups with respect to salinity factor
    GLYCOPHYTE = "hlico"
    HALOPHYTE = "halo"

    salin_choices = (
        (EMPTY, ""),
        (GLYCOPHYTE, "Гликофит"),
        (HALOPHYTE, "Галофит")
    )

    # Prepare choices for ecological groups with respect to acidity factor
    ACIDOPHYTE = "acid"
    NEUTROPHYTE = "neutro"
    BASOPHYTE = "baso"

    acidity_choices = (
        (EMPTY, ""),
        (ACIDOPHYTE, "Ацидофит"),
        (NEUTROPHYTE, "Нейтрофит"),
        (BASOPHYTE, "Базофит")
    )

    # Prepare choices for plants groups according to phenorythm-types
    PHENO_A = "a"
    PHENO_B = "b"
    PHENO_B1 = "b1"
    PHENO_C = "c"
    PHENO_D = "d"
    PHENO_E = "e"
    PHENO_F = "f"
    PHENO_F1 = "f1"
    PHENO_G = "g"
    PHENO_H = "h"
    PHENO_I = "i"
    PHENO_J = "j"

    pheno_choices = (
        (EMPTY, ""),
        (PHENO_A, "Вечнозеленое"),
        (PHENO_B, "Летне-зимнезеленое"),
        (PHENO_B1, "Летне-зимнезеленое с периодом лентнего полупокоя"),
        (PHENO_C, "Летне-зимнезеленое с кратковременным периодои осеннего покоя"),
        (PHENO_D, "Осенне-зимне-весеннезеленое с периодом летнего покоя"),
        (PHENO_E, "Весенне-летне-осеннезеленое с периодом летнего покоя"),
        (PHENO_F, "Весенне-осеннезеленое с периодои летнего и зимнего покоя"),
        (PHENO_F1, "Весенне-осеннезеленое с периодои летнего покоя и зимнего плодоношения"),
        (PHENO_G, "Весенне-раннелетнезеленое"),
        (PHENO_H, "Летне-осеннезеленое"),
        (PHENO_I, "Весеннезеленый эфимер"),
        (PHENO_J, "Летнезеленый эфимер")
    )

    # Prepare choices for plants groups according to phytocenotypes
    CENO_A = "a"
    CENO_B = "b"
    CENO_C = "c"
    CENO_D = "d"
    CENO_E = "e"
    CENO_F = "f"
    CENO_G = "g"
    CENO_H = "h"
    CENO_I = "i"
    CENO_J = "j"

    cenotype_choices = (
        (CENO_A, "Сильвант"),
        (CENO_B, "Пратант"),
        (CENO_C, "Палюдант"),
        (CENO_D, "Степант"),
        (CENO_E, "Аквант"),
        (CENO_F, "Псамофант"),
        (CENO_G, "Петрофант"),
        (CENO_H, "Альпмонтант"),
        (CENO_I, "Галофант"),
        (CENO_J, "Синантропант")
    )

    tu_id = models.OneToOneField(TaxonomicUnit, on_delete=models.CASCADE)
    raunc_form = models.CharField(max_length=30, null=True, choices=raunc_form_choices)
    moisture = models.CharField(max_length=30, null=True, choices=moisture_choices)
    light = models.CharField(max_length=30, null=True, choices=light_choices)
    fertility = models.CharField(max_length=30, null=True, choices=fertil_choices)
    salinity = models.CharField(max_length=30, null=True, choices=salin_choices)
    acidity = models.CharField(max_length=30, null=True, choices=acidity_choices)
    phenotype = models.CharField(max_length=100, null=True, choices=pheno_choices)
    cenotype = MultiSelectField(choices=cenotype_choices)

    class Meta:
        verbose_name = "Obligatory info"
        verbose_name_plural = "Obligatory info"

    def __str__(self):
        # Get number of filled columns
        col_n = (self.raunc_form is not None) \
                + (self.moisture is not None) \
                + (self.light is not None) \
                + (self.fertility is not None) \
                + (self.salinity is not None) \
                + (self.acidity is not None) \
                + (self.phenotype is not None) \
                + (self.cenotype is not None)
        return f"Additional info, for TU: {self.tu_id}. {str(col_n)} columns filled."


class NonObligatoryInfo(models.Model):
    tu_id = models.ForeignKey(TaxonomicUnit, on_delete=models.CASCADE)
    input_auth = models.ForeignKey(User, on_delete=models.CASCADE)
    input_date = models.DateField(null=False)
    reference = models.CharField(max_length=500, null=False)
    category = models.CharField(max_length=100, null=False)
    info = RichTextField()

    class Meta:
        verbose_name = "Nonobligatory info"
        verbose_name_plural = "Nonobligatory info"

    def __str__(self):
        return f"Additional non obligatory info, for TU: {self.tu_id}."


class TaxonomicUnitImages(models.Model):
    tu_id = models.ForeignKey(TaxonomicUnit, on_delete=models.CASCADE)
    input_auth = models.CharField(max_length=500, null=False)
    img_author = models.CharField(max_length=500, null=False)
    input_date = models.DateField(null=False)
    img_date = models.DateField(null=True)
    img_caption = models.CharField(max_length=100, null=False)
    img_description = models.CharField(max_length=500, null=True)
    img_3x_jpg = models.ImageField(upload_to="tu_images", blank=True, null=True)  # Initial size
    img_2x_jpg = models.ImageField(upload_to="tu_images", blank=True, null=True)  # 0.66% of initial 90% quality
    img_1x_jpg = models.ImageField(upload_to="tu_images", blank=True, null=True)  # 0.33% of initial 90% quality
    img_3x_webp = models.ImageField(upload_to="tu_images", blank=True, null=True)
    img_2x_webp = models.ImageField(upload_to="tu_images", blank=True, null=True)
    img_1x_webp = models.ImageField(upload_to="tu_images", blank=True, null=True)
    img_thunb_jpg = models.ImageField(upload_to="tu_images", blank=True, null=True)  # ca. 500 x 400 x 80% quality
    img_thunb_webp = models.ImageField(upload_to="tu_images", blank=True, null=True)
    img_rate = models.IntegerField(default=3, null=False)
    img_main_scr = models.BooleanField()

    class Meta:
        verbose_name = "Taxonomic unit image"
        verbose_name_plural = "Taxonomic unit images"

    def add_image(self, tax_unit, cleaned_data=None, image=None):
        assert cleaned_data.get("img_init_path_full") is not None or image is not None
        # Populate data fields
        self.tu_id = tax_unit
        self.input_auth = cleaned_data.get("author")
        self.img_caption = cleaned_data.get("caption")
        self.img_description = cleaned_data.get("description")
        self.img_rate = cleaned_data.get("rate")
        self.img_main_scr = cleaned_data.get("main_scr")

        # Convert date from string if needed
        if type(cleaned_data.get("date")) is not date:
            self.input_date = datetime.strptime(cleaned_data.get("date"), "%Y-%m-%d").date()
        else:
            self.input_date = cleaned_data.get("date")

        # Save current instance to get id
        self.save()
        im_id = self.id

        # Construct base name and path
        im_base_name = f"{slugify(self.tu_id.full_name, separator='_')}_{im_id}"

        if cleaned_data.get("img_init_path_full"):
            # The case of initial images addition through `new_tu_5` form
            path = cleaned_data.get("img_init_path_full")
            image = None
        else:
            # The case when new (single) images is added via `add_image` form
            path = None
            image = Image.open(image)
            # Reorient image according to EXIF
            image = ImageOps.exif_transpose(image)

        # Process all images and save them to corresponding fields
        self.img_3x_jpg.save(im_base_name + "_3x.jpg", img_convert_to_buffer(path, image, otype="JPEG", size="3x"))
        self.img_3x_webp.save(im_base_name + "_3x.webp", img_convert_to_buffer(path, image, otype="WebP", size="3x"))
        self.img_2x_jpg.save(im_base_name + "_2x.jpg", img_convert_to_buffer(path, image, otype="JPEG", size="2x"))
        self.img_2x_webp.save(im_base_name + "_2x.webp", img_convert_to_buffer(path, image, otype="WebP", size="2x"))
        self.img_1x_jpg.save(im_base_name + "_1x.jpg", img_convert_to_buffer(path, image, otype="JPEG", size="1x"))
        self.img_1x_webp.save(im_base_name + "_1x.webp", img_convert_to_buffer(path, image, otype="WebP", size="1x"))
        self.img_thunb_jpg.save(im_base_name + "_thumb.jpg", img_convert_to_buffer(path, image, otype="JPEG", size="th"))
        self.img_thunb_webp.save(im_base_name + "_thumb.webp", img_convert_to_buffer(path, image, otype="WebP", size="th"))
        self.save()

    def full_caption(self):
        cap = f"<p>{self.img_caption}.</p><i>Автор: {self.input_auth} ({self.input_date})</i>"
        return cap


class TropicosInfo(models.Model):
    tu_id = models.OneToOneField(TaxonomicUnit, on_delete=models.CASCADE)
    total_error = models.BooleanField()
    distribution = JSONField(null=True)
    chromosomes = JSONField(null=True)
    last_check = models.DateField(null=False)

    class Meta:
        verbose_name = "TROPICOS info"
        verbose_name_plural = "TROPICOS info"

    def __str__(self):
        return f"Tropicos info for {self.tu_id.genera_name} {self.tu_id.species_name}"

    def populate(self, tax_unit, cleaned_data):
        self.tu_id = tax_unit
        if cleaned_data.get("status") == "Success":
            self.total_error = False
        else:
            self.total_error = True
        self.distribution = cleaned_data.get("distribution")
        self.chromosomes = cleaned_data.get("chromosomes")
        self.last_check = date.today()
        self.save()
