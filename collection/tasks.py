from __future__ import absolute_import
from celery import shared_task
from celery import Celery
from collection.utils import redlist_query
from collection.utils import redlist_distr_map
from collection.utils import tropicos_search_query
from collection.utils import tropicos_distrib_query
from collection.utils import tropicos_chrom_query
from collection.utils import find_working_proxy
from collection.utils import check_site_access
from subprocess import call

import os
import pathlib
import shutil

# Load session
from importlib import import_module
from django.conf import settings

app = Celery('tasks', broker='redis://localhost:6379', backend='redis://localhost:6379')

''' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    On session.save() the current 'session' object will be saved!
    Therefore, if another process will alter session data,
    when 'session' variable in this script was already fetched
    from 'session_store' those changes not appear in
    current 'session'. => saving current 'session' object
    will overwrite external changes.

    Solution: re fetch session form session store each time,
    just before you want to alter it and save. Minimal time
    should pass between fetching and saving. So
'''

@shared_task(name='red_list_query', bind=True, trail=True)
def redlist_query_async(self, session_key, genera, species, subspecies="", full_name=""):
    """
    An asynchronous task for Red List API querying in background
    results are saved into session
    :param self: view self (it is for access to request info)
    :param session_key: the current request session key
    :param genera: genera name
    :param species: species name
    :param subspecies: subspecies name (or "" if not exists)
    :param full_name: full species name for map title
    :return: null
    """

    # Get session store
    session_store = import_module(settings.SESSION_ENGINE).SessionStore

    # Check if session exists
    if not session_store().exists(session_key=session_key):
        return

    # Get session
    session = session_store(session_key=session_key)

    '''
    I do not now why, but this trick with active task doesn't work
    each time new task is running, session remains in a state before task run
    therefore status: Active never appears in view check
    '''
    # Tell session that task is active
    session["red_list"] = {"active_status": "Active", "task_id": self.request.id}
    session.save()

    # Get Red List data
    rl_res = redlist_query(genera, species, subspecies)

    # Check if it does not contain errors
    if rl_res["parser_status"] == "Error":
        session = session_store(session_key=session_key)  # Re fetch session
        session["red_list"] = {"status": "Error", "active_status": "Done"}
        session.save()
        return

    # Now rl_res does not contain errors

    # Prepare distribution map
    if rl_res["parser_status"] != "NotInRedList":
        if not rl_res["distr_error"]:
            # Construct file path (media/tmp/red_list_map/<session_key>/map.svg)
            path = os.path.join(settings.MEDIA_ROOT, "tmp/red_list_map", session_key, "map.svg")
            path_dir = os.path.dirname(path)

            # Remove tmp folder if already exists
            # and create empty
            if os.path.exists(path_dir):
                shutil.rmtree(path_dir)
                pathlib.Path(path_dir).mkdir(mode=0o777, parents=True, exist_ok=True)
            else:
                # Ensure to create output dir with parents
                pathlib.Path(path_dir).mkdir(mode=0o777, parents=True, exist_ok=True)

            redlist_distr_map(redlist_distr_list=rl_res["distribution"],
                              species_name=full_name,
                              out_path=path,
                              map_geojson="spatial/data/geojson_lores_world_map/countries.geo.json")

            # Compress output svg
            svgo_path = os.path.join(settings.BASE_DIR, "node_modules/svgo/bin/svgo")
            call([svgo_path, path, "-o", path])
            pathlib.Path(path).chmod(0o777)

            # add map_url and map_path to rl_res
            rl_res.update({
                "map_url": os.path.join("/media/tmp/red_list_map", session_key, "map.svg"),
                "map_path": path,
            })

    rl_res.update({"active_status": "Done"})
    session = session_store(session_key=session_key)  # Re fetch session
    session["red_list"] = rl_res
    session.save()
    return True


@shared_task(name='tropicos_query', bind=True, trail=True)
def tropicos_query_async(self, session_key, genera, species, infra_name="", infra_mark="",  author=""):
    """
    An asynchronous task for Tropicos API querying in background
    results are saved into session.
    Workflow
    1. Find working proxy
    2. Try to find names combination in Tropicos, if so get ID
    3. Try to find spatial distribution data.
    4*. If distribution data was found, plot a map
    5. Try to find chromosomes counts
    :param self: view self (it is for access to request info)
    :param session_key: the current request session key
    :param genera: genera name
    :param species: species name
    :param infra_name: infraspecies name ("" if blank)
    :param infra_mark: infraspecies mark ("" if blank)
    :param author: author abbreviation ("" if blank)
    :return: null
    """
    # Get session store
    session_store = import_module(settings.SESSION_ENGINE).SessionStore

    # Check if session exists
    if not session_store().exists(session_key=session_key):
        return

    # Get session
    session = session_store(session_key=session_key)

    # Tell session that task is active
    session["tropicos"] = {"active_status": "Active", "task_id": self.request.id}
    session.save()

    # Check if Tropicos.org is accessible. Try to find proxy otherwise
    tropic_accessible = check_site_access('http://www.tropicos.org')
    if tropic_accessible:
        proxy = None
    else:
        # Find proxy or throw error if was not found
        proxy = find_working_proxy('http://www.tropicos.org')
        if not proxy:
            session = session_store(session_key=session_key)  # Re fetch session
            session["tropicos"] = {"status": "Error", "active_status": "Done"}
            session.save()
            return

    # Perform Tropicos search (aim: to get id)
    id = tropicos_search_query(genera, species, infra_name, infra_mark,  author, proxy)

    # Stop if Name was not found or Error
    if id.get("parser_status") == "Error":
        session = session_store(session_key=session_key)  # Re fetch session
        session["tropicos"] = {"status": "Error", "active_status": "Done"}
        session.save()
        return

    # Now we know that search call did not return an error
    # Get Tropicos distribution data
    trop_distr = tropicos_distrib_query(id.get("id"), proxy)
    if trop_distr.get("parser_status") == "Success":
        session = session_store(session_key=session_key)  # Re fetch session
        session["tropicos"] = {"status": "Success",
                               "distr_status": "Success",
                               "distribution": trop_distr.get("Distribution")}
        session.save()
    else:
        session = session_store(session_key=session_key)  # Re fetch session
        session["tropicos"] = {"status": "Error",
                               "distr_status": "Error",
                               "distribution": None}
        session.save()

    # Get chromosomes counts
    chrom = tropicos_chrom_query(id.get("id"), proxy)

    # Get status of a distribution request
    distr_status = session["tropicos"]["distr_status"]

    # If chromosomes request was successful update session info
    if chrom.get("parser_status") == "Success":
        session = session_store(session_key=session_key)  # Re fetch session
        session["tropicos"].update({"status": "Success",
                                    "chromosomes_status": "Success",
                                    "chromosomes": chrom.get("ChromosomeCounts"),
                                    "active_status": "Done"})
        session.save()
        return True
    # Now we know that chromosomes API call returned error,
    # so we need to return status error if both Distribution and Chromosomes count
    # queries returned Errors or status success if Distribution call was successful.
    # In the last case we want to pass Chromasomes: None to output
    else:
        if distr_status == "Error":
            # I.e. both chromosomes count and distribution returned Error
            session = session_store(session_key=session_key)  # Re fetch session
            session["tropicos"] = {"status": "Error", "active_status": "Done"}
            session.save()
            return
        else:
            # I.e. chromosomes returned Error and distribution returned Success
            session = session_store(session_key=session_key)  # Re fetch session
            session["tropicos"].update({"chromosomes_status": "Error",
                                        "chromosomes": None,
                                        "active_status": "Done"})
            session.save()
            return True
