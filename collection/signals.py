from django.db.models.signals import post_save
from django.dispatch import receiver

# Import models
from .models import ColSpecies
from .models import TaxonomicUnit


@receiver(post_save, sender=TaxonomicUnit)
def tu_is_syn_of_col_sp(sender, instance, created, **kwargs):
    """
    The post_save callback for `TaxonomicUnit` model.
    It will check if saved taxonomic unit (TU) is present in `ColSpecies` as "synonym" or "accepted" but doesn't
    have reference to corresponding `ColSpecies` object as foreign key in `col_id` field.
    Such situation is possible if user decided to add new TU without checking with COL data.
    If TU is really present in `ColSpecies` we will add appropriate references (foreign keys) to each model:
        `TaxonomicUnit.id --> ColSpecies.used_by_tu` if TU is a synonym of `ColSpecies`
        `ColSpecies.col_id --> TaxonomicUnit.col_id` if TU is present in `ColSpecies` as accepted
    """
    if instance.col_id is None and created:
        # Check if instance is a synonym of ColSpecies
        col_sp_obj = ColSpecies.objects.col_species_exact_match(genera=instance.genera_name,
                                                                species=instance.species_name,
                                                                infra_mark=instance.infra_mark,
                                                                infra_name=instance.infra_name,
                                                                author=instance.author)
        if col_sp_obj:
            if col_sp_obj.col_status == "accepted":
                # col_sp_obj is accepted = add foreign key ColSpecies.col_id --> TaxonomicUnit.col_id
                instance.col_id = col_sp_obj
                instance.save()
            else:
                # col_sp_obj is synonym = add foreign key TaxonomicUnit.id --> ColSpecies.used_by_tu
                col_sp_obj.used_by_tu = instance
                col_sp_obj.save()
