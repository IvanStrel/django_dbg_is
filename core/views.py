from django.shortcuts import render

# load models
from collection.models import TaxonomicUnit
from collection.models import ColSpecies
from collection.models import TaxonomicUnitImages
from collection.models import NonObligatoryInfo
from collection.models import Vernacular
from collection.models import RedListIUCN
from core.models import HomePageImages


def home_page(request):
    # Count TU
    tu = TaxonomicUnit.objects.count()
    # Count synonyms
    syn = ColSpecies.objects.raw('''
        SELECT DISTINCT syn.col_id
        FROM collection_taxonomicunit AS tu 
        LEFT JOIN collection_colspecies as acc ON acc.col_id =  tu.col_id
        LEFT JOIN collection_colspecies as syn on syn.col_accepted_id = acc.col_id;
    ''')
    syn = len(syn)
    # Count images
    img = TaxonomicUnitImages.objects.count()
    # Count additional info
    non_obl = NonObligatoryInfo.objects.count()
    # Count vernaculars
    vern = Vernacular.objects.count()
    # Count in red list
    in_rl = RedListIUCN.objects.filter(in_red_list=True).distinct('tu_id__genera_name',
                                                                  'tu_id__species_name',
                                                                  'tu_id__infra_name').count()

    # Get random image for background
    # Check if any image exists
    background = HomePageImages.objects.get_random()
    if len(background):
        background = background[0]
    else:
        background = None

    data = {"tu": tu, "syn": syn, "img": img, "non_obl": non_obl, "vern": vern, "in_rl": in_rl, "bg": background}
    return render(request, 'home_page/home_page.html', data)
