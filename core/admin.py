from django.contrib import admin

# Models registration
from .models import HomePageImages


@admin.register(HomePageImages)
class HomePageImagesAdmin(admin.ModelAdmin):
    pass
