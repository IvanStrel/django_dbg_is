from django.forms import ChoiceField


class ChoiceFieldNoValidation(ChoiceField):
    """
    The subclass of ChoiceField without choices validation (for example, if choices provided with JS on frontend)
    """
    def validate(self, value):
        pass
