/* Should be included after Leaflet.js */

function LeafletWithMarkers(map_element, options) {
    // Initialization ================================================================================================
    var self = this; // for use in functions
    this.options = options;
    // Initiate map
    this.drag_options = {};
    if (options.mobile) {
        this.drag_options = {dragging: !L.Browser.mobile, tap: !L.Browser.mobile};
    }
    this.map = L.map(map_element, this.drag_options);

    // Set view if provided in options
    if (options.center) {
        if (options.init_zoom) {
            this.map.setView(options.center, options.init_zoom)
        } else {
            this.map.setView(options.center, 9)
        }
    }

    // Initiate tile layer
    if (options.tiles === 'osm') {
        this.tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 18
        });
    } else {
        this.tiles = L.tileLayer(options.tiles, {
            maxZoom: 22,
            attribution: '&copy; ГУ "ДБС"'
        })
    }

    // Add tile layer to map
    this.tiles.addTo(this.map);

    // Methods ========================================================================================================
    // Create function for form coordinates input populating
    this.assign_marker_coords = function (layer, coord_input) {
        lon = layer.getLatLng().lng;
        lat = layer.getLatLng().lat;

        var output;
        output = {"type":"Point","coordinates":[lon, lat], "crs":{"type":"name","properties":{"name":"EPSG:4326"}}};

        coord_input.value = JSON.stringify(output);
    };

    // Function for map size invalidation (in case if map was initialized on hidden div)
    this.invalidate_size = function() {
        this.map.invalidateSize()
    };

    // Marker Layers ==================================================================================================

    // Add permanent marker layers (for example to show location of existing individuals) should be provided as array
    // of JSONs and Styles
    this.perm_marker_lyrs = [];

    // Loop over json objects (feature collections produced with `serialize("geojson", ...)` in view)
    if (options.perm_markers_json) {
        // Loop over each provided point feature json
        for (var i = 0; i < options.perm_markers_json.length; i++) {
            // Marker style. Get from options or set default
            if (options.perm_marker_style[i]) {
                this.perm_marker_style = options.perm_marker_style[i];
            } else {
                this.perm_marker_style = {
                        radius: 5,
                        fillColor: "red",
                        color: "black",
                        weight: 1,
                        opacity: 1,
                        fillOpacity: 0.8
                    }
            }
            // Initiate layer
            this.perm_marker_lyrs[i] = L.geoJSON(options.perm_markers_json[i].features, {
                pointToLayer: function (feature, latlng) {
                    return L.circleMarker(latlng, self.perm_marker_style);
                },

                // Bind popup (default if not provided by options)
                onEachFeature: function (feature, layer) {
                    var popup = '<p>Особь вида</p><h5>' + tu_name + '</h5>' +
                                '<h6> номер в базе данных: ' +feature.properties.pk+ '</h6>' +
                                '<h6> полевой номер: ' +feature.properties.field_number+ '</h6>'
                    if (options.perm_marker_popup) {
                        if (options.perm_marker_popup[i]) {
                            popup = options.perm_marker_popup[i]
                        }
                    }
                    layer.bindPopup(popup);
                }
            });
            // Add `perm_marker` to map
            this.perm_marker_lyrs[i].addTo(self.map);
        }
    }
    // Loop over geojson objects (single feature (point) produced with `{{object.point.geojson|safe}}` in template)
    if (options.perm_marker_point_geojson) {
        // Get number of previously added self.perm_marker_lyrs
        var n_lyr = this.perm_marker_lyrs.length;
        for (var i = 0; i < options.perm_marker_point_geojson.length; i++) {
            this.perm_marker_lyrs[n_lyr + i] = L.geoJSON(options.perm_marker_point_geojson[i]);
            this.perm_marker_lyrs[n_lyr + i].addTo(self.map);
        }
    }

    // Create interactive marker layer ================================================================================
    this.marker_inter = null;
    // If initial marker was provided add it to map
    if (options.initial_marker_json) {
        this.marker_inter = L.geoJSON(options.initial_marker_json, {
            pointToLayer: function (feature, latlng) {
                // Add coordinates to input field
                var output;
                output = {
                    "type":"Point","coordinates":[latlng.lng, latlng.lat],
                    "crs":{"type":"name","properties":{"name":"EPSG:4326"}}
                };
                options.coord_input.value = JSON.stringify(output);
                var new_marker;
                new_marker = new L.Marker(latlng, {draggable: 'true'});
                new_marker.on('dragend', function(event){
                    var marker = event.target;
                    var position = marker.getLatLng();
                    marker.setLatLng(new L.LatLng(position.lat, position.lng), {draggable:'true'});

                    // Get marker coordinates and add to input
                    self.assign_marker_coords(marker, options.coord_input);
                });
                return new_marker
            }
        });
        this.map.addLayer(this.marker_inter);
    }

    // Prepare function for interactive marker creation on mouse click
    this.on_click = function(e) {
        if (self.marker_inter) { // check
            self.map.removeLayer(self.marker_inter); // remove
        }
        self.marker_inter = new L.Marker(e.latlng, {draggable:'true'});
        // Get marker coordinates and add to input
        self.assign_marker_coords(self.marker_inter, options.coord_input);

        self.marker_inter.on('dragend', function(event){
            var marker = event.target;
            var position = marker.getLatLng();
            marker.setLatLng(new L.LatLng(position.lat, position.lng), {draggable:'true'});

            // Get marker coordinates and add to input
            self.assign_marker_coords(marker, options.coord_input);
        });
        self.map.addLayer(self.marker_inter);
    };

    if (options.interactive) {
        this.map.on('click', this.on_click);
    }
}

// // Example Usage
// var dbg_ter_map = new LeafletWithMarkers(document.getElementById('map'), {
//     tiles: 'http://localhost:8080/territory-map/{z}/{x}/{y}.png',
//     init_zoom: 18,
//     mobile: true,
//     center: [48.010, 37.88],
//     interactive: true,
//     initial_marker_json: initial_pos,
//     coord_input: document.getElementById('id_spatial_pos'),
//     perm_markers_json: [exist_ind],
//     perm_marker_style: [{
//                     radius: 5,
//                     fillColor: "red",
//                     color: "black",
//                     weight: 1,
//                     opacity: 1,
//                     fillOpacity: 0.8
//                 }]
//
// });


const red_marker_style = {
    radius: 5,
    fillColor: "red",
    color: "black",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};

var blue_marker_style = {
    radius: 5,
    fillColor: "#272dc2",
    color: "black",
    weight: 1,
    opacity: 1,
    fillOpacity: 0.8
};