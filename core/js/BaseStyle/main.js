// Import libraries
//=require ../../../node_modules/jquery/dist/jquery.js
//=require ../../../node_modules/materialize-css/dist/js/materialize.min.js

/** Elements initialization */
document.addEventListener('DOMContentLoaded', function() {
    // Sidenva collapsible
    var side_nav_collaps = document.getElementById('sidenav-collapsible');
    var side_nav_collaps_inst = M.Collapsible.init(side_nav_collaps, null);

    // Sidenav
    var elems = document.querySelectorAll('.sidenav');
    var instances = M.Sidenav.init(elems, null);

    // Dropdown
    var dropdown_elems = document.querySelectorAll('.dropdown-trigger');
    var dropdown_instances = M.Dropdown.init(dropdown_elems, {
        constrainWidth: false,
        hover: false
    });
});

/** Navbar show-hide on scroll (https://www.w3schools.com/howto/howto_js_navbar_hide_scroll.asp)*/
/* When the user scrolls down, hide the navbar. When the user scrolls up, show the navbar */
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("navbar").style.top = "0";
  } else {
    document.getElementById("navbar").style.top = "-64px";
  }
  prevScrollpos = currentScrollPos;
}