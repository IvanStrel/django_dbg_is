// Import libraries
//=require ../../../node_modules/typewriter-effect/dist/core.js

/** Typewriter */
var app = document.getElementById('typewriter');

var typewriter = new Typewriter(app, {
    loop: true
});

var add_affix = function(string, number, affixes, remove) {
    var slice_end = string.length - remove;
    var out_str = string;
    // Check for 10 -- 20 cases (always `ов` affix)
    if (number > 5 && number <= 20) {
        out_str = out_str.slice(0, slice_end);
        out_str += affixes[1];
    // Check for `a` `ов` affixes
    } else if (number % 10 >= 2 && number % 10 < 5) {
        out_str = out_str.slice(0, slice_end);
        out_str += affixes[0];
    } else if (number % 10 >= 5) {
        out_str = out_str.slice(0, slice_end);
        out_str += affixes[1];
    } else if (number % 10 === 0) {
        out_str = out_str.slice(0, slice_end);
        out_str += affixes[1];
    }
    // Other cases goes without changes
    return out_str;
};

// Calculate variables length and strings
var tu_l = tu.length;
var tu_str = add_affix("таксон", tu, ["а", "ов"], 0);

var syn_l = syn.length;
var syn_str = add_affix("синоним", syn, ["а", "ов"], 0);

var img_l = img.length;
var img_str = add_affix("изображение", img, ["я", "й"], 1);

var non_obl_l = non_obl.length;
var non_obl_str_1 = add_affix("информативная", non_obl, ["ые", "ых"], 2);
var non_obl_str_2 = add_affix("запись", non_obl, ["и", "ей"], 1);

//var vern_l = vern.length;
var vern_str = add_affix("вернакуляр", vern, ["а", "ов"], 0);

//var in_rl_l = in_rl.length;
var in_rl_str_1= add_affix("числится", in_rl, ["ятся", "ятся"], 4);
var in_rl_str_2= add_affix("таксон", in_rl, ["а", "ов"], 0);

typewriter.typeString('Добро пожаловать!')
    .pauseFor(1500)
    .deleteAll()
    .typeString('База содержит: ' + tu + " " + tu_str)
    .pauseFor(1500)
    .deleteChars(tu_str.length + 1 + tu_l)
    .typeString(syn + ' ' + syn_str)
    .pauseFor(1500)
    .deleteChars(syn_str.length + 1 + syn_l)
    .typeString(img + ' ' + img_str)
    .pauseFor(1500)
    .deleteChars(img_str.length + 1 + img_l)
    .typeString(non_obl + ' ' + non_obl_str_1 + ' ' + non_obl_str_2)
    .pauseFor(1500)
    .deleteChars(non_obl_str_1.length + non_obl_str_2.length + 2 + non_obl_l)
    .typeString(vern + ' ' + vern_str)
    .pauseFor(1500)
    .deleteAll()
    .typeString('В красной книге МСОП ' + in_rl_str_1 + ' ' + in_rl + " " + in_rl_str_2)
    .pauseFor(1500)
    .start();

/** Fast search */
var fast_search_inp = document.getElementById("fast-search");

// Execute a function when the user releases an Enter key (code 13) on the keyboard
fast_search_inp.addEventListener("keyup", function(event) {
    if (event.key === 'Enter') {
        // Get search string from field value
        var query = fast_search_inp.value;
        // Prepare an URL
        var full_url = fast_search_url + "?query=" + query;
        // Go to url
        window.location.assign(full_url);
  }
});

/** Particles */
// Depricated
// var particles_conf = {
//   "particles": {
//     "number": {
//       "value": 80,
//       "density": {
//         "enable": true,
//         "value_area": 800
//       }
//     },
//     "color": {
//       "value": "#ffffff"
//     },
//     "shape": {
//       "type": "circle",
//       "stroke": {
//         "width": 0,
//         "color": "#000000"
//       },
//       "polygon": {
//         "nb_sides": 5
//       },
//       "image": {
//         "src": "img/github.svg",
//         "width": 100,
//         "height": 100
//       }
//     },
//     "opacity": {
//       "value": 0.5,
//       "random": false,
//       "anim": {
//         "enable": false,
//         "speed": 1,
//         "opacity_min": 0.1,
//         "sync": false
//       }
//     },
//     "size": {
//       "value": 3,
//       "random": true,
//       "anim": {
//         "enable": false,
//         "speed": 40,
//         "size_min": 0.1,
//         "sync": false
//       }
//     },
//     "line_linked": {
//       "enable": true,
//       "distance": 150,
//       "color": "#ffffff",
//       "opacity": 0.4,
//       "width": 1
//     },
//     "move": {
//       "enable": true,
//       "speed": 4,
//       "direction": "none",
//       "random": false,
//       "straight": false,
//       "out_mode": "out",
//       "bounce": false,
//       "attract": {
//         "enable": false,
//         "rotateX": 600,
//         "rotateY": 1200
//       }
//     }
//   },
//   "interactivity": {
//     "detect_on": "window",
//     "events": {
//       "onhover": {
//         "enable": true,
//         "mode": "repulse"
//       },
//       "onclick": {
//         "enable": false,
//         "mode": "push"
//       },
//       "resize": true
//     },
//     "modes": {
//       "grab": {
//         "distance": 299.68113926781893,
//         "line_linked": {
//           "opacity": 1
//         }
//       },
//       "bubble": {
//         "distance": 400,
//         "size": 40,
//         "duration": 2,
//         "opacity": 8,
//         "speed": 3
//       },
//       "repulse": {
//         "distance": 200,
//         "duration": 0.4
//       },
//       "push": {
//         "particles_nb": 4
//       },
//       "remove": {
//         "particles_nb": 2
//       }
//     }
//   },
//   "retina_detect": true
// };
//
// document.addEventListener('DOMContentLoaded', function() {
//   particlesJS("particles-div", particles_conf);
// });
