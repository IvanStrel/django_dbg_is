from __future__ import absolute_import
from celery import shared_task
from subprocess import Popen
from subprocess import PIPE
from django.conf import settings

from git import Repo
from datetime import datetime


@shared_task(name='reserve_copy')
def dump_db_send_to_git():
    out_path = settings.DATABASES_DUMP.get("DUMP_PATH")
    # Dump to file
    p = Popen(["pg_dump",
               "-U", "celery",
               "-h", settings.DATABASES.get("default").get("HOST"),
               settings.DATABASES.get("default").get("NAME"),
               "-f", out_path], stderr=PIPE)

    # Commit changes to git repo
    full_local_path = "/home/celery/db_dumps/"
    repo = Repo(full_local_path)
    repo.git.add(".")
    repo.index.commit(f"{datetime.now().date()}")

    # Push changes to remotes
    origin = repo.remote(name="origin")
    origin.push()
    new_origin = repo.remote(name="new-origin")
    new_origin.push()
