-- Create indexes on parentNameUsageID for faster search
CREATE INDEX idx ON taxon ("parentNameUsageID");

DROP TABLE IF EXISTS "temp_taxon";

CREATE TABLE "temp_taxon" (
  "col_family_id" int NOT NULL,
  "col_family" varchar(255) default NULL,
  "col_order" varchar(255) default NULL,
  "col_class" varchar(255) default NULL,
  "col_phylum" varchar(255) default NULL,
  "col_kingdom" varchar(255) default NULL,
  PRIMARY KEY  ("col_family_id")
);

INSERT INTO temp_taxon(col_family_id, col_family)
SELECT taxon."taxonID", taxon."scientificName"
FROM taxon WHERE taxon."taxonRank"='family'
GROUP BY taxon."taxonID";

-- Somehow next queries do not work without VACUUM
VACUUM (ANALYZE) temp_taxon;

-- ORDER
UPDATE temp_taxon
SET col_order=ord."scientificName"
FROM taxon AS fam
LEFT JOIN taxon AS ord ON fam."parentNameUsageID"=ord."taxonID"
WHERE fam."taxonID" = temp_taxon.col_family_id;

VACUUM;
-- CLASS
UPDATE temp_taxon
SET col_class=class."scientificName"
FROM taxon AS ord
LEFT JOIN taxon AS class ON ord."parentNameUsageID"=class."taxonID"
WHERE ord."scientificName" = temp_taxon.col_order;

VACUUM;
-- PHYLUM
UPDATE temp_taxon
SET col_phylum=phyl."scientificName"
FROM taxon AS class
LEFT JOIN taxon AS phyl ON class."parentNameUsageID"=phyl."taxonID"
WHERE class."scientificName" = temp_taxon.col_class;

VACUUM;
-- PHYLUM
UPDATE temp_taxon
SET col_kingdom=kingdom."scientificName"
FROM taxon AS phyl
LEFT JOIN taxon AS kingdom ON phyl."parentNameUsageID"=kingdom."taxonID"
WHERE phyl."scientificName" = temp_taxon.col_phylum;