#!/bin/sh

stty -echo
printf "Password for postgres user: "
read PASSWORD
stty echo

# Variables
conf_string="host=localhost user=postgres port=5432 password=$PASSWORD"
conf_string_tmp="host=localhost user=postgres port=5432 password=$PASSWORD dbname=col_tmp"
conf_string_dbg="host=localhost user=postgres port=5432 password=$PASSWORD dbname=django_dbg_is"
path="$PWD/archive-kingdom-plantae-bl3"

echo "The path to dataset is: $path"
echo "If it is not a right path, change 'col_prepare.sh' file"
# Create temp database
psql "$conf_string" -c "DROP DATABASE IF EXISTS col_tmp;"
psql "$conf_string" -c "CREATE DATABASE col_tmp;"

# Create tables
psql "$conf_string_tmp" -f "create_temporal_tables.sql"

# Create temporal older
rm -rf ./tmp
mkdir ./tmp
chmod 777 ./tmp

# Load data ===========================================================================================================
# reference 1. Clean from quotes 2. Import
tr -d '"' <"$path/reference.txt" >"$PWD/tmp/reference.txt"
psql "$conf_string_tmp" -c "COPY reference FROM '$PWD/tmp/reference.txt' DELIMITER E'\t' QUOTE E'\b' CSV HEADER NULL AS '';"
# taxon
psql "$conf_string_tmp" -c "COPY taxon FROM '$path/taxa.txt' DELIMITER E'\t' CSV HEADER NULL AS '';"
# distribution
psql "$conf_string_tmp" -c "COPY distribution FROM '$path/distribution.txt' DELIMITER E'\t' CSV HEADER NULL AS '';"
# vernacular
psql "$conf_string_tmp" -c "COPY vernacular FROM '$path/vernacular.txt' DELIMITER E'\t' CSV HEADER NULL AS '';"


# Export taxonomy =====================================================================================================
psql "$conf_string_tmp" -c "VACUUM"
psql "$conf_string_tmp" -e -b -f ./export_taxonomy.sql
psql "$conf_string_tmp" -c "COPY (SELECT * FROM temp_taxon) TO '$PWD/tmp/tax_output.csv' DELIMITER ',' CSV HEADER;"

# Read taxonomy data into `collection_coltaxonomy` table
psql "$conf_string_dbg" -c "TRUNCATE collection_coltaxonomy RESTART IDENTITY CASCADE;"
psql "$conf_string_dbg" -c "COPY collection_coltaxonomy
                             FROM '$PWD/tmp/tax_output.csv'
                             DELIMITER ',' CSV HEADER;"

# Export species data =================================================================================================
psql "$conf_string_tmp" -c "VACUUM"
psql "$conf_string_tmp" -f ./export_species.sql

# Here we need to rearrange columns as Django shufls columns on index creation
psql "$conf_string_tmp" -c "COPY (SELECT
                                    col_id, col_identifier, col_dataset, col_dataset_id,
                                    col_status, col_genera, col_species_name, col_infra_name,
                                    col_infra_mark, col_author, col_db_id, col_link, col_full_name,
                                    col_name_ref, col_accepted_id, col_family_id,
                                    null AS used_by_tu_id, col_full_name_html
                                  FROM temp_sp) TO '$PWD/tmp/sp_output.csv' CSV HEADER;"

# Read taxonomy data into `collection_colspecies` table
psql "$conf_string_dbg" -c "TRUNCATE collection_colspecies RESTART IDENTITY CASCADE;"
psql "$conf_string_dbg" -c "VACUUM (ANALYZE);"
psql "$conf_string_dbg" -c "COPY collection_colspecies
                             FROM '$PWD/tmp/sp_output.csv'
                             CSV HEADER;"

# CLean up ============================================================================================================
psql "$conf_string" -c "DROP DATABASE IF EXISTS col_tmp;"
rm -rf ./tmp
