DROP TABLE IF EXISTS "temp_sp";

-- Create temporal table
CREATE TABLE "temp_sp" (
  "col_id" int NOT NULL, -- "taxonID"
  "col_identifier" varchar(255) default NULL, -- "identifier"
  "col_dataset" varchar(255) default NULL, -- "datasetName"
  "col_dataset_id" varchar(255) default NULL, -- "datasetID"
  "col_status" varchar(255) default NULL, -- "taxonomicStatus"
  "col_accepted_id" int default NULL REFERENCES temp_sp("col_id"),
  "col_family_id" int default NULL REFERENCES temp_taxon("col_family_id"),
  "col_genera" varchar(255) default NULL, -- "genus"
  "col_species_name" varchar(255) default NULL, -- "specificEpithet"
  "col_infra_name" varchar(255) default NULL, -- "infraspecificEpithet"
  "col_infra_mark" varchar(255) default NULL, -- "verbatimTaxonRank"
  "col_author" varchar(255) default NULL, -- "scientificNameAuthorship"
  "col_db_id" varchar(255) default NULL, -- "scientificNameID"
  "col_link" varchar(255) default NULL, -- "references"
  "col_full_name" varchar(512) default NULL,
  "col_name_ref" varchar(1024) default NULL,
  "col_full_name_html" varchar(512) default NULL,
  PRIMARY KEY  ("col_id")
);

VACUUM;

-- Populate temporal table with species and subspesies
INSERT INTO temp_sp(col_id, col_identifier, col_dataset, col_dataset_id,
                    col_status, col_accepted_id, col_genera, col_species_name, col_infra_name,
                    col_infra_mark, col_author, col_db_id, col_link)
SELECT t."taxonID", t."identifier", t."datasetName", t."datasetID", t."taxonomicStatus",
       t."acceptedNameUsageID", t."genus", t."specificEpithet", t."infraspecificEpithet",
       t."verbatimTaxonRank", t."scientificNameAuthorship", t."scientificNameID", t."references"
FROM taxon AS t
WHERE t."taxonRank"='species' OR t."taxonRank"='infraspecies';

VACUUM (ANALYZE) temp_sp;

-- Update COL status to binomial "Accepted" OR "Synonym"
UPDATE temp_sp
SET col_status=CASE
                WHEN (t."taxonomicStatus"='accepted name' OR t."taxonomicStatus"='provisionally accepted name')
                THEN 'accepted'
                ELSE 'synonym'
               END
FROM taxon AS t
WHERE temp_sp.col_id = t."taxonID";

VACUUM temp_sp;
-- Update COL col_infra_mark to get rid of misspelled marks (agamo..., d., b., subvar --> subvar.).
UPDATE temp_sp
SET col_infra_mark=CASE
                    WHEN (t.col_infra_mark='agamosp.') THEN 'subsp.'
                    WHEN (t.col_infra_mark='agamossp.') THEN 'subsp.'
                    WHEN (t.col_infra_mark='agamovar.') THEN 'var.'
                    WHEN (t.col_infra_mark='nvar.') THEN 'var.'
                    WHEN (t.col_infra_mark='subvar') THEN 'subvar.'
                    WHEN (t.col_infra_mark='b.') THEN 'var.'
                    WHEN (t.col_infra_mark='d.') THEN 'var.'
                    ELSE t.col_infra_mark
                   END
FROM temp_sp AS t
WHERE temp_sp.col_id = t.col_id;

VACUUM temp_sp;
-- Update 'col_family_id' for species here taxon."parentNameUsageID" == genus
UPDATE temp_sp
SET col_family_id=fam."taxonID"
FROM temp_sp AS tmp
LEFT JOIN taxon as sp ON tmp.col_id=sp."taxonID"
LEFT JOIN taxon AS gen ON sp."parentNameUsageID"=gen."taxonID"
LEFT JOIN taxon AS fam ON gen."parentNameUsageID"=fam."taxonID"
WHERE sp."taxonID" = temp_sp.col_id AND sp."taxonRank"='species';

VACUUM;
-- Update 'col_family_id' for infraspecies here taxon."parentNameUsageID" == species
UPDATE temp_sp
SET col_family_id=fam."taxonID"
FROM (SELECT * FROM taxon WHERE "taxonRank"='infraspecies') AS infr
LEFT JOIN taxon AS sp ON infr."parentNameUsageID"=sp."taxonID"
LEFT JOIN taxon AS gen ON sp."parentNameUsageID"=gen."taxonID"
LEFT JOIN taxon AS fam ON gen."parentNameUsageID"=fam."taxonID"
WHERE infr."taxonID" = temp_sp.col_id;

VACUUM;

-- Populate a column with full name
UPDATE temp_sp
SET col_full_name=lower(concat_ws(' ', tmp.col_genera, tmp.col_species_name,
                            tmp.col_infra_mark, tmp.col_infra_name, tmp.col_author))
FROM temp_sp as tmp
WHERE temp_sp.col_id = tmp.col_id;

VACUUM;

-- Populate a column with scientific name reference
UPDATE temp_sp
SET "col_name_ref"=concat_ws(' ', ref."creator", ref."title", ref."description")
FROM reference as ref
WHERE temp_sp."col_id"=ref."taxonID";

-- Populate html name column
UPDATE temp_sp
SET "col_full_name_html"=concat('<i>', tmp."col_genera", ' ', tmp."col_species_name", '</i> ',
           CASE WHEN (tmp."col_infra_mark" IS NOT NULL) THEN concat(tmp."col_infra_mark", ' ') END,
           CASE WHEN (tmp."col_infra_name" IS NOT NULL) THEN concat(' <i>', tmp."col_infra_name", '</i> ') END,
           CASE WHEN (tmp."col_author" IS NOT NULL) THEN tmp."col_author" END
        )
FROM temp_sp as tmp
WHERE temp_sp."col_id"=tmp."col_id";
