#!/bin/sh

stty -echo
printf "Password for postgres user: "
read PASSWORD
stty echo

# Variables
conf_string="host=localhost user=postgres port=5432 password=$PASSWORD"
conf_string_dbg="host=localhost user=postgres port=5432 password=$PASSWORD dbname=django_dbg_is"
conf_string_itis="host=localhost user=postgres port=5432 password=$PASSWORD dbname=ITIS"
path="$PWD/itisPostgreSql022620"


# You may want to truncate taxonomy table
psql "$conf_string_dbg" -c "TRUNCATE collection_taxonomy RESTART IDENTITY CASCADE;"

# Upload itis data to database
psql "$conf_string" -f "$path/ITIS.sql"
  # The script will create ITIS database, so you will need to remove it afterward

# Create prcession function
psql "$conf_string_itis" -f ./wide_table.sql

# Create temp_file for csv output
mkdir ./tmp
chmod 777 ./tmp
# Save wide table results to temporal csv (should be deleted afterward)
# AND
# Populate `collection_taxonomy` table from generated csv
# AND
# Remove ITIS database
psql "$conf_string_itis" -c "COPY (SELECT * FROM wide_table()) TO '$PWD/tmp/tab_output.csv' DELIMITER ',' CSV HEADER;"
psql "$conf_string_dbg" -c "COPY collection_taxonomy
                             FROM '$PWD/tmp/tab_output.csv'
                             DELIMITER ',' CSV HEADER;"

psql "$conf_string" -c 'DROP DATABASE "ITIS";'
# Clean up
rm -rf ./tmp