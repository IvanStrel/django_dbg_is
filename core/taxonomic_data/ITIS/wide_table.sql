-- Function which will create a wide table from self referenced hierarchy ITIS database.
DROP FUNCTION wide_table();

CREATE OR REPLACE FUNCTION wide_table()
RETURNS TABLE (
    itis_kingdom varchar(50),
    itis_subkingdom varchar(50),
    itis_infrakingdom varchar(50),
    itis_superdivision varchar(50),
    itis_division varchar(50),
    itis_subdivision varchar(50),
    itis_infradivision varchar(50),
    itis_superclass varchar(50),
    itis_class varchar(50),
    itis_subclass varchar(50),
    itis_infraclass varchar(50),
    itis_superorder varchar(50),
    itis_order varchar(50),
    itis_suborder varchar(50),
    itis_family varchar(50),
    itis_family_id int
  ) AS $$
DECLARE
  h_string varchar(300);
  f_tsn int;
  h_tsn int;

BEGIN
  -- Initiate temporal table (drop if exist)
  DROP TABLE IF EXISTS temporal; 
  CREATE TEMP TABLE temporal (
    itis_kingdom varchar(50),
    itis_subkingdom varchar(50),
    itis_infrakingdom varchar(50),
    itis_superdivision varchar(50),
    itis_division varchar(50),
    itis_subdivision varchar(50),
    itis_infradivision varchar(50),
    itis_superclass varchar(50),
    itis_class varchar(50),
    itis_subclass varchar(50),
    itis_infraclass varchar(50),
    itis_superorder varchar(50),
    itis_order varchar(50),
    itis_suborder varchar(50),
    itis_family varchar(50),
    itis_family_tsn int,
    itis_name_usage varchar(50)
  );

  -- Insert families names and their tsn into new table
  INSERT INTO
    temporal (itis_family_tsn, itis_family, itis_name_usage)
  SELECT
    tsn, unit_name1, name_usage
  FROM
    taxonomic_units
  WHERE
    --kingdom_id = 3 AND rank_id = 140 AND name_usage = 'accepted';
    kingdom_id = 3 AND rank_id = 140;

  -- For each row (i.e. for each family) parse hierarchy and insert names in appropriate columns
  FOR h_string, f_tsn IN SELECT hierarchy_string, tsn FROM hierarchy WHERE tsn IN (SELECT itis_family_tsn FROM temporal)
  LOOP
    -- Fore each tsn in hierarchy sequence decide which taxon it is and populate appropriate fields
    FOR h_tsn IN SELECT regexp_split_to_table(hierarchy_string, E'\\-')::int FROM hierarchy WHERE tsn = f_tsn
    LOOP
      CASE
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 10 THEN
          UPDATE temporal 
          SET itis_kingdom = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 20 THEN
          UPDATE temporal 
          SET itis_subkingdom = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 25 THEN
          UPDATE temporal
          SET itis_infrakingdom = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 27 THEN
          UPDATE temporal
          SET itis_superdivision = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 30 THEN
          UPDATE temporal
          SET itis_division = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 40 THEN
          UPDATE temporal
          SET itis_subdivision = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 40 THEN
          UPDATE temporal
          SET itis_subdivision = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 45 THEN
          UPDATE temporal
          SET itis_infradivision = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 50 THEN
          UPDATE temporal
          SET itis_superclass = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 60 THEN
          UPDATE temporal
          SET itis_class = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 70 THEN
          UPDATE temporal
          SET itis_subclass = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 80 THEN
          UPDATE temporal
          SET itis_infraclass = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 90 THEN
          UPDATE temporal
          SET itis_superorder = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 100 THEN
          UPDATE temporal
          SET itis_order = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        WHEN (SELECT rank_id FROM taxonomic_units WHERE tsn = h_tsn) = 110 THEN
          UPDATE temporal
          SET itis_suborder = (SELECT unit_name1 FROM taxonomic_units WHERE tsn = h_tsn)
          WHERE itis_family_tsn = f_tsn;
        ELSE
      END CASE;
    END LOOP;
  END LOOP;
  -- Rename family_tsn to family_id in order to match output naming
  ALTER TABLE temporal RENAME COLUMN itis_family_tsn TO itis_family_id;
  RETURN QUERY SELECT * FROM temporal;
  RETURN;
END; $$ LANGUAGE plpgsql;

--SELECT * FROM wide_table() LIMIT 10;
--COPY (SELECT * FROM wide_table()) TO '/home/ivan/tab.csv' DELIMITER ',' CSV HEADER;