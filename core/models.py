import os
from django.contrib.gis.db import models
from slugify import slugify
from django.utils.deconstruct import deconstructible


class HomePageImagesManager(models.Manager):
    def get_random(self):
        """
        Return random image from listed in table
        :return:
        """
        raw_sql = "SELECT * FROM core_homepageimages ORDER BY random() LIMIT 1;"
        return self.get_queryset().raw(raw_sql)


@deconstructible
class UploadAndRenameImage(object):
    def __init__(self, path, affix):
        self.path = path
        self.affix = affix

    def __call__(self, instance, filename):
        # Get extension
        ext = filename.split('.')[-1]
        # Get slug name
        name = slugify(instance.name, separator="_")
        # Create file path
        f_path = f"{name}_{self.affix}_{instance.pk}.{ext}"
        return os.path.join(self.path, f_path)


class HomePageImages(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    img_2x_land = models.ImageField(upload_to=UploadAndRenameImage("home_page_images", "2x_land"),
                                    blank=True, null=True)  # Initial size landscape
    img_1x_land = models.ImageField(upload_to=UploadAndRenameImage("home_page_images", "1x_land"),
                                    blank=True, null=True)  # Half size landscape
    img_2x_port = models.ImageField(upload_to=UploadAndRenameImage("home_page_images", "2x_port"),
                                    blank=True, null=True)  # Initial size portrait
    img_1x_port = models.ImageField(upload_to=UploadAndRenameImage("home_page_images", "1x_port"),
                                    blank=True, null=True)  # Half size portrait

    # Custom manager
    objects = HomePageImagesManager()

    class Meta:
        verbose_name = "Home page image"
        verbose_name_plural = "Home page images"

