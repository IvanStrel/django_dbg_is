# Deployment

## Create new user with sudo privileges

```shell script
adduser dbg_is
usermod -aG sudo dbg_is
```
Then login into system with new user

## Install required software

#### Install packages

```shell script
a="libssl-dev gdal-bin libgdal-dev nodejs mythes-ru hyphen-ru hunspell-ru libjpeg9 python3-pip libboost-all-dev "
b="tmux iftop libpng-dev libproj-dev libtiff-dev libboost-python-dev hunspell-en-us npm libmapnik-dev nginx "
c="build-essential tcl libpango1.0-dev unzip libgdk-pixbuf2.0-0 python3-cffi python3-ply python3-pycparser "
sudo apt install --install-recommends $a $b $c
```

#### Install virtualenv

```shell script
sudo pip3 install virtualenv
mkdir ~/.virtualenvs
sudo pip3 install virtualenvwrapper

virt_set_string="
# Set virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.8
export WORKON_HOME=$HOME/.virtualenvs
export VIRTUALENVWRAPPER_SCRIPT=/usr/local/bin/virtualenvwrapper.sh
source /usr/local/bin/virtualenvwrapper_lazy.sh
"

sudo echo "$virt_set_string" >> ~/.bashrc

# Create virtual environment for the project
mkvirtualenv django_dbg_is

```

#### Install python-mapnik (build from git)

```shell script
git clone --single-branch --branch v3.0.x https://github.com/mapnik/python-mapnik.git
cd python-mapnik
workon django_dbg_is
export BOOST_PYTHON_LIB=boost_python38
export BOOST_SYSTEM_LIB=boost_system
export BOOST_THREAD_LIB=boost_thread
python setup.py install
```

### Redis installation (Ubuntu 20.04)

##### Installation

```shell script
cd /tmp
curl -O http://download.redis.io/redis-stable.tar.gz
tar xzvf redis-stable.tar.gz
cd redis-stable
make
make test
sudo make install
```

##### Configuration

```shell script
# Create config dir
sudo mkdir /etc/redis
# Copy a config file template from installation
sudo cp /tmp/redis-stable/redis.conf /etc/redis
# Edit config file:
sudo nano /etc/redis/redis.conf
```

In `/etc/redis/redis.conf`
1. The `supervised` directive is set to no by default. Set to `systemd` [supervised systemd]
2. Find the `dir` directive. This option specifies the directory which Redis will use to dump persistent data. Set to `/var/lib/redis` [dir /var/lib/redis]
3. Save and close file.

##### Creating a Redis systemd Unit File

Create and open the `/etc/systemd/system/redis.service` file to get started:

```shell script
sudo nano /etc/systemd/system/redis.service
```

In `/etc/systemd/system/redis.service` file:
1. paste next:

```text
[Unit]
Description=Redis In-Memory Data Store
After=network.target

[Service]
User=redis
Group=redis
ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf
ExecStop=/usr/local/bin/redis-cli shutdown
Restart=always

[Install]
WantedBy=multi-user.target
```

Save and close the file when you are finished.

##### Creating the Redis User, Group, and Directories

Creating the redis user and group

```shell script
sudo adduser --system --group --no-create-home redis
```

Create the /var/lib/redis directory (which is referenced in the redis.conf file.

```shell script
sudo mkdir /var/lib/redis
# Give the redis user and group ownership over this directory
sudo chown redis:redis /var/lib/redis
# Prevent access for other users
sudo chmod 770 /var/lib/redis
```

##### Start service (for first time test. Afterward it will start on system startup)

```shell script
sudo systemctl start redis
# Test:
sudo systemctl status redis
redis-cli
# In redis client check that `ping` return `pong`
ping
exit
# Enable redis
sudo systemctl enable redis
```

### PostgreSQL 12 (Ubuntu 20.04)

#### Install postgresql

```shell script
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ focal-pgdg main" -y
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql postgresql-contrib postgresql-12-postgis-3
```
#### Install timescale db

```shell script
sudo add-apt-repository ppa:timescale/timescaledb-ppa
sudo apt-get update
sudo apt install timescaledb-2-postgresql-12
```

#### Set password

```shell script
sudo -u postgres psql
```
After connection

```shell script
\password
```

#### Change authentication method

```shell script
sudo nano /etc/postgresql/12/main/pg_hba.conf
# Restart postgresql
/etc/init.d/postgresql restart
```

Replace `peer` to `md5`

#### Initiate `django_dbg_is` data base and user !!! Change password in production !!!

```shell script
sudo -u postgres psql
```

Then

```sql
-- Create user
CREATE USER django_dbg_is WITH CREATEDB ENCRYPTED PASSWORD '[PASSWORD]';
-- Create db
CREATE DATABASE django_dbg_is OWNER django_dbg_is;
-- Attach to DB
\c django_dbg_is
-- Create extensions
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_raster;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS timescaledb;
-- Grant additional permissions
GRANT USAGE ON SCHEMA topology to django_dbg_is;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA topology TO django_dbg_is;
GRANT SELECT ON ALL TABLES IN SCHEMA topology TO django_dbg_is;
-- Create user for celery
CREATE USER celery WITH ENCRYPTED PASSWORD '[PASSWORD]';
GRANT USAGE ON SCHEMA topology to celery;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA topology TO celery;
GRANT SELECT ON ALL TABLES IN SCHEMA topology TO celery;
GRANT USAGE ON SCHEMA public TO celery;
GRANT SELECT ON ALL SEQUENCES IN SCHEMA public TO celery;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO celery;
\q
```

## Install and configure project

### Clone project

```shell script
cd ~
git clone https://IvanStrel@bitbucket.org/IvanStrel/django_dbg_is.git
cd django_dbg_is
```

Install dependencies:

```shell script
# Here we want exclude mapnik as it was installed locally
pip install $(grep -ivE "mapnik" requirements.txt)
npm install
sudo npm install --global gulp-cli
```

### Prepare media/tmp and logging folders with permissions

```shell script
cd ~/django_dbg_is
sudo mkdir -p media/tmp
sudo chown -R dbg_is:dbg_is media
# Grunt permission for user and group
# Further we will assign celery user to dbg_is group so it will be abel to
# write and read files in media/tmp
sudo chmod -R 777 media/tmp

# Create logging folder
sudo mkdir -p /var/log/django
sudo touch /var/log/django/warning.log
sudo chown -R dbg_is:dbg_is /var/log/django
sudo chmod -R 776 /var/log/django # 776 as Celery also need rights to write here
```

### Prepare git folder for dumps repository

```shell script
# Create and configure GitHub repo
sudo -u celery git clone https://USERNAME:PASSWORD@github.com/DBG-info-system/DB-backups.git /home/celery/db_dumps/
sudo -u celery git -C /home/celery/db_dumps/ config user.name  DBG-info-system
sudo -u celery git -C /home/celery/db_dumps/ config user.email dbginfosystem@gmail.com

# Configure GitLab repo
sudo -u celery git -C /home/celery/db_dumps/ remote add new-origin https://DBG-info-system:LmhvXk-VX8oB3w6D65FT@gitlab.com/dbginfosystem/db-backups
sudo -u celery git -C /home/celery/db_dumps/ push new-origin

# Install git large files
sudo -u celery git -C /home/celery/db_dumps/ lfs install
sudo -u celery git -C /home/celery/db_dumps/ lfs track "*.sql"
sudo -u celery git -C /home/celery/db_dumps/ add .gitattributes
sudo -u celery git -C /home/celery/db_dumps/ commit -m "lfs attributes"
```


### Settings

Copy file `settings_example.ini` as `settings.ini` in the root of a project:

```shell script
cp settings_example.ini settings.ini
nano settings.ini
```

Edit values in the `settings.ini` file, i.e. replace with actual values.

## Preparations

### Create tables from Django migrations

Next should be done from Django project (virtualenv).

```shell script
python manage.py migrate
```

### Populate `itis_taxonomy` table

Go to `PROJECT_DIR/collection/taxonomic_data/ITIS/`

1. If needed (fresh installation or DB update) download last ITIS DB dump
and unpak it here

```shell script
wget https://www.itis.gov/downloads/itisPostgreSql.zip
unzip itisPostgreSql.zip
```

2. In the file `itis_processing.sh` change path in the line `path="$PWD/itisPostgreSql022620"` such, that it points to actual `ITIS.sql` location.
3. Open `PROJECT_DIR/collection/taxonomic_data/ITIS/` in terminal.
4. Launch `itis_processing.sh` with `sudo`.

```shell script
sudo sh itis_processing.sh
```

### Populate `col_taxonomy` and `col_species` tables

Go to `PROJECT_DIR/core/taxonomic_data/COL/`
1. If needed (fresh installation or DB update) download last COL DB dump from [i4Life WP4 Download Service](http://www.catalogueoflife.org/DCA_Export/). Restrict download for Plantae kingdom. and unpak it here

```shell script
# For example
wget http://www.catalogueoflife.org/DCA_Export/zip/archive-kingdom-plantae-bl3.zip
unzip archive-kingdom-plantae-bl3.zip -d ./archive-kingdom-plantae-bl3
# Remove zip
rm archive-kingdom-plantae-bl3.zip
```

2. In the file `col_processing.sh` change path in the line `path="$PWD/archive-kingdom-plantae-bl3"` such,
that it points to actual location of data folder (extracted archive).
3. Open `PROJECT_DIR/core/taxonomic_data/COL/` in terminal.
4. Launch `col_preparation.sh` with `sudo`.

```shell script
sudo sh col_preparation.sh
```

### Populate spatial tables

#### Territory map

1. Open `PROJECT_DIR/spatial/data/initial_territory_map/` in terminal.
2. Launch `init_territory_geojson_2_postgis.sh` with `sudo`. It will ask a __django_bg_is__ user password.

```shell script
sudo sh init_territory_geojson_2_postgis.sh
```

#### World map

1. Open `PROJECT_DIR/spatial/data/world_map/` in terminal.
2. Launch `world_map_geojson_2_postgis.sh` with `sudo`. It will ask a __django_bg_is__ user password.

```shell script
sudo sh world_map_geojson_2_postgis.sh
```

#### Update indexes
```shell script
sudo -u postgres psql -d django_dbg_is -c 'REINDEX DATABASE django_dbg_is;'
```

## Set up servers (nginx, gunicorn, celey)

### Celery setup (systemd)

Prepare

```shell script
# Create a user for celery
sudo mkdir /home/celery
sudo useradd celery -d /home/celery -b /bin/bash
sudo chown celery:celery /home/celery
sudo chmod 700 /home/celery
# Add celery user to dbg_is group
sudo usermod -a -G dbg_is celery
# Create home and give rights
```

Add .pgpass to celery home. Such it would be able to access PostgreSQL

```shell script
sudo touch /home/celery/.pgpass
sudo chown celery:celery /home/celery/.pgpass
sudo chmod 600 /home/celery/.pgpass
sudo nano /home/celery/.pgpass
# Paste next
# *:*:django_dbg_is:celery:[PASSWORD]
# save and exit Ctr^O, Ctr^X
sudo mkdir /home/celery/db_dumps
sudo chown celery:celery /home/celery/db_dumps
```

Create files for pid and log

```shell script
sudo mkdir /var/log/celery
sudo chown -R celery:celery /var/log/celery
sudo chmod -R 755 /var/log/celery

sudo mkdir /var/run/celery
sudo chown -R celery:celery /var/run/celery
sudo chmod -R 755 /var/run/celery
```

Create configuration file for

```shell script
sudo nano /etc/conf.d/celery
```

Put next lines

```text
# Name of nodes to start
# here we have a single node
CELERYD_NODES="w1"

# Absolute or relative path to the 'celery' command:
CELERY_BIN="/home/dbg_is/.virtualenvs/django_dbg_is/bin/celery"

# App instance to use
# comment out this line if you don't use an app
CELERY_APP="django_dbg_is"

# How to call manage.py
CELERYD_MULTI="multi"

# Extra command-line arguments to the worker
CELERYD_OPTS="--time-limit=300 --concurrency=4"

# - %n will be replaced with the first part of the nodename.
# - %I will be replaced with the current child process index
#   and is important when using the prefork pool to avoid race conditions.
CELERYD_PID_FILE="/var/run/celery/%n.pid"
CELERYD_LOG_FILE="/var/log/celery/%n%I.log"
CELERYD_LOG_LEVEL="INFO"

ELERYBEAT_PID_FILE="/var/run/celery/beat.pid"
CELERYBEAT_LOG_FILE="/var/log/celery/beat.log"
```

Save and close file

Create celery service file

```shell script
sudo nano /etc/systemd/system/celery.service
```

Put next lines:

```text
[Unit]
Description=Celery Service
After=network.target

[Service]
Type=forking
User=celery
Group=celery
EnvironmentFile=/etc/conf.d/celery
WorkingDirectory=/home/dbg_is/django_dbg_is
ExecStart=/bin/sh -c '${CELERY_BIN} multi start ${CELERYD_NODES} \
  -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
  --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'
ExecStop=/bin/sh -c '${CELERY_BIN} multi stopwait ${CELERYD_NODES} \
  --pidfile=${CELERYD_PID_FILE}'
ExecReload=/bin/sh -c '${CELERY_BIN} multi restart ${CELERYD_NODES} \
  -A ${CELERY_APP} --pidfile=${CELERYD_PID_FILE} \
  --logfile=${CELERYD_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL} ${CELERYD_OPTS}'

[Install]
WantedBy=multi-user.target
```

Enable Celery service

```shell script
sudo systemctl enable celery.service
```

Create celery beat service file

```shell script
sudo nano /etc/systemd/system/celerybeat.service
```

Put next lines:

```text
[Unit]
Description=Celery Beat Service
After=network.target

[Service]
Type=simple
User=celery
Group=celery
EnvironmentFile=/etc/conf.d/celery
WorkingDirectory=/home/dbg_is/django_dbg_is
ExecStart=/bin/sh -c '${CELERY_BIN} \
    -A ${CELERY_APP} beat --pidfile=${CELERYBEAT_PID_FILE} \
    --logfile=${CELERYBEAT_LOG_FILE} --loglevel=${CELERYD_LOG_LEVEL}'

[Install]
WantedBy=multi-user.target
```

Enable Celery service

```shell script
sudo systemctl enable celerybeat.service
```

Initiate celery with

```shell script
sudo systemctl daemon-reload
sudo systemctl start celery
sudo systemctl start celerybeat
```

### Gunicorn setup

#### Logging for Gunicorn prepare

```shell script
sudo mkdir nano /var/log/gunicorn
sudo chown -R dbg_is:dbg_is /var/log/gunicorn
sudo chmod -R 755 /var/log/gunicorn
```

#### Main server for django

Create systemd service

```shell script
sudo nano /etc/systemd/system/dbg_is_main_gunicorn.service
```

Paste next lines:

```text
[Unit]
Description=gunicorn daemon for django_dbg_is
After=network.target

[Service]
User=dbg_is
Group=dbg_is
WorkingDirectory=/home/dbg_is/django_dbg_is
ExecStart=/home/dbg_is/.virtualenvs/django_dbg_is/bin/gunicorn --capture-output \
     --enable-stdio-inheritance --access-logfile /var/log/gunicorn/main_access.log \
     --error-logfile /var/log/gunicorn/main_error.log --workers 6 --timeout 60\ 
     --bind unix:/home/dbg_is/sockets/dbg_is_main_gunicorn.sock django_dbg_is.wsgi:application

[Install]
WantedBy=multi-user.target
```

Enable Main Gunicorn service

```shell script
sudo systemctl enable dbg_is_main_gunicorn.service
```

#### The server for tilestach

First add changes to Tilestach (there is deprecated function call)

```shell script
cd ~/.virtualenvs/django_dbg_is/lib/python3.8/site-packages/TileStache
sudo nano py3_compat.py
```

change `from cgi import parse_qs` with `from urllib.parse import parse_qs`
Save file and close

Create systemd service

```shell script
sudo nano /etc/systemd/system/dbg_is_tiles_gunicorn.service
```

Paste next lines:

```text
[Unit]
Description=gunicorn daemon for django_dbg_is TileStach server
After=network.target

[Service]
User=dbg_is
Group=dbg_is
WorkingDirectory=/home/dbg_is/django_dbg_is
ExecStart=/home/dbg_is/.virtualenvs/django_dbg_is/bin/gunicorn --access-logfile - \ 
    --error-logfile /var/log/gunicorn/tile_error.log --workers 4 \ 
    --bind unix:/home/dbg_is/sockets/dbg_is_tilestach_gunicorn.sock "TileStache:WSGITileServer('spatial/tielstach_config/base_config.cfg')"

[Install]
WantedBy=multi-user.target
```

Enable Main Gunicorn service

```shell script
sudo systemctl enable dbg_is_tiles_gunicorn.service
```

### NGINX setup

#### Self signed certificate (if domain name is absent)

Create certificate with OpenSSL

```shell script
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
```

Create a strong Diffie-Hellman group

```shell script
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
```

Create a Configuration Snippet Pointing to the SSL Key and Certificate

```shell script
sudo nano /etc/nginx/snippets/self-signed.conf
```

Inside this file paste:

```text
ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
```

Save and close file

Create a Configuration Snippet with Strong Encryption Settings

```shell script
sudo nano /etc/nginx/snippets/ssl-params.conf
```

Inside this file paste:

```text
# from https://cipherli.st/
# and https://raymii.org/s/tutorials/Strong_SSL_Security_On_nginx.html

ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;

ssl_dhparam /etc/ssl/certs/dhparam.pem;
```

Save and close file


#### Compile nginx_brotli plugin

Find which nginx version is currently installed

```shell script
sudo nginx -v
# Example output
# nginx version: nginx/1.17.10 (Ubuntu)
# We will need 1.17.10
ngv='1.17.10'
```

Download Nginx version that matches the current installed version. Then extract it using the commands below:

```shell script
cd ~/
wget "https://nginx.org/download/nginx-$ngv.tar.gz"
tar zxvf "nginx-$ngv.tar.gz"
```

Clone ngx_brotli module from Github using the commands:

```shell script
cd ~/
git clone https://github.com/eustas/ngx_brotli.git
cd ~/ngx_brotli
git submodule update --init
```

Go to extracted nginx folder and 

```shell script
cd ~
cd "nginx-$ngv"
./configure --with-compat --add-dynamic-module=../ngx_brotli
make modules
# On first test Nginx was looking into /usr/share/nginx/modules so we copy
# into it, still on some distribution it could be /etc/nginx/modules-available
# Test configuration with sudo nginx -t
# sudo cp objs/*.so /etc/nginx/modules-available
sudo cp objs/*.so /usr/share/nginx/modules
```

Clean after yourself

```shell script
cd ~
rm -r "nginx-$ngv"
rm -rf ngx_brotli
rm "nginx-$ngv.tar.gz"
```

#### NGINX settings

Tell base configuration to use brotli module

```shell script
sudo nano /etc/nginx/nginx.conf
```

Paste next lines at the begin of file:

```text
load_module modules/ngx_http_brotli_filter_module.so;
load_module modules/ngx_http_brotli_static_module.so;
```

Save and close the file

Create server block in Nginx’s sites-available directory

```shell script
sudo nano /etc/nginx/sites-available/django_dbg_is
```

Insert next lines

```text
server {
    # SSL configuration
    listen 8080 ssl http2 default_server;
    listen [::]:8080 ssl http2 default_server;
    include mime.types;
    include snippets/self-signed.conf;
    include snippets/ssl-params.conf;

    server_name 109.254.93.80;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        root /home/dbg_is/;
        expires 1y;
    }
    
    location /media/ {
        root /home/dbg_is/django_dbg_is/;
        expires 1y;
    }
    
    location /territory-map/ {
        include proxy_params;
        proxy_pass http://unix:/home/dbg_is/sockets/dbg_is_tilestach_gunicorn.sock;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:/home/dbg_is/sockets/dbg_is_main_gunicorn.sock;
        client_max_body_size 1024M;
    }
    # Brotli support
    brotli on;
    brotli_static on;
    brotli_types *;
}
```

Save and close the file.

Enable the file by linking it to the `sites-enabled` directory:

```shell script
sudo ln -s /etc/nginx/sites-available/django_dbg_is /etc/nginx/sites-enabled
```

Test your Nginx configuration for syntax errors by typing:

```shell script
sudo nginx -t
```