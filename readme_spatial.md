# Useful snippets for work with spatial data

## Add new city (populated place)

```postgresql
INSERT INTO spatial_populatedplaces
    VALUES((SELECT max(id) + 1
            FROM spatial_populatedplaces
            ),
           'Salaspils', 'Саласпилс', ST_GeomFromText('POINT(24.36544 56.86014)', 4326),
           (SELECT id
            FROM spatial_adminregions
            WHERE ST_Contains(spatial_adminregions.poly, ST_GeomFromText('POINT(24.36544 56.86014)', 4326))
            ),
           (
            SELECT id
            FROM spatial_sovcountries
            WHERE ST_Contains(spatial_sovcountries.poly, ST_GeomFromText('POINT(24.36544 56.86014)', 4326))
            ));
```